//
//  AppDelegate.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/14.
//  Copyright © 2018年 zhoujian. All rights reserved.
//

#import "AppDelegate.h"
#import "DBXBaseTabBarController.h"
#import "DBXLoginViewController.h"
#import "DBXBaseNavigationController.h"
//#import <UserNotifications/UserNotifications.h>
#import "DBXLinkyorntwoApi.h"
#import "DBXPeripheralManagementViewController.h"
#import "ZJDeviceManageViewController.h"
#import "ZJCheckVersionTool.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    YTKNetworkConfig *config = [YTKNetworkConfig sharedConfig];
    config.baseUrl = kBaseUrl;
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kToken]) {

        self.window.rootViewController = [DBXBaseTabBarController new];

        // 判断账号是否激活
        [self requestLinkyorntwoApi];

    } else {
    
        DBXBaseNavigationController *baseNavC = [[DBXBaseNavigationController alloc] initWithRootViewController:[DBXLoginViewController new]];
        self.window.rootViewController = baseNavC;
    
    }
    
    // 检查版本更新
    [ZJCheckVersionTool checkVersion];
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

/**
 请求账号是否激活接口
 */
- (void)requestLinkyorntwoApi
{
    DBXLinkyorntwoApi *api = [[DBXLinkyorntwoApi alloc] init];
    
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        if (request.responseJSONObject) {
            NSDictionary *dic = request.responseJSONObject;
            // 设备未建立过连接 或 设备未激活
            if ([dic[@"state"] integerValue] == 1041 || [dic[@"state"] integerValue] == 1043) {

                ZJDeviceManageViewController *deviceManageVC = [ZJDeviceManageViewController new];
                deviceManageVC.navigationItem.title = @"设备管理";
                deviceManageVC.isNewUser = YES;
                DBXBaseNavigationController *baseNavC = [[DBXBaseNavigationController alloc] initWithRootViewController:deviceManageVC];
                self.window.rootViewController = baseNavC;

            } else {
                
                self.window.rootViewController = [DBXBaseTabBarController new];
                
            }
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        DBXBaseNavigationController *baseNavC = [[DBXBaseNavigationController alloc] initWithRootViewController:[DBXLoginViewController new]];
        self.window.rootViewController = baseNavC;
        
    }];
    
}

#pragma mark - 申请通知权限
//// 申请通知权限
//- (void)replyPushNotificationAuthorization:(UIApplication *)application{
//
//    if (IOS10_OR_LATER) {
//        //iOS 10 later
//        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
//        //必须写代理，不然无法监听通知的接收与点击事件
//        center.delegate = self;
//        [center requestAuthorizationWithOptions:(UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert) completionHandler:^(BOOL granted, NSError * _Nullable error) {
//            if (!error && granted) {
//                //用户点击允许
//                NSLog(@"注册成功");
//            }else{
//                //用户点击不允许
//                NSLog(@"注册失败");
//            }
//        }];
//
//        // 可以通过 getNotificationSettingsWithCompletionHandler 获取权限设置
//        //之前注册推送服务，用户点击了同意还是不同意，以及用户之后又做了怎样的更改我们都无从得知，现在 apple 开放了这个 API，我们可以直接获取到用户的设定信息了。注意UNNotificationSettings是只读对象哦，不能直接修改！
//        [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
//            NSLog(@"========%@",settings);
//        }];
//    }else if (IOS8_OR_LATER) {
//        //iOS 8 - iOS 10系统
//        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
//        [application registerUserNotificationSettings:settings];
//    }else{
//        //iOS 8.0系统以下
//        [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound];
//    }
//
//    //注册远端消息通知获取device token
//    [application registerForRemoteNotifications];
//}
//
//#pragma  mark - 获取device Token
////获取DeviceToken成功
//- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
//
//    //解析NSData获取字符串
//    //我看网上这部分直接使用下面方法转换为string，你会得到一个nil（别怪我不告诉你哦）
//    //错误写法
//    //NSString *string = [[NSString alloc] initWithData:deviceToken encoding:NSUTF8StringEncoding];
//
//    //正确写法
//    NSString *deviceString = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
//
//    deviceString = [deviceString stringByReplacingOccurrencesOfString:@" " withString:@""];
//
//    NSLog(@"deviceToken===========%@", deviceString);
//}
//
////获取DeviceToken失败
//- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
//{
//    NSLog(@"[DeviceToken Error]:%@\n", error.description);
//}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
