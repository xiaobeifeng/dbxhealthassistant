//
//  CustomDefine.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/23.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#ifndef CustomDefine_h
#define CustomDefine_h

#pragma mark - 颜色
/** tabBar标题选中颜色*/
#define kTabBarSelectTitleColor [UIColor colorWithRed:0.27 green:0.35 blue:0.93 alpha:1.00]
/** tabBar标题普通状态颜色 */
#define kTabBarTitleColor [UIColor colorWithRed:0.64 green:0.64 blue:0.64 alpha:1.00]
/** tableView分割线颜色 */
#define kTableSeparatorLineColor [UIColor colorWithRed:0.91 green:0.91 blue:0.92 alpha:1.00]
/** 导航栏颜色 */
#define kNavigationBarColor [UIColor colorWithRed:0.39 green:0.49 blue:0.93 alpha:1.00]

#pragma mark - 字体
/** tableView的主标题字体 */
#define kCellTitleFont kFont(16)

#pragma mark - 距离
#define kSafeAreaTopHeight (kScreenHeight == 812.0 ? 88 : 64)

#pragma mark - 表名
#define kBloodSugarTableName @"peripheral_bloodSugar"   // 血糖表名
#define kBloodPressureTableName @"peripheral_bloodPressure" // 血压表名
#define kLinkDevicesTableName @"linkDevices" // 连接设备表名


#pragma mark - 本地存储
#define kToken @"token"
#define kAccount @"account"
#define kPassword @"password"
#define kIsOnSwitchView @"kIsOnSwitchView"
#define kAge @"age"
#define kSex @"sex"

#pragma mark - 通知
#define kReloadDataApiAndDataDetailApi @"reloadDataApiAndDataDetailApi"

#define IOS10_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)
#define IOS9_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)
#define IOS8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define IOS7_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#pragma mark - 请求接口
#define kBaseUrl @"http://223.100.25.250"  // 正式环境
//#define kBaseUrl @"http://192.168.10.80:8080"   // 测试环境

// 头像路径
#define kImagePath(imageName)  [NSString stringWithFormat:@""kBaseUrl"/mp/uploadFiles/touxiang/%@", imageName]

#define DEVICE_NAME [NSString stringWithFormat:@"%@-deviceName", [[NSUserDefaults standardUserDefaults] objectForKey:kAccount]]

#endif /* CustomDefine_h */
