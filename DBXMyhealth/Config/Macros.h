//
//  Macros.h
//  SMKJSmartDiagnose
//
//  Created by zhoujian on 2018/6/27.
//  Copyright © 2018年 zhoujian. All rights reserved.
//

#ifndef Macros_h
#define Macros_h

/** 输出宏(可以同时打印出方法名和行号)*/
#ifdef __OBJC__
#ifdef DEBUG
#define NSLog(fmt,...) NSLog((@"%s [Line %d] " fmt),__PRETTY_FUNCTION__,__LINE__,##__VA_ARGS__)
#else
#define NSLog(...)
#endif
#endif

/** 字体*/
#define kFont(x) [UIFont systemFontOfSize:x]
#define kBoldFont(x) [UIFont boldSystemFontOfSize:x]

/** 颜色*/
#define kRGBColor(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]
#define kRGBAColor(r,g,b,a) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]
#define kRGB16Color(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

/** View 圆角和加边框*/
#define kViewBorderRadius(View, Radius, Width, Color)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES];\
[View.layer setBorderWidth:(Width)];\
[View.layer setBorderColor:[Color CGColor]]

/** View 圆角*/
#define kViewRadius(View, Radius)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES]

/** 字符串是否为空*/
#define kStringIsEmpty(str) ([str isKindOfClass:[NSNull class]] || str == nil || [str length] < 1 ? YES : NO )

/** 数组是否为空*/
#define kArrayIsEmpty(array) (array == nil || [array isKindOfClass:[NSNull class]] || array.count == 0)

/** 字典是否为空*/
#define kDictIsEmpty(dic) (dic == nil || [dic isKindOfClass:[NSNull class]] || dic.allKeys == 0)

/** 是否是空对象*/
#define kObjectIsEmpty(_object) (_object == nil \
|| [_object isKindOfClass:[NSNull class]] \
|| ([_object respondsToSelector:@selector(length)] && [(NSData *)_object length] == 0) \
|| ([_object respondsToSelector:@selector(count)] && [(NSArray *)_object count] == 0))

/** UIApplication Delegate*/
#define kApplicationDelegate [UIApplication sharedApplication].delegate

/** Document路径*/
#define kDocumentPath [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES)  lastObject]

/** 本地存储 */
#define kUserDefaults [NSUserDefaults standardUserDefaults]
#define kUserDefaultsGet(key) [[NSUserDefaults standardUserDefaults] objectForKey:key]
#define kUserDefaultsSet(objt, key)\
\
[[NSUserDefaults standardUserDefaults] setObject:objt forKey:key];\
[[NSUserDefaults standardUserDefaults] synchronize]


/**
 本地版本号
 */
#define kLocalVersionString [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"]

/**
 app在app store的id
 */
#define kAppStoreId  @"1395770781"


#endif /* Macros_h */
