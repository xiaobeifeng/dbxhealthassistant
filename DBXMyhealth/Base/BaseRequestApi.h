//
//  BaseRequestApi.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/14.
//  Copyright © 2018年 zhoujian. All rights reserved.
//

#import "YTKBaseRequest.h"

@interface BaseRequestApi : YTKBaseRequest


@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSDictionary *result;

@end
