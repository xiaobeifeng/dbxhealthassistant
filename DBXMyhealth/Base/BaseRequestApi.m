//
//  BaseRequestApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/14.
//  Copyright © 2018年 zhoujian. All rights reserved.
//

#import "BaseRequestApi.h"
#import "DBXBaseNavigationController.h"
#import "DBXLoginViewController.h"

@implementation BaseRequestApi

- (NSTimeInterval)requestTimeoutInterval {
    return 30.0;
}

 /**
  默认请求体是自身转json
  */
- (id)requestArgument {
    
    return [self yy_modelToJSONObject];
}

/**
 请求失败过滤器
 */
- (void)requestFailedFilter {
    //失败处理器
    NSLog(@"失败了");

}

- (void)requestCompleteFilter {
  
    if (self.responseJSONObject) {
        self.result = self.responseJSONObject;
    }
    
    if ([self code] == 105) {
        
        [MBProgressHUD showTipMessageInView:[self message]];
        [self stop];
        
        DBXBaseNavigationController *baseNavC = [[DBXBaseNavigationController alloc] initWithRootViewController:[DBXLoginViewController new]];
        [UIApplication sharedApplication].keyWindow.rootViewController = baseNavC;
        
    }

}

- (id)jsonValidator
{

    return @{
             @"message":[NSString class],
             @"data":[NSObject class],
             @"state":[NSNumber class]
             };
    
}

- (NSString *)message {
    if (self.error) {
        return self.error.localizedDescription;
    }
    NSString *message = [NSString stringWithFormat:@"%@",self.result[@"message"]];
    return message;
}

- (NSInteger)code {
    NSString *code = [NSString stringWithFormat:@"%@",self.result[@"state"]];
    return [code integerValue];
}

- (NSDictionary<NSString *,NSString *> *)requestHeaderFieldValueDictionary {
    
    NSLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:kToken]);
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:kToken];
    if (token.length > 0) {
        return @{kToken: [[NSUserDefaults standardUserDefaults] objectForKey:kToken]};
    } else {
        return nil;
    }
    
}



//- (BOOL)isRequestSuccess {
//
//    NSString *code = [self code];
//    NSString *message = [self message];
//
//    BOOL isSuccess = NO;
//
//    if ([code isEqualToString:@"0"]) {
//
//        isSuccess = YES;
//
//    } else if ([code isEqualToString:@"1"]) {
//
//        NSLog(@"用户名或密码错误");
//
//        [MBProgressHUD showTipMessageInView:message];
//
//    } else if([code isEqualToString:@"2"]){
//
//        NSLog(@"用户未登录");
//        [MBProgressHUD showTipMessageInView:message];
//
//    } else if([code isEqualToString:@"101"]){
//
//        NSLog(@"缺少token");
//        [MBProgressHUD showTipMessageInView:message];
//
//    } else if([code isEqualToString:@"102"]){
//
//        NSLog(@"参数错误");
//        [MBProgressHUD showTipMessageInView:message];
//
//    } else if([code isEqualToString:@"103"]){
//
//        NSLog(@"服务器内部错误");
//        [MBProgressHUD showTipMessageInView:message];
//
//    } else if([code isEqualToString:@"104"]){
//
//        NSLog(@"自定义错误");
//        [MBProgressHUD showTipMessageInView:message];
//
//    } else {
//
//
//    }
//
//    return  isSuccess;
//
//}

- (BOOL)statusCodeValidator {
    
    
    NSInteger statusCode = [self responseStatusCode];
    
    if (statusCode == 1) {
        
        return NO;
    } else {
        
        return YES;
    }
    
}
    

@end
