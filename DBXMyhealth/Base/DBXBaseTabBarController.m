//
//  DBXBaseTabBarController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/20.
//  Copyright © 2018年 zhoujian. All rights reserved.
//

#import "DBXBaseTabBarController.h"
#import "DBXBaseNavigationController.h"
#import "ZJDataViewController.h"
#import "DBXReportViewController.h"
#import "DBXPeripheralViewController.h"
#import "DBXSportViewController.h"
#import "DBXMeViewController.h"

@interface DBXBaseTabBarController ()

@end

@implementation DBXBaseTabBarController

+ (void)load
{
//    [[UITabBar appearance] setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRed:0.28 green:0.80 blue:0.82 alpha:1.00]]];
    UITabBarItem *item = [UITabBarItem appearanceWhenContainedInInstancesOfClasses:@[self]];
    
    // 选中颜色
    NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
    attrs[NSForegroundColorAttributeName] = kTabBarSelectTitleColor;
    attrs[NSFontAttributeName] = kFont(10);
    [item setTitleTextAttributes:attrs forState:UIControlStateSelected];
    
    // 普通状态下颜色
    NSMutableDictionary *attrsNor = [NSMutableDictionary dictionary];
    attrsNor[NSFontAttributeName] = kFont(10);
    attrsNor[NSForegroundColorAttributeName] = kTabBarTitleColor;
    [item setTitleTextAttributes:attrsNor forState:UIControlStateNormal];
    
    UITabBar *tabBar = [UITabBar appearance];
    tabBar.translucent = NO;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    ZJDataViewController *dataVC = [ZJDataViewController new];
    [self setupNavigationControllerWithViewController:dataVC
                                                title:@"数据"
                                                image:@"tabbar_data"
                                        selectedImage:@"tabbar_data_pre"];
    
    DBXReportViewController *reportVC = [DBXReportViewController new];
    [self setupNavigationControllerWithViewController:reportVC title:@"报告" image:@"tabbar_report" selectedImage:@"tabbar_report_pre"];
    
    DBXPeripheralViewController *peripheralVC = [DBXPeripheralViewController new];
    [self setupNavigationControllerWithViewController:peripheralVC
                                                title:@"设备"
                                                image:@"tabbar_peripheral"
                                        selectedImage:@"tabbar_peripheral_pre"];
    
    DBXSportViewController *sportVC = [DBXSportViewController new];
    [self setupNavigationControllerWithViewController:sportVC
                                                title:@"运动"
                                                image:@"tabbar_sport"
                                        selectedImage:@"tabbar_sport_pre"];
    
    DBXMeViewController *meVC = [DBXMeViewController new];
    [self setupNavigationControllerWithViewController:meVC
                                                title:@"我的"
                                                image:@"tabbar_me"
                                        selectedImage:@"tabbar_me_pre"];
    
    self.tabBar.backgroundColor = [UIColor whiteColor];
}

- (void)setupNavigationControllerWithViewController:(UIViewController *)viewController
                                              title:(NSString *)title
                                              image:(NSString *)image
                                      selectedImage:(NSString *)selectedImage
{
    
    DBXBaseNavigationController *baseNavC = [[DBXBaseNavigationController alloc] initWithRootViewController:viewController];
    baseNavC.tabBarItem.title = title;
    baseNavC.tabBarItem.image = [UIImage imageWithOriginal:image];
    baseNavC.tabBarItem.selectedImage = [UIImage imageWithOriginal:selectedImage];
    [self addChildViewController:baseNavC];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
