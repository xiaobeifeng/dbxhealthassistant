//
//  DBXNavigationController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/20.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXBaseNavigationController.h"

@interface DBXBaseNavigationController ()

@end

@implementation DBXBaseNavigationController

+ (void)initialize
{
    
    UINavigationBar *navBar = [UINavigationBar appearance];
    
    [navBar setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor], NSFontAttributeName : kFont(18)}];
    
    [navBar setBackgroundImage:[UIImage imageWithColor:kNavigationBarColor] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    
    navBar.shadowImage = [UIImage new];
    
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (self.viewControllers.count > 0) {
        viewController.hidesBottomBarWhenPushed = YES;
        
        viewController.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithimage:[UIImage imageNamed:@"nav_back_normal"] highImage:[UIImage imageNamed:@"nav_back_normal"] target:self action:@selector(back)];
    }
    
    [super pushViewController:viewController animated:animated];
}

- (void)back
{
    [self popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
