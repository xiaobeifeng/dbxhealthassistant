//
//  ZJCheckVersionTool.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/12/28.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZJCheckVersionTool : NSObject

/**
 检查版本更新
 */
+ (void)checkVersion;

@end

NS_ASSUME_NONNULL_END
