//
//  DBXUpdatePhoneAndEmailApi.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/14.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "BaseRequestApi.h"

/**
 更新电话和邮箱
 */
@interface DBXUpdatePhoneAndEmailApi : BaseRequestApi

- (id)initWithPhone:(NSString *)phone
              email:(NSString *)email;

@end
