//
//  DBXPhoneAndEmailApi.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/14.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "BaseRequestApi.h"


/** 获取用户的手机和邮箱 */
@interface DBXPhoneAndEmailApi : BaseRequestApi

@end
