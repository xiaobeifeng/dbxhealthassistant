//
//  DBXUploadAvatarApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/10/15.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "DBXUploadAvatarApi.h"

@interface DBXUploadAvatarApi()

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *sizeName;
@end

@implementation DBXUploadAvatarApi

- (id)initWithImage:(UIImage *)image userName:(NSString *)userName sizeName:(NSString *)sizeName
{
    self = [super init];
    if (self) {
        _image = image;
        _userName = userName;
        _sizeName = sizeName;
    }
    return self;
}

- (YTKRequestMethod)requestMethod
{
    return YTKRequestMethodPOST;
}

- (NSString *)requestUrl {
    
    return [NSString stringWithFormat:@"/AppService/lohas/health/auto/avatar/save/%@/%@", _sizeName,_userName];
}

- (AFConstructingBlock)constructingBodyBlock
{
    return ^(id<AFMultipartFormData> formData) {
        NSData *data = UIImageJPEGRepresentation(self.image, 0.5);
        NSString *name = @"file";
        NSString *formKey = @"file";
        NSString *type = @"multipart/form-data";
        [formData appendPartWithFileData:data name:formKey fileName:name mimeType:type];
    };
}
//
//- (id)jsonValidator {
//    return @{ @"file": [NSString class] };
//}
//
//- (NSString *)responseImageId {
//    NSDictionary *dict = self.responseJSONObject;
//    return dict[@"file"];
//}

@end
