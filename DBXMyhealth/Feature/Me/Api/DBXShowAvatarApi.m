//
//  DBXShowAvatarApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/10/15.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "DBXShowAvatarApi.h"

@implementation DBXShowAvatarApi

- (NSString *)requestUrl {
    
    return @"/AppService/lohas/health/avatar";
}


- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
