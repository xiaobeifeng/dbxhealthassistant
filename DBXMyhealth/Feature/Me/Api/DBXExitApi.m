//
//  DBXExitApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/14.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXExitApi.h"

@implementation DBXExitApi

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return @"/AppService/lohas/user/token";
}

// 请求方法，某人是GET
- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodDELETE;
}

@end
