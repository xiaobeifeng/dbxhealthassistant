//
//  DBXFeedBackApi.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/14.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "BaseRequestApi.h"

@interface DBXFeedBackApi : BaseRequestApi

- (id)initWithBackmessage:(NSString *)backmessage;

@end
