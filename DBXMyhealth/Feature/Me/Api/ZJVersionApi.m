
//  Created by zhoujian on 2018/10/30.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "ZJVersionApi.h"

@implementation ZJVersionApi

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl
{
    return @"/AppService/lohas/health/auto/remind/update/bytime/iosversion";
}

// 请求方法，某人是GET
- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
