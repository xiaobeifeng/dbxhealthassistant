//
//  DBXMePersonalinfoApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/3.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXMePersonalinfoApi.h"

@implementation DBXMePersonalinfoApi

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return @"/AppService/lohas/health/archives";
}

// 请求方法，某人是GET
- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
