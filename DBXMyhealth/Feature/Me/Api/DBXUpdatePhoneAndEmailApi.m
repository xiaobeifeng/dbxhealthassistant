//
//  DBXUpdatePhoneAndEmailApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/14.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXUpdatePhoneAndEmailApi.h"

@interface DBXUpdatePhoneAndEmailApi()

@property(nonatomic, copy) NSString *phone;
@property(nonatomic, copy) NSString *email;

@end

@implementation DBXUpdatePhoneAndEmailApi
- (id)initWithPhone:(NSString *)phone
              email:(NSString *)email
{
    
    self = [super init];
    if (self) {
        _phone = phone;
        _email = email;
    }
    return self;
}

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return @"/AppService/lohas/health/userquery";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

// 请求体
- (id)requestArgument {
    
    NSDictionary *dic =  [NSDictionary dictionaryWithObjectsAndKeys:_phone, @"phone", _email,@"email", nil];
    
    return dic;
}
@end
