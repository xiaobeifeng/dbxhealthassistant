//
//  DBXPhoneAndEmailApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/14.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXPhoneAndEmailApi.h"

@implementation DBXPhoneAndEmailApi

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    return @"/AppService/lohas/health/phoneandemail";
}

// 请求方法，某人是GET
- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
