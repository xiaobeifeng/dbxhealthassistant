//
//  DBXUploadAvatarApi.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/10/15.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "BaseRequestApi.h"

NS_ASSUME_NONNULL_BEGIN

/**
 头像上传
 */
@interface DBXUploadAvatarApi : BaseRequestApi


/**
 初始化上传头像接口

 @param image 头像
 @return
 */
- (id)initWithImage:(UIImage *)image userName:(NSString *)userName sizeName:(NSString *)sizeName;
- (NSString *)responseImageId;

@end

NS_ASSUME_NONNULL_END
