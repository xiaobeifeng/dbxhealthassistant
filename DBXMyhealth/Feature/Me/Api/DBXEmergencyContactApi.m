//
//  DBXEmergencyContactApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/3.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXEmergencyContactApi.h"

@implementation DBXEmergencyContactApi

- (NSString *)requestUrl {
    
    return @"/AppService/lohas/health/person";
}


- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
