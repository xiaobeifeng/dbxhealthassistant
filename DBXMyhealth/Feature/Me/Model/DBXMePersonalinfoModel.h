//
//  DBXMePersonalinfoModel.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/3.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBXMePersonalinfoModelData :NSObject
@property (nonatomic , copy) NSString              * username;
@property (nonatomic , copy) NSString              * cmbCity;
@property (nonatomic , copy) NSString              * highpre;
@property (nonatomic , copy) NSString              * hight;
@property (nonatomic , copy) NSString              * vocation;
@property (nonatomic , copy) NSString              * wadd;
@property (nonatomic , copy) NSString              * weight;
@property (nonatomic , copy) NSString              * cmbArea;
@property (nonatomic , copy) NSString              * cardId;
@property (nonatomic , copy) NSString              * tel;
@property (nonatomic , copy) NSString              * sex;
@property (nonatomic , copy) NSString              * cmbProvince;
@property (nonatomic , copy) NSString              * nation;
@property (nonatomic , copy) NSString              * birth;
@property (nonatomic , copy) NSString              * email;
@property (nonatomic , copy) NSString              * phone;
@property (nonatomic , copy) NSString              * blsugar;
@property (nonatomic , copy) NSString              * spo;
@property (nonatomic , copy) NSString              * blood;
@property (nonatomic , copy) NSString              * addr;
@property (nonatomic , copy) NSString              * married;
@property (nonatomic , copy) NSString              * lowpre;
@property (nonatomic , copy) NSString              * fmxy;
@property (nonatomic , copy) NSString              * culture;

@end

@interface DBXMePersonalinfoModel :NSObject
@property (nonatomic , copy) NSString              * message;
@property (nonatomic , copy) NSArray<DBXMePersonalinfoModelData *>              * data;
@property (nonatomic , copy) NSString              * state;

@end
