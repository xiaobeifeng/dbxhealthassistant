//
//  DBXMeTableViewCell.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/31.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBXMeTableViewCell : UITableViewCell

@property(nonatomic, copy) NSString *icon;
@property(nonatomic, copy) NSString *title;
@end
