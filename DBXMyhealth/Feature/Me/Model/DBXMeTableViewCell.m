//
//  DBXMeTableViewCell.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/31.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXMeTableViewCell.h"

@interface DBXMeTableViewCell()
@property(nonatomic, strong) UIImageView *iconImageView;
@property(nonatomic, strong) UILabel *titleLbl;
@property(nonatomic, strong) UIView *lineView;
@end

@implementation DBXMeTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        _iconImageView = [UIImageView new];
        _iconImageView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_iconImageView];
        
        _titleLbl = [UILabel new];
        [_titleLbl sizeToFit];
        _titleLbl.font = kCellTitleFont;
        [self.contentView addSubview:_titleLbl];
        
        _lineView = [UIView new];
        _lineView.backgroundColor = kTableSeparatorLineColor;
        [self.contentView addSubview:_lineView];
        
    }
    return self;
}

+ (BOOL)requiresConstraintBasedLayout
{
    return YES;
}

- (void)updateConstraints
{
    [_iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).with.offset(13);
        make.top.mas_equalTo(self.contentView).with.offset(13);
        make.width.height.mas_equalTo(@20);
        make.bottom.mas_equalTo(self.contentView).with.offset(-13);
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).with.offset(13);
        make.right.mas_equalTo(self.contentView).with.offset(100);
//        make.top.mas_equalTo(self.iconImageView.bottom).with.offset(13);
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(self.contentView);
    }];
    
    [_titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.iconImageView.mas_centerY);
        make.left.mas_equalTo(self.iconImageView.mas_right).with.offset(13);
    }];
    
    [super updateConstraints];
}

- (void)setIcon:(NSString *)icon
{
    if (!kStringIsEmpty(icon)) {
        _iconImageView.image = [UIImage imageNamed:icon];
    }
}

- (void)setTitle:(NSString *)title
{
    if (!kStringIsEmpty(title)) {
        _titleLbl.text = title;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
