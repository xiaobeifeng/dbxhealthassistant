
//  Created by zhoujian on 2018/10/30.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZJVersionModelData: NSObject
@property (nonatomic ,copy) NSString * version_id;
@property (nonatomic ,copy) NSString * version_name;
@property (nonatomic ,copy) NSString * save_time;
@property (nonatomic ,copy) NSString * state;
@property (nonatomic ,copy) NSString * updataInfo;
@end

@interface ZJVersionModel: NSObject
@property (nonatomic ,assign) NSInteger  state;
@property (nonatomic ,copy) NSString * message;
@property (nonatomic ,copy) NSArray<ZJVersionModelData *> * data;
@end

NS_ASSUME_NONNULL_END
