//
//  DBXEmergencyContactModel.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/3.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBXEmergencyContactModelData :NSObject
@property (nonatomic , copy) NSString              * hip;
@property (nonatomic , copy) NSString              * tj;
@property (nonatomic , copy) NSString              * gn;
@property (nonatomic , copy) NSString              * naplace;
@property (nonatomic , copy) NSString              * temperature;
@property (nonatomic , copy) NSString              * jw;
@property (nonatomic , copy) NSString              * reone;
@property (nonatomic , copy) NSString              * wc;
@property (nonatomic , copy) NSString              * tethree;
@property (nonatomic , copy) NSString              * tetwo;
@property (nonatomic , copy) NSString              * breath;
@property (nonatomic , copy) NSString              * sh;
@property (nonatomic , copy) NSString              * az;
@property (nonatomic , copy) NSString              * jz;
@property (nonatomic , copy) NSString              * qw;
@property (nonatomic , copy) NSString              * pulse;
@property (nonatomic , copy) NSString              * rethree;
@property (nonatomic , copy) NSString              * mb;
@property (nonatomic , copy) NSString              * teone;
@property (nonatomic , copy) NSString              * bmi;
@property (nonatomic , copy) NSString              * hy;
@property (nonatomic , copy) NSString              * grs;
@property (nonatomic , copy) NSString              * bfr;
@property (nonatomic , copy) NSString              * ss;
@property (nonatomic , copy) NSString              * retwo;
@property (nonatomic , copy) NSString              * kr;
@property (nonatomic , copy) NSString              * gm;

@end

@interface DBXEmergencyContactModel :NSObject
@property (nonatomic , copy) NSString              * message;
@property (nonatomic , copy) NSArray<DBXEmergencyContactModelData *>              * data;
@property (nonatomic , copy) NSString              * state;

@end
