//
//  DBXFeedBackViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/10.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXFeedBackViewController.h"
#import "DBXFeedBackApi.h"

@interface DBXFeedBackViewController ()

@property(nonatomic, strong) UITextView *inputTextView;


@end

@implementation DBXFeedBackViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self){
        [self initializeForm];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self){
        [self initializeForm];
    }
    return self;
}

- (void)initializeForm
{
    XLFormDescriptor * form;
    XLFormSectionDescriptor * section;
    XLFormRowDescriptor * row;
    
    form = [XLFormDescriptor formDescriptorWithTitle:@"个人资料"];
    section = [XLFormSectionDescriptor formSection];
    
    [form addFormSection:section];
    
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"feedback" rowType:XLFormRowDescriptorTypeTextView];
    [row.cellConfigAtConfigure setObject:@"请输入遇到的问题或建议" forKey:@"textView.placeholder"];
    [section addFormRow:row];
    
    
    XLFormSectionDescriptor *section2 = [XLFormSectionDescriptor formSection];
    [form addFormSection:section2];
    
    // 联系电话
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"phone" rowType:XLFormRowDescriptorTypePhone title:@"联系电话"];
    [row.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.color"];
    [row.cellConfig setObject:@"选填，便于我们联系你" forKey:@"textField.placeholder"];
    [section2 addFormRow:row];
    
    self.form = form;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"意见反馈";
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem titleItem:@"提交" target:self action:@selector(commitBtnClick)];
}

- (void)commitBtnClick
{
    NSDictionary *dic = [self formValues];
    
    NSString *feedback = dic[@"feedback"];
    NSString *phone = dic[@"phone"];
    
    if (kStringIsEmpty(feedback)) {
        [MBProgressHUD showInfoMessage:@"请填写问题或意见"];
        return;
    }
    
    [MBProgressHUD showActivityMessageInView:@"提交中"];
    [MBProgressHUD hideHUD];
    DBXFeedBackApi *api = [[DBXFeedBackApi alloc] initWithBackmessage:feedback];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        XLFormRowDescriptor *feedbackRow = [self.form formRowWithTag:@"feedback"];
        feedbackRow.value = @"";
        [self reloadFormRow:feedbackRow];
        
        XLFormRowDescriptor *phoneRow = [self.form formRowWithTag:@"phone"];
        phoneRow.value = @"";
        [self reloadFormRow:phoneRow];
        
        
        [MBProgressHUD hideHUD];
        [MBProgressHUD showInfoMessage:@"提交完成"];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        
    }];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // change cell height of a particular cell
    if ([[self.form formRowAtIndex:indexPath].tag isEqualToString:@"FeedbackTextView"]) {
        return 200.0;
    }
    
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


@end
