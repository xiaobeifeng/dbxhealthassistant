//
//  DBXAccountManageViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/10.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXAccountManageViewController.h"
#import "DBXChangePasswordViewController.h"
#import "DBXUpdatePhoneAndEmailApi.h"
#import "DBXPhoneAndEmailApi.h"

#define kTextColor [UIColor colorWithRed:0.62 green:0.63 blue:0.64 alpha:1.00]

@interface DBXAccountManageViewController ()
@property(nonatomic, copy) NSString *currentPhone;
@property(nonatomic, copy) NSString *currentMail;
@end

@implementation DBXAccountManageViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self){
        [self initializeForm];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self){
        [self initializeForm];
    }
    return self;
}

- (void)initializeForm
{
    DBXPhoneAndEmailApi *api = [[DBXPhoneAndEmailApi alloc] init];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        if (request.responseJSONObject) {
            
            NSDictionary *dic = request.responseJSONObject;
            
            NSArray *array = dic[@"data"];
            
            if (!kArrayIsEmpty(array)) {
                
                NSDictionary *dataDic = [dic[@"data"] firstObject];
                
                if (kDictIsEmpty(dataDic)) {
                    return;
                }
                
                self.currentPhone = dataDic[@"PHONE"];
                XLFormRowDescriptor *phoneRow = [self.form formRowWithTag:@"phone"];
                phoneRow.value = self.currentPhone;
                [self reloadFormRow:phoneRow];
                
                self.currentMail = dataDic[@"EMAIL"];
                XLFormRowDescriptor *emailRow = [self.form formRowWithTag:@"mail"];
                emailRow.value = self.currentMail;
                [self reloadFormRow:emailRow];
            }
        }
        
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
    
    XLFormDescriptor * form;
    XLFormSectionDescriptor * section;
    XLFormRowDescriptor * row;
    
    form = [XLFormDescriptor formDescriptorWithTitle:@"个人资料"];
    section = [XLFormSectionDescriptor formSection];

    [form addFormSection:section];
    
    // 手机号码
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"phone" rowType:XLFormRowDescriptorTypePhone title:@"手机号码"];
    [row.cellConfig setObject:[[NSAttributedString alloc] initWithString:@"暂未填写"
                                                              attributes:@{
                                                                           NSForegroundColorAttributeName:kTextColor
//                                                                           NSFontAttributeName:kFont(14)
                                                                           }]
                       forKey:@"textField.attributedPlaceholder"];
    [row.cellConfig setObject:kTextColor forKey:@"textField.textColor"];
//    [row.cellConfig setObject:[UIFont fontWithName:@"Helvetica" size:14] forKey:@"textLabel.font"];
    [row.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [section addFormRow:row];
    
    // 邮件地址
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"mail" rowType:XLFormRowDescriptorTypeEmail title:@"邮件地址"];
    [row.cellConfig setObject:[[NSAttributedString alloc] initWithString:@"暂未填写"
                                                              attributes:@{
                                                                           NSForegroundColorAttributeName:kTextColor
//                                                                           NSFontAttributeName:kFont(14)
                                                                           }]
                       forKey:@"textField.attributedPlaceholder"];
    [row.cellConfig setObject:kTextColor forKey:@"textField.textColor"];
    [row.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
//    [row.cellConfig setObject:[UIFont fontWithName:@"Helvetica" size:14] forKey:@"textLabel.font"];
    [section addFormRow:row];
    
    XLFormSectionDescriptor *section2 = [XLFormSectionDescriptor formSection];
    [form addFormSection:section2];
    
    // 修改密码
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"changePassword" rowType:XLFormRowDescriptorTypeButton title:@"修改密码"];
    [row.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.color"];
    [row.cellConfigAtConfigure setObject:[UIColor whiteColor] forKey:@"backgroundColor"];
    [row.cellConfig setObject:@(NSTextAlignmentNatural) forKey:@"textLabel.textAlignment"];
    [row.cellConfig setObject:@(UITableViewCellAccessoryDisclosureIndicator) forKey:@"accessoryType"];
//    [row.cellConfig setObject:[UIFont fontWithName:@"Helvetica" size:14] forKey:@"textLabel.font"];
    [section2 addFormRow:row];
    
    self.form = form;

}

- (void)endEditing:(XLFormRowDescriptor *)rowDescriptor
{
    NSLog(@"%@, %@", rowDescriptor.tag, rowDescriptor.value);
    if ([rowDescriptor.tag isEqualToString:@"phone"]) {

        if (kObjectIsEmpty(rowDescriptor.value)) {
            self.currentPhone = @"";
        } else {
            self.currentPhone = [NSString stringWithFormat:@"%@", rowDescriptor.value];
        }
        
        DBXUpdatePhoneAndEmailApi *api = [[DBXUpdatePhoneAndEmailApi alloc] initWithPhone:self.currentPhone email:self.currentMail];
        [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            
            NSLog(@"%@", request.responseJSONObject);
            
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            
        }];
    }
    
    if ([rowDescriptor.tag isEqualToString:@"mail"]) {

        if (kObjectIsEmpty(rowDescriptor.value)) {
            self.currentMail = @"";
        } else {
            self.currentMail = [NSString stringWithFormat:@"%@", rowDescriptor.value];
        }
        DBXUpdatePhoneAndEmailApi *api = [[DBXUpdatePhoneAndEmailApi alloc] initWithPhone:self.currentPhone email:self.currentMail];
        [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            
            NSLog(@"%@", request.responseJSONObject);
            
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            
        }];
    }

}

- (void)didSelectFormRow:(XLFormRowDescriptor *)formRow
{
    // 点击修改密码栏
    if([formRow.tag isEqualToString:@"changePassword"] && formRow.rowType == XLFormRowDescriptorTypeButton) {
        NSLog(@"点击修改密码栏");
        [self deselectFormRow:formRow];
        DBXChangePasswordViewController *changePasswordVC = [DBXChangePasswordViewController new];
        [self.navigationController pushViewController:changePasswordVC animated:YES];
    }
        
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
