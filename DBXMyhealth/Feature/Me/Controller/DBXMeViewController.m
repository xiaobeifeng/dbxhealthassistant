//
//  DBXMeViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/20.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXMeViewController.h"
#import "DBXMeTableViewCell.h"
#import "DBXMePersonalinfoViewController.h"
#import "DBXSettingViewController.h"
#import "DBXShowAvatarApi.h"
#import "DBXUploadAvatarApi.h"
#import "DBXUploadAvatarModel.h"

static NSString *kMeTableViewCellIdentifier = @"DBXMeTableViewCell";

@interface DBXMeViewController ()<UITableViewDelegate, UITableViewDataSource,UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, copy) NSArray *titleArray;
@property(nonatomic, copy) NSArray *iconArray;
/** 头像 */
@property(nonatomic, strong) UIImageView *avatarImageView;

@end

@implementation DBXMeViewController
#pragma mark - 懒加载
- (NSArray *)titleArray
{
    if (!_titleArray) {
        _titleArray = @[@"个人资料", @"设置"];
    }
    return _titleArray;
}

- (NSArray *)iconArray
{
    if (!_iconArray) {
        _iconArray = @[@"me_userInfo", @"me_setting"];
    }
    return _iconArray;
}

- (UITableView *)tableView
{
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-self.tabBarController.tabBar.height) style:UITableViewStylePlain];
        [_tableView registerClass:[DBXMeTableViewCell class] forCellReuseIdentifier:kMeTableViewCellIdentifier];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        // 解决滑动视图顶部空出状态栏高度的问题
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)]) {
                self.edgesForExtendedLayout = UIRectEdgeNone;
            }
        }
        _tableView.tableFooterView = [UIView new];
        [self.view addSubview:_tableView];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, -5000, kScreenWidth, 5000)];
        imageView.backgroundColor = kNavigationBarColor;
        [_tableView addSubview:imageView];
        
    }
    return _tableView;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIView *headerView = [UIView new];
    headerView.backgroundColor = kNavigationBarColor;
    headerView.frame = CGRectMake(0, 0, kScreenWidth, 250);
    self.tableView.tableHeaderView = headerView;
    
    _avatarImageView = [UIImageView new];
    _avatarImageView.image = [UIImage imageNamed:@"me_avatar_placeholder"];
    _avatarImageView.backgroundColor = [UIColor whiteColor];
    kViewRadius(_avatarImageView, 40);
    _avatarImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(avatarSettingTap)];
    [_avatarImageView addGestureRecognizer:tap];
    [headerView addSubview:_avatarImageView];
    [_avatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(80);
        make.centerX.mas_equalTo(headerView.mas_centerX);
        make.centerY.mas_equalTo(headerView.mas_centerY).with.offset(-10);
    }];
    
    UILabel *nameLbl = [UILabel new];
    nameLbl.text = !kStringIsEmpty([[NSUserDefaults standardUserDefaults] objectForKey:kAccount])?[[NSUserDefaults standardUserDefaults] objectForKey:kAccount]:@"加载中...";
    nameLbl.font = kFont(14);
    nameLbl.textColor = [UIColor whiteColor];
    [nameLbl sizeToFit];
    [headerView addSubview:nameLbl];
    [nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(headerView.mas_centerX);
        make.top.mas_equalTo(self.avatarImageView.mas_bottom).with.offset(15);
    }];
    
    [self showUserAvatar];
}

/**
 展示用户头像
 */
- (void)showUserAvatar
{
    DBXShowAvatarApi *api = [[DBXShowAvatarApi alloc] init];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        if (request.responseJSONObject) {
            
            NSArray *array = request.responseJSONObject[@"data"];
            if (array.count > 0) {
                NSString *imageName = [array.firstObject objectForKey:@"path"];
                [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:kImagePath(imageName)] placeholderImage:[UIImage imageNamed:@"me_avatar_placeholder"]];
                
               
            }
            
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
    
}


/**
 头像设置
 */
- (void)avatarSettingTap
{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"" message:@"设置头像" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *takePhotoAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate = self;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePickerController.allowsEditing = YES;
        [self presentViewController:imagePickerController
                           animated:YES
                         completion:NULL];
        
    }];
    
    UIAlertAction *choosePhotoAction = [UIAlertAction actionWithTitle:@"从手机相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate = self;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.allowsEditing = YES;
        [self presentViewController:imagePickerController
                           animated:YES
                         completion:NULL];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alertVC addAction:takePhotoAction];
    [alertVC addAction:choosePhotoAction];
    [alertVC addAction:cancelAction];
    
    [self presentViewController:alertVC animated:YES completion:nil];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.iconArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DBXMeTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:kMeTableViewCellIdentifier forIndexPath:indexPath];
    [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    return cell;
}

- (void)setupModelOfCell:(DBXMeTableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.icon = self.iconArray[indexPath.row];
    cell.title = self.titleArray[indexPath.row];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView fd_heightForCellWithIdentifier:kMeTableViewCellIdentifier cacheByIndexPath:indexPath configuration:^(id cell) {
        
        [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        // 个人资料
        DBXMePersonalinfoViewController *mePersonalinfoVC = [DBXMePersonalinfoViewController new];
        [self.navigationController pushViewController:mePersonalinfoVC animated:YES];
    } else if (indexPath.row == 1) {
        // 设置
        DBXSettingViewController *settingVC = [DBXSettingViewController new];
        [self.navigationController pushViewController:settingVC animated:YES];
    }
}

#pragma mark - UIImagePickerControllerDelegate Methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    [MBProgressHUD showActivityMessageInWindow:@""];
    if (info[UIImagePickerControllerEditedImage] == nil) {
        [MBProgressHUD hideHUD];
        [MBProgressHUD showTipMessageInWindow:@"请重试"];
        return;
    }
    
    NSData *data = UIImageJPEGRepresentation(info[UIImagePickerControllerEditedImage], 0.5);
    NSInteger dataLength = [data length];
    NSString *sizeName = [NSString stringWithFormat:@"%ld.png", (long)dataLength];

    // 上传用户头像
    DBXUploadAvatarApi *api = [[DBXUploadAvatarApi alloc] initWithImage:info[UIImagePickerControllerEditedImage] userName:kUserDefaultsGet(kAccount) sizeName:sizeName];
    
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [MBProgressHUD hideHUD];
        NSLog(@"%@", request.responseJSONObject);
        DBXUploadAvatarModel *model = [DBXUploadAvatarModel yy_modelWithJSON:request.responseJSONObject];
        
        // 上传成功
        if ([model.state integerValue] == 0) {
            // 展示上传成功的图片
            self.avatarImageView.image = info[UIImagePickerControllerEditedImage];
            
        } else {
            // 对接口返回的错误信息进行展示，不更新头像
            [MBProgressHUD showTipMessageInWindow:model.message];
        }
        
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        [MBProgressHUD hideHUD];
        [MBProgressHUD showTipMessageInWindow:@"请重试"];
        NSLog(@"失败了");
    }];
    
    // 处理完毕   回到个人信息页面
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES
                               completion:NULL];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
