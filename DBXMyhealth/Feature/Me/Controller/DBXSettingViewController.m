//
//  DBXSettingViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/3.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXSettingViewController.h"
#import "DBXPeripheralTableViewCell.h"
#import "DBXAccountManageViewController.h"
#import "DBXFeedBackViewController.h"
#import "DBXLoginViewController.h"
#import "DBXExitApi.h"
#import "ZJCheckVersionTool.h"

static NSString *cellIdentifier = @"DBXPeripheralTableViewCell";

@interface DBXSettingViewController ()<UITableViewDelegate, UITableViewDataSource>

@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, copy) NSArray *titleArray;

@end

@implementation DBXSettingViewController
#pragma mark - 懒加载
- (NSArray *)titleArray
{
    if (!_titleArray) {
        _titleArray = @[@[@"账号管理"], @[@"检查更新", @"意见反馈"]];
    }
    return _titleArray;
}

- (UITableView *)tableView
{
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
//        _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, CGFLOAT_MIN)];
//        _tableView.sectionHeaderHeight = CGFLOAT_MIN;
//        _tableView.sectionFooterHeight = 10;
        [_tableView registerClass:[DBXPeripheralTableViewCell class] forCellReuseIdentifier:cellIdentifier];
//        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
     
    }
    
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"设置";
    [self.view addSubview:self.tableView];
    
    UIView *footerView = [[UIView alloc] init];
    footerView.backgroundColor = [UIColor clearColor];
    footerView.frame = CGRectMake(0, 0, kScreenWidth, 100);
    self.tableView.tableFooterView = footerView;
    
    UIButton *exitButton = [UIButton new];
    exitButton.backgroundColor = [UIColor colorWithHexString:@"#FF5A59"];
    [exitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [exitButton setTitle:@"退出当前账号" forState:UIControlStateNormal];
    [exitButton addTarget:self action:@selector(exitCurrentAccount) forControlEvents:UIControlEventTouchUpInside];
    kViewRadius(exitButton, 20.0f);
    [footerView addSubview:exitButton];
    [exitButton  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(footerView.mas_centerX);
        make.centerY.mas_equalTo(footerView.mas_centerY);
        make.height.mas_equalTo(44);
        make.left.mas_equalTo(footerView).with.offset(40);
        make.right.mas_equalTo(footerView).with.offset(-40);
    }];
    
}

#pragma mark - tableView Delegate and DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.titleArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.titleArray objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DBXPeripheralTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

    cell.line.hidden = YES;
    
    if (indexPath.section == 3) {
        cell.titleLbl.textAlignment = NSTextAlignmentCenter;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.titleLbl.textColor = [UIColor redColor];
    } else {
        cell.titleLbl.textAlignment = NSTextAlignmentLeft;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    
    
    
    return cell;
}

- (void)setupModelOfCell:(DBXPeripheralTableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.title = self.titleArray[indexPath.section][indexPath.row];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView fd_heightForCellWithIdentifier:cellIdentifier cacheByIndexPath:indexPath configuration:^(id cell) {
        
        [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // 账号管理
    if (indexPath.section == 0) {
        DBXAccountManageViewController *accountManageVC = [DBXAccountManageViewController new];
        [self.navigationController pushViewController:accountManageVC animated:YES];
    }
    
    if (indexPath.section == 1) {
        
        // 检查更新
        if (indexPath.row == 0) {
            [ZJCheckVersionTool checkVersion];
        }
        
        // 意见反馈
        if (indexPath.row == 1) {
            DBXFeedBackViewController *feedBackVC = [DBXFeedBackViewController new];
            [self.navigationController pushViewController:feedBackVC animated:YES];
        }
    }
    

}


/**
 退出当前账号
 */
- (void)exitCurrentAccount
{
    
    DBXExitApi *api = [[DBXExitApi alloc] init];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kToken];
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kAccount];
        
        DBXLoginViewController *loginVC = [DBXLoginViewController new];
        DBXBaseNavigationController *nav = [[DBXBaseNavigationController alloc] initWithRootViewController:loginVC];
        [UIApplication sharedApplication].keyWindow.rootViewController = nav;
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [MBProgressHUD showInfoMessage:@"退出登录失败，请稍后再试"];
    }];
    
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
