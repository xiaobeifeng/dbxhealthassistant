//
//  DBXChangePasswordViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/10.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXChangePasswordViewController.h"
#import "DBXChangePasswordApi.h"
#import "DBXBaseNavigationController.h"
#import "DBXLoginViewController.h"

@interface DBXChangePasswordViewController ()

@end

@implementation DBXChangePasswordViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self){
        [self initializeForm];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self){
        [self initializeForm];
    }
    return self;
}

- (void)initializeForm
{
    XLFormDescriptor * form;
    XLFormSectionDescriptor * section;
    XLFormRowDescriptor * row;
    
    form = [XLFormDescriptor formDescriptorWithTitle:@"修改密码"];
    section = [XLFormSectionDescriptor formSection];
    [form addFormSection:section];
    
//    // 手机号
//    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"phone" rowType:XLFormRowDescriptorTypeNumber title:@"手机号"];
//    [row.cellConfig setObject:@"请输入手机号" forKey:@"textField.placeholder"];
//    [section addFormRow:row];
    
    // 旧密码
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"oldPassword" rowType:XLFormRowDescriptorTypePassword title:@"旧密码"];
    [row.cellConfig setObject:@"请输入旧密码" forKey:@"textField.placeholder"];
    [section addFormRow:row];
    
    // 新密码
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"newPassword" rowType:XLFormRowDescriptorTypePassword title:@"新密码"];
    [row.cellConfig setObject:@"请输入新密码" forKey:@"textField.placeholder"];
    [section addFormRow:row];
    
    // 再次输入新密码
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"againNewPassword" rowType:XLFormRowDescriptorTypePassword title:@"确认密码"];
    [row.cellConfig setObject:@"请再次输入新密码" forKey:@"textField.placeholder"];
    [section addFormRow:row];
    
    self.form = form;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
   
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem titleItem:@"保存" target:self action:@selector(saveBtnClick)];
}


/**
 保存设置
 */
- (void)saveBtnClick
{
    //获取表单所有到的值
    NSDictionary *values =  [self formValues];
//    NSString *phone = values[@"phone"];
    NSString *oldPassword = values[@"oldPassword"];
    NSString *newPassword = values[@"newPassword"];
    NSString *againNewPassword = values[@"againNewPassword"];
    
    // 校验
    //    if (kObjectIsEmpty(phone)) {
    //        [MBProgressHUD showInfoMessage:@"请输入手机号"];
    //        return;
    //    }
    
    if (kObjectIsEmpty(oldPassword)) {
        [MBProgressHUD showInfoMessage:@"请输入旧密码"];
        return;
    }
    
    if (oldPassword.length > 20) {
        [MBProgressHUD showInfoMessage:@"密码太长啦～"];
        return;
    }
    
    if (!kObjectIsEmpty(newPassword) && !kObjectIsEmpty(againNewPassword)) {
        
        if (![newPassword isEqualToString:againNewPassword]) {
            [MBProgressHUD showInfoMessage:@"新密码和确认密码不一致，请检查后再试"];
            return;
        }
        
    } else {
        
        if (kObjectIsEmpty(newPassword)) {
            [MBProgressHUD showInfoMessage:@"请输入新密码"];
            return;
        } else {
            [MBProgressHUD showInfoMessage:@"请输入确认密码"];
            return;
        }
        
    }
    
    [MBProgressHUD showActivityMessageInWindow:@""];
    DBXChangePasswordApi *api = [[DBXChangePasswordApi alloc] initWithOldPassword:oldPassword newsPassword:newPassword];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [MBProgressHUD hideHUD];
        NSLog(@"%@", request.responseJSONObject);
        if (request.responseJSONObject) {
            
            NSString *message = request.responseJSONObject[@"message"];
            [MBProgressHUD showInfoMessage:message];
            
            // 用户重新登录
            /* 接口注：
            保存成功还是失败state都是0，不能作为判断基准。判断是否成功的基准是message，如果原密码输入错误会返回“原密码错误”。如果post的参数不是oldPassword会按照原密码输入错误进行返回，如果post的参数不是newPassword，会返回“参数错误”（后边两种错误是只有在开发的过程中才会出现的，不影响正常使用，只是增加一个判断）
            */
            if ([message isEqualToString:@"成功"]) {
                
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:kToken];
                
                DBXBaseNavigationController *baseNavC = [[DBXBaseNavigationController alloc] initWithRootViewController:[DBXLoginViewController new]];
                [UIApplication sharedApplication].keyWindow.rootViewController = baseNavC;
                
                [MBProgressHUD showInfoMessage:@"修改成功，请重新登录"];
                
            }
            
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [MBProgressHUD hideHUD];
    }];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
