//
//  DBXCircleView.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/14.
//  Copyright © 2018年 zhoujian. All rights reserved.
//

#import "DBXCircleView.h"

@interface DBXCircleView ()
{
    CAShapeLayer* _trackLayer;
    CAShapeLayer* _progressLayer;
}

@end

@implementation DBXCircleView

- (instancetype)initWithFrame:(CGRect)frame
           gradientLayerColor:(UIColor *)gradientLayerColor
                  circleWidth:(CGFloat)circleWidth
{
    if (self = [super initWithFrame:frame]) {
        
        float centerX = self.bounds.size.width/2.0;
        float centerY = self.bounds.size.height/2.0;
        //半径
        float radius = (self.bounds.size.width-circleWidth)/2.0;
        
        //创建贝塞尔路径
        UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(centerX, centerY) radius:radius startAngle:(-0.5f*M_PI) endAngle:1.5f*M_PI clockwise:YES];
        
        //添加背景圆环
        CAShapeLayer *backLayer = [CAShapeLayer layer];
        backLayer.frame = self.bounds;
        backLayer.fillColor =  [[UIColor clearColor] CGColor];
        backLayer.strokeColor  = [UIColor colorWithRed:1.00 green:1.00 blue:1.00 alpha:0.3].CGColor;
        backLayer.lineWidth = circleWidth;
        backLayer.path = [path CGPath];
        backLayer.strokeEnd = 1;
        [self.layer addSublayer:backLayer];
        
        // 创建进度layer
        _progressLayer = [CAShapeLayer layer];
        _progressLayer.frame = self.bounds;
        _progressLayer.fillColor =  [[UIColor clearColor] CGColor];
        // 指定path的渲染颜色
        _progressLayer.strokeColor  = [[UIColor blackColor] CGColor];
        _progressLayer.lineCap = kCALineCapRound;
        _progressLayer.lineWidth = circleWidth;
        _progressLayer.path = [path CGPath];
        _progressLayer.strokeEnd = 0;
        
        //设置渐变颜色
        CAGradientLayer *gradientLayer =  [CAGradientLayer layer];
        gradientLayer.frame = self.bounds;
        [gradientLayer setColors:[NSArray arrayWithObjects:(id)[[UIColor whiteColor] CGColor],(id)[gradientLayerColor CGColor], nil]];
        gradientLayer.startPoint = CGPointMake(1, 0);
        gradientLayer.endPoint = CGPointMake(0, 1);
        [gradientLayer setMask:_progressLayer]; // 用progressLayer来截取渐变层
        [self.layer addSublayer:gradientLayer];
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
}

- (void)buildLayout
{
    
}

- (void)setProgress:(double)progress
{
    _progress = progress;
    
    CABasicAnimation *pathAnima = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnima.duration = 2.0f;
    pathAnima.fromValue = @0;
    pathAnima.toValue = @(_progress);
    pathAnima.removedOnCompletion = NO;
    pathAnima.fillMode = kCAFillModeForwards;
    [_progressLayer addAnimation:pathAnima forKey:@"strokeEndAnimation"];

}


@end
