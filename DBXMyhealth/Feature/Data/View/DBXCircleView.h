//
//  DBXCircleView.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/14.
//  Copyright © 2018年 zhoujian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBXCircleView : UIView

- (instancetype)initWithFrame:(CGRect)frame
           gradientLayerColor:(UIColor *)gradientLayerColor
                  circleWidth:(CGFloat)circleWidth;

@property (assign,nonatomic) double progress;


@end
