//
//  DBXDataBpApi.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/4.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "BaseRequestApi.h"

@interface DBXDataBpApi : BaseRequestApi

- (id)initWithCount:(NSInteger)count doa:(NSInteger)doa;

@end
