//
//  DBXDataBloodSugarApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/4.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXDataBloodSugarApi.h"

@interface DBXDataBloodSugarApi()

@property(nonatomic, assign) NSInteger count;
@property(nonatomic, assign) NSInteger doa;
@property(nonatomic, assign) NSInteger mCondition;
@end

@implementation DBXDataBloodSugarApi

- (id)initWithCount:(NSInteger)count doa:(NSInteger)doa mCondition:(NSInteger)mCondition
{
    self = [super init];
    if (self) {
        _count = count;
        _doa = doa;
        _mCondition = mCondition;
    }
    return self;
}

- (NSString *)requestUrl {
    
    NSString *url = [NSString stringWithFormat:@"/AppService/lohas/health/glutwo/%ld/%ld", (long)_count, (long)_doa];
    return url;
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

// 请求体
- (id)requestArgument {
    return @{@"mCondition": @(_mCondition)};
}

@end
