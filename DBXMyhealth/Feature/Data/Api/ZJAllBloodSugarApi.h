//
//  ZJAllBloodSugarApi.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/13.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "BaseRequestApi.h"

NS_ASSUME_NONNULL_BEGIN

/**
 请求全部血糖信息
 */
@interface ZJAllBloodSugarApi : BaseRequestApi

- (id)initWithCount:(NSInteger)count doa:(NSInteger)doa;

@end

NS_ASSUME_NONNULL_END
