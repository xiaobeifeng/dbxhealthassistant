//
//  DBXBloodSugarHistoryTableViewCell.m
//  DBXHealthAssistant
//
//  Created by zhoujian on 2018/8/31.
//  Copyright © 2018年 mc. All rights reserved.
//

#import "DBXBloodSugarHistoryTableViewCell.h"

@interface DBXBloodSugarHistoryTableViewCell()
/**
 日期
 */
@property(nonatomic, strong) UILabel *dateLbl;
/**
 血糖
 */
@property(nonatomic, strong) UILabel *bloodSugarLbl;

/**
 血糖警告信息
 */
@property(nonatomic, strong) UILabel *bsWarnLbl;

@property(nonatomic, strong) UIView *lineView;
@property(nonatomic, strong) UIView *pointView;
@property(nonatomic, strong) UIView *contentBackgroundView;

@end

@implementation DBXBloodSugarHistoryTableViewCell

-  (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.contentView.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.96 alpha:1.00];
        
        _lineView = [UIView new];
        _lineView.backgroundColor = kNavigationBarColor;
        [self.contentView addSubview:_lineView];
        
        _pointView = [UIView new];
        //        _pointView.backgroundColor = [UIColor redColor];
        kViewRadius(_pointView, 5);
        [_lineView addSubview:_pointView];
        
        _contentBackgroundView = [UIView new];
        _contentBackgroundView.backgroundColor = [UIColor whiteColor];
        kViewRadius(_contentBackgroundView, 5.0f);
        [self.contentView addSubview:_contentBackgroundView];
        
        _dateLbl = [UILabel new];
        _dateLbl.backgroundColor = [UIColor whiteColor];
        _dateLbl.textColor = [UIColor colorWithHexString:@"52396C"];
        _dateLbl.font = [UIFont systemFontOfSize:12];
        [_contentBackgroundView addSubview:_dateLbl];
        
        _bloodSugarLbl = [UILabel new];
        _bloodSugarLbl.backgroundColor = [UIColor whiteColor];
        _bloodSugarLbl.textColor = [UIColor colorWithHexString:@"52396C"];
        _bloodSugarLbl.font = [UIFont systemFontOfSize:16];
        [_contentBackgroundView addSubview:_bloodSugarLbl];
        
        _bsWarnLbl = [UILabel new];
        _bsWarnLbl.backgroundColor = [UIColor whiteColor];
        _bsWarnLbl.font = [UIFont systemFontOfSize:14];
        [_contentBackgroundView addSubview:_bsWarnLbl];
        
    }
    return self;
}

+ (BOOL)requiresConstraintBasedLayout
{
    return YES;
}

- (void)updateConstraints
{
    
    [_contentBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).with.offset(13);
        make.left.mas_equalTo(self.contentView).with.offset(80);
        make.right.mas_equalTo(self.contentView).with.offset(-13);
        make.height.mas_equalTo(100);
        make.bottom.mas_equalTo(self.contentView).with.offset(-13);
    }];
    
    [@[_dateLbl, _bloodSugarLbl, _bsWarnLbl] mas_distributeViewsAlongAxis:MASAxisTypeVertical withFixedSpacing:10 leadSpacing:13 tailSpacing:13];
    
    [@[_dateLbl, _bloodSugarLbl, _bsWarnLbl] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentBackgroundView).with.offset(13);
        make.right.mas_equalTo(self.contentBackgroundView).with.offset(-13);
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView);
        make.bottom.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView).with.offset(39);
        make.width.mas_equalTo(2);
    }];
    [_pointView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.lineView.mas_centerY);
        make.centerX.mas_equalTo(self.lineView.mas_centerX);
        make.width.height.mas_equalTo(10);
    }];
    
    
    [super updateConstraints];
}

- (void)setDate:(NSString *)date
{
    if (date.length>0) {
        _dateLbl.text = date;
    }
}

- (void)setBloodSugar:(NSString *)bloodSugar
{
    _bloodSugarLbl.text = bloodSugar;
}

- (void)setIsBsWarn:(BOOL)isBsWarn
{
    if (isBsWarn) {
        _bsWarnLbl.text = @"血糖不正常，请咨询专业医师";
        _bsWarnLbl.textColor = [UIColor colorWithHexString:@"EA5C21"];
        _pointView.backgroundColor = [UIColor colorWithHexString:@"B956D2"];
        
    } else {
        _bsWarnLbl.text = @"血糖正常，请继续保持";
        _bsWarnLbl.textColor = [UIColor colorWithHexString:@"52396C"];
        _pointView.backgroundColor = [UIColor colorWithHexString:@"0499FC"];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
