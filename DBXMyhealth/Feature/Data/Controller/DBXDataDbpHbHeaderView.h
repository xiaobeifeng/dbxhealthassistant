//
//  DBXDataDbpHbHeaderView.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/4.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBXDataDbpHbHeaderView : UIView

@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSString *dPressValue;
@property(nonatomic, copy) NSString *hbValue;
@end
