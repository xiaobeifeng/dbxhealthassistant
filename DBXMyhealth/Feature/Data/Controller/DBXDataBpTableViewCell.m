//
//  DBXDataBpTableViewCell.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/4.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXDataBpTableViewCell.h"

@interface DBXDataBpTableViewCell()

@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UIView *line;
@property (nonatomic, strong) UIImageView *sbpBarImageView;
@property (nonatomic, strong) UIImageView *dbpBarImageView;
@property (nonatomic, strong) UILabel *subTitleLabel;       // 子标题信息（血压数据）

@end

@implementation DBXDataBpTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _iconImageView = [UIImageView new];
        _iconImageView.image = [UIImage imageNamed:@"health_bp_icon"];
        _iconImageView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_iconImageView];
        
        _titleLbl = [UILabel new];
        _titleLbl.text = @"血压";
        _titleLbl.textColor = [UIColor colorWithRed:0.33 green:0.79 blue:0.88 alpha:1.00];
        _titleLbl.font = kFont(14);
        [_titleLbl sizeToFit];
        [self.contentView addSubview:_titleLbl];
        
        _subTitleLabel = [UILabel new];
        _subTitleLabel.text = @"- mmHg";
        _subTitleLabel.textColor = [UIColor colorWithRed:0.33 green:0.79 blue:0.88 alpha:1.00];
        _subTitleLabel.font = kFont(14);
        [_subTitleLabel sizeToFit];
        [self.contentView addSubview:_subTitleLabel];
        
        _dateLbl = [UILabel new];
        _dateLbl.textColor = [UIColor lightGrayColor];
        _dateLbl.font = kFont(12);
        [_dateLbl sizeToFit];
        [self.contentView addSubview:_dateLbl];
        
        _sbpBarImageView = [UIImageView new];
        _sbpBarImageView.image = [UIImage imageNamed:@"health_bp_line"];
        _sbpBarImageView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_sbpBarImageView];
        

        _sbpWarnButton = [UIButton new];
        _sbpWarnButton.titleLabel.font = [UIFont systemFontOfSize:10];
        _sbpWarnButton.backgroundColor = [UIColor clearColor];
        [_sbpWarnButton setTitle:@"正常" forState:UIControlStateNormal];
        [_sbpWarnButton setImage:[UIImage imageNamed:@"nowarn_button"] forState:UIControlStateNormal];
        [_sbpWarnButton setTitleColor:[UIColor colorWithHexString:@"#84DCEA"] forState:UIControlStateNormal];
        [_sbpWarnButton setTitle:@"异常" forState:UIControlStateSelected];
        [_sbpWarnButton setImage:[UIImage imageNamed:@"warn_button"] forState:UIControlStateSelected];
        [_sbpWarnButton setTitleColor:[UIColor colorWithHexString:@"F5907B"] forState:UIControlStateSelected];
        [_sbpWarnButton sizeToFit];
        [self.contentView addSubview:_sbpWarnButton];
        
        _dbpBarImageView = [UIImageView new];
        _dbpBarImageView.image = [UIImage imageNamed:@"health_bp_line"];
        _dbpBarImageView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_dbpBarImageView];
        

        _dbpWarnButton = [UIButton new];
        _dbpWarnButton.titleLabel.font = [UIFont systemFontOfSize:10];
        _dbpWarnButton.backgroundColor = [UIColor clearColor];
        [_dbpWarnButton setTitle:@"正常" forState:UIControlStateNormal];
        [_dbpWarnButton setImage:[UIImage imageNamed:@"nowarn_button"] forState:UIControlStateNormal];
        [_dbpWarnButton setTitleColor:[UIColor colorWithHexString:@"#84DCEA"] forState:UIControlStateNormal];
        [_dbpWarnButton setTitle:@"异常" forState:UIControlStateSelected];
        [_dbpWarnButton setImage:[UIImage imageNamed:@"warn_button"] forState:UIControlStateSelected];
        [_dbpWarnButton setTitleColor:[UIColor colorWithHexString:@"F5907B"] forState:UIControlStateSelected];
        [_dbpWarnButton sizeToFit];
        [self.contentView addSubview:_dbpWarnButton];
        
        _line = [UIView new];
        _line.backgroundColor = kTableSeparatorLineColor;
        [self.contentView addSubview:_line];
        
    }
    
    return self;
}

+ (BOOL)requiresConstraintBasedLayout
{
    return YES;
}

- (void)updateConstraints
{
    [_iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(13);
        make.width.mas_equalTo(13);
        make.height.mas_equalTo(17);
    }];
    
    [_titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.iconImageView.mas_centerY);
        make.left.mas_equalTo(self.iconImageView.mas_right).with.offset(13);
    }];
    
    [_subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(_titleLbl.mas_centerY);
        make.left.mas_equalTo(_titleLbl.mas_right).with.offset(13);
    }];
    
    [_dateLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLbl.mas_bottom).with.offset(13);
        make.left.mas_equalTo(self.titleLbl.mas_left);
    }];
    
    [_sbpBarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.dateLbl.mas_bottom).with.offset(30);
        make.left.mas_equalTo(self.contentView).with.offset(46);
        make.right.mas_equalTo(self.contentView).with.offset(-46);
    }];
    
    
    [_sbpWarnButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.sbpBarImageView.mas_top);
    }];
    
    [_dbpBarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.sbpBarImageView.mas_bottom).with.offset(30);
        make.left.mas_equalTo(self.sbpBarImageView.mas_left);
        make.right.mas_equalTo(self.contentView).with.offset(-46);
    }];
    
   
    [_dbpWarnButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.dbpBarImageView.mas_top);
    }];
    
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.dbpBarImageView.mas_bottom).with.offset(13);
        make.bottom.mas_equalTo(self.contentView).priority(MASLayoutPriorityDefaultLow);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(kScreenWidth);
    }];
    
    [super updateConstraints];
}

- (void)setSbpValue:(double)sbpValue {
    
    _sbpValue = sbpValue;
    
    double barLength = kScreenWidth - 46*2;
    double left_pad = sbpValue/300 * barLength - 10;  // 高压最大值300
    
    [_sbpWarnButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_sbpBarImageView).with.offset(left_pad);
    }];
    
    BOOL isNormal = [ZJIsNormalNormTool zj_isNormalSbp:_sbpValue];
    if (isNormal) {
        _sbpWarnButton.selected = NO;
    } else {
        _sbpWarnButton.selected = YES;
    }

}

- (void)setDbpValue:(double)dbpValue {
    
    _dbpValue = dbpValue;
    
    _subTitleLabel.text = [NSString stringWithFormat:@"%.0f/%.0f mmHg", _sbpValue, _dbpValue];  // 设置子标题文字
    
    double barLength = kScreenWidth - 46*2;
    double left_pad = dbpValue/300 * barLength - 10;  // 高压最大值300
    
    [_dbpWarnButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_dbpBarImageView).with.offset(left_pad);
    }];
    
    BOOL isNormal = [ZJIsNormalNormTool zj_isNormalDbp:_dbpValue];
    if (isNormal) {
        _dbpWarnButton.selected = NO;
    } else {
        _dbpWarnButton.selected = YES;
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
