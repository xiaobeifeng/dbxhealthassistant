//
//  DBXBpBsHbHistoryModel.h
//  DBXHealthAssistant
//
//  Created by zhoujian on 2018/8/31.
//  Copyright © 2018年 mc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBXBpBsHbHistoryModelData :NSObject
@property (nonatomic , copy) NSString              * upsaveTime;
@property (nonatomic , copy) NSString              * Id;
@property (nonatomic , copy) NSString              * pulse;
@property (nonatomic , copy) NSString              * sPressure;
@property (nonatomic , copy) NSString              * dPressure;
@property (nonatomic , copy) NSString              * oxygen;

@end

@interface DBXBpBsHbHistoryModel :NSObject
@property (nonatomic , copy) NSString              * message;
@property (nonatomic , copy) NSArray<DBXBpBsHbHistoryModelData *>              * data;
@property (nonatomic , copy) NSString              * state;

@end
