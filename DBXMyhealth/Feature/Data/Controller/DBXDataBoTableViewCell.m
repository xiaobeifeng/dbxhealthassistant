//
//  DBXDataBoTableViewCell.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/4.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXDataBoTableViewCell.h"

@interface DBXDataBoTableViewCell()


@property(nonatomic, strong) UIView *line;
@property(nonatomic, assign) CGFloat padding;


@end

@implementation DBXDataBoTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _iconImageView = [UIImageView new];
        _iconImageView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_iconImageView];
        
        _titleLbl = [UILabel new];
        _titleLbl.textColor = [UIColor colorWithRed:0.33 green:0.79 blue:0.88 alpha:1.00];
        _titleLbl.font = kFont(14);
        [_titleLbl sizeToFit];
        [self.contentView addSubview:_titleLbl];
        
        _subTitleLbl = [UILabel new];
        _subTitleLbl.textColor = [UIColor colorWithRed:0.33 green:0.79 blue:0.88 alpha:1.00];
        _subTitleLbl.font = kFont(14);
        [_subTitleLbl sizeToFit];
        [self.contentView addSubview:_subTitleLbl];
        
        _dateLbl = [UILabel new];
        _dateLbl.textColor = [UIColor lightGrayColor];
        _dateLbl.font = kFont(12);
        _dateLbl.text = @"asdadasd";
        [_dateLbl sizeToFit];
        [self.contentView addSubview:_dateLbl];
        
        _barImageView = [UIImageView new];
        _barImageView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_barImageView];
        
        _warnButton = [[UIButton alloc] init];
        _warnButton.titleLabel.font = [UIFont systemFontOfSize:10];
        _warnButton.backgroundColor = [UIColor clearColor];
        [_warnButton setTitle:@"正常" forState:UIControlStateNormal];
        [_warnButton setImage:[UIImage imageNamed:@"nowarn_button"] forState:UIControlStateNormal];
        [_warnButton setTitleColor:[UIColor colorWithHexString:@"#84DCEA"] forState:UIControlStateNormal];
        [_warnButton setTitle:@"异常" forState:UIControlStateSelected];
        [_warnButton setImage:[UIImage imageNamed:@"warn_button"] forState:UIControlStateSelected];
        [_warnButton setTitleColor:[UIColor colorWithHexString:@"F5907B"] forState:UIControlStateSelected];
        [_warnButton sizeToFit];
        [self.contentView addSubview:_warnButton];
        
        _line = [UIView new];
        _line.backgroundColor = kTableSeparatorLineColor;
        [self.contentView addSubview:_line];
    }
    
    return self;
}

+ (BOOL)requiresConstraintBasedLayout
{
    return YES;
}

- (void)updateConstraints
{
    [_iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(13);
        make.width.mas_equalTo(16);
        make.height.mas_equalTo(18);
    }];
    
    [_titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.iconImageView.mas_centerY);
        make.left.mas_equalTo(self.iconImageView.mas_right).with.offset(13);
    }];
    
    [_subTitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(_titleLbl.mas_centerY);
        make.left.mas_equalTo(_titleLbl.mas_right).with.offset(13);
    }];
    
    [_dateLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLbl.mas_bottom).with.offset(13);
        make.left.mas_equalTo(self.titleLbl.mas_left);
    }];
    
    [_barImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.dateLbl.mas_bottom).with.offset(30);
        make.left.mas_equalTo(self.dateLbl.mas_left);
        make.right.mas_equalTo(self.contentView).with.offset(-46);
    }];
    
    [_warnButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.barImageView.mas_top);
    }];
    
    
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.barImageView.mas_bottom).with.offset(13);
        make.bottom.mas_equalTo(self.contentView).priority(MASLayoutPriorityDefaultLow);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(kScreenWidth);
    }];
    
    [super updateConstraints];
}

- (void)setBsCondition:(NSInteger)bsCondition {
    _bsCondition = bsCondition;
}

- (void)setBsValue:(double)bsValue {
    
    double barLength = kScreenWidth - 46*2;
    double left_pad = bsValue/33 * barLength - 10;  // 血糖最大值33
    
    [_warnButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_barImageView).with.offset(left_pad);
    }];
    
    BOOL isNormal = [ZJIsNormalNormTool zj_isNormalBs:_bsCondition bs:bsValue];
    if (isNormal) {
        _warnButton.selected = NO;
    } else {
        _warnButton.selected = YES;
    }

}

- (void)setBoValue:(double)boValue {
    
    double barLength = kScreenWidth - 46*2;
    double left_pad = boValue/100 * barLength - 10;  // 血氧最大值100
    
    [_warnButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_barImageView).with.offset(left_pad);
    }];
    
    BOOL isNormal = [ZJIsNormalNormTool zj_isNormalBo:boValue];
    if (isNormal) {
        _warnButton.selected = NO;
    } else {
        _warnButton.selected = YES;
    }
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
