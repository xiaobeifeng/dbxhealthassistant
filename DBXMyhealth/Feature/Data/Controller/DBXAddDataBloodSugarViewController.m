//
//  DBXAddDataBloodSugarViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/12.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXAddDataBloodSugarViewController.h"
#import "SCChart.h"
#import "DBXAddDataInputView.h"
#import <BRPickerView/BRPickerView.h>
#import "DBXDataBloodSugarApi.h"
#import "DBXBloodSugarModel.h"
#import "DBXBloodSugarUploadApi.h"
#import "ZJAllBloodSugarApi.h"

@interface DBXAddDataBloodSugarViewController ()<UITextFieldDelegate>

@property(nonatomic, strong) SCCircleChart *bloodSugarChartView;
@property(nonatomic, strong) DBXAddDataInputView *timeInputView;
@property(nonatomic, strong) DBXAddDataInputView *bloodSugarInputView;
@property(nonatomic, copy) NSArray *timeArray;

@end

@implementation DBXAddDataBloodSugarViewController

- (NSArray *)timeArray
{
    if (!_timeArray) {
        _timeArray = @[@"空腹", @"早餐前", @"早餐后", @"午餐前", @"午餐后", @"晚餐前", @"晚餐后", @"睡前"];
    }
    return _timeArray;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)loadView
{
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, kSafeAreaTopHeight, kScreenWidth, kScreenHeight-kSafeAreaTopHeight)];
    scrollView.contentSize = CGSizeMake(kScreenWidth, kScreenHeight-kSafeAreaTopHeight);
    self.view = scrollView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"添加数据";
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIView *bpView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 150)];
    bpView.backgroundColor = kTableSeparatorLineColor;
    [self.view addSubview:bpView];
    
    _bloodSugarChartView = [[SCCircleChart alloc] initWithFrame:CGRectMake((kScreenWidth-80)/2, (150-80)/2, 80, 80.0)
                                                      total:@33.3
                                                    current:@0
                                                  clockwise:YES
                                                     shadow:YES
                                                shadowColor:[UIColor whiteColor]];
    [_bloodSugarChartView setStrokeColor:SCBlue];
    _bloodSugarChartView.chartType = SCChartFormatTypeNone;
    _bloodSugarChartView.format = @"血糖\n%.1f mmol/L";
    [_bloodSugarChartView strokeChart];
    [bpView addSubview:_bloodSugarChartView];
    
    _timeInputView = [[DBXAddDataInputView alloc] initWithTitle:@"测量时间：" unit:@""];
    _timeInputView.textField.delegate = self;
    _timeInputView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_timeInputView];
    [_timeInputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).with.offset(200);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(40);
    }];
    
    UIButton *timeBtn = [UIButton new];
    [timeBtn addTarget:self action:@selector(handleTimeBtnEvent) forControlEvents:UIControlEventTouchUpInside];
    [_timeInputView addSubview:timeBtn];
    [timeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.timeInputView.mas_centerX);
        make.centerY.mas_equalTo(self.timeInputView.mas_centerY);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(40);
    }];
    
    _bloodSugarInputView = [[DBXAddDataInputView alloc] initWithTitle:@"血糖：" unit:@"mmol/L"];
    _bloodSugarInputView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_bloodSugarInputView];
    [_bloodSugarInputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.timeInputView.mas_bottom).with.offset(20);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(40);
    }];
    
    UIButton *saveBtn = [UIButton new];
    saveBtn.backgroundColor = kNavigationBarColor;
    [saveBtn setTitle:@"保 存" forState:UIControlStateNormal];
    [saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    kViewRadius(saveBtn, 5.0f);
    saveBtn.titleLabel.font = kFont(14);
    [saveBtn addTarget:self action:@selector(handleSaveBtnEvent) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:saveBtn];
    [saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bloodSugarInputView.mas_bottom).with.offset(40);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(40);
        make.centerX.mas_equalTo(self.view.mas_centerX);
    }];
    
    [self startRequestApi];

}

/**
 保存
 */
- (void)handleSaveBtnEvent
{
    NSString *time = _timeInputView.textField.text;
    NSString *bloodSugar = _bloodSugarInputView.textField.text;
    
    float bloodSugarFloat = [[NSString stringWithFormat:@"%.1f", [bloodSugar floatValue]] floatValue];
    
    // 时间段校验
    if (kStringIsEmpty(time)) {
        [MBProgressHUD showInfoMessage:@"请选择测量时间段"];
        return;
    }
    
    // 血糖校验
    if (!kStringIsEmpty(bloodSugar)) {
        if ([bloodSugar integerValue] < 0 || [bloodSugar integerValue] > 33.3) {
            [MBProgressHUD showInfoMessage:@"请填写正确的血糖数值"];
            return;
        }
    } else {
        [MBProgressHUD showInfoMessage:@"请填写血糖数值"];
        return;
    }
    
    
    [_bloodSugarChartView updateChartByCurrent:[NSNumber numberWithFloat:bloodSugarFloat]];
    
    NSInteger mCondition = [self.timeArray indexOfObject:time];
    DBXBloodSugarUploadApi *api = [[DBXBloodSugarUploadApi alloc] initWithBloodGluVal:bloodSugarFloat mCondition:mCondition];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        [MBProgressHUD hideHUD];
        [MBProgressHUD showTipMessageInView:@"保存成功"];
        
        self.timeInputView.textField.text = @"";
        self.bloodSugarInputView.textField.text = @"";
        
        // 发送通知
        [[NSNotificationCenter defaultCenter] postNotificationName:kReloadDataApiAndDataDetailApi object:nil userInfo:nil];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReloadDataApiAndDataDetailApi object:nil];
}

- (void)startRequestApi
{
    ZJAllBloodSugarApi *api = [[ZJAllBloodSugarApi alloc] initWithCount:1 doa:0];
    
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        DBXBloodSugarModel *model = [DBXBloodSugarModel yy_modelWithJSON:request.responseJSONObject];
        
        NSArray *array = [NSArray yy_modelArrayWithClass:[DBXBloodSugarModelData class] json:model.data];
        
        if (!kArrayIsEmpty(array)) {
            
            DBXBloodSugarModelData *modelData = array.firstObject;
            [self.bloodSugarChartView updateChartByCurrent:[NSNumber numberWithFloat:[modelData.bloodGluVal floatValue]]];

        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

- (void)handleTimeBtnEvent
{
    [BRStringPickerView showStringPickerWithTitle:@"请选择测量时间段" dataSource:self.timeArray defaultSelValue:self.timeArray[0] isAutoSelect:NO themeColor:nil resultBlock:^(id selectValue) {
        
        self.timeInputView.textField.text = selectValue;
        
    } cancelBlock:^{
        
        NSLog(@"点击了背景视图或取消按钮");
        
    }];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == _timeInputView.textField) {
        return NO;
    } else {
        return YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
