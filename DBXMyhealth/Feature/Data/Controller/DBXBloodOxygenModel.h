//
//  DBXBloodOxygenModel.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/13.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <Foundation/Foundation.h>


/** 血氧数据模型 */
@interface DBXBloodOxygenModelData :NSObject
//@property (nonatomic , copy) NSString              * id;
@property (nonatomic , copy) NSString              * oxygen;
@property (nonatomic , copy) NSString              * upsaveTime;

@end

@interface DBXBloodOxygenModel :NSObject
@property (nonatomic , copy) NSString              * message;
@property (nonatomic , copy) NSArray<DBXBloodOxygenModelData *>              * data;
@property (nonatomic , copy) NSString              * state;

@end
