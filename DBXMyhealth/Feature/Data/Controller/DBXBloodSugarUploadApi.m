//
//  DBXBloodSugarUploadApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/12.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXBloodSugarUploadApi.h"

@interface DBXBloodSugarUploadApi()
{
    float _bloodGluVal;
    NSInteger _mCondition;
}

@end

@implementation DBXBloodSugarUploadApi

- (id)initWithBloodGluVal:(float)bloodGluVal
              mCondition:(NSInteger)mCondition
{
    
    self = [super init];
    if (self) {
        _bloodGluVal = bloodGluVal;
        _mCondition = mCondition;
    }
    return self;
}

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return @"/AppService/lohas/health/glusave";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

// 请求体
- (id)requestArgument {
    return @{
             @"bloodGluVal": @(_bloodGluVal),
             @"mCondition": @(_mCondition),
             };
}

@end
