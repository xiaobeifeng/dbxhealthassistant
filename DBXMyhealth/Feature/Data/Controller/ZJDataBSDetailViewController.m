//
//  ZJDataBSDetailViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/12.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "ZJDataBSDetailViewController.h"
#import "DBXDataHbBoBsDetailTableViewCell.h"
#import "SCChart.h"
#import "DBXDataBloodSugarApi.h"
#import "DBXBloodSugarModel.h"
#import "ZJDropSelectView.h"
#import "DBXAddDataBloodSugarViewController.h"
#import "DBXBloodSugarHistoryViewController.h"

@interface ZJDataBSDetailViewController ()<SCChartDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, ZJDropSelectViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, copy) NSArray *dataArray;
@property(nonatomic, strong) SCChart *chartView;
@property (nonatomic, strong) ZJDropSelectView *dropSelectView;
@property (nonatomic, strong) UIView *dropBackgroundView;   // 下拉视图的背景图
@property (nonatomic, strong) UIButton *navButton;  // 导航栏按钮
@property (nonatomic, assign) NSInteger mCondition; // 当前选择的时间段
@property (nonatomic, strong) FFDropDownMenuView *dropDownMenu; // 右上角下拉菜单
@end

@implementation ZJDataBSDetailViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self startRequsetBloodSugarApi:_mCondition];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNav];
    
    [self setupUI];
    
    [self setupDropDownMenu];

    _mCondition = 0;
}


/**
 设置导航栏
 */
- (void)setupNav {
    
    _navButton = [UIButton new];
    _navButton.frame = CGRectMake(0, 0, 200, 40);
    [_navButton setTitle:@"血糖-空腹" forState:UIControlStateNormal];
    [_navButton setImage:[UIImage imageNamed:@"nav_drop"] forState:UIControlStateNormal];
    [_navButton layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleRight imageTitleSpace:5];
    [_navButton addTarget:self action:@selector(navigationButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = _navButton;
    
    // 右侧导航栏按钮
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithimage:[UIImage imageNamed:@"nav_rightBar_more"] selImage:[UIImage imageNamed:@"nav_rightBar_more"] target:self action:@selector(rightBarButtonClick)];
}

- (void)rightBarButtonClick {
    [_dropBackgroundView removeFromSuperview];
    [_dropDownMenu showMenu];
}

/**
 导航栏按钮点击
 */
- (void)navigationButtonClick:(UIButton *)button {
    
    if (!button.selected) {
        button.selected = YES;
        _dropBackgroundView = [UIView new];
        _dropBackgroundView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
        _dropBackgroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
        [self.view addSubview:_dropBackgroundView];
        [_dropBackgroundView.superview bringSubviewToFront:_dropBackgroundView];
        
        _dropSelectView = [[ZJDropSelectView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 320)];
        _dropSelectView.backgroundColor = [UIColor lightGrayColor];
        _dropBackgroundView.userInteractionEnabled = YES;
        _dropSelectView.delegate = self;
        [_dropBackgroundView addSubview:_dropSelectView]; //这是是自己要做的事情一般是添加一个View 这个需要自己写
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeDropBackgroundView)];
        tap.delegate = self;
        [_dropBackgroundView addGestureRecognizer:tap];
        
    } else {
        button.selected = NO;
        [_dropBackgroundView removeFromSuperview];
    }
 
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
   
    CGPoint p = [touch locationInView:_dropSelectView];

    if(CGRectContainsPoint (_dropSelectView.frame, p)){
        return NO;
    }
    return YES;

}

/**
 移除下拉视图的背景图
 */
- (void)removeDropBackgroundView {
    [_dropBackgroundView removeFromSuperview];
}

/**
 搭建视图
 */
- (void)setupUI {
    
    self.tableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
    self.tableView.backgroundColor = kTableSeparatorLineColor;
    [self.tableView registerClass:[DBXDataHbBoBsDetailTableViewCell class] forCellReuseIdentifier:NSStringFromClass([DBXDataHbBoBsDetailTableViewCell class])];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRefresh)];
    
    _chartView = [[SCChart alloc] initwithSCChartDataFrame:CGRectMake(0, 0, kScreenWidth, 150)
                                                withSource:self
                                                 withStyle:SCChartLineStyle];
    _chartView.backgroundColor = [UIColor whiteColor];
    self.tableView.tableHeaderView = _chartView;
    _chartView.hidden = YES;
    [self.view addSubview:self.tableView];
}

/**
 头部视图刷新
 */
- (void)headerRefresh {
    [self startRequsetBloodSugarApi:_mCondition];
}


/**
 右上角下拉视图
 */
- (void)setupDropDownMenu
{
    NSArray *modelsArray = [self getMenuModelsArray];
    
    self.dropDownMenu = [FFDropDownMenuView ff_DefaultStyleDropDownMenuWithMenuModelsArray:modelsArray menuWidth:FFDefaultFloat eachItemHeight:FFDefaultFloat menuRightMargin:FFDefaultFloat triangleRightMargin:FFDefaultFloat];
    
    self.dropDownMenu.ifShouldScroll = NO;
    
    self.dropDownMenu.iconSize = CGSizeMake(0, 0);
    self.dropDownMenu.menuWidth = 120;
    
    [self.dropDownMenu setup];
}

/** 获取菜单模型数组 */
- (NSArray *)getMenuModelsArray
{
    __weak typeof(self) weakSelf = self;
    
    FFDropDownMenuModel *menuModel0 = [FFDropDownMenuModel ff_DropDownMenuModelWithMenuItemTitle:@"添加" menuItemIconName:@""  menuBlock:^{
        
            DBXAddDataBloodSugarViewController *addDataBloodSugarVC = [DBXAddDataBloodSugarViewController new];
            [weakSelf.navigationController pushViewController:addDataBloodSugarVC animated:YES];
        
    }];
    
    FFDropDownMenuModel *menuModel1 = [FFDropDownMenuModel ff_DropDownMenuModelWithMenuItemTitle:@"查询历史" menuItemIconName:@"" menuBlock:^{
   
            DBXBloodSugarHistoryViewController *bloodSugarHistoryVC = [DBXBloodSugarHistoryViewController new];
            [weakSelf.navigationController pushViewController:bloodSugarHistoryVC animated:YES];

    }];
    
    NSArray *menuModelArr = @[menuModel0, menuModel1];
    return menuModelArr;
}


/**
 请求血糖的最新10条数据
 
 @param mCondtition  血糖上传类型
 */
- (void)startRequsetBloodSugarApi:(NSInteger)mCondtition {
    _mCondition = mCondtition;
    DBXDataBloodSugarApi *bloodSugarApi = [[DBXDataBloodSugarApi alloc] initWithCount:10 doa:0 mCondition:mCondtition];
    [bloodSugarApi startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSLog(@"%@", request.requestUrl);
        NSLog(@"%@", request.responseJSONObject);
        DBXBloodSugarModel *model = [DBXBloodSugarModel yy_modelWithJSON:request.responseJSONObject];
        
        if (!kArrayIsEmpty(model.data)) {
            
            NSArray *array = [NSArray yy_modelArrayWithClass:[DBXBloodSugarModelData class] json:model.data];
            self.dataArray = [NSArray arrayWithArray:array];
            [self.tableView reloadData];
            [self.tableView.mj_header endRefreshing];
            [self.chartView strokeChart];
            self.chartView.hidden = NO;
        } else {
            self.dataArray = [NSMutableArray new];
            [self.tableView reloadData];
            [self.tableView.mj_header endRefreshing];
            [self.chartView strokeChart];
            self.chartView.hidden = YES;
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView reloadData];
        [MBProgressHUD showTipMessageInView:@"请稍后再试"];
    }];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DBXDataHbBoBsDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DBXDataHbBoBsDetailTableViewCell class]) forIndexPath:indexPath];
    
    [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)setupModelOfCell:(DBXDataHbBoBsDetailTableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DBXBloodSugarModelData *model = self.dataArray[indexPath.row];
    cell.bloodSugarValue = model.bloodGluVal;
    cell.date = model.upsaveTime;
    
    BOOL isNormal = [ZJIsNormalNormTool zj_isNormalBs:[model.mCondition integerValue] bs:[model.bloodGluVal doubleValue]];
    if (isNormal) {
        cell.isBloodSugarWarn = NO;
    } else {
        cell.isBloodSugarWarn = YES;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [tableView fd_heightForCellWithIdentifier:NSStringFromClass([DBXDataHbBoBsDetailTableViewCell class]) cacheByIndexPath:indexPath configuration:^(id cell) {
        [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    }];
}

#pragma mark - SCChartDataSource
// 横坐标标题数组
- (NSArray *)SCChart_xLableArray:(SCChart *)chart
{
    return @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10"];
}

//数值多重数组
- (NSArray *)SCChart_yValueArray:(SCChart *)chart
{
    
    NSMutableArray *bsValues = [NSMutableArray new];
    for (DBXBloodSugarModelData *model in self.dataArray) {
        NSInteger bsValue = [model.bloodGluVal integerValue];
        [bsValues addObject:@(bsValue)];
    }
    return @[bsValues];
    
}

// 颜色数组
- (NSArray *)SCChart_ColorArray:(SCChart *)chart
{
    return @[SCBlue];
}

// 判断显示横线条
- (BOOL)SCChart:(SCChart *)chart ShowHorizonLineAtIndex:(NSInteger)index {
    return YES;
}

//判断显示最大最小值
- (BOOL)SCChart:(SCChart *)chart ShowMaxMinAtIndex:(NSInteger)index
{
    return NO;
}


# pragma mark - emptyDataSet
/**
 *  返回占位图图片
 */
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIImage imageNamed:@"report_notFound"];
}
/**
 *  返回标题文字
 */
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"暂无数据";
    UIFont *font = [UIFont systemFontOfSize:14.0];
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:text];
    [attStr addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, text.length)];
    return attStr;
}
/**
 *  处理空白区域的点击事件
 */
- (void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view {
    NSLog(@"%s",__FUNCTION__);
}
- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    return -70.0f;
}
- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 20;
}

# pragma mark ZJDropSelectViewDelegate
- (void)selectIndex:(NSInteger)index title:(NSString *)title {
    NSLog(@"%ld", (long)index);
    [_navButton setTitle:[NSString stringWithFormat:@"血糖-%@", title] forState:UIControlStateNormal];
    [_navButton layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleRight imageTitleSpace:5];
    [self startRequsetBloodSugarApi:index];
    [_dropBackgroundView removeFromSuperview];
}


@end
