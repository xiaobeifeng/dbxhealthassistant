//
//  DBXAddDataBpHbBoViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/5.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXAddDataBpHbBoViewController.h"
#import "DBXDataBpCircleViewController.h"
#import "XMGTitleButton.h"
#import "SCChart.h"
#import "DBXDataBpApi.h"
#import "DBXAddDataInputView.h"
#import "DBXBpHbBoUploadApi.h"
#import "DBXDataBpApi.h"
#import "DBXDataHeartbeatApi.h"
#import "DBXDataOxygenApi.h"
#import "DBXHeartBeatModel.h"
#import "DBXBloodPressModel.h"
#import "DBXBloodOxygenModel.h"

@interface DBXAddDataBpHbBoViewController ()<UIScrollViewDelegate>
/** 用来存放所有子控制器view的scrollView */
@property (nonatomic, weak) UIScrollView *scrollView;
/** 标题栏 */
@property (nonatomic, weak) UIView *titlesView;
/** 标题下划线 */
@property (nonatomic, weak) UIView *titleUnderline;
/** 上一次点击的标题按钮 */
@property (nonatomic, weak) XMGTitleButton *previousClickedTitleButton;

@property(nonatomic, strong) SCCircleChart *sPressChartView;
@property(nonatomic, strong) SCCircleChart *dPressChartView;
@property(nonatomic, strong) SCCircleChart *hbChartView;
@property(nonatomic, strong) SCCircleChart *boChartView;

@property(nonatomic, strong) DBXAddDataInputView *sysInputView;
@property(nonatomic, strong) DBXAddDataInputView *dbpInputView;
@property(nonatomic, strong) DBXAddDataInputView *heartBeatInputView;
@property(nonatomic, strong) DBXAddDataInputView *bloodOxygenInputView;

@end

@implementation DBXAddDataBpHbBoViewController

- (void)loadView
{
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, kSafeAreaTopHeight, kScreenWidth, kScreenHeight-kSafeAreaTopHeight)];
    scrollView.contentSize = CGSizeMake(kScreenWidth, kScreenHeight-kSafeAreaTopHeight);
    self.view = scrollView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"添加数据";

    // scrollView
    [self setupScrollView];
    
    // 标题栏
    [self setupTitlesView];
    
    [self startRequestHbBoBpApi];
    
    
    _sysInputView = [[DBXAddDataInputView alloc] initWithTitle:@"收缩压：" unit:@"mmHg"];
    _sysInputView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_sysInputView];
    [_sysInputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).with.offset(200);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(40);
    }];

    _dbpInputView = [[DBXAddDataInputView alloc] initWithTitle:@"舒张压：" unit:@"mmHg"];
    _dbpInputView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_dbpInputView];
    [_dbpInputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.sysInputView.mas_bottom).with.offset(20);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(40);
    }];
    
    _heartBeatInputView = [[DBXAddDataInputView alloc] initWithTitle:@"心率：" unit:@"bpm"];
    _heartBeatInputView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_heartBeatInputView];
    [_heartBeatInputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.dbpInputView.mas_bottom).with.offset(20);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(40);
    }];
    
    _bloodOxygenInputView = [[DBXAddDataInputView alloc] initWithTitle:@"血氧：" unit:@"%"];
    _bloodOxygenInputView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_bloodOxygenInputView];
    [_bloodOxygenInputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.heartBeatInputView.mas_bottom).with.offset(20);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(40);
    }];
    
    UIButton *saveBtn = [UIButton new];
    saveBtn.backgroundColor = kNavigationBarColor;
    [saveBtn setTitle:@"保 存" forState:UIControlStateNormal];
    [saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    kViewRadius(saveBtn, 5.0f);
    saveBtn.titleLabel.font = kFont(14);
    [saveBtn addTarget:self action:@selector(handleSaveBtnEvent) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:saveBtn];
    [saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bloodOxygenInputView.mas_bottom).with.offset(40);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(40);
        make.centerX.mas_equalTo(self.view.mas_centerX);
    }];

}


/**
 保存填入数据按钮
 */
- (void)handleSaveBtnEvent
{
    NSString *sys = _sysInputView.textField.text;
    NSString *dbp = _dbpInputView.textField.text;
    NSString *heartBeat = _heartBeatInputView.textField.text;
    NSString *bloodOxygen = _bloodOxygenInputView.textField.text;
    
    // 校验收缩压数值
    if (!kStringIsEmpty(sys)) {
        if ([sys integerValue] < 0 || [sys integerValue] > 270) {
            [MBProgressHUD showInfoMessage:@"请输入正确的收缩压数值"];
            return;
        }
    } else {
        [MBProgressHUD showInfoMessage:@"请输入收缩压"];
        return;
    }
    
    // 校验舒张压数值
    if (!kStringIsEmpty(dbp)) {
        if ([dbp integerValue] < 0 || [dbp integerValue] > 270) {
            [MBProgressHUD showInfoMessage:@"请输入正确的舒张压数值"];
            return;
        }
    } else {
        [MBProgressHUD showInfoMessage:@"请输入舒张压"];
        return;
    }
    
    // 校验心率数值
    if (!kStringIsEmpty(heartBeat)) {
        if ([heartBeat integerValue] < 0 || [heartBeat integerValue] > 100) {
            [MBProgressHUD showInfoMessage:@"请输入正确的心率数值"];
            return;
        }
    } else {
        [MBProgressHUD showInfoMessage:@"请输入心率"];
        return;
    }
    
    // 校验血氧数值
    if (!kStringIsEmpty(bloodOxygen)) {
        if ([bloodOxygen integerValue] < 0 || [bloodOxygen integerValue] > 100) {
            [MBProgressHUD showInfoMessage:@"请输入正确的血氧数值"];
            return;
        }
    } else {
        [MBProgressHUD showInfoMessage:@"请输入血氧"];
        return;
    }
    
    // 更新图表数值
    [self.sPressChartView updateChartByCurrent:[NSNumber numberWithInteger:[sys integerValue]]];
    [self.dPressChartView updateChartByCurrent:[NSNumber numberWithInteger:[dbp integerValue]]];
    [self.hbChartView updateChartByCurrent:[NSNumber numberWithInteger:[heartBeat integerValue]]];
    [self.boChartView updateChartByCurrent:[NSNumber numberWithInteger:[bloodOxygen integerValue]]];
    
    [MBProgressHUD showActivityMessageInView:@"同步中"];
    
    // 上传数值
    DBXBpHbBoUploadApi *api = [[DBXBpHbBoUploadApi alloc] initWithSPress:[sys intValue] dPress:[dbp intValue] heartBeat:[heartBeat intValue] bloodOxygen:[bloodOxygen intValue]];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        [MBProgressHUD hideHUD];
        [MBProgressHUD showTipMessageInView:@"保存成功"];
        
        self.sysInputView.textField.text = @"";
        self.dbpInputView.textField.text = @"";
        self.heartBeatInputView.textField.text = @"";
        self.bloodOxygenInputView.textField.text = @"";
        
        //发送通知
        [[NSNotificationCenter defaultCenter] postNotificationName:kReloadDataApiAndDataDetailApi object:nil userInfo:nil];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
    
}

- (void)dealloc
{
   [[NSNotificationCenter defaultCenter] removeObserver:self name:kReloadDataApiAndDataDetailApi object:nil];
}

/**
 *  scrollView
 */
- (void)setupScrollView
{

    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.backgroundColor = [UIColor blueColor];
    scrollView.frame = CGRectMake(0, 35, kScreenWidth, 150);
    scrollView.delegate = self;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.pagingEnabled = YES;
    [self.view addSubview:scrollView];
    self.scrollView = scrollView;
    
    // 添加子控制器的view
    NSUInteger count = self.childViewControllers.count;
    CGFloat scrollViewW = scrollView.xmg_width;
    CGFloat scrollViewH = scrollView.xmg_height;
    
    UIView *bpView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, scrollViewW, scrollViewH)];
    bpView.backgroundColor = kTableSeparatorLineColor;
    [scrollView addSubview:bpView];
    
    _sPressChartView = [[SCCircleChart alloc] initWithFrame:CGRectMake(50, (150-80)/2, 80, 80.0)
                                                      total:@270
                                                    current:@0
                                                  clockwise:YES
                                                     shadow:YES
                                                shadowColor:[UIColor whiteColor]];
    [_sPressChartView setStrokeColor:SCBlue];
    _sPressChartView.chartType = SCChartFormatTypeNone;
    _sPressChartView.format = @"收缩压\n%d mmHg";
    [_sPressChartView strokeChart];
    [bpView addSubview:_sPressChartView];
    
    _dPressChartView = [[SCCircleChart alloc] initWithFrame:CGRectMake(scrollViewW-80-50, (150-80)/2, 80, 80.0)
                                                      total:@270
                                                    current:@0
                                                  clockwise:YES
                                                     shadow:YES
                                                shadowColor:[UIColor whiteColor]];
    [_sPressChartView setStrokeColor:SCBlue];
    _dPressChartView.chartType = SCChartFormatTypeNone;
    _dPressChartView.format = @"舒张压\n%d mmHg";
    [_dPressChartView strokeChart];
    [bpView addSubview:_dPressChartView];
    
    UIView *hbView = [[UIView alloc] initWithFrame:CGRectMake(scrollViewW, 0, scrollViewW, scrollViewH)];
    hbView.backgroundColor = kTableSeparatorLineColor;
    [scrollView addSubview:hbView];
    
    _hbChartView = [[SCCircleChart alloc] initWithFrame:CGRectMake((scrollViewW-80)/2, (150-80)/2, 80, 80.0)
                                                      total:@270
                                                    current:@0
                                                  clockwise:YES
                                                 shadow:YES
                                            shadowColor:[UIColor whiteColor]];
    [_hbChartView setStrokeColor:SCBlue];
    _hbChartView.chartType = SCChartFormatTypeNone;
    _hbChartView.format = @"心率\n%d bpm";
    [_hbChartView strokeChart];
    [hbView addSubview:_hbChartView];
    
    UIView *boView = [[UIView alloc] initWithFrame:CGRectMake(scrollViewW*2, 0, scrollViewW, scrollViewH)];
    boView.backgroundColor = kTableSeparatorLineColor;
    [scrollView addSubview:boView];
    
    _boChartView = [[SCCircleChart alloc] initWithFrame:CGRectMake((scrollViewW-80)/2, (150-80)/2, 80, 80.0)
                                                  total:@270
                                                current:@0
                                              clockwise:YES
                                                 shadow:YES
                                            shadowColor:[UIColor whiteColor]];
    [_boChartView setStrokeColor:SCBlue];
    _boChartView.chartType = SCChartFormatTypeNone;
    _boChartView.format = @"血氧\n%d%%";
    [_boChartView strokeChart];
    [boView addSubview:_boChartView];
    
    scrollView.contentSize = CGSizeMake(count * scrollViewW, 0);
    
}

/**
 请求获取血氧，心率，血压接口
 */
- (void)startRequestHbBoBpApi
{
    [self startRequsetHeartBeatApi];
    [self startRequsetBloodPressApi];
    [self startRequsetOxygenApi];
}


/**
 请求心率数据
 */
- (void)startRequsetHeartBeatApi
{
    DBXDataHeartbeatApi *api = [[DBXDataHeartbeatApi alloc] initWithCount:1 doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            DBXHeartBeatModel *model = [DBXHeartBeatModel yy_modelWithJSON:request.responseJSONObject];
            NSString *heartbeat;
            if (!kArrayIsEmpty(model.data)) {
            
                NSArray *array = [NSArray yy_modelArrayWithClass:[DBXHeartBeatModelData class] json:model.data];
                
                DBXHeartBeatModelData *modelData = array[0];
                heartbeat = modelData.pulse;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
               
                [self.hbChartView updateChartByCurrent:[NSNumber numberWithInteger:[heartbeat integerValue]]];
            });
            
            
        });
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}


/**
 请求血压数据
 */
- (void)startRequsetBloodPressApi
{
    DBXDataBpApi *api = [[DBXDataBpApi alloc] initWithCount:1 doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSLog(@"%@", request.responseJSONObject);
            
            DBXBloodPressModel *model = [DBXBloodPressModel yy_modelWithJSON:request.responseJSONObject];
            
            NSString *sPress;
            NSString *dPress;
            if (!kArrayIsEmpty(model.data)) {

                NSArray *array = [NSArray yy_modelArrayWithClass:[DBXBloodPressModelData class] json:model.data];
                
                DBXBloodPressModelData *modelData = array[0];
                
                sPress = modelData.sPressure;
                dPress = modelData.dPressure;
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // 更新图表数值
                [self.sPressChartView updateChartByCurrent:[NSNumber numberWithInteger:[sPress integerValue]]];
                [self.dPressChartView updateChartByCurrent:[NSNumber numberWithInteger:[dPress integerValue]]];
            });
            
        });
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

/**
 请求血氧数据
 */
- (void)startRequsetOxygenApi
{
    DBXDataOxygenApi *api = [[DBXDataOxygenApi alloc] initWithCount:1 doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            DBXBloodOxygenModel *model = [DBXBloodOxygenModel yy_modelWithJSON:request.responseJSONObject];
            
            NSString *oxygen;
            if (!kArrayIsEmpty(model.data)) {
                
                
                NSArray *array = [NSArray yy_modelArrayWithClass:[DBXBloodOxygenModelData class] json:model.data];
                
                DBXBloodOxygenModelData *modelData = array[0];
                oxygen = modelData.oxygen;
                
            }
            dispatch_async(dispatch_get_main_queue(), ^{
          
                 [self.boChartView updateChartByCurrent:[NSNumber numberWithInteger:[oxygen integerValue]]];
            });
            
        });
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}



/**
 *  标题栏
 */
- (void)setupTitlesView
{
    UIView *titlesView = [[UIView alloc] init];
    titlesView.backgroundColor = [UIColor whiteColor];
    titlesView.frame = CGRectMake(0, 0, self.view.xmg_width, 35);
    [self.view addSubview:titlesView];
    self.titlesView = titlesView;
    
    // 标题栏按钮
    [self setupTitleButtons];
    
    // 标题下划线
    [self setupTitleUnderline];
}

/**
 *  标题栏按钮
 */
- (void)setupTitleButtons
{
    // 文字
    NSArray *titles = @[@"血压", @"心率", @"血氧"];
    NSUInteger count = titles.count;
    
    // 标题按钮的尺寸
    CGFloat titleButtonW = self.titlesView.xmg_width / count;
    CGFloat titleButtonH = self.titlesView.xmg_height;
    
    // 创建5个标题按钮
    for (NSUInteger i = 0; i < count; i++) {
        XMGTitleButton *titleButton = [[XMGTitleButton alloc] init];
        titleButton.tag = i;
        [titleButton addTarget:self action:@selector(titleButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.titlesView addSubview:titleButton];
        // frame
        titleButton.frame = CGRectMake(i * titleButtonW, 0, titleButtonW, titleButtonH);
        // 文字
        [titleButton setTitle:titles[i] forState:UIControlStateNormal];
    }
}

/**
 *  标题下划线
 */
- (void)setupTitleUnderline
{
    // 标题按钮
    XMGTitleButton *firstTitleButton = self.titlesView.subviews.firstObject;
    
    // 下划线
    UIView *titleUnderline = [[UIView alloc] init];
    titleUnderline.xmg_height = 2;
    titleUnderline.xmg_y = self.titlesView.xmg_height - titleUnderline.xmg_height;
    titleUnderline.backgroundColor = [firstTitleButton titleColorForState:UIControlStateSelected];
    [self.titlesView addSubview:titleUnderline];
    self.titleUnderline = titleUnderline;
    
    // 切换按钮状态
    firstTitleButton.selected = YES;
    self.previousClickedTitleButton = firstTitleButton;
    
    [firstTitleButton.titleLabel sizeToFit]; // 让label根据文字内容计算尺寸
    self.titleUnderline.xmg_width = firstTitleButton.titleLabel.xmg_width + 10;
    self.titleUnderline.xmg_centerX = firstTitleButton.xmg_centerX;
}

#pragma mark - 监听
/**
 *  点击标题按钮
 */
- (void)titleButtonClick:(XMGTitleButton *)titleButton
{
    // 切换按钮状态
    self.previousClickedTitleButton.selected = NO;
    titleButton.selected = YES;
    self.previousClickedTitleButton = titleButton;
    
    [UIView animateWithDuration:0.25 animations:^{
        // 处理下划线
        self.titleUnderline.xmg_width = titleButton.titleLabel.xmg_width + 10;
        self.titleUnderline.xmg_centerX = titleButton.xmg_centerX;

        CGFloat offsetX = self.scrollView.xmg_width * titleButton.tag;
        self.scrollView.contentOffset = CGPointMake(offsetX, self.scrollView.contentOffset.y);
    }];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
