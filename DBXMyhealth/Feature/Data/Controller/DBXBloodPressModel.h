//
//  DBXBloodPressModel.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/13.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBXBloodPressModelData :NSObject
@property (nonatomic , copy) NSString              * upsaveTime;
@property (nonatomic , copy) NSString              * Id;
@property (nonatomic , copy) NSString              * sPressure;
@property (nonatomic , assign) NSString              * dPressure;

@end

@interface DBXBloodPressModel :NSObject
@property (nonatomic , copy) NSString              * message;
@property (nonatomic , copy) NSArray<DBXBloodPressModelData *>              * data;
@property (nonatomic , copy) NSString              * state;

@end
