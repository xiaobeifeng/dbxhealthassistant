//
//  DBXDataBpDetailTableViewCell.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/6.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBXDataBpDetailTableViewCell : UITableViewCell

@property(nonatomic, copy) NSString *sPressValue;
@property(nonatomic, copy) NSString *dPressValue;
@property(nonatomic, copy) NSString *date;
@property(nonatomic, assign) BOOL isPressWarn;

@end
