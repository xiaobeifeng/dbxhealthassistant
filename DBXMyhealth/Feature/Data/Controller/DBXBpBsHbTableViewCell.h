//
//  DBXBpBsHbTableViewCell.h
//  DBXHealthAssistant
//
//  Created by zhoujian on 2018/8/31.
//  Copyright © 2018年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBXBpBsHbTableViewCell : UITableViewCell

@property(nonatomic, copy) NSString *date;
@property(nonatomic, copy) NSString *sysPressure;
@property(nonatomic, copy) NSString *diaPressure;
@property(nonatomic, copy) NSString *heartBeat;
@property(nonatomic, copy) NSString *oxy;
@property(nonatomic, assign) BOOL isBpWarn;

@end
