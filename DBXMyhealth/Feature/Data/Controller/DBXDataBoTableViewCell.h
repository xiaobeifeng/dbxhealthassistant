//
//  DBXDataBoTableViewCell.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/4.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 注：血氧,血糖,心率共用一个cell
 */
@interface DBXDataBoTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *iconImageView;       // 图标
@property (nonatomic, strong) UILabel *titleLbl;                // 主标题
@property (nonatomic, strong) UILabel *subTitleLbl;             // 子标题
@property (nonatomic, strong) UIImageView *barImageView;        // 条形图
@property (nonatomic, strong) UIButton *warnButton;             // 数据异常视图
@property (nonatomic, strong) UILabel *dateLbl;                 // 日期
@property (nonatomic, assign) double boValue;                   // 血氧值
@property (nonatomic, assign) double bsValue;                   // 血糖值
@property (nonatomic, assign) NSInteger bsCondition;               // 血糖测量时间段
@end
