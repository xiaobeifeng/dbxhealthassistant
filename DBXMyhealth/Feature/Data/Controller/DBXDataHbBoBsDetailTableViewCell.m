//
//  DBXDataHbBoBsDetailTableViewCell.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/5.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXDataHbBoBsDetailTableViewCell.h"

@interface DBXDataHbBoBsDetailTableViewCell()

@property(nonatomic, strong) UILabel *dateLbl;
@property(nonatomic, strong) UILabel *valueLbl;
@property(nonatomic, strong) UILabel *warnInfoLbl;
@property(nonatomic, strong) UIView *contantBackgroundView;
@property(nonatomic, strong) UIView *lineView;
@property(nonatomic, strong) UIView *pointView;

@end

@implementation DBXDataHbBoBsDetailTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.contentView.backgroundColor = kTableSeparatorLineColor;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _contantBackgroundView = [UIView new];
        _contantBackgroundView.backgroundColor = [UIColor whiteColor];
        kViewRadius(_contantBackgroundView, 5.0f);
        [self.contentView addSubview:_contantBackgroundView];
        
        _dateLbl = [UILabel new];
        _dateLbl.font = kFont(12);
        _dateLbl.textColor = [UIColor colorWithRed:0.44 green:0.36 blue:0.52 alpha:1.00];
        [self.contentView addSubview:_dateLbl];
        
        _valueLbl = [UILabel new];
        _valueLbl.backgroundColor = [UIColor clearColor];
        _valueLbl.textColor = [UIColor colorWithRed:0.44 green:0.36 blue:0.52 alpha:1.00];
        _valueLbl.textAlignment = NSTextAlignmentCenter;
        [_contantBackgroundView addSubview:_valueLbl];
        
        _warnInfoLbl = [UILabel new];
        _warnInfoLbl.font = kFont(12);
        _warnInfoLbl.textColor = [UIColor colorWithRed:0.44 green:0.36 blue:0.52 alpha:1.00];
        [_contantBackgroundView addSubview:_warnInfoLbl];
        
        _lineView = [UIView new];
        _lineView.backgroundColor = kNavigationBarColor;
        [self.contentView addSubview:_lineView];
        
        _pointView = [UIView new];
        kViewRadius(_pointView, 5);
        _pointView.backgroundColor = [UIColor colorWithHexString:@"0499FC"];
        [self.contentView addSubview:_pointView];
        
    }
    
    return self;
}

+ (BOOL)requiresConstraintBasedLayout
{
    return YES;
}

- (void)updateConstraints
{
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView);
        make.bottom.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView).with.offset(34);
        make.width.mas_equalTo(2);
    }];
    
    [_pointView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(10);
        make.centerX.mas_equalTo(self.lineView.mas_centerX);
        make.centerY.mas_equalTo(self.lineView.mas_centerY);
    }];
    
    [_contantBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(13);
        make.left.mas_equalTo(70);
        make.right.mas_equalTo(-13);
        make.height.mas_equalTo(80);
        make.bottom.mas_equalTo(-13);
    }];
    
    [_dateLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contantBackgroundView);
        make.width.mas_equalTo(self.contantBackgroundView.mas_width).multipliedBy(0.9);
        make.height.mas_equalTo(20);
        make.centerX.mas_equalTo(self.contantBackgroundView.mas_centerX);
    }];
    
    [_valueLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.dateLbl.mas_bottom);
        make.width.mas_equalTo(self.contantBackgroundView.mas_width).multipliedBy(0.9);
        make.height.mas_equalTo(40);
        make.centerX.mas_equalTo(self.contantBackgroundView.mas_centerX);
    }];
    
    [_warnInfoLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.valueLbl.mas_bottom);
        make.width.mas_equalTo(self.contantBackgroundView.mas_width).multipliedBy(0.9);
        make.height.mas_equalTo(20);
        make.centerX.mas_equalTo(self.contantBackgroundView.mas_centerX);
    }];
    
    [super updateConstraints];
}

- (void)setPulseValue:(NSString *)pulseValue
{
    if (kStringIsEmpty(pulseValue)) {
        _valueLbl.text = @"--";
    } else {
        // 设置显示值
        NSMutableAttributedString *mValue = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", pulseValue]];
        [mValue addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:NSMakeRange(0, pulseValue.length)];
    
        NSMutableAttributedString *mUnit = [[NSMutableAttributedString alloc] initWithString:@" bpm"];
        [mUnit addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(0, 4)];
        
        [mValue appendAttributedString:mUnit];
        
        _valueLbl.attributedText = mValue;
    }
    
}



- (void)setIsPulseWarn:(BOOL)isPulseWarn
{
    if (isPulseWarn) {
        _warnInfoLbl.text = @"心率不正常，请咨询专业医师";
        _warnInfoLbl.textColor = [UIColor orangeColor];
        _pointView.backgroundColor = [UIColor colorWithHexString:@"B956D2"];
    } else {
        _warnInfoLbl.text = @"心率正常，请继续保持";
        _warnInfoLbl.textColor = [UIColor colorWithRed:0.44 green:0.36 blue:0.52 alpha:1.00];
        _pointView.backgroundColor = [UIColor colorWithHexString:@"0499FC"];
    }
}

- (void)setOxygenValue:(NSString *)oxygenValue
{
  
    if (kStringIsEmpty(oxygenValue)) {
        _valueLbl.text = @"-";
    } else {
        // 设置显示值
        NSMutableAttributedString *mValue = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", oxygenValue]];
        [mValue addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:NSMakeRange(0, oxygenValue.length)];
        
        NSMutableAttributedString *mUnit = [[NSMutableAttributedString alloc] initWithString:@" %"];
        [mUnit addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(0, 2)];
        
        [mValue appendAttributedString:mUnit];
        
        _valueLbl.attributedText = mValue;
    }
}

- (void)setIsOxygenWarn:(BOOL)isOxygenWarn
{
    if (isOxygenWarn) {
        _warnInfoLbl.text = @"血氧不正常，请咨询专业医师";
        _pointView.backgroundColor = [UIColor colorWithHexString:@"B956D2"];
        _warnInfoLbl.textColor = [UIColor orangeColor];
    } else {
        _warnInfoLbl.text = @"血氧正常，请继续保持";
        _pointView.backgroundColor = [UIColor colorWithHexString:@"0499FC"];
        _warnInfoLbl.textColor = [UIColor colorWithRed:0.44 green:0.36 blue:0.52 alpha:1.00];
    }
}

- (void)setBloodSugarValue:(NSString *)bloodSugarValue
{
    if (kStringIsEmpty(bloodSugarValue)) {
        _valueLbl.text = @"--";
    } else {
        // 设置显示值
        
        // 重新处理后的bloodSugarValue
        NSString *reSetBloodSugar = [NSString stringWithFormat:@"%.1f", [bloodSugarValue floatValue]];
        NSMutableAttributedString *mValue = [[NSMutableAttributedString alloc] initWithString:reSetBloodSugar];
        [mValue addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:NSMakeRange(0, reSetBloodSugar.length)];
        
        NSMutableAttributedString *mUnit = [[NSMutableAttributedString alloc] initWithString:@" mmol/L"];
        [mUnit addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(0, 7)];
        
        [mValue appendAttributedString:mUnit];
        
        _valueLbl.attributedText = mValue;
    }
}

- (void)setIsBloodSugarWarn:(BOOL)isBloodSugarWarn
{
    if (isBloodSugarWarn) {
        _warnInfoLbl.text = @"血糖不正常，请咨询专业医师";
        _pointView.backgroundColor = [UIColor colorWithHexString:@"B956D2"];
        _warnInfoLbl.textColor = [UIColor orangeColor];
    } else {
        _warnInfoLbl.text = @"血糖正常，请继续保持";
        _pointView.backgroundColor = [UIColor colorWithHexString:@"0499FC"];
        _warnInfoLbl.textColor = [UIColor colorWithRed:0.44 green:0.36 blue:0.52 alpha:1.00];
    }
}

- (void)setDate:(NSString *)date
{
    _dateLbl.text = date;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
