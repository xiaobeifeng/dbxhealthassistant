//
//  DBXDataBpTableViewCell.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/4.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBXDataBpTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, assign) double sbpValue;
@property (nonatomic, assign) double dbpValue;
@property (nonatomic, strong) UILabel *dateLbl;          // 日期Label
@property (nonatomic, strong) UIButton *sbpWarnButton;   // 高压警告
@property (nonatomic, strong) UIButton *dbpWarnButton;   // 低压警告
@end
