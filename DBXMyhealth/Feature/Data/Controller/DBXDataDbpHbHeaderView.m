//
//  DBXDataDbpHbHeaderView.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/4.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXDataDbpHbHeaderView.h"

@interface DBXDataDbpHbHeaderView()

/**
 标题
 */
@property(nonatomic, strong) UILabel *titleLbl;

/**
 数值
 */
@property(nonatomic, strong) UILabel *valueLbl;

@end

@implementation DBXDataDbpHbHeaderView

- (instancetype)init
{
    if (self = [super init]) {
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI
{
    _titleLbl = [UILabel new];
    _titleLbl.text = @"舒张压(mmHg)";
    _titleLbl.font = kFont(13);
    _titleLbl.textColor = [UIColor whiteColor];
    _titleLbl.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_titleLbl];
    [_titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.mas_top);
        make.width.mas_equalTo(self.mas_width);
        make.height.mas_equalTo(20);
        make.centerX.mas_equalTo(self.mas_centerX);
    }];
    
    _valueLbl = [UILabel new];
    _valueLbl.text = @"80";
    _valueLbl.font = kFont(20);
    _valueLbl.textColor = [UIColor whiteColor];
    _valueLbl.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_valueLbl];
    [_valueLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLbl.mas_bottom);
        make.width.mas_equalTo(self.mas_width);
        make.bottom.mas_equalTo(self.mas_bottom);
        make.centerX.mas_equalTo(self.mas_centerX);
    }];
}

- (void)setTitle:(NSString *)title
{
    _titleLbl.text = title;
}

- (void)setDPressValue:(NSString *)dPressValue
{
    if (dPressValue.length != 0) {
        NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", dPressValue]];
        _valueLbl.attributedText = attri;
    } else {
        _valueLbl.text= @"--";
    }
   
}

- (void)setHbValue:(NSString *)hbValue
{
    if (hbValue.length != 0) {
        UIImage *img = [UIImage imageNamed:@"health_heartBeat"];
        NSTextAttachment *textAttach = [[NSTextAttachment alloc]init];
        textAttach.image = img;
        NSAttributedString *strA =[NSAttributedString attributedStringWithAttachment:textAttach];
        NSMutableAttributedString *attri1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ", hbValue]];
        [attri1 appendAttributedString:strA];
        _valueLbl.attributedText = attri1;
        
    } else {
        _valueLbl.text= @"--";
    }
}

@end
