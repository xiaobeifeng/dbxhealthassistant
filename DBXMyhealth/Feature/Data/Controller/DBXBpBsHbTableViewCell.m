//
//  DBXBpBsHbTableViewCell.m
//  DBXHealthAssistant
//
//  Created by zhoujian on 2018/8/31.
//  Copyright © 2018年 mc. All rights reserved.
//

#import "DBXBpBsHbTableViewCell.h"
@interface DBXBpBsHbTableViewCell()

/**
 日期
 */
@property(nonatomic, strong) UILabel *dateLbl;
/**
 收缩压
 */
@property(nonatomic, strong) UILabel *sysPressureLbl;
/**
 舒张压
 */
@property(nonatomic, strong) UILabel *diaPressureLbl;
/**
 心率
 */
@property(nonatomic, strong) UILabel *heartBeatLbl;
/**
 血氧
 */
@property(nonatomic, strong) UILabel *oxyLbl;
/**
 血压警告信息
 */
@property(nonatomic, strong) UILabel *bpWarnLbl;

@property(nonatomic, strong) UIView *lineView;
@property(nonatomic, strong) UIView *pointView;
@property(nonatomic, strong) UIView *contentBackgroundView;

@end

@implementation DBXBpBsHbTableViewCell

-  (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.contentView.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.96 alpha:1.00];
        
        _lineView = [UIView new];
        _lineView.backgroundColor = kNavigationBarColor;
        [self.contentView addSubview:_lineView];
        
        _pointView = [UIView new];
//        _pointView.backgroundColor = [UIColor redColor];
        kViewRadius(_pointView, 5);
        [_lineView addSubview:_pointView];
        
        _contentBackgroundView = [UIView new];
        _contentBackgroundView.backgroundColor = [UIColor whiteColor];
        kViewRadius(_contentBackgroundView, 5.0f);
        [self.contentView addSubview:_contentBackgroundView];
        
        _dateLbl = [UILabel new];
        _dateLbl.backgroundColor = [UIColor whiteColor];
        _dateLbl.textColor = [UIColor colorWithHexString:@"52396C"];
        _dateLbl.font = [UIFont systemFontOfSize:12];
        [_contentBackgroundView addSubview:_dateLbl];
        
        _sysPressureLbl = [UILabel new];
        _sysPressureLbl.backgroundColor = [UIColor whiteColor];
        _sysPressureLbl.textColor = [UIColor colorWithHexString:@"52396C"];
        _sysPressureLbl.font = [UIFont systemFontOfSize:14];
        [_contentBackgroundView addSubview:_sysPressureLbl];
        
        _diaPressureLbl = [UILabel new];
        _diaPressureLbl.backgroundColor = [UIColor whiteColor];
        _diaPressureLbl.textColor = [UIColor colorWithHexString:@"52396C"];
        _diaPressureLbl.font = [UIFont systemFontOfSize:14];
        [_contentBackgroundView addSubview:_diaPressureLbl];
        
        _heartBeatLbl = [UILabel new];
        _heartBeatLbl.backgroundColor = [UIColor whiteColor];
        _heartBeatLbl.textColor = [UIColor colorWithHexString:@"52396C"];
        _heartBeatLbl.font = [UIFont systemFontOfSize:14];
        [_contentBackgroundView addSubview:_heartBeatLbl];
        
        _oxyLbl = [UILabel new];
        _oxyLbl.backgroundColor = [UIColor whiteColor];
        _oxyLbl.textColor = [UIColor colorWithHexString:@"52396C"];
        _oxyLbl.font = [UIFont systemFontOfSize:14];
        [_contentBackgroundView addSubview:_oxyLbl];
        
        _bpWarnLbl = [UILabel new];
        _bpWarnLbl.backgroundColor = [UIColor whiteColor];
        _bpWarnLbl.font = [UIFont systemFontOfSize:14];
        [_contentBackgroundView addSubview:_bpWarnLbl];
        
    }
    return self;
}

+ (BOOL)requiresConstraintBasedLayout
{
    return YES;
}

- (void)updateConstraints
{
    
    [_contentBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).with.offset(13);
        make.left.mas_equalTo(self.contentView).with.offset(70);
        make.right.mas_equalTo(self.contentView).with.offset(-13);
        make.height.mas_equalTo(165);
        make.bottom.mas_equalTo(self.contentView).with.offset(-13);
    }];
    
    [@[_dateLbl, _sysPressureLbl, _diaPressureLbl, _heartBeatLbl, _oxyLbl, _bpWarnLbl] mas_distributeViewsAlongAxis:MASAxisTypeVertical withFixedSpacing:5 leadSpacing:13 tailSpacing:13];
    
    [@[_dateLbl, _sysPressureLbl, _diaPressureLbl, _heartBeatLbl, _oxyLbl, _bpWarnLbl] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentBackgroundView).with.offset(13);
        make.right.mas_equalTo(self.contentBackgroundView).with.offset(-13);
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView);
        make.bottom.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView).with.offset(34);
        make.width.mas_equalTo(2);
    }];
    [_pointView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.lineView.mas_centerY);
        make.centerX.mas_equalTo(self.lineView.mas_centerX);
        make.width.height.mas_equalTo(10);
    }];
    
    
    [super updateConstraints];
}

- (void)setDate:(NSString *)date
{
    if (date.length>0) {
        _dateLbl.text = date;
    }
}

- (void)setSysPressure:(NSString *)sysPressure
{
    if (sysPressure.length > 0) {
        _sysPressureLbl.text = [NSString stringWithFormat:@"收缩压：%@ mmHg", sysPressure];
    } else {
        _sysPressureLbl.text = @"收缩压：-- mmHg";
    }
}

- (void)setDiaPressure:(NSString *)diaPressure
{
    if (diaPressure.length > 0) {
        _diaPressureLbl.text = [NSString stringWithFormat:@"舒张压：%@ mmHg", diaPressure];
    } else {
         _diaPressureLbl.text = @"舒张压：-- mmHg";
    }
}

- (void)setHeartBeat:(NSString *)heartBeat
{
    if (heartBeat.length > 0) {
        _heartBeatLbl.text = [NSString stringWithFormat:@"心率：%@ bpm", heartBeat];
    } else {
         _heartBeatLbl.text = @"心率：-- bpm";
    }
}

- (void)setOxy:(NSString *)oxy
{
    if (oxy.length > 0) {
        _oxyLbl.text = [NSString stringWithFormat:@"血氧：%@ %%", oxy];
    } else {
        _oxyLbl.text = @"血氧：-- %";
    }
}

- (void)setIsBpWarn:(BOOL)isBpWarn
{
    if (isBpWarn) {
        _bpWarnLbl.text = @"血压不正常，请咨询专业医师";
        _bpWarnLbl.textColor = [UIColor colorWithHexString:@"EA5C21"];
        _pointView.backgroundColor = [UIColor colorWithHexString:@"B956D2"];
        
    } else {
        _bpWarnLbl.text = @"血压正常，请继续保持";
        _bpWarnLbl.textColor = [UIColor colorWithHexString:@"52396C"];
        _pointView.backgroundColor = [UIColor colorWithHexString:@"0499FC"];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
