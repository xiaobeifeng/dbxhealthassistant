//
//  DBXDeleteBpBsHbHistoryApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/12.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXDeleteBpBsHbHistoryApi.h"
@interface DBXDeleteBpBsHbHistoryApi()

@property(nonatomic, copy) NSString *Id;

@end

@implementation DBXDeleteBpBsHbHistoryApi
- (id)initWithId:(NSString *)Id
{
    
    self = [super init];
    if (self) {
        _Id = Id;
    }
    return self;
}

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return @"/AppService/lohas/health/bpremovebyid";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

// 请求体
- (id)requestArgument {
    return @{
             @"id": _Id
             };
}
@end
