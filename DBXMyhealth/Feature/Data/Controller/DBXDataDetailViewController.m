//
//  DBXDataDetailViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/5.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXDataDetailViewController.h"
#import "DBXDataHbBoBsDetailTableViewCell.h"
#import "DBXDataBpApi.h"
#import "DBXDataBloodSugarApi.h"
#import "DBXBloodSugarModel.h"
#import "DBXDataBpDetailTableViewCell.h"
#import "SCChart.h"
#import "DBXAddDataBpHbBoViewController.h"
#import "DBXBpBsHbHistoryViewController.h"
#import "DBXAddDataBloodSugarViewController.h"
#import "DBXBloodSugarHistoryViewController.h"

#import "DBXDataBloodSugarApi.h"
#import "DBXDataBpApi.h"
#import "DBXDataHeartbeatApi.h"
#import "DBXDataOxygenApi.h"

#import "DBXHeartBeatModel.h"
#import "DBXBloodPressModel.h"
#import "DBXBloodOxygenModel.h"
#import "DBXBloodSugarModel.h"

static NSString *kHeartBeatNavTitle = @"心率";
static NSString *kBloodPressNavTitle = @"血压";
static NSString *kBloodOxygenNavTitle = @"血氧";
static NSString *kBloodSugarNavTitle = @"血糖";

static NSString *kDBXDataHbBoBsDetailTableViewCellIdentifier = @"DBXDataHbBoBsDetailTableViewCell";
static NSString *kDataBpDetailTableViewCellIdentifier = @"DBXDataBpDetailTableViewCell";

@interface DBXDataDetailViewController ()<UITableViewDelegate, UITableViewDataSource, SCChartDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@property(nonatomic, strong) UITableView *tableView;
/**
 血氧，血压，心率数据
 */
@property(nonatomic, strong) NSMutableArray *dataMarray;
/**
 血糖数据
 */
@property(nonatomic, strong) NSMutableArray *bsDataMarray;

@property(nonatomic, strong) SCChart *chartView;

@property(nonatomic, strong) NSMutableArray *heartBeatMarray;
@property(nonatomic, strong) NSMutableArray *bloodOxygenMarray;
@property(nonatomic, strong) NSMutableArray *bloodSugarMarray;
/**
 高压
 */
@property(nonatomic, strong) NSMutableArray *sPressMarray;

/**
 低压
 */
@property(nonatomic, strong) NSMutableArray *dPressMarray;
/** 下拉菜单 */
@property (nonatomic, strong) FFDropDownMenuView *dropDownMenu;


/**
 导航栏标题
 */
@property(nonatomic, copy) NSString *navTitle;

@property (nonatomic, strong) UIButton *navigationButton;

@property (nonatomic, strong) UIView *dropView; // 下拉视图

@end

@implementation DBXDataDetailViewController
#pragma mark - 懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-kSafeAreaTopHeight) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = kTableSeparatorLineColor;
        [_tableView registerClass:[DBXDataHbBoBsDetailTableViewCell class] forCellReuseIdentifier:kDBXDataHbBoBsDetailTableViewCellIdentifier];
        [_tableView registerClass:[DBXDataBpDetailTableViewCell class] forCellReuseIdentifier:kDataBpDetailTableViewCellIdentifier];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.emptyDataSetSource = self;
        _tableView.emptyDataSetDelegate = self;
        
        
    }
    
    return _tableView;
}

- (NSMutableArray *)dataMarray
{
    if (!_dataMarray) {
        _dataMarray = [NSMutableArray new];
    }
    return _dataMarray;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithimage:[UIImage imageNamed:@"nav_rightBar_more"] selImage:[UIImage imageNamed:@"nav_rightBar_more"] target:self action:@selector(rightBarButtonClick)];
    
    self.navTitle = self.navigationItem.title;
    
    [self.view addSubview:self.tableView];
    
    [self setupDropDownMenu];
    
    [self setupTableViewChatHeaderView];
    
    NSLog(@"title - %@", self.navigationItem.title);
    
    [self startRequestApi];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startRequestApi) name:kReloadDataApiAndDataDetailApi object:nil];
}

/**
 搭建头部折线图表
 */
- (void)setupTableViewChatHeaderView
{
    _chartView = [[SCChart alloc] initwithSCChartDataFrame:CGRectMake(0, 0, kScreenWidth, 150)
                                                withSource:self
                                                 withStyle:SCChartLineStyle];
    _chartView.backgroundColor = [UIColor whiteColor];
    self.tableView.tableHeaderView = _chartView;
    _chartView.hidden = YES;
}

#pragma mark - 请求api
- (void)startRequestApi
{
    // 心率
    if ([self.navTitle isEqualToString:kHeartBeatNavTitle]) {
        [self startRequsetHeartBeatApi];
    }
    
    // 血压
    if ([self.navTitle isEqualToString:kBloodPressNavTitle]) {
        [self startRequsetBloodPressApi];
    }
    
    // 血氧
    if ([self.navTitle isEqualToString:kBloodOxygenNavTitle]) {
        [self startRequsetOxygenApi];
    }
    
    // 血糖
    if ([self.navTitle isEqualToString:kBloodSugarNavTitle]) {
        [self startRequsetBloodSugarApi];
    }
}

/**
 请求心率数据
 */
- (void)startRequsetHeartBeatApi
{
    DBXDataHeartbeatApi *api = [[DBXDataHeartbeatApi alloc] initWithCount:10 doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            DBXHeartBeatModel *model = [DBXHeartBeatModel yy_modelWithJSON:request.responseJSONObject];
            
            if (!kArrayIsEmpty(model.data)) {
                
                if (!kArrayIsEmpty(self.dataMarray)) {
                    [self.dataMarray removeAllObjects];
                }
                
                NSArray *array = [NSArray yy_modelArrayWithClass:[DBXHeartBeatModelData class] json:model.data];
                
                [self.dataMarray addObjectsFromArray:array];
                
            } else {
                
                [self.dataMarray removeAllObjects];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.tableView reloadData];
                [self reloadChartView];
            });
            
        });
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}


/**
 请求血压数据
 */
- (void)startRequsetBloodPressApi
{
    DBXDataBpApi *api = [[DBXDataBpApi alloc] initWithCount:10 doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSLog(@"%@", request.responseJSONObject);
            
            DBXBloodPressModel *model = [DBXBloodPressModel yy_modelWithJSON:request.responseJSONObject];
            
            if (!kArrayIsEmpty(model.data)) {
                
                if (!kArrayIsEmpty(self.dataMarray)) {
                    [self.dataMarray removeAllObjects];
                }
                
                NSArray *array = [NSArray yy_modelArrayWithClass:[DBXBloodPressModelData class] json:model.data];
                [self.dataMarray addObjectsFromArray:array];
    
            } else {
                
                [self.dataMarray removeAllObjects];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
                [self reloadChartView];
            });
            
        });
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

/**
 请求血氧数据
 */
- (void)startRequsetOxygenApi
{
    DBXDataOxygenApi *api = [[DBXDataOxygenApi alloc] initWithCount:10 doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            DBXBloodOxygenModel *model = [DBXBloodOxygenModel yy_modelWithJSON:request.responseJSONObject];
            
            if (!kArrayIsEmpty(model.data)) {
                
                if (!kArrayIsEmpty(self.dataMarray)) {
                    [self.dataMarray removeAllObjects];
                }
                
                NSArray *array = [NSArray yy_modelArrayWithClass:[DBXBloodOxygenModelData class] json:model.data];
                
                [self.dataMarray addObjectsFromArray:array];
                
            } else {
                
                [self.dataMarray removeAllObjects];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
                [self reloadChartView];
            });
            
        });
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

/**
 请求获取血糖接口
 */
- (void)startRequsetBloodSugarApi
{
    DBXDataBloodSugarApi *bloodSugarApi = [[DBXDataBloodSugarApi alloc] initWithCount:10 doa:0 mCondition:0];
    [bloodSugarApi startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            DBXBloodSugarModel *model = [DBXBloodSugarModel yy_modelWithJSON:request.responseJSONObject];
            
            if (!kArrayIsEmpty(model.data)) {
                
                if (!kArrayIsEmpty(self.dataMarray)) {
                    [self.dataMarray removeAllObjects];
                }
                
                NSArray *array = [NSArray yy_modelArrayWithClass:[DBXBloodSugarModelData class] json:model.data];
                [self.dataMarray addObjectsFromArray:array];
        
            } else {
                
                [self.dataMarray removeAllObjects];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
                [self reloadChartView];
            });
        });
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        [self.tableView.mj_header endRefreshing];
    }];
}

- (void)toInputPage
{
    // 显示下拉界面
    [self.dropDownMenu showMenu];
    
}

- (void)setupDropDownMenu
{
    NSArray *modelsArray = [self getMenuModelsArray];
    
    self.dropDownMenu = [FFDropDownMenuView ff_DefaultStyleDropDownMenuWithMenuModelsArray:modelsArray menuWidth:FFDefaultFloat eachItemHeight:FFDefaultFloat menuRightMargin:FFDefaultFloat triangleRightMargin:FFDefaultFloat];
    
    self.dropDownMenu.ifShouldScroll = NO;
    
    self.dropDownMenu.iconSize = CGSizeMake(0, 0);
    self.dropDownMenu.menuWidth = 120;
    
    [self.dropDownMenu setup];
}

/** 获取菜单模型数组 */
- (NSArray *)getMenuModelsArray
{
    __weak typeof(self) weakSelf = self;
    
    FFDropDownMenuModel *menuModel0 = [FFDropDownMenuModel ff_DropDownMenuModelWithMenuItemTitle:@"添加" menuItemIconName:@""  menuBlock:^{
        
        if ([weakSelf.navigationItem.title isEqualToString:@"血糖"]) {
            
            DBXAddDataBloodSugarViewController *addDataBloodSugarVC = [DBXAddDataBloodSugarViewController new];
            [weakSelf.navigationController pushViewController:addDataBloodSugarVC animated:YES];
            
        } else {
            
            DBXAddDataBpHbBoViewController *addDataBpHbBoVC = [DBXAddDataBpHbBoViewController new];
            [weakSelf.navigationController pushViewController:addDataBpHbBoVC animated:YES];
            
        }
        
    }];
    
    FFDropDownMenuModel *menuModel1 = [FFDropDownMenuModel ff_DropDownMenuModelWithMenuItemTitle:@"查询历史" menuItemIconName:@"" menuBlock:^{
        
        if ([weakSelf.navigationItem.title isEqualToString:@"血糖"]) {
            
            DBXBloodSugarHistoryViewController *bloodSugarHistoryVC = [DBXBloodSugarHistoryViewController new];
            [weakSelf.navigationController pushViewController:bloodSugarHistoryVC animated:YES];
            
        } else {
            
            DBXBpBsHbHistoryViewController *bpBsHbHistoryVC = [DBXBpBsHbHistoryViewController new];
            [weakSelf.navigationController pushViewController:bpBsHbHistoryVC animated:YES];
            
        }
        
        
        
    }];
    
    NSArray *menuModelArr = @[menuModel0, menuModel1];
    return menuModelArr;
}


#pragma mark - SCChartDataSource
// 横坐标标题数组
- (NSArray *)SCChart_xLableArray:(SCChart *)chart
{
    return @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10"];
}

//数值多重数组
- (NSArray *)SCChart_yValueArray:(SCChart *)chart
{
    // 心率
    if ([self.navTitle isEqualToString:kHeartBeatNavTitle]) {
        return @[self.heartBeatMarray];
    }
    // 血压
    if ([self.navTitle isEqualToString:kBloodPressNavTitle]) {
        return @[self.sPressMarray, self.dPressMarray];
    }
    // 血氧
    if ([self.navTitle isEqualToString:kBloodOxygenNavTitle]) {
        return @[self.bloodOxygenMarray];
    }
    // 血糖
    if ([self.navTitle isEqualToString:kBloodSugarNavTitle]) {
        return @[self.bloodSugarMarray];
    }
    
    return nil;
}

// 颜色数组
- (NSArray *)SCChart_ColorArray:(SCChart *)chart
{
    return @[SCBlue,SCRed];
}

// 判断显示横线条
- (BOOL)SCChart:(SCChart *)chart ShowHorizonLineAtIndex:(NSInteger)index {
    return YES;
}

//判断显示最大最小值
- (BOOL)SCChart:(SCChart *)chart ShowMaxMinAtIndex:(NSInteger)index
{
    return NO;
}

/**
 刷新图标数据
 */
- (void)reloadChartView
{
    
    self.heartBeatMarray = [NSMutableArray new];
    self.dPressMarray = [NSMutableArray new];
    self.sPressMarray = [NSMutableArray new];
    self.bloodOxygenMarray = [NSMutableArray new];
    self.bloodSugarMarray = [NSMutableArray new];
    
    // 心率
    if ([self.navTitle isEqualToString:kHeartBeatNavTitle]) {
        
        for (DBXHeartBeatModelData *model in self.dataMarray) {
            
            [self.heartBeatMarray addObject:model.pulse];
        }

    }
    
    // 血压
    if ([self.navTitle isEqualToString:kBloodPressNavTitle]) {

        for (DBXBloodPressModelData *model in self.dataMarray) {
            
            [self.sPressMarray addObject:model.sPressure];
            [self.dPressMarray addObject:model.dPressure];
        }
    }
    
    // 血氧
    if ([self.navTitle isEqualToString:kBloodOxygenNavTitle]) {
        for (DBXBloodOxygenModelData *model in self.dataMarray) {
            [self.bloodOxygenMarray addObject:model.oxygen];
        }
    }
    
    // 血糖
    if ([self.navTitle isEqualToString:kBloodSugarNavTitle]) {
        for (DBXBloodSugarModelData *model in self.dataMarray) {
            
            [self.bloodSugarMarray addObject:model.bloodGluVal];
        }
    }
    
    [self.chartView strokeChart];
    
    if (kArrayIsEmpty(self.dataMarray)) {
        self.chartView.hidden = YES;
    } else {
        self.chartView.hidden = NO;
    }
    
}

#pragma mark - tableView Delegate and DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataMarray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([self.navTitle isEqualToString:kBloodPressNavTitle]) {
        DBXDataBpDetailTableViewCell *bpCell = [tableView dequeueReusableCellWithIdentifier:kDataBpDetailTableViewCellIdentifier forIndexPath:indexPath];
        
        [self setupModelOfBloodPressCell:bpCell cellForRowAtIndexPath:indexPath];
        
        return bpCell;
        
    } else {
        
        DBXDataHbBoBsDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDBXDataHbBoBsDetailTableViewCellIdentifier forIndexPath:indexPath];
        
        [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
        
        return cell;
    }
    
}

- (void)setupModelOfBloodPressCell:(DBXDataBpDetailTableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DBXBloodPressModelData *model = self.dataMarray[indexPath.row];
    cell.sPressValue = model.sPressure;
    cell.dPressValue = model.dPressure;
    cell.date = model.upsaveTime;
    
    BOOL isNormal = [ZJIsNormalNormTool zj_isNormalBp:[model.sPressure doubleValue] dbp:[model.dPressure doubleValue]];
    if (isNormal) {
        cell.isPressWarn = NO;
    } else {
        cell.isPressWarn = YES;
    }

}

- (void)setupModelOfCell:(DBXDataHbBoBsDetailTableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 心率
    if ([self.navTitle isEqualToString:kHeartBeatNavTitle]) {
        
        DBXHeartBeatModelData *modelData = self.dataMarray[indexPath.row];
        cell.pulseValue = modelData.pulse;
        cell.date = modelData.upsaveTime;
       
        BOOL isNormal = [ZJIsNormalNormTool zj_isNormalHb:[modelData.pulse doubleValue]];
        if (isNormal) {
            cell.isPulseWarn = NO;
        } else {
            cell.isPulseWarn = YES;
        }
    }
    
    // 血氧
    if ([self.navTitle isEqualToString:kBloodOxygenNavTitle]) {
        
        DBXBloodOxygenModelData *model = self.dataMarray[indexPath.row];
        cell.oxygenValue = model.oxygen;
        cell.date = model.upsaveTime;
        
        BOOL isNormal = [ZJIsNormalNormTool zj_isNormalBo:[model.oxygen doubleValue]];
        if (isNormal) {
            cell.isOxygenWarn = NO;
        } else {
            cell.isOxygenWarn = YES;
        }
    }
    
    // 血糖
    if ([self.navTitle isEqualToString:kBloodSugarNavTitle]) {
        
        DBXBloodSugarModelData *model = self.dataMarray[indexPath.row];
        cell.bloodSugarValue = model.bloodGluVal;
        cell.date = model.upsaveTime;
        
        BOOL isNormal = [ZJIsNormalNormTool zj_isNormalBs:[model.mCondition integerValue] bs:[model.bloodGluVal doubleValue]];
        if (isNormal) {
            cell.isBloodSugarWarn = NO;
        } else {
            cell.isBloodSugarWarn = YES;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.navTitle isEqualToString:kBloodPressNavTitle]) {

        return [tableView fd_heightForCellWithIdentifier:kDataBpDetailTableViewCellIdentifier cacheByIndexPath:indexPath configuration:^(id cell) {

            [self setupModelOfBloodPressCell:cell cellForRowAtIndexPath:indexPath];
        }];
    } else {

        return [tableView fd_heightForCellWithIdentifier:kDBXDataHbBoBsDetailTableViewCellIdentifier cacheByIndexPath:indexPath configuration:^(id cell) {

            [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
        }];
    }

}

- (void)rightBarButtonClick
{
    [self toInputPage];
    //    DBXDataFillInBpHbBoViewController *fillInBpHbBoVC = [DBXDataFillInBpHbBoViewController new];
    //    [self.navigationController pushViewController:fillInBpHbBoVC animated:YES];
}

# pragma mark - emptyDataSet
/**
 *  返回占位图图片
 */
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIImage imageNamed:@"report_notFound"];
}
/**
 *  返回标题文字
 */
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"暂无数据";
    UIFont *font = [UIFont systemFontOfSize:14.0];
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:text];
    [attStr addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, text.length)];
    return attStr;
}
/**
 *  处理空白区域的点击事件
 */
- (void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view {
    NSLog(@"%s",__FUNCTION__);
}
- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    return -70.0f;
}
- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 20;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
