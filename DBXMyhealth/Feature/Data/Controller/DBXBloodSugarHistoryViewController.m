//
//  DBXBloodSugarHistoryViewController.m
//  DBXHealthAssistant
//
//  Created by zhoujian on 2018/8/31.
//  Copyright © 2018年 mc. All rights reserved.
//

#import "DBXBloodSugarHistoryViewController.h"
#import "DBXBloodSugarHistoryTableViewCell.h"
#import "DBXBloodSugarModel.h"
#import "SMKJSelectDateView.h"
#import "DBXBloodSugarModel.h"
#import "DBXBloodSugarHistoryApi.h"
#import "DBXDeleteBloodSugarHistoryApi.h"

static NSString * const kBloodSugarHistoryTableViewCellIdentifier = @"DBXBloodSugarHistoryTableViewCell";

@interface DBXBloodSugarHistoryViewController ()<UITableViewDelegate, UITableViewDataSource, SMKJSelectDateViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) SMKJSelectDateView *selectDateView;
@property (nonatomic, strong) NSDate *chooseDate;
@property (nonatomic, copy) NSString *selectDateStr;
@property(nonatomic, assign) NSInteger currentPage;
@property (nonatomic, strong) NSMutableArray*dataSourceArray;

@end

@implementation DBXBloodSugarHistoryViewController

#pragma mark - 懒加载
- (NSMutableArray *)dataSourceArray {
    if (!_dataSourceArray) {
        _dataSourceArray = [NSMutableArray array];
    }
    return _dataSourceArray;
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 50, kScreenWidth, kScreenHeight-kSafeAreaTopHeight-50) style:UITableViewStylePlain];
        [_tableView registerClass:[DBXBloodSugarHistoryTableViewCell class] forCellReuseIdentifier:kBloodSugarHistoryTableViewCellIdentifier];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.96 alpha:1.00];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(handleHeaderEvent)];
        _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(handleFooterEvent)];
        _tableView.tableFooterView = [UIView new];
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.emptyDataSetSource = self;
        _tableView.emptyDataSetDelegate = self;
    
    }
    return _tableView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"查询历史";
    
    _chooseDate = [NSDate date];
    _selectDateView = [[SMKJSelectDateView alloc] initWithDate:_chooseDate];
    _selectDateView.delegate = self;
    _selectDateView.frame = CGRectMake(0, 0, kScreenWidth, 50);
    _selectDateView.backgroundColor = [UIColor colorWithRed:0.89 green:0.89 blue:0.90 alpha:1.00];
    [self.view addSubview: _selectDateView];
    
    [self.view addSubview:self.tableView];
    
    [self handleHeaderEvent];
    
}

/**
 下拉刷新
 */
- (void)handleHeaderEvent {
    _currentPage = 0;
    [self requestListDataWithDate:[self dateFormatterWithDate:_chooseDate]];
}

/**
 上拉刷新
 */
- (void)handleFooterEvent {
    [self requestListDataWithDate:[self dateFormatterWithDate:_chooseDate]];
}

/**
 将日期格式化指定样式的字符串
 NO:YYYY-MM
 
 @param date 需要转成字符串的日期
 @return 指定样式字符串
 */
- (NSString *)dateFormatterWithDate:(NSDate *)date {
    
    
    NSDateFormatter *formatterYearAndMonth = [[NSDateFormatter alloc] init];
    
    formatterYearAndMonth.dateFormat = @"YYYY-MM";
    
    NSString *dateStr = [formatterYearAndMonth stringFromDate:date];
    return dateStr;
}

- (void)requestListDataWithDate:(NSString *)dateStr
{
    DBXBloodSugarHistoryApi *api = [[DBXBloodSugarHistoryApi alloc] initWithSelectTime:dateStr currentPage:_currentPage];
    
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        if (request.responseJSONObject) {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                DBXBloodSugarModel *model = [DBXBloodSugarModel yy_modelWithJSON:request.responseJSONObject];
                
                // 请求成功
                if ([model.state integerValue] == 0) {
                    
                    // currentPage为0
                    if (self.currentPage == 0) {
                        [self.dataSourceArray removeAllObjects];
                    }
                    [self.dataSourceArray addObjectsFromArray:[NSArray yy_modelArrayWithClass:[DBXBloodSugarModelData class] json:model.data]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (self.currentPage != 0) {
                            
                            [self.tableView reloadData];
                            [self.tableView.mj_footer endRefreshing];
                            
                        } else {
                            
                            [self.tableView reloadData];
                            [self.tableView.mj_header endRefreshing];
                            
                        }
                        
                        if (model.data.count != 0) {
                            self.currentPage += 1;
                        }
                        
                        // TODO: 根据数据源控制上拉刷新显隐
                        if (self.dataSourceArray.count < 10) {
                            self.tableView.mj_footer.hidden = YES;
                        } else {
                            self.tableView.mj_footer.hidden = NO;
                        }
                        
                        
                    });
                    
                }
            });
            
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
    
    //    NSString *url = [NSString stringWithFormat:@"%s/health/glupagebytime", serverUrl];
    //    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    //    [manager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    //    NSDictionary *parms = @{
    //                            @"selecttime":dateStr,
    //                            @"doa":@0,
    //                            @"pagesize":@10,
    //                            @"pagenow":@(_currentPage)
    //                            };
    //
    //    [manager POST:url parameters:parms success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    //
    //        NSLog(@"%@", responseObject);
    //
    //        if (responseObject) {
    //
    //            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    //
    //                DBXBloodSugarModel *model = [DBXBloodSugarModel yy_modelWithJSON:responseObject];
    //
    //                //  请求成功
    //                if ([model.state integerValue] == 0) {
    //
    //                    // currentPage为0
    //                    if (_currentPage == 0) {
    //                        [self.dataSourceArray removeAllObjects];
    //                    }
    //                    [self.dataSourceArray addObjectsFromArray:[NSArray yy_modelArrayWithClass:[DBXBloodSugarModelData class] json:model.data]];
    //
    //                    dispatch_async(dispatch_get_main_queue(), ^{
    //                        if (self.currentPage != 0) {
    //
    //                            [self.tableView reloadData];
    //                            [self.tableView.mj_footer endRefreshing];
    //
    //                        } else {
    //
    //                            [self.tableView reloadData];
    //                            [self.tableView.mj_header endRefreshing];
    //
    //                        }
    //
    //                        if (model.data.count != 0) {
    //                            self.currentPage += 1;
    //                        }
    //
    //                        // TODO: 根据数据源控制上拉刷新显隐
    //                        if (self.dataSourceArray.count < 10) {
    //                            self.tableView.mj_footer.hidden = YES;
    //                        } else {
    //                            self.tableView.mj_footer.hidden = NO;
    //                        }
    //
    //
    //                    });
    //
    //                }
    //            });
    //
    //        }
    //
    //    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    //        [SVProgressHUD showErrorWithStatus:@"请稍后再试"];
    //    }];
    
}

# pragma mark - selectDateView Delegate
- (void)selectDateViewClick:(SMKJSelectDateBtnType)type
{
    [self.tableView  scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    
    if (type == MonthReduce) {
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        dateComponents.month = -1;
        NSDate *fromMonth = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:_chooseDate options:0];
        _chooseDate = fromMonth;
        NSDateFormatter *formatterYearAndMonth = [[NSDateFormatter alloc] init];
        formatterYearAndMonth.dateFormat = @"YYYY年MM月";
        NSString *chooseDate = [formatterYearAndMonth stringFromDate:_chooseDate];
        _selectDateView.selectDateString = chooseDate;
        
        NSDateFormatter *formatterYearAndMonth1 = [[NSDateFormatter alloc] init];
        formatterYearAndMonth1.dateFormat = @"YYYY-MM";
        _selectDateStr = [formatterYearAndMonth1 stringFromDate:_chooseDate];
        [self handleHeaderEvent];
    }
    
    if (type == MonthAdd) {
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        dateComponents.month = +1;
        NSDate *fromMonth = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:_chooseDate options:0];
        _chooseDate = fromMonth;
        NSDateFormatter *formatterYearAndMonth = [[NSDateFormatter alloc] init];
        formatterYearAndMonth.dateFormat = @"YYYY年MM月";
        NSString *chooseDate = [formatterYearAndMonth stringFromDate:_chooseDate];
        _selectDateView.selectDateString = chooseDate;
        
        NSDateFormatter *formatterYearAndMonth1 = [[NSDateFormatter alloc] init];
        formatterYearAndMonth1.dateFormat = @"YYYY-MM";
        _selectDateStr = [formatterYearAndMonth1 stringFromDate:_chooseDate];
        [self handleHeaderEvent];
    }
    
    if (type == CalendarShow) {
        //        SMKJCalendarView *calendarView = [[SMKJCalendarView alloc] initWithCurrentSelectDate:_chooseDate];
        //        calendarView.delegate = self;
        //        [calendarView show];
    }
}

#pragma mark - UITableView代理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DBXBloodSugarHistoryTableViewCell *bloodSugarHistoryTableViewCell = [tableView dequeueReusableCellWithIdentifier:kBloodSugarHistoryTableViewCellIdentifier forIndexPath:indexPath];
    
    [self setupModelOfCell:bloodSugarHistoryTableViewCell cellForRowAtIndexPath:indexPath];
    
    return bloodSugarHistoryTableViewCell;
    
}

- (void)setupModelOfCell:(DBXBloodSugarHistoryTableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DBXBloodSugarModelData *model = self.dataSourceArray[indexPath.row];
    cell.date = model.upsaveTime;
    //    cell.bloodSugar =
    NSString *bloodSugar;
    NSInteger state = [model.mCondition integerValue];
    switch (state) {
        case 0: // 空腹
        {
            bloodSugar = [NSString stringWithFormat:@"血糖：%@ mmol/L 空腹", model.bloodGluVal];
            if ([model.bloodGluVal integerValue] >= 3.9 && [model.bloodGluVal integerValue] <= 6.1) {
                cell.isBsWarn = NO;
            } else {
                cell.isBsWarn = YES;
            }
        }
            break;
        case 1: // 早餐前
        {
            bloodSugar = [NSString stringWithFormat:@"血糖：%@ mmol/L 早餐前", model.bloodGluVal];
            if ([model.bloodGluVal integerValue] >= 6.7 && [model.bloodGluVal integerValue] <= 9.4) {
                cell.isBsWarn = NO;
            } else {
                cell.isBsWarn = YES;
            }
        }
            break;
        case 2: // 早餐后
        {
            bloodSugar = [NSString stringWithFormat:@"血糖：%@ mmol/L 早餐后", model.bloodGluVal];
            if ([model.bloodGluVal integerValue] >= 3.9 && [model.bloodGluVal integerValue] <= 7.8) {
                cell.isBsWarn = NO;
            } else {
                cell.isBsWarn = YES;
            }
        }
            break;
        case 3: // 午餐前
        {
            bloodSugar = [NSString stringWithFormat:@"血糖：%@ mmol/L 午餐前", model.bloodGluVal];
            if ([model.bloodGluVal integerValue] >= 6.7 && [model.bloodGluVal integerValue] <= 9.4) {
                cell.isBsWarn = NO;
            } else {
                cell.isBsWarn = YES;
            }
        }
            break;
        case 4: // 午餐后
        {
            bloodSugar = [NSString stringWithFormat:@"血糖：%@ mmol/L 午餐后", model.bloodGluVal];
            if ([model.bloodGluVal integerValue] >= 3.9 && [model.bloodGluVal integerValue] <= 7.8) {
                cell.isBsWarn = NO;
            } else {
                cell.isBsWarn = YES;
            }
        }
            break;
        case 5: // 晚餐前
        {
            bloodSugar = [NSString stringWithFormat:@"血糖：%@ mmol/L 晚餐前", model.bloodGluVal];
            if ([model.bloodGluVal integerValue] >= 6.7 && [model.bloodGluVal integerValue] <= 9.4) {
                cell.isBsWarn = NO;
            } else {
                cell.isBsWarn = YES;
            }
        }
            break;
        case 6: // 晚餐后
        {
            bloodSugar = [NSString stringWithFormat:@"血糖：%@ mmol/L 晚餐后", model.bloodGluVal];
            if ([model.bloodGluVal integerValue] >= 3.9 && [model.bloodGluVal integerValue] <= 7.8) {
                cell.isBsWarn = NO;
            } else {
                cell.isBsWarn = YES;
            }
        }
            break;
        case 7: // 睡前
        {
            bloodSugar = [NSString stringWithFormat:@"血糖：%@ mmol/L 睡前", model.bloodGluVal];
            if ([model.bloodGluVal integerValue] >= 6.7 && [model.bloodGluVal integerValue] <= 9.4) {
                cell.isBsWarn = NO;
            } else {
                cell.isBsWarn = YES;
            }
        }
            break;
        default:
            break;
    }
    
    cell.bloodSugar = bloodSugar;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView fd_heightForCellWithIdentifier:kBloodSugarHistoryTableViewCellIdentifier configuration:^(id cell) {
        [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    }];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath*)indexPath
{
    // 执行删除操作
    DBXBloodSugarModelData *model = self.dataSourceArray[indexPath.row];
    
    DBXDeleteBloodSugarHistoryApi *api = [[DBXDeleteBloodSugarHistoryApi alloc] initWithId:model.Id];
    
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        [self.dataSourceArray removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:indexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
        
        // 发送通知
        [[NSNotificationCenter defaultCenter] postNotificationName:kReloadDataApiAndDataDetailApi object:nil userInfo:nil];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

 /** 删除 cell (UITableView 方法). */
- (void)deleteRowsAtIndexPaths:(NSArray *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation
{
    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReloadDataApiAndDataDetailApi object:nil];
}

# pragma mark - emptyDataSet
/**
 *  返回占位图图片
 */
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIImage imageNamed:@"report_notFound"];
}
/**
 *  返回标题文字
 */
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"暂无数据";
    UIFont *font = [UIFont systemFontOfSize:14.0];
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:text];
    [attStr addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, text.length)];
    return attStr;
}
/**
 *  处理空白区域的点击事件
 */
- (void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view {
    NSLog(@"%s",__FUNCTION__);
}
- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    return -70.0f;
}
- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 20;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
