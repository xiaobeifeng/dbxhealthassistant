//
//  DBXBloodSugarUploadApi.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/12.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "BaseRequestApi.h"

@interface DBXBloodSugarUploadApi : BaseRequestApi

- (id)initWithBloodGluVal:(float)bloodGluVal
          mCondition:(NSInteger)mCondition;

@end
