//
//  DBXDataHbBoBsDetailTableViewCell.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/5.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBXDataHbBoBsDetailTableViewCell : UITableViewCell

@property(nonatomic, assign) BOOL isPulseWarn;
@property(nonatomic, copy) NSString *pulseValue;
@property(nonatomic, assign) BOOL isOxygenWarn;
@property(nonatomic, copy) NSString *oxygenValue;
@property(nonatomic, assign) BOOL isBloodSugarWarn;
@property(nonatomic, copy) NSString *bloodSugarValue;
@property(nonatomic, copy) NSString *date;

@end
