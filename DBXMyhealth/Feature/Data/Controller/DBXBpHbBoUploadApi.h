//
//  DBXBpHbBoUploadApi.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/11.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 手动输入的血压，血氧，心率上传到服务器
 */
@interface DBXBpHbBoUploadApi : BaseRequestApi

- (id)initWithSPress:(int)sPress
              dPress:(int)dPress
           heartBeat:(int)heartBeat
         bloodOxygen:(int)bloodOxygen;

@end
