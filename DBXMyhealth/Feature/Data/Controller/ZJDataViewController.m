
//  Created by zhoujian on 2018/8/14.
//  Copyright © 2018年 zhoujian. All rights reserved.
//

#import "ZJDataViewController.h"
#import "DBXCircleView.h"
#import "UIColor+Color.h"
#import "DBXDataBpTableViewCell.h"
#import "DBXDataBoTableViewCell.h"
#import "DBXDataDbpHbHeaderView.h"
#import "DBXDataDetailViewController.h"
#import "DBXDataBloodSugarApi.h"
#import "DBXDataBpApi.h"
#import "DBXDataHeartbeatApi.h"
#import "DBXDataOxygenApi.h"
#import "DBXHeartBeatModel.h"
#import "DBXBloodPressModel.h"
#import "DBXBloodOxygenModel.h"
#import "DBXBloodSugarModel.h"
#import "DBXLinkyorntwoApi.h"
#import "DBXPeripheralManagementViewController.h"
#import "DBXBaseNavigationController.h"
#import "ZJDataBSDetailViewController.h"
#import "ZJAllBloodSugarApi.h"

static NSString *kDataBpTableViewCellIdentifier = @"DBXDataBpTableViewCell";
static NSString *kDataBoTableViewCellIdentifier = @"DBXDataBoTableViewCell";
static CGFloat kTopViewHeight = 220;
static CGFloat kCircleOutImageViewWidth = 170;
static CGFloat kBloodPressViewWidth = 150;
static CGFloat kCircleInImageViewWidth = 120;

@interface ZJDataViewController ()<UITableViewDelegate, UITableViewDataSource>

/** 收缩压 */
@property(nonatomic, copy) NSString *sPressure;
/** 舒张压 */
@property(nonatomic, copy) NSString *dPressure;
/** 血压上传时间*/
@property(nonatomic, copy) NSString *bloodPressUpsaveTime;
/** 心率*/
@property(nonatomic, copy) NSString *pulse;
/** 心率上传时间*/
@property(nonatomic, copy) NSString *pulseUpsaveTime;
/** 血氧*/
@property(nonatomic, copy) NSString *oxygen;
/** 血氧上传时间*/
@property(nonatomic, copy) NSString *oxygenUpsaveTime;
/** 血糖 */
@property(nonatomic, copy) NSString *bloodSugar;
/** 血糖测量时的状态 */
@property(nonatomic, copy) NSString *bloodSugarCondition;
/** 血糖上传时间 */
@property(nonatomic, copy) NSString *bloodSugarUpsaveTime;
/** 舒张压View */
@property(nonatomic, strong) DBXDataDbpHbHeaderView *dbpView;
/** 心率View */
@property(nonatomic, strong) DBXDataDbpHbHeaderView *hbView;

/** 顶部血压信息 */
@property(nonatomic, strong) UILabel *bloodPressLbl;

//----------------------重新设计------------------------
@property (nonatomic, strong) DBXBloodPressModelData *bloodPressModelData;
@property (nonatomic, strong) DBXHeartBeatModelData *heartBeatModelData;
@property (nonatomic, strong) DBXBloodOxygenModelData *bloodOxygenModelData;
@property (nonatomic, strong) DBXBloodSugarModelData *bloodSugarModelData;
@property(nonatomic, strong) DBXCircleView *bpCircleView;     // 高压进度环
@property(nonatomic, strong) UITableView *tableView;

@end

@implementation ZJDataViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    [self startRequsetBloodPressApi];     // 请求血压api
    [self startRequsetHeartBeatApi];      // 请求心率api
    [self startRequsetBloodSugarApi];     // 请求血糖api
    [self startRequsetOxygenApi];         // 请求血氧api
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];

    [self setupUI];
    
}



///**
// 显示更新提示弹框
// 
// @param updateInfo 更新信息
// @param isMustUpdate 是否强制升级
// */
//- (void)showAlertWithInfo:(NSString *)updateInfo isMustUpdate:(BOOL)isMustUpdate
//{
//    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"检测到新版本" message:updateInfo preferredStyle:UIAlertControllerStyleAlert];
//    
//    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"更新" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://itunes.apple.com/app/id%@?mt=8", APPID]]];
//        
//    }];
//    
//    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        
//    }];
//    
//    [alertController addAction:okAction];
//    if (!isMustUpdate) {
//        [alertController addAction:cancelAction];
//    }
//    
//    [self presentViewController:alertController animated:YES completion:nil];
//}


- (void)requestLinkyorntwoApi
{
    DBXLinkyorntwoApi *api = [[DBXLinkyorntwoApi alloc] init];
    
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        if (request.responseJSONObject) {
            NSDictionary *dic = request.responseJSONObject;
            // 设备未建立过连接 或 设备未激活
            if ([dic[@"state"] integerValue] == 1041 || [dic[@"state"] integerValue] == 1043) {
                
                DBXPeripheralManagementViewController *peripheralManagementVC = [DBXPeripheralManagementViewController new];
                DBXBaseNavigationController *nav = [[DBXBaseNavigationController alloc] initWithRootViewController:peripheralManagementVC];
                [self presentViewController:nav animated:YES completion:nil];
                
            }
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
    
}

#pragma mark - UI搭建
- (void)setupUI {
    // 顶部总背景
    UIView *topView = [UIView new];
    [topView setFrame:CGRectMake(0, 0, kScreenWidth, kTopViewHeight)];
    topView.userInteractionEnabled = YES;
    topView.backgroundColor = kNavigationBarColor;
    
    // 外圈（点）
    UIImageView *circleOutImageView = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenWidth-kCircleOutImageViewWidth)/2, (kTopViewHeight-kCircleOutImageViewWidth)/2, kCircleOutImageViewWidth, kCircleOutImageViewWidth)];
    circleOutImageView.image = [UIImage imageNamed:@"health_chart_out"];
    circleOutImageView.backgroundColor = [UIColor clearColor];
    circleOutImageView.userInteractionEnabled = YES;
    [topView addSubview:circleOutImageView];
    
    // 圆环进度条
    _bpCircleView = [[DBXCircleView alloc] initWithFrame:CGRectMake((kCircleOutImageViewWidth-kBloodPressViewWidth)/2, (kCircleOutImageViewWidth-kBloodPressViewWidth)/2, kBloodPressViewWidth, kBloodPressViewWidth) gradientLayerColor:[UIColor blueColor] circleWidth:10.0f];
    _bpCircleView.progress = 0;
    _bpCircleView.userInteractionEnabled = YES;
    [circleOutImageView addSubview:_bpCircleView];
    
    // 内圈（圆形闪点线）
    UIImageView *circleInImageView = [[UIImageView alloc] initWithFrame:CGRectMake((kBloodPressViewWidth-kCircleInImageViewWidth)/2, (kBloodPressViewWidth-kCircleInImageViewWidth)/2, kCircleInImageViewWidth, kCircleInImageViewWidth)];
    circleInImageView.image = [UIImage imageNamed:@"health_chart_in"];
    circleInImageView.userInteractionEnabled = YES;
    circleInImageView.backgroundColor = [UIColor clearColor];
    [_bpCircleView addSubview:circleInImageView];
    
    // 圆环内部数值
    UILabel *titleLbl = [UILabel new];
    titleLbl.text = @"血压(mmHg)";
    titleLbl.font = kFont(12);
    titleLbl.textColor = [UIColor whiteColor];
    [titleLbl sizeToFit];
    [circleInImageView addSubview:titleLbl];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(circleInImageView).with.offset(20);
        make.centerX.mas_equalTo(circleInImageView.mas_centerX);
    }];
    
    _bloodPressLbl = [UILabel new];
    _bloodPressLbl.text = @"--/--";
    _bloodPressLbl.font = kBoldFont(22);
    _bloodPressLbl.textColor = [UIColor whiteColor];
    [_bloodPressLbl sizeToFit];
    [circleInImageView addSubview:_bloodPressLbl];
    [_bloodPressLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(circleInImageView.mas_centerX);
        make.centerY.mas_equalTo(circleInImageView.mas_centerY);
    }];
    
    UIButton *enterBtn = [UIButton new];
    [enterBtn setTitle:@"点击进入" forState:UIControlStateNormal];
    [enterBtn setImage:[UIImage imageNamed:@"health_chart_arrow"] forState:UIControlStateNormal];
    [enterBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    enterBtn.adjustsImageWhenHighlighted = NO;
    [enterBtn addTarget:self action:@selector(handleEnterBtnEvent:) forControlEvents:UIControlEventTouchUpInside];
    enterBtn.titleLabel.font = kFont(12);
    [enterBtn sizeToFit];
    [circleInImageView addSubview:enterBtn];
    [enterBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(circleInImageView).with.offset(-20);
        make.centerX.equalTo(circleInImageView.mas_centerX);
    }];
    [enterBtn layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleRight imageTitleSpace:2];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-self.tabBarController.tabBar.height) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.estimatedRowHeight = 0;
    _tableView.estimatedSectionHeaderHeight = 0;
    _tableView.estimatedSectionFooterHeight = 0;
    [_tableView registerClass:[DBXDataBpTableViewCell class] forCellReuseIdentifier:kDataBpTableViewCellIdentifier];
    [_tableView registerClass:[DBXDataBoTableViewCell class] forCellReuseIdentifier:kDataBoTableViewCellIdentifier];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    // 解决滑动视图顶部空出状态栏高度的问题
    if (@available(iOS 11.0, *)) {
        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)]) {
            self.edgesForExtendedLayout = UIRectEdgeNone;
        }
    }
    
    _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRefresh)];
    
    [self.view addSubview:_tableView];
    
    _tableView.tableHeaderView = topView;
}

#pragma mark - action事件
/**
 点击进入
 */
- (void)handleEnterBtnEvent:(UIButton *)button
{
    DBXDataDetailViewController *dataDetailVC = [DBXDataDetailViewController new];
    dataDetailVC.title = @"血压";
    [self.navigationController pushViewController:dataDetailVC animated:YES];
}

// 下拉刷新
- (void)headerRefresh {
    [self startRequsetBloodPressApi];     // 请求血压api
    [self startRequsetHeartBeatApi];      // 请求心率api
    [self startRequsetBloodSugarApi];     // 请求血糖api
    [self startRequsetOxygenApi];         // 请求血氧api
}

#pragma mark - 请求Api

// 请求血压数据
- (void)startRequsetBloodPressApi {
    
    DBXDataBpApi *api = [[DBXDataBpApi alloc] initWithCount:1 doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [self.tableView.mj_header endRefreshing];
        
        DBXBloodPressModel *model = [DBXBloodPressModel yy_modelWithJSON:request.responseJSONObject];
        if (!kArrayIsEmpty(model.data)) {
            NSArray *array = [NSArray yy_modelArrayWithClass:[DBXBloodPressModelData class] json:model.data];
            _bloodPressModelData = array.firstObject;
            _bloodPressLbl.text = [NSString stringWithFormat:@"%@/%@", _bloodPressModelData.sPressure, _bloodPressModelData.dPressure];
            _bpCircleView.progress = [_bloodPressModelData.sPressure doubleValue]/300;  // 300为高压最大值
        } else {
            _bloodPressModelData = nil;
            _bloodPressLbl.text = @"-";
        }
        
        // 刷新cell
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
  
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        _bloodPressModelData = nil;
        _bloodPressLbl.text = @"-";
        // 刷新cell
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
        [self.tableView.mj_header endRefreshing];
    }];
}

// 请求心率数据
- (void)startRequsetHeartBeatApi {
    DBXDataHeartbeatApi *api = [[DBXDataHeartbeatApi alloc] initWithCount:1 doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        DBXHeartBeatModel *model = [DBXHeartBeatModel yy_modelWithJSON:request.responseJSONObject];
        
        if (!kArrayIsEmpty(model.data)) {
            NSArray *array = [NSArray yy_modelArrayWithClass:[DBXHeartBeatModelData class] json:model.data];
            _heartBeatModelData = array.firstObject;
            
        } else {
            _heartBeatModelData = nil;
        }

        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        _heartBeatModelData = nil;

        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }];
}

// 请求血氧数据
- (void)startRequsetOxygenApi {
    DBXDataOxygenApi *api = [[DBXDataOxygenApi alloc] initWithCount:1 doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        DBXBloodOxygenModel *model = [DBXBloodOxygenModel yy_modelWithJSON:request.responseJSONObject];
        
        if (!kArrayIsEmpty(model.data)) {
            NSArray *array = [NSArray yy_modelArrayWithClass:[DBXBloodOxygenModelData class] json:model.data];
            _bloodOxygenModelData = array.firstObject;
            
        } else {
            _bloodOxygenModelData = nil;
        }
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:3 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        _bloodOxygenModelData = nil;
 
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:3 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }];
}

// 请求血糖数据
- (void)startRequsetBloodSugarApi {
    ZJAllBloodSugarApi *bloodSugarApi = [[ZJAllBloodSugarApi alloc] initWithCount:1 doa:0];
    [bloodSugarApi startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        DBXBloodSugarModel *model = [DBXBloodSugarModel yy_modelWithJSON:request.responseJSONObject];
        
        if (!kArrayIsEmpty(model.data)) {
            NSArray *array = [NSArray yy_modelArrayWithClass:[DBXBloodSugarModelData class] json:model.data];
            _bloodSugarModelData = array.firstObject;
        } else {
            
            _bloodSugarModelData = nil;
        }
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        _bloodSugarModelData = nil;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }];
}


#pragma mark - tableView Delegate and DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) { // 血压
        
        DBXDataBpTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDataBpTableViewCellIdentifier forIndexPath:indexPath];
        
        [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
        
        return cell;
        
    } else {
        
        DBXDataBoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDataBoTableViewCellIdentifier forIndexPath:indexPath];
        
        [self setupModelOfDataBoCell:cell cellForRowAtIndexPath:indexPath];
        
        return cell;
    
    }
}


/**
 血糖 血氧 心率模型cell设置
 
 @param cell DBXDataBoTableViewCell
 @param indexPath indexPath
 */
- (void)setupModelOfDataBoCell:(DBXDataBoTableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // 心率
    if (indexPath.row == 1) {
        
        cell.warnButton.hidden = YES;
        cell.titleLbl.text = @"心率";
        cell.iconImageView.image = [UIImage imageNamed:@"health_pulse_icon"];
        cell.barImageView.image = [UIImage imageNamed:@"health_pulse_line"];
        
        if (!kObjectIsEmpty(_heartBeatModelData)) {
            
            cell.dateLbl.hidden = NO;
            cell.subTitleLbl.text = [NSString stringWithFormat:@"%@ bpm", _heartBeatModelData.pulse];
            cell.dateLbl.text = _heartBeatModelData.upsaveTime;
            
        } else {
            cell.subTitleLbl.text = @"- bpm";
            cell.dateLbl.hidden = YES;
        } 
    }
    
    // 血糖
    if (indexPath.row == 2) {
        
        cell.iconImageView.image = [UIImage imageNamed:@"health_bg_icon"];
        cell.barImageView.image = [UIImage imageNamed:@"health_bg_line"];
        cell.titleLbl.text = @"血糖";
        if (!kObjectIsEmpty(_bloodSugarModelData)) {
            cell.warnButton.hidden = NO;
            cell.dateLbl.hidden = NO;
            cell.subTitleLbl.text = [NSString stringWithFormat:@"%@ mmol/L", _bloodSugarModelData.bloodGluVal];
            cell.dateLbl.text = _bloodSugarModelData.upsaveTime;
            cell.bsCondition = [_bloodSugarModelData.mCondition integerValue];
            cell.bsValue = [_bloodSugarModelData.bloodGluVal doubleValue];
        } else {
            cell.dateLbl.hidden = YES;
            cell.warnButton.hidden = YES;
            cell.subTitleLbl.text = @"- mmol/L";
        }
        
    }
    
    // 血氧
    if (indexPath.row == 3) {
        
        cell.titleLbl.text = @"血氧";
        cell.iconImageView.image = [UIImage imageNamed:@"health_oxygen_icon"];
        cell.barImageView.image = [UIImage imageNamed:@"health_oxygen_line"];
        
        if (!kObjectIsEmpty(_bloodOxygenModelData)) {
            cell.dateLbl.hidden = NO;
            cell.warnButton.hidden = NO;
            cell.subTitleLbl.text = [NSString stringWithFormat:@"%@ %%", _bloodOxygenModelData.oxygen];
            cell.dateLbl.text = _bloodOxygenModelData.upsaveTime;
            cell.boValue = [_bloodOxygenModelData.oxygen doubleValue];
        } else {
            cell.subTitleLbl.text = @"- %";
            cell.warnButton.hidden = YES;
        }
    }

}

/**
 血压模型cell设置

 */
- (void)setupModelOfCell:(DBXDataBpTableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
    if (!kObjectIsEmpty(_bloodPressModelData)) {
        
        cell.sbpWarnButton.hidden = NO;
        cell.dbpWarnButton.hidden = NO;
        cell.dateLbl.hidden = NO;
        
        cell.sbpValue = [_bloodPressModelData.sPressure doubleValue];
        cell.dbpValue = [_bloodPressModelData.dPressure doubleValue];
        cell.dateLbl.text = _bloodPressModelData.upsaveTime;
        
    } else {
        
        cell.sbpWarnButton.hidden = YES;
        cell.dbpWarnButton.hidden = YES;
        cell.dateLbl.hidden = YES;
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) { // 血压
        
        return [tableView fd_heightForCellWithIdentifier:kDataBpTableViewCellIdentifier cacheByIndexPath:indexPath configuration:^(id cell) {
            
            [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
        }];
        
    } else  {
        
        return [tableView fd_heightForCellWithIdentifier:kDataBoTableViewCellIdentifier cacheByIndexPath:indexPath configuration:^(id cell) {
            
            [self setupModelOfDataBoCell:cell cellForRowAtIndexPath:indexPath];
        }];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DBXDataDetailViewController *dataDetailVC = [DBXDataDetailViewController new];
    
    if (indexPath.row == 0) {
        dataDetailVC.navigationItem.title = @"血压";
        [self.navigationController pushViewController:dataDetailVC animated:YES];
    } else if (indexPath.row == 1) {
        dataDetailVC.navigationItem.title = @"心率";
        [self.navigationController pushViewController:dataDetailVC animated:YES];
    } else if (indexPath.row == 2) {
        ZJDataBSDetailViewController *bsDetailVC = [ZJDataBSDetailViewController new];
        [self.navigationController pushViewController:bsDetailVC animated:YES];
    } else {
        dataDetailVC.navigationItem.title = @"血氧";
        [self.navigationController pushViewController:dataDetailVC animated:YES];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
