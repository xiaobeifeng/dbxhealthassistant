//
//  DBXBloodSugarHistoryTableViewCell.h
//  DBXHealthAssistant
//
//  Created by zhoujian on 2018/8/31.
//  Copyright © 2018年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBXBloodSugarHistoryTableViewCell : UITableViewCell

@property(nonatomic, copy) NSString *date;
@property(nonatomic, copy) NSString *bloodSugar;
@property(nonatomic, assign) BOOL isBsWarn;

@end
