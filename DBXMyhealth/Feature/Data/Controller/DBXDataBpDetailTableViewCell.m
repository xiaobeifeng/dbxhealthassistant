//
//  DBXDataBpDetailTableViewCell.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/6.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXDataBpDetailTableViewCell.h"

@interface DBXDataBpDetailTableViewCell()

@property(nonatomic, strong) UILabel *dateLbl;
@property(nonatomic, strong) UILabel *sPressValueLbl;
@property(nonatomic, strong) UILabel *dPressValueLbl;
@property(nonatomic, strong) UILabel *warnInfoLbl;
@property(nonatomic, strong) UIView *contantBackgroundView;
@property(nonatomic, strong) UIView *lineView;
@property(nonatomic, strong) UIView *pointView;

@end

@implementation DBXDataBpDetailTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.contentView.backgroundColor = kTableSeparatorLineColor;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _contantBackgroundView = [UIView new];
        kViewRadius(_contantBackgroundView, 5.0f);
        _contantBackgroundView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_contantBackgroundView];
        
        _dateLbl = [UILabel new];
        _dateLbl.font = kFont(12);
        _dateLbl.textColor = [UIColor colorWithRed:0.44 green:0.36 blue:0.52 alpha:1.00];
        [self.contentView addSubview:_dateLbl];
        
        _sPressValueLbl = [UILabel new];
        _sPressValueLbl.backgroundColor = [UIColor clearColor];
        _sPressValueLbl.textColor = [UIColor colorWithRed:0.44 green:0.36 blue:0.52 alpha:1.00];
        _sPressValueLbl.textAlignment = NSTextAlignmentCenter;
        [_contantBackgroundView addSubview:_sPressValueLbl];
        
        _dPressValueLbl = [UILabel new];
        _dPressValueLbl.backgroundColor = [UIColor clearColor];
        _dPressValueLbl.textColor = [UIColor colorWithRed:0.44 green:0.36 blue:0.52 alpha:1.00];
        _dPressValueLbl.textAlignment = NSTextAlignmentCenter;
        [_contantBackgroundView addSubview:_dPressValueLbl];
        
        _warnInfoLbl = [UILabel new];
        _warnInfoLbl.font = kFont(12);
        _warnInfoLbl.backgroundColor = [UIColor clearColor];
        _warnInfoLbl.textColor = [UIColor colorWithRed:0.44 green:0.36 blue:0.52 alpha:1.00];
        _warnInfoLbl.text = @"心率正常，请继续保持";
        [_contantBackgroundView addSubview:_warnInfoLbl];
        
        _lineView = [UIView new];
        _lineView.backgroundColor = kNavigationBarColor;
        [self.contentView addSubview:_lineView];
        
        _pointView = [UIView new];
        kViewRadius(_pointView, 5);
        _pointView.backgroundColor = [UIColor colorWithHexString:@"B956D2"];
        [self.contentView addSubview:_pointView];
        
    }
    
    return self;
}

+ (BOOL)requiresConstraintBasedLayout
{
    return YES;
}

- (void)updateConstraints
{
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView);
        make.bottom.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView).with.offset(34);
        make.width.mas_equalTo(2);
    }];
    
    [_pointView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(10);
        make.centerX.mas_equalTo(self.lineView.mas_centerX);
        make.centerY.mas_equalTo(self.lineView.mas_centerY);
    }];
    
    [_contantBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(13);
        make.left.mas_equalTo(70);
        make.right.mas_equalTo(-13);
        make.height.mas_equalTo(80);
        make.bottom.mas_equalTo(-13);
    }];
    
    [_dateLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contantBackgroundView);
        make.width.mas_equalTo(self.contantBackgroundView.mas_width).multipliedBy(0.9);
        make.height.mas_equalTo(20);
        make.centerX.mas_equalTo(self.contantBackgroundView.mas_centerX);
    }];
    
    [_sPressValueLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.dateLbl.mas_bottom);
        make.width.mas_equalTo(self.contantBackgroundView.mas_width).multipliedBy(0.5);
        make.height.mas_equalTo(40);
        make.left.mas_equalTo(self.contantBackgroundView.mas_left);
    }];
    
    [_dPressValueLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.dateLbl.mas_bottom);
        make.width.mas_equalTo(self.contantBackgroundView.mas_width).multipliedBy(0.5);;
        make.height.mas_equalTo(40);
        make.left.mas_equalTo(self.sPressValueLbl.mas_right);
    }];
    
    [_warnInfoLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.sPressValueLbl.mas_bottom);
        make.width.mas_equalTo(self.contantBackgroundView.mas_width).multipliedBy(0.9);
        make.height.mas_equalTo(20);
        make.centerX.mas_equalTo(self.contantBackgroundView.mas_centerX);
    }];
    
    [super updateConstraints];
}

- (void)setSPressValue:(NSString *)sPressValue
{
    
    if (kStringIsEmpty(sPressValue)) {
        _sPressValueLbl.text = @"--";
    } else {
        // 设置显示值
        NSMutableAttributedString *mValue = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", sPressValue]];
        [mValue addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:NSMakeRange(0, sPressValue.length)];
        
        NSDictionary *dictionary = @{NSForegroundColorAttributeName:[UIColor colorWithRed:0.44 green:0.36 blue:0.52 alpha:1.00], NSFontAttributeName:[UIFont systemFontOfSize:12]};
        NSMutableAttributedString *mUnit = [[NSMutableAttributedString alloc] initWithString:@" mmHg"];
        [mUnit addAttributes:dictionary range:NSMakeRange(0, 5)];
        
        [mValue appendAttributedString:mUnit];
        
        _sPressValueLbl.attributedText = mValue;
    }
    
}

- (void)setDPressValue:(NSString *)dPressValue
{
    if (kStringIsEmpty(dPressValue)) {
        _dPressValueLbl.text = @"--";
    } else {
        // 设置显示值
        NSMutableAttributedString *mValue = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", dPressValue]];
        [mValue addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:NSMakeRange(0, dPressValue.length)];
        
        NSDictionary *dictionary = @{NSForegroundColorAttributeName:[UIColor colorWithRed:0.44 green:0.36 blue:0.52 alpha:1.00], NSFontAttributeName:[UIFont systemFontOfSize:12]};
        NSMutableAttributedString *mUnit = [[NSMutableAttributedString alloc] initWithString:@" mmHg"];
        [mUnit addAttributes:dictionary range:NSMakeRange(0, 5)];
        
        [mValue appendAttributedString:mUnit];
        
        _dPressValueLbl.attributedText = mValue;
    }
    
}

- (void)setDate:(NSString *)date
{
    _dateLbl.text = date;
}

- (void)setIsPressWarn:(BOOL)isPressWarn
{
    if (isPressWarn) {
        _warnInfoLbl.text = @"血压不正常，请咨询专业医师";
        _warnInfoLbl.textColor = [UIColor orangeColor];
        _pointView.backgroundColor = [UIColor colorWithHexString:@"B956D2"];
    } else {
        _warnInfoLbl.text = @"血压正常，请继续保持";
        _warnInfoLbl.textColor = [UIColor colorWithRed:0.44 green:0.36 blue:0.52 alpha:1.00];
        _pointView.backgroundColor = [UIColor colorWithHexString:@"0499FC"];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
