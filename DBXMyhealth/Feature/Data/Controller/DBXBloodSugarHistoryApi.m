//
//  DBXBloodSugarHistoryApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/12.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXBloodSugarHistoryApi.h"

@interface DBXBloodSugarHistoryApi()
{
    NSString *_selectTime;
    NSInteger _currentPage;
}
@end

@implementation DBXBloodSugarHistoryApi
- (id)initWithSelectTime:(NSString *)selectTime
             currentPage:(NSInteger)currentPage
{
    
    self = [super init];
    if (self) {
        _selectTime = selectTime;
        _currentPage = currentPage;
    }
    return self;
}

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return @"/AppService/lohas/health/glupagebytime";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

// 请求体
- (id)requestArgument {
    return @{
             @"selecttime": _selectTime,
             @"doa": @(0),
             @"pagesize": @(10),
             @"pagenow": @(_currentPage)
             };
}

@end
