//
//  ZJCheckVersionTool.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/12/28.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "ZJCheckVersionTool.h"
#import "ZJVersionModel.h"
#import "ZJVersionApi.h"

@interface ZJCheckVersionTool ()

@end

@implementation ZJCheckVersionTool

+ (void)checkVersion {
    ZJVersionApi *api = [[ZJVersionApi alloc] init];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", [request.responseJSONObject jsonPrettyStringEncoded]);
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            ZJVersionModel *model = [ZJVersionModel yy_modelWithJSON:request.responseJSONObject];
            NSArray *array = [NSArray yy_modelArrayWithClass:[ZJVersionModelData class] json:model.data];
            
            if (kArrayIsEmpty(array)) {
//                [MBProgressHUD showTipMessageInView:@"请稍后再试"];
                return;
            }
            
            ZJVersionModelData *dataModel = array.firstObject;
            NSString *serverVersionNum = dataModel.version_id;  // 服务器上的版本号
            BOOL isForce = [dataModel.state boolValue];         // 是否强制更新
            NSString *updataMessage = dataModel.updataInfo;     // 更新信息
            
            NSLog(@"kLocalVersionString - %@", kLocalVersionString);
            
            NSComparisonResult result = [serverVersionNum compare:kLocalVersionString];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // 服务器的版本号大于本地版本号，进行更新提示
                if (result == NSOrderedDescending) {
                    NSLog(@"更新提示");
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"检测到新版本" message:updataMessage preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"立即更新" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://itunes.apple.com/app/id%@?mt=8", kAppStoreId]]];
                    }];
                    
                    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    
                    [alertController addAction:okAction];
                    
                    if (!isForce) {
                        [alertController addAction:cancelAction];
                    }
                    
                    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
                    
                } else {
                    NSLog(@"不更新");
                }
            });
            
        });
        
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [MBProgressHUD showTipMessageInView:@"请稍后再试"];
        
    }];
}

@end
