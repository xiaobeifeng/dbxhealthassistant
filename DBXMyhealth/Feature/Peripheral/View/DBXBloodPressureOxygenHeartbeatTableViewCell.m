//
//  DBXBloodPressureOxygenHeartbeatTableViewCell.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/27.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXBloodPressureOxygenHeartbeatTableViewCell.h"

@interface DBXBloodPressureOxygenHeartbeatTableViewCell()

@property(nonatomic, strong) UILabel *bloodPressureLbl;
@property(nonatomic, strong) UILabel *bloodOxygenLbl;
@property(nonatomic, strong) UILabel *heartbeatLbl;
@property(nonatomic, strong) UILabel *timeLbl;
@property(nonatomic, strong) UIView *views;
@property(nonatomic, strong) UIView *line;

@end

@implementation DBXBloodPressureOxygenHeartbeatTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        // 取消选中的蓝色背景
        self.multipleSelectionBackgroundView = [UIView new];
        
        _views = [UIView new];
        _views.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_views];
        
        _bloodPressureLbl = [UILabel new];
        _bloodPressureLbl.font = kFont(12);
        _bloodPressureLbl.textAlignment = NSTextAlignmentCenter;
        _bloodPressureLbl.backgroundColor = [UIColor whiteColor];
        [_views addSubview:_bloodPressureLbl];
        
        _bloodOxygenLbl = [UILabel new];
        _bloodOxygenLbl.font = kFont(12);
        _bloodOxygenLbl.textAlignment = NSTextAlignmentCenter;
        _bloodOxygenLbl.backgroundColor = [UIColor whiteColor];
        [_views addSubview:_bloodOxygenLbl];

        _heartbeatLbl = [UILabel new];
        _heartbeatLbl.font = kFont(12);
        _heartbeatLbl.textAlignment = NSTextAlignmentCenter;
        _heartbeatLbl.backgroundColor = [UIColor whiteColor];
        [_views addSubview:_heartbeatLbl];
        
        _timeLbl = [UILabel new];
        _timeLbl.font = kFont(12);
        _timeLbl.numberOfLines = 0;
        _timeLbl.textAlignment = NSTextAlignmentCenter;
        _timeLbl.backgroundColor = [UIColor whiteColor];
        [_views addSubview:_timeLbl];
        
        _line = [UIView new];
        _line.backgroundColor = kTableSeparatorLineColor;
        [self.contentView addSubview:_line];

    }
    
    return self;
}

+ (BOOL)requiresConstraintBasedLayout
{
    return YES;
}

- (void)updateConstraints
{
    [_views mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(13);
        make.height.equalTo(@35);
        make.width.right.equalTo(self.contentView);
    }];
    
    [@[_bloodPressureLbl, _bloodOxygenLbl, _heartbeatLbl, _timeLbl] mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:10 leadSpacing:13 tailSpacing:13];
    
    [@[_bloodPressureLbl, _bloodOxygenLbl, _heartbeatLbl, _timeLbl] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.views);
        make.height.equalTo(@35);
    }];
    
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.views.mas_bottom).with.offset(13);
        make.bottom.mas_equalTo(self.contentView);
        make.height.mas_equalTo(0.5);
        make.left.mas_equalTo(self.contentView).with.offset(13);
        make.right.mas_equalTo(self.contentView).with.offset(50);
    }];
    
    [super updateConstraints];
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
}

- (void)setBloodPressure:(NSString *)bloodPressure
{
    if ([bloodPressure isEqualToString:@"0/0"]) {
        _bloodPressureLbl.text = @"--";
    } else {
        _bloodPressureLbl.text = bloodPressure;
    }
    
}

- (void)setBloodOxygen:(NSString *)bloodOxygen
{
    if ([bloodOxygen isEqualToString:@"0"]) {
        _bloodOxygenLbl.text = @"--";
    } else {
        _bloodOxygenLbl.text = bloodOxygen;
    }
    
}

- (void)setHeartbeat:(NSString *)heartbeat
{
    if ([heartbeat isEqualToString:@"0"]) {
        _heartbeatLbl.text = @"--";
    } else {
        _heartbeatLbl.text = heartbeat;
    }
}

- (void)setTime:(NSString *)time
{
    
    _timeLbl.text = time;

}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    [_line setBackgroundColor:kTableSeparatorLineColor];
}

@end
