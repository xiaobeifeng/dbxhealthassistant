//
//  DBXPeripheralTableViewCell.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/23.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXPeripheralTableViewCell.h"

@interface DBXPeripheralTableViewCell()




@end

@implementation DBXPeripheralTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{

    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        _titleLbl = [UILabel new];
        [_titleLbl sizeToFit];
        [self.contentView addSubview:_titleLbl];
        
        _line = [UIView new];
        _line.backgroundColor = kTableSeparatorLineColor;
        [self.contentView addSubview:_line];
        
    }
    
    return self;
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [_titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView).with.offset(13);
        make.right.mas_equalTo(self.contentView).with.offset(-13);
        make.top.mas_equalTo(self.contentView).with.offset(13);
    }];
    
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLbl.mas_bottom).with.offset(13);
        make.bottom.mas_equalTo(self.contentView);
        make.height.mas_equalTo(@0.5);
        make.left.mas_equalTo(self.contentView).with.offset(13);
        make.right.mas_equalTo(self.contentView).with.offset(50);
    }];
}

- (void)setTitle:(NSString *)title
{
    _titleLbl.text = title;   
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
