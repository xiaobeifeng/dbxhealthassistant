//
//  DBXStepNumberTableViewCell.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/27.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBXStepNumberTableViewCell : UITableViewCell

@property(nonatomic, copy) NSString *stepNumber;
@property(nonatomic, copy) NSString *calorie;
@property(nonatomic, copy) NSString *time;

@end
