//
//  DBXStepNumberTableViewCell.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/27.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXStepNumberTableViewCell.h"

@interface DBXStepNumberTableViewCell()

@property(nonatomic, strong) UILabel *stepNumberLbl;
@property(nonatomic, strong) UILabel *calorieLbl;
@property(nonatomic, strong) UILabel *timeLbl;
@property(nonatomic, strong) UIView *views;
@property(nonatomic, strong) UIView *line;

@end

@implementation DBXStepNumberTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        // 取消选中的蓝色背景
        self.multipleSelectionBackgroundView = [UIView new];
        
        _views = [UIView new];
        _views.backgroundColor = [UIColor yellowColor];
        [self.contentView addSubview:_views];
        
        _stepNumberLbl = [UILabel new];
        _stepNumberLbl.font = kCellTitleFont;
        _stepNumberLbl.textAlignment = NSTextAlignmentCenter;
        _stepNumberLbl.backgroundColor = [UIColor redColor];
        [_views addSubview:_stepNumberLbl];
        
        _calorieLbl = [UILabel new];
        _calorieLbl.font = kCellTitleFont;
        _calorieLbl.textAlignment = NSTextAlignmentCenter;
        _calorieLbl.backgroundColor = [UIColor redColor];
        [_views addSubview:_calorieLbl];
        
        _timeLbl = [UILabel new];
        _timeLbl.font = kCellTitleFont;
        _timeLbl.textAlignment = NSTextAlignmentCenter;
        _timeLbl.backgroundColor = [UIColor redColor];
        [_views addSubview:_timeLbl];
        
        _line = [UIView new];
        _line.backgroundColor = kTableSeparatorLineColor;
        [self.contentView addSubview:_line];
    }
    
    return self;
}

+ (BOOL)requiresConstraintBasedLayout
{
    return YES;
}

- (void)updateConstraints
{
    [_views mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(13);
        make.height.equalTo(@20);
        make.width.right.equalTo(self.contentView);
    }];
    
    [@[_stepNumberLbl, _calorieLbl, _timeLbl] mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:10 leadSpacing:13 tailSpacing:13];
    
    [@[_stepNumberLbl, _calorieLbl, _timeLbl] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.views);
        make.height.equalTo(@20);
    }];
    
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.views.mas_bottom).with.offset(13);
        make.bottom.mas_equalTo(self.contentView);
        make.height.mas_equalTo(0.5);
        make.left.mas_equalTo(self.contentView).with.offset(13);
        make.right.mas_equalTo(self.contentView).with.offset(50);
    }];
    
    [super updateConstraints];
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    [_line setBackgroundColor:kTableSeparatorLineColor];
}

@end
