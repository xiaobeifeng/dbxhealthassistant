//
//  DBXBloodPressureOxygenHeartbeatTableViewCell.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/27.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 血压，血氧，心率
 */
@interface DBXBloodPressureOxygenHeartbeatTableViewCell : UITableViewCell

@property(nonatomic, copy) NSString *bloodPressure;
@property(nonatomic, copy) NSString *bloodOxygen;
@property(nonatomic, copy) NSString *heartbeat;
@property(nonatomic, copy) NSString *time;

@end
