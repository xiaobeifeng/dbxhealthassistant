//
//  DBXBloodSugarTableViewCell.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/26.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBXBloodSugarTableViewCell : UITableViewCell

@property(nonatomic, copy) NSString *title;
@property(nonatomic, copy) NSString *subTitle;

@end
