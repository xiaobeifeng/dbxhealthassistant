//
//  DBXBloodSugarTableViewCell.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/26.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXBloodSugarTableViewCell.h"

@interface DBXBloodSugarTableViewCell()

@property(nonatomic, strong) UILabel *titleLbl;
@property(nonatomic, strong) UILabel *subTitleLbl;
@property(nonatomic, strong) UIView *views;
@property(nonatomic, strong) UIView *line;

@end

@implementation DBXBloodSugarTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        // 取消选中的蓝色背景
        self.multipleSelectionBackgroundView = [UIView new];
        
        _views = [UIView new];
        _views.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_views];
        
        _titleLbl = [UILabel new];
        _titleLbl.font = kFont(12);
        _titleLbl.textAlignment = NSTextAlignmentCenter;
        _titleLbl.backgroundColor = [UIColor whiteColor];
        [_views addSubview:_titleLbl];
        
        _subTitleLbl = [UILabel new];
        _subTitleLbl.backgroundColor = [UIColor whiteColor];
        _subTitleLbl.numberOfLines = 0;
        _subTitleLbl.textAlignment = NSTextAlignmentCenter;
        _subTitleLbl.font = kFont(12);
        [_views addSubview:_subTitleLbl];
        
        _line = [UIView new];
        _line.backgroundColor = kTableSeparatorLineColor;
        [self.contentView addSubview:_line];
    }
    
    return self;
}

+ (BOOL)requiresConstraintBasedLayout
{
    return YES;
}

- (void)updateConstraints
{
    [_views mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(13);
        make.height.equalTo(@35);
        make.width.right.equalTo(self.contentView);
    }];
    
    [@[_titleLbl, _subTitleLbl] mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:10 leadSpacing:13 tailSpacing:13];
    
    [@[_titleLbl, _subTitleLbl] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.height.equalTo(@35);
    }];
    
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.views.mas_bottom).with.offset(13);
        make.bottom.mas_equalTo(self.contentView);
        make.height.mas_equalTo(0.5);
        make.left.mas_equalTo(self.contentView).with.offset(13);
        make.right.mas_equalTo(self.contentView).with.offset(50);
    }];
    
    [super updateConstraints];
    
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    
}

- (void)setTitle:(NSString *)title
{
    if (![title isEqualToString:@"NNN"]) {
        _titleLbl.text = title;
    } else {
        _titleLbl.text = @"未测量";
    }
}

- (void)setSubTitle:(NSString *)subTitle
{

    _subTitleLbl.text = subTitle;
 
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    [_line setBackgroundColor:kTableSeparatorLineColor];
}

@end
