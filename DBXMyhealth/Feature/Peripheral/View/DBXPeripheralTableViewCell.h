//
//  DBXPeripheralTableViewCell.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/23.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBXPeripheralTableViewCell : UITableViewCell

@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) UIView *line;
@property(nonatomic, strong) UILabel *titleLbl;

@end
