//
//  DBXPeripheralManagementViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/23.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXPeripheralManagementViewController.h"
#import "DBXPeripheralTableViewCell.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "DBXLoginViewController.h"
#import "DBXLinkyornoneApi.h"
#import "DBXFirstLinkUploadTimeApi.h"
#import "DBXRemovebind.h"
#import "DBXMePersonalinfoApi.h"
#import "DBXMePersonalinfoModel.h"

static NSString *cellIdentifier = @"DBXPeripheralTableViewCell";
static CGFloat const kBindingViewHeight = 80;

@interface DBXPeripheralManagementViewController ()<UITableViewDelegate, UITableViewDataSource, CBCentralManagerDelegate, CBPeripheralDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) CBCentralManager *manager;
/** 保存外设数组 */
@property (nonatomic, strong) NSMutableArray *peripherals;
/** 当前连接外设 */
@property (nonatomic, strong) CBPeripheral *per;
@property (nonatomic, strong) CBCharacteristic *characteristic;
/** 底部绑定按钮 */
@property(nonatomic, strong) UIButton *bindingBtn;
/** 头部绑定图片 */
@property (nonatomic, strong) UIImageView *bindingImageView;
/** 头部绑定信息 */
@property (nonatomic, strong) UILabel *bindingPeripheralNameLabel;
/** 连接超时的定时器 */
@property (nonatomic, strong) NSTimer *connectTimer;

@end

@implementation DBXPeripheralManagementViewController

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setupNav];
    
    [self setupUI];
    
    if (_isTimeCorrect) {
        [self startRequestPersonInfo];
    } else {
        [self setupBlueTooth];
    }
  
}

#pragma mark - UI

- (void)setupNav
{
    if (_isFirstLink) {
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem titleItem:@"切换账号" target:self action:@selector(changeAccountAction)];
    }
}
/**
 搭建UI界面
 */
- (void)setupUI
{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-kSafeAreaTopHeight-kBindingViewHeight) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView registerClass:[DBXPeripheralTableViewCell class] forCellReuseIdentifier:cellIdentifier];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (@available(iOS 11.0, *)) {
        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)]) {
            self.edgesForExtendedLayout = UIRectEdgeNone;
        }
    }
    
    // 防止UITableView下拉出现白色背景
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, -5000, kScreenWidth, 5000)];
    imageView.backgroundColor = kNavigationBarColor;
    [_tableView addSubview:imageView];
    [self.view addSubview:_tableView];
    
    // TableView头部视图
    UIView *headerView = [UIView new];
    headerView.backgroundColor = kNavigationBarColor;
    headerView.frame = CGRectMake(0, 0, kScreenWidth, 250-self.navigationController.navigationBar.height-[UIApplication sharedApplication].statusBarFrame.size.height);
    self.tableView.tableHeaderView = headerView;
    
    // 头部视图图片
    _bindingImageView = [UIImageView new];
    _bindingImageView.backgroundColor = [UIColor clearColor];
    _bindingImageView.image = [UIImage imageNamed:@"peripheral_unbinding"];
    [headerView addSubview:_bindingImageView];
    [_bindingImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(@60);
        make.height.mas_equalTo(@65);
        make.centerX.mas_equalTo(headerView.mas_centerX);
        make.centerY.mas_equalTo(headerView.mas_centerY).with.offset(-20);
    }];
    
    // 头部视图文本
    _bindingPeripheralNameLabel = [UILabel new];
    _bindingPeripheralNameLabel.text = @"暂无绑定设备";
    _bindingPeripheralNameLabel.textColor = [UIColor whiteColor];
    _bindingPeripheralNameLabel.textAlignment = NSTextAlignmentCenter;
    _bindingPeripheralNameLabel.font = [UIFont systemFontOfSize:14];
    [headerView addSubview:_bindingPeripheralNameLabel];
    [_bindingPeripheralNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(@200);
        make.centerX.mas_equalTo(headerView.mas_centerX);
        make.top.mas_equalTo(self.bindingImageView.mas_bottom).with.offset(20);
    }];
    
    // 底部功能栏
    UIView *bindingView = [UIView new];
    bindingView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bindingView];
    [bindingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(kBindingViewHeight);
        make.width.mas_equalTo(kScreenWidth);
        make.bottom.mas_equalTo(self.view);
    }];
    _bindingBtn = [UIButton new];
    [_bindingBtn setTitle:@"绑定设备" forState:UIControlStateNormal];
    [_bindingBtn setTitle:@"解绑设备" forState:UIControlStateSelected];
    [_bindingBtn setTitleColor:kNavigationBarColor forState:UIControlStateNormal];
    kViewBorderRadius(_bindingBtn, 10, 1, kTableSeparatorLineColor);
    _bindingBtn.titleLabel.font = kCellTitleFont;
    _bindingBtn.backgroundColor = [UIColor whiteColor];
    [_bindingBtn addTarget:self action:@selector(handleBindingBtnEvent:) forControlEvents:UIControlEventTouchUpInside];
    [bindingView addSubview:_bindingBtn];
    [_bindingBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bindingView).with.offset(40);
        make.right.mas_equalTo(bindingView).with.offset(-40);
        make.bottom.mas_equalTo(bindingView).with.offset(-20);
        make.top.mas_equalTo(bindingView).with.offset(13);
    }];
}

#pragma mark - 初始化蓝牙
/**
 初始化蓝牙配置
 */
- (void)setupBlueTooth
{
    // 蓝牙初始化
    _manager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue()];
    _peripherals = [NSMutableArray new];
}


#pragma mark - CBCentralManagerDelegate
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    
    if (central.state == CBManagerStatePoweredOn) { // 蓝牙已开启可以正常使用
        
        // 获取本地存储的已经连接过的外设信息
        NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:kAccount];
        NSString *key = [NSString stringWithFormat:@"%@_peripheralIdentifier", userName];
        NSString *identifier = [[NSUserDefaults standardUserDefaults] objectForKey:key];
        
        if (identifier.length != 0) {
            
            // 获取之前连接过的外设
            NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:identifier];
            NSArray *perArray = [_manager retrievePeripheralsWithIdentifiers:@[uuid]];
            
            // 绑定过的设备数量大于0
            if (perArray.count > 0) {
                
                _per = perArray.firstObject;
                
                // 数据写入成功后更换成已经绑定样式
                _bindingPeripheralNameLabel.text = [NSString stringWithFormat:@"已绑定 %@", _per.name];
                _bindingImageView.image = [UIImage imageNamed:@"peripheral_binding"];
                _bindingBtn.selected = YES;
                
                // 若是时间校准功能就进行蓝牙连接，然后校准时间
                if (_isTimeCorrect) {
                    [MBProgressHUD showActivityMessageInView:@"时间同步中"];
                    [_manager connectPeripheral:_per options:nil];
                    // 开一个定时器监控连接超时的情况
                    _connectTimer = [NSTimer scheduledTimerWithTimeInterval:15.0f target:self selector:@selector(connectTimeout) userInfo:nil repeats:NO];
                }
            }
            
        } else { // 开始扫描周围外设
            
            [_manager scanForPeripheralsWithServices:nil options:nil];
            _bindingBtn.selected = NO;
        }
        
    } else if (central.state == CBManagerStatePoweredOff) { // 蓝牙处于关闭状态
        
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"请到系统设置中打开蓝牙" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            // 返回上一级
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [alertVC addAction:okAction];
        [alertVC addAction:cancelAction];
        [self presentViewController:alertVC animated:YES completion:nil];
    }
    
    else {    // 蓝牙未开启或处于不正常状态
        
        [MBProgressHUD showActivityMessageInWindow:@"请检查蓝牙相关设置后再试"];
        // 返回上一级
        [self.navigationController popViewControllerAnimated:YES];
    }
}

/**
 扫描到设备会进入方法
 */
- (void)centralManager:(CBCentralManager *)central
 didDiscoverPeripheral:(CBPeripheral *)peripheral
     advertisementData:(NSDictionary *)advertisementData
                  RSSI:(NSNumber *)RSSI
{
    NSLog(@"当扫描到设备:%@",peripheral.name);
    
    if (peripheral.name.length == 17 && [peripheral.name containsString:@":"]) {
        
        if (![_peripherals containsObject:peripheral]) {
            [_peripherals addObject:peripheral];
            [_tableView reloadData];
        }
        
    }
}

/**
 连接Peripherals成功
 
 @param central 中心设备
 @param peripheral 连接的外设
 */
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    NSLog(@">>>连接到名称为（%@）的设备-成功 %@", peripheral.name, [peripheral identifier]);
    
    // 停止计时器
    [_connectTimer invalidate];
    
    [peripheral setDelegate:self];
    
    [peripheral discoverServices:nil];
}

#pragma mark - CBPeripheralDelegate
// 扫描到Services
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    NSLog(@">>>扫描到服务：%@",peripheral.services);
    if (error) {
        NSLog(@">>>Discovered services for %@ with error: %@", peripheral.name, [error localizedDescription]);
        return;
    }
    
    for (CBService *service in peripheral.services) {
        

        NSLog(@"%@", service.UUID);
        if ([[service.UUID UUIDString] isEqualToString:@"FFF0"]) {
            CBUUID *uuid6 = [CBUUID UUIDWithString:@"FFF6"];
            [peripheral discoverCharacteristics:@[uuid6] forService:service];
        }
    }

}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    if (error) {
        NSLog(@"error Discovered characteristics for %@ with error: %@", service.UUID, [error localizedDescription]);
        return;
    }
    
    NSLog(@"characteristics - %@", service.characteristics);
    if (service.characteristics.count == 0) {
        [MBProgressHUD hideHUD];
        [MBProgressHUD showTipMessageInWindow:@"该设备无法连接，请尝试连接其他设备"];
        return;
    }
    
    for (CBCharacteristic *characteristic in service.characteristics) {
        NSLog(@"service:%@ 的 Characteristic: %@",service.UUID,characteristic.UUID);
        
        NSLog(@"properties - %lu", (unsigned long)characteristic.properties);
        if ([characteristic.UUID.UUIDString isEqualToString:@"FFF6"]) {
            
            _per = peripheral;
            _characteristic = characteristic;
            
            [_per readValueForCharacteristic:_characteristic];
            
            [peripheral setNotifyValue:YES forCharacteristic:characteristic];
            
            NSLog(@"%lu", (unsigned long)characteristic.properties);
            
            // 只有 characteristic.properties 有write的权限才可以写
            if(characteristic.properties & CBCharacteristicPropertyWrite) {
                /*
                 最好一个type参数可以为CBCharacteristicWriteWithResponse或type:CBCharacteristicWriteWithResponse,区别是是否会有反馈
                 */
                NSLog(@"该字段可写！");
                
                // 蓝牙成功连接后上传激活时间（时间同步不进行激活时间上传）
                if (!_isTimeCorrect) {
                    // 上传设备激活号
                    [self requestActiveDeviceNumberApi];
                } else {
                    // 向外设写入数据
                    [self startRequestMePersonalinfoApi];
                }
                
            } else {
                
                NSLog(@"该字段不可写！");
                
            }
            
        }
        
    }

}

/**
 获取Characteristic的值，读到数据会进入方法
 */
- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    NSLog(@"11111");
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    
    NSLog(@"I'm here!!!!!");
    if([characteristic.UUID.UUIDString isEqualToString:@"FFF6"]) {
        // 获取订阅特征回复的数据
        NSData *value = characteristic.value;
        NSString *str = [[NSString alloc] initWithData:value encoding:NSUTF8StringEncoding];
        NSLog(@"蓝牙回复：%@", str);
    }
}

// 连接到Peripherals-失败
-(void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@">>>连接到名称为（%@）的设备-失败,原因:%@",[peripheral name],[error localizedDescription]);
    [MBProgressHUD hideHUD];
}

// Peripherals断开连接
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@">>>外设连接断开连接 %@: %@\n", [peripheral name], [error localizedDescription]);
    
    [MBProgressHUD hideHUD];
    if (error.localizedDescription.length > 0) {
        [MBProgressHUD showTipMessageInWindow:@"连接超时，请重试"];
    }
    
}

#pragma mark - tableView Delegate and DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _peripherals.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DBXPeripheralTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)setupModelOfCell:(DBXPeripheralTableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBPeripheral *peripheral = _peripherals[indexPath.row];
    
    cell.title = peripheral.name;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView fd_heightForCellWithIdentifier:cellIdentifier cacheByIndexPath:indexPath configuration:^(id cell) {
        
        [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _per = _peripherals[indexPath.row];
}

#pragma mark - 接口请求
/**
 上传激活设备号
 */
- (void)requestActiveDeviceNumberApi
{
    NSString *deviceCode = [_per.name stringByReplacingOccurrencesOfString:@":" withString:@""];
    DBXLinkyornoneApi *api = [[DBXLinkyornoneApi alloc] initWithDeviceCode:deviceCode];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [MBProgressHUD hideHUD];
        NSLog(@"%@", request.responseJSONObject);
        if (request.responseJSONObject) {
            NSDictionary *dic = request.responseJSONObject;
            
            // 设备未激活或未建立链接的情况下，第一次链接上传链接时间
            if ([dic[@"state"] integerValue] == 1043 || [dic[@"state"] integerValue] == 1041) {
                [self uploadFirstLinkTimeWithDeviceCode:deviceCode];
            } else {
                
                // 存储蓝牙连接信息
                NSString *identifier = [self.per name];
                NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:kAccount];
                NSString *key = [NSString stringWithFormat:@"%@_peripheralIdentifier", userName];
                [[NSUserDefaults standardUserDefaults] setObject:identifier forKey:key];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                // 向外设写入数据
                [self startRequestMePersonalinfoApi];
                
                UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"设备绑定成功" preferredStyle:UIAlertControllerStyleAlert];
                
                __weak __typeof(self)weakSelf = self;
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    if (self.isFirstLink) {
                        [UIApplication sharedApplication].keyWindow.rootViewController = [DBXBaseTabBarController new];
                        
                    } else {
                        [weakSelf.navigationController popViewControllerAnimated:YES];
                    }
                    
                }];
                
                [alertVC addAction:okAction];
                
                [self presentViewController:alertVC animated:YES completion:nil];
            }
            
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
     
        [MBProgressHUD hideHUD];
    }];
    
}

/**
 上传当前连接时间
 */
- (void)uploadFirstLinkTimeWithDeviceCode:(NSString *)deviceCode
{
    // 获取当前时间
    NSDateFormatter *formatterYearAndMonth = [[NSDateFormatter alloc] init];
    formatterYearAndMonth.dateFormat = @"YYYY-MM-dd HH:mm:ss";
    NSString *firstLinkTime = [formatterYearAndMonth stringFromDate:[NSDate date]];
    DBXFirstLinkUploadTimeApi *api = [[DBXFirstLinkUploadTimeApi alloc] initWithDeviceCode:deviceCode firstLinkTime:firstLinkTime];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        if (request.responseJSONObject) {
            
            NSDictionary *dic = request.responseJSONObject;
            
            NSInteger state = [dic[@"state"] integerValue];
            if (state == 0) {   // 成功

                // 存储蓝牙连接信息
                NSString *identifier = [self.per.identifier UUIDString];
                NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:kAccount];
                NSString *key = [NSString stringWithFormat:@"%@_peripheralIdentifier", userName];
                [[NSUserDefaults standardUserDefaults] setObject:identifier forKey:key];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                // 向外设写入数据
                [self startRequestMePersonalinfoApi];
                
                UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"设备绑定成功" preferredStyle:UIAlertControllerStyleAlert];
                
                __weak __typeof(self)weakSelf = self;
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    if (self.isFirstLink) {
                        [UIApplication sharedApplication].keyWindow.rootViewController = [DBXBaseTabBarController new];
                        
                    } else {
                        [weakSelf.navigationController popViewControllerAnimated:YES];
                    }

                }];
                
                [alertVC addAction:okAction];
                
                [self presentViewController:alertVC animated:YES completion:nil];
                
            } else {
                
           
                [MBProgressHUD showTipMessageInWindow:dic[@"message"]];
                
            }
            
            
            
        } else {
     
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
     
    }];
    
}

/**
 设备解绑
 */
- (void)reuquestDeviceDisBindApi
{
    
    NSString *deviceCode = [_per.name stringByReplacingOccurrencesOfString:@":" withString:@""];
    DBXRemovebind *api = [[DBXRemovebind alloc] initWithDeviceCode:deviceCode];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        NSString *message = request.responseJSONObject[@"message"];
        [MBProgressHUD showTipMessageInWindow:message];
        
        NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:kAccount];
        NSString *key = [NSString stringWithFormat:@"%@_peripheralIdentifier", userName];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        // 更换成已经未绑定样式
        self.bindingPeripheralNameLabel.text = @"暂无绑定设备";
        self.bindingImageView.image = [UIImage imageNamed:@"peripheral_unbinding"];
        self.bindingBtn.selected = NO;
        
        // 断开蓝牙与外设的连接
        [self.manager cancelPeripheralConnection:self.per];
        
        // 开始扫描周围外设
        [self.manager scanForPeripheralsWithServices:nil options:nil];
        
        // 外设置空
        self.per = nil;
        
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        
    }];
    
}

/**
 获取个人信息
 */
- (void)startRequestPersonInfo
{
    DBXMePersonalinfoApi *api = [[DBXMePersonalinfoApi alloc] init];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        DBXMePersonalinfoModel *model = [DBXMePersonalinfoModel yy_modelWithJSON:request.responseJSONObject];
        NSArray *array = [NSArray yy_modelArrayWithClass:[DBXMePersonalinfoModelData class] json:model.data];
        
        DBXMePersonalinfoModelData *modelData;
        if (!kArrayIsEmpty(array)) {
            modelData = array.firstObject;
            
            if (kStringIsEmpty(modelData.sex) || kStringIsEmpty(modelData.birth)) {
                
                UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"请完善个人信息后再试" message:@"" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    [self.navigationController popViewControllerAnimated:YES];
                    
                }];
                
                [alertVC addAction:okAction];
                
                [self presentViewController:alertVC animated:YES completion:nil];
                
            } else {
                
                // 蓝牙初始化
                self.manager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue()];
                self.peripherals = [NSMutableArray new];
                
            }
            
        } else {
            
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"请完善个人信息后再试" message:@"" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [self.navigationController popViewControllerAnimated:YES];
                
            }];
            
            [alertVC addAction:okAction];
            
            [self presentViewController:alertVC animated:YES completion:nil];
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}


/**
 获取身高体重年龄性别信息
 */
- (void)startRequestMePersonalinfoApi
{
    
    // 获取身高体重
    DBXMePersonalinfoApi *api = [DBXMePersonalinfoApi new];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        DBXMePersonalinfoModel *model = [DBXMePersonalinfoModel yy_modelWithJSON:request.responseJSONObject];
        NSArray *array = [NSArray yy_modelArrayWithClass:[DBXMePersonalinfoModelData class] json:model.data];
        
        DBXMePersonalinfoModelData *modelData;
        if (!kArrayIsEmpty(array)) {
            
            modelData = array.firstObject;
            NSInteger height = [modelData.hight integerValue];
            NSInteger weight = [modelData.weight integerValue];
            
            NSInteger sex;
            if ([modelData.sex isEqualToString:@"男"]) {
                sex = 1;
            } else {
                sex = 0;
            }
            
            // 获取当前时间
            NSDate *currentDate = [NSDate date];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *currentDateStr = [dateFormatter stringFromDate:currentDate];
            NSInteger currentYear = [[currentDateStr substringWithRange:NSMakeRange(0, 4)] integerValue];
            NSString *birth = modelData.birth;
            NSString *year_birth = [birth substringWithRange:NSMakeRange(0, 4)];
            NSInteger age = currentYear - [year_birth integerValue];
            
            [self writeDataToWatchWithHeight:height weight:weight sex:sex age:age];
            
        } else {
            
            [self writeDataToWatchWithHeight:170 weight:65 sex:0 age:0];
            
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [MBProgressHUD showTipMessageInWindow:@"请稍后再试"];
    }];
    
}

#pragma mark - 其他执行事件
/**
 设备连接超时时间
 */
- (void)connectTimeout
{
    [MBProgressHUD hideHUD];
    [MBProgressHUD showTipMessageInWindow:@"设备连接超时"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.manager cancelPeripheralConnection:self.per];
        [self.navigationController popViewControllerAnimated:YES];
    });
}


/**
 向手表写入数据
 
 @param height 身高
 @param weight 体重
 @param sex 男1 女0
 @param age 年龄
 */
- (void)writeDataToWatchWithHeight:(NSInteger)height weight:(NSInteger)weight sex:(NSInteger)sex age:(NSInteger)age
{
    // 获取当前时间
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *currentDateStr = [dateFormatter stringFromDate:currentDate];
    // 2018-09-05 14:25:46
    NSInteger year = [[currentDateStr substringWithRange:NSMakeRange(2, 2)] integerValue];
    NSInteger month = [[currentDateStr substringWithRange:NSMakeRange(5, 2)] integerValue];
    NSInteger day = [[currentDateStr substringWithRange:NSMakeRange(8, 2)] integerValue];
    //    NSInteger day = 17;
    NSInteger hour = [[currentDateStr substringWithRange:NSMakeRange(11, 2)] integerValue];
    NSInteger minute = [[currentDateStr substringWithRange:NSMakeRange(14, 2)] integerValue];
    NSInteger second = [[currentDateStr substringWithRange:NSMakeRange(17, 2)] integerValue];
    
    // 校验当前时间
    // 年
    Byte yearByte[2];
    yearByte[0] = 0xcc;
    yearByte[1] = (Byte)year;
    
    // 月
    Byte monthByte[2];
    monthByte[0] = 0xcc;
    monthByte[1] = (Byte)month;
    
    // 日
    Byte dayByte[2];
    dayByte[0] = 0xcc;
    dayByte[1] = (Byte)day;
    
    // 时
    Byte hourByte[2];
    hourByte[0] = 0xcc;
    hourByte[1] = (Byte)hour;
    
    // 分
    Byte minuteByte[2];
    minuteByte[0] = 0xcc;
    minuteByte[1] = (Byte)minute;
    
    // 秒
    Byte secondByte[2];
    secondByte[0] = 0xcc;
    secondByte[1] = (Byte)second;
    
    // 当前星期
    Byte weekByte[2];
    weekByte[0] = 0xcc;
    weekByte[1] = (Byte)([self getCurrentWeekDay]);

    
    // 身高
    Byte heightByte[2];
    heightByte[0] = 0xdd;
    heightByte[1] = (Byte)height;
    
    // 体重
    Byte weightByte[2];
    weightByte[0] = 0xdd;
    weightByte[1] = (Byte)weight;
    
    // 性别
    Byte sexByte[2];
    sexByte[0] = 0xdd;
    sexByte[1] = (Byte)sex;
    
    // 年龄
    Byte ageByte[2];
    ageByte[0] = 0xdd;
    ageByte[1] = (Byte)20;
    
    // 开始写入数据
    [_per writeValue:[NSData dataWithBytes:&yearByte length:sizeof(yearByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithResponse];
    [_per writeValue:[NSData dataWithBytes:&monthByte length:sizeof(monthByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithResponse];
    [_per writeValue:[NSData dataWithBytes:&dayByte length:sizeof(dayByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithResponse];
    [_per writeValue:[NSData dataWithBytes:&hourByte length:sizeof(hourByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithResponse];
    [_per writeValue:[NSData dataWithBytes:&minuteByte length:sizeof(minuteByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithResponse];
    [_per writeValue:[NSData dataWithBytes:&secondByte length:sizeof(secondByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithResponse];
    [_per writeValue:[NSData dataWithBytes:&weekByte length:sizeof(weekByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithResponse];
    [_per writeValue:[NSData dataWithBytes:&heightByte length:sizeof(heightByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithResponse];
    [_per writeValue:[NSData dataWithBytes:&weightByte length:sizeof(weightByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithResponse];
    [_per writeValue:[NSData dataWithBytes:&sexByte length:sizeof(sexByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithResponse];
    [_per writeValue:[NSData dataWithBytes:&ageByte length:sizeof(ageByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithResponse];// 获取当前时间
    
    // 数据写入成功后更换成已经绑定样式
    _bindingPeripheralNameLabel.text = [NSString stringWithFormat:@"已绑定 %@", _per.name];
    _bindingImageView.image = [UIImage imageNamed:@"peripheral_binding"];
    // 绑定按钮变成选中状态
    _bindingBtn.selected = YES;
    
    // 停止扫描外设
    [_manager stopScan];
    // 断开蓝牙连接
//    [_manager cancelPeripheralConnection:_per];
    [_peripherals removeAllObjects];
    [_tableView reloadData];
    
    if (_isTimeCorrect) {   // 时间同步完成提示
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [MBProgressHUD showTipMessageInWindow:@"时间同步完成"];
            [self.navigationController popViewControllerAnimated:YES];
        });
    }
    
}

/**
 切换账号
 */
- (void)changeAccountAction
{
    DBXBaseNavigationController *baseNavC = [[DBXBaseNavigationController alloc] initWithRootViewController:[DBXLoginViewController new]];
    [UIApplication sharedApplication].keyWindow.rootViewController = baseNavC;
}

- (NSInteger)getCurrentWeekDay
{
    NSTimeInterval interval = [[NSDate date] timeIntervalSince1970];
    return [self getWeekDayFordate:interval];
}

- (NSInteger)getWeekDayFordate:(NSTimeInterval)data
{
    NSDate *newDate = [NSDate dateWithTimeIntervalSince1970:data];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitWeekday fromDate:newDate];
    
    return components.weekday;
}

/**
 绑定设备按钮
 */
- (void)handleBindingBtnEvent:(UIButton *)button
{
    
    if (button.isSelected) {
        // 解绑设备
        [self reuquestDeviceDisBindApi];
        
    } else {
        
        if (_per) { // 存在已经选择的外设
            
            [MBProgressHUD showActivityMessageInView:@"绑定中"];
            
            // 连接当前要绑定的外设
            [_manager connectPeripheral:_per options:nil];
            
        } else {
            [MBProgressHUD showTipMessageInView:@"请选择要连接的外设"];
        }
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
