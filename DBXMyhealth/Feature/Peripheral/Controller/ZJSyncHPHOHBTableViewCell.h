//
//  ZJSyncHPHOHBTableViewCell.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/6.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZJSyncHPHOHBModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ZJSyncHPHOHBTableViewCellDelegate <NSObject>

- (void)cellButtonAcion:(UIButton *)button;

@end

@interface ZJSyncHPHOHBTableViewCell : UITableViewCell

@property (nonatomic, strong) UIButton *selectButton;
@property (nonatomic, strong) UILabel *bloodPreLabel;
@property (nonatomic, strong) UILabel *bloodOxygenLabel;
@property (nonatomic, strong) UILabel *heartBeatLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) ZJSyncHPHOHBModel *model;

@property (nonatomic, assign) id<ZJSyncHPHOHBTableViewCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
