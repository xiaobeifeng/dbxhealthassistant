//
//  DBXBloodSugarViewController.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/26.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WMStickyPageViewController.h"

/**
 血糖
 */
@interface DBXBloodSugarViewController : UIViewController<WMStickyPageViewControllerDelegate>

@property(nonatomic, strong) NSMutableArray *bloodSugarDataMarray;
@property(nonatomic, strong) NSMutableArray *timeDataMarray;
@end
