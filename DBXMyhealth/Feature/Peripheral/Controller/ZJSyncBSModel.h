//
//  ZJSyncBSModel.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/9.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZJSyncBSModel : NSObject

@property (nonatomic, assign) BOOL isSelect;
@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *bloodSugar;    // 血糖
@property (nonatomic, copy) NSString *time;      // 时间
@property (nonatomic, assign) NSInteger type;      // 测量时段

@end

NS_ASSUME_NONNULL_END
