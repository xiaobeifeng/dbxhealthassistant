//
//  ZJSyncHPHOHBViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/6.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "ZJSyncHPHOHBViewController.h"
#import "ZJSyncHPHOHBTableViewCell.h"
#import "ZJSyncHPHOHBModel.h"
#import <BabyBluetooth.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "DBXMePersonalinfoApi.h"
#import "DBXMePersonalinfoModel.h"
#import "DBXBpOxyHbApi.h"

@interface ZJSyncHPHOHBViewController ()<ZJSyncHPHOHBTableViewCellDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *selectMutableArray;
@property (nonatomic, strong) FMDatabase *dataBase;
@property (nonatomic, strong) NSMutableArray *dataMutableArray;

@end

@implementation ZJSyncHPHOHBViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadView:) name:@"CompleteSync" object:nil];
 
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
   
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"CompleteSync" object:nil];
}

- (void)reloadView:(NSNotification *)notification {
    
    NSLog(@"%@", notification);
    
    // 读取数据库数据
    NSString *fmdbName = [NSString stringWithFormat:@"%@_PeripheralSyncData.sqlite",  [[NSUserDefaults standardUserDefaults] objectForKey:kAccount]];
    NSString *fileName = [kDocumentPath stringByAppendingPathComponent:fmdbName];
    _dataBase = [FMDatabase databaseWithPath:fileName];
    if ([_dataBase open]) {
        NSString * sql = @"SELECT * FROM t_recevieInfos";
        FMResultSet * rs = [_dataBase executeQuery:sql];
        while ([rs next]) {
            
            NSString *Id = [rs stringForColumn:@"id"];
            NSString *recevice = [rs stringForColumn:@"recevice"];
            
            NSLog(@"%@, %@", Id, recevice);
            
            // 获取数据段的时间 28～40
            NSString *receviceTime = [recevice substringWithRange:NSMakeRange(recevice.length-12, 12)];
            // 高压
            NSString *sp = [recevice substringWithRange:NSMakeRange(12, 3)];
            // 低压
//            NSString *dp = [recevice substringWithRange:NSMakeRange(15, 3)];
            // 血氧
            NSString *bloodOxygen = [recevice substringWithRange:NSMakeRange(6, 3)];
            // 心率
            NSString *heartBeat = [recevice substringWithRange:NSMakeRange(9, 3)];
            
            if (![sp isEqualToString:@"NNN"] || ![bloodOxygen isEqualToString:@"NNN"] || ![heartBeat isEqualToString:@"NNN"]) {
                if (![receviceTime containsString:@"N"]) {  // 如果接收到的时间字符中含有N,就舍弃该条接收的字符串
                    [self insertTableView:recevice Id:Id];
                }
            }

            NSLog(@"receviceTime - %@", receviceTime);
            
        }

        [_dataBase close];
    }
    
}


/**
 TableView插入接收到的字符串

 @param recevice 接收到的字符串
 */
- (void)insertTableView:(NSString *)recevice Id:(NSString *)Id {
    
    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_dataMutableArray.count inSection:0];
    [indexPaths addObject:indexPath];
    
    NSString *heartBeat = [recevice substringWithRange:NSMakeRange(9, 3)];    // 心率
    if ([heartBeat isEqualToString:@"NNN"]) {
        heartBeat = @"--";
    } else {
        heartBeat = [NSString stringWithFormat:@"%i", [heartBeat intValue]];
    }
    
    NSString *bloodOxygen = [recevice substringWithRange:NSMakeRange(6, 3)];    // 血氧
    if ([bloodOxygen isEqualToString:@"NNN"]) {
        bloodOxygen = @"--";
    } else {
        bloodOxygen = [NSString stringWithFormat:@"%i", [bloodOxygen intValue]];
    }
    
    NSString *sp = [recevice substringWithRange:NSMakeRange(12, 3)];         // 高压
    if ([sp isEqualToString:@"NNN"]) {
        sp = @"--";
    } else {
        sp = [NSString stringWithFormat:@"%i", [sp intValue]];
    }
    
    NSString *dp = [recevice substringWithRange:NSMakeRange(15, 3)];         // 低压
    if ([dp isEqualToString:@"NNN"]) {
        dp = @"--";
    } else {
        dp = [NSString stringWithFormat:@"%i", [dp intValue]];
    }
    
    // 时间
    NSString *year = [NSString stringWithFormat:@"%@", [recevice substringWithRange:NSMakeRange(28, 2)]];      // 年
    NSString *month = [NSString stringWithFormat:@"%@", [recevice substringWithRange:NSMakeRange(30, 2)]];     // 月
    NSString *day = [NSString stringWithFormat:@"%@", [recevice substringWithRange:NSMakeRange(32, 2)]];        // 日
    NSString *hour = [NSString stringWithFormat:@"%@", [recevice substringWithRange:NSMakeRange(34, 2)]];        // 时
    NSString *minute = [NSString stringWithFormat:@"%@", [recevice substringWithRange:NSMakeRange(36, 2)]];        // 分
    NSString *second = [NSString stringWithFormat:@"%@", [recevice substringWithRange:NSMakeRange(38, 2)]];        // 秒
    NSString *time = [NSString stringWithFormat:@"20%@-%@-%@ %@:%@:%@", year, month, day, hour, minute, second];
    
    ZJSyncHPHOHBModel *model = [ZJSyncHPHOHBModel new];
    model.isSelect = NO;
    model.heartBeat = heartBeat;
    model.sp = sp;
    model.dp = dp;
    model.time = time;
    model.bloodOxygen = bloodOxygen;
    model.Id = Id;
    
    [_dataMutableArray addObject:model];
    
    [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor yellowColor];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-kSafeAreaTopHeight-150-40-60) style:UITableViewStylePlain];
    [self.tableView registerClass:[ZJSyncHPHOHBTableViewCell class] forCellReuseIdentifier:NSStringFromClass([ZJSyncHPHOHBTableViewCell class])];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    
    _dataMutableArray = [NSMutableArray new];
    _selectMutableArray = [NSMutableArray new];

    [self setupHeaderView];
    [self setupBottomView];

}

- (void)setupBottomView {
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.tableView.height, kScreenWidth, 60)];
    bottomView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bottomView];
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = kTableSeparatorLineColor;
    [bottomView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bottomView);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(kScreenWidth);
    }];
    
    UIButton *deleteBtn = [UIButton new];
    deleteBtn.titleLabel.font = kFont(14);
    kViewRadius(deleteBtn, 5.0f);
    [deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
    [deleteBtn addTarget:self action:@selector(deleteBtnAction) forControlEvents:UIControlEventTouchUpInside];
    deleteBtn.backgroundColor = [UIColor redColor];
    [bottomView addSubview:deleteBtn];
    
    UIButton *uploadBtn = [UIButton new];
    uploadBtn.titleLabel.font = kFont(14);
    kViewRadius(uploadBtn, 5.0f);
    [uploadBtn setTitle:@"上传" forState:UIControlStateNormal];
    [uploadBtn addTarget:self action:@selector(uploadBtnAction) forControlEvents:UIControlEventTouchUpInside];
    uploadBtn.backgroundColor = kNavigationBarColor;
    [bottomView addSubview:uploadBtn];
    
    [@[deleteBtn, uploadBtn] mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedItemLength:120 leadSpacing:30 tailSpacing:30];
    [@[deleteBtn, uploadBtn] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bottomView.mas_centerY);
        make.bottom.equalTo(bottomView).with.offset(-13);
    }];
}

- (void)setupHeaderView {
    
    UIView *headerView = [UIView new];
    headerView.frame = CGRectMake(0, 0, kScreenWidth, 40);
    self.tableView.tableHeaderView = headerView;
    
    UIButton *selectAllButton = [UIButton new];
    [selectAllButton setImage:[UIImage imageNamed:@"button_cell"] forState:UIControlStateNormal];
    [selectAllButton setImage:[UIImage imageNamed:@"button_cell_select"] forState:UIControlStateSelected];
    [selectAllButton addTarget:self action:@selector(selectAllButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:selectAllButton];
    [selectAllButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(25);
        make.centerY.equalTo(headerView.mas_centerY);
        make.left.equalTo(headerView).with.offset(20);
    }];
    
    UILabel *bloodPreLabel = [UILabel new];
    bloodPreLabel.font = [UIFont systemFontOfSize:14];
    bloodPreLabel.textAlignment = NSTextAlignmentCenter;
    bloodPreLabel.text = @"血压";
    [headerView addSubview:bloodPreLabel];
    
    UILabel *bloodOxygenLabel = [UILabel new];
    bloodOxygenLabel.font = [UIFont systemFontOfSize:14];
    bloodOxygenLabel.textAlignment = NSTextAlignmentCenter;
    bloodOxygenLabel.text = @"血氧";
    [headerView addSubview:bloodOxygenLabel];
    
    UILabel *heartBeatLabel = [UILabel new];
    heartBeatLabel.font = [UIFont systemFontOfSize:14];
    heartBeatLabel.textAlignment = NSTextAlignmentCenter;
    heartBeatLabel.text = @"心率";
    [headerView addSubview:heartBeatLabel];
    
    UILabel *timeLabel = [UILabel new];
    timeLabel.font = [UIFont systemFontOfSize:14];
    timeLabel.numberOfLines = 0;
    timeLabel.textAlignment = NSTextAlignmentCenter;
    timeLabel.text = @"时间";
    [headerView addSubview:timeLabel];
    
    NSArray *buttons = @[bloodPreLabel, bloodOxygenLabel, heartBeatLabel,timeLabel];
    // 水平方向控件间隔固定等间隔
    [buttons mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:2 leadSpacing:65 tailSpacing:20];
    [buttons mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(headerView.mas_centerY);
        make.height.mas_equalTo(40);
    }];
    
    UIView *line = [UIView new];
    line.backgroundColor = kTableSeparatorLineColor;
    [headerView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(headerView.mas_bottom);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(0.5);
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataMutableArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZJSyncHPHOHBTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZJSyncHPHOHBTableViewCell class]) forIndexPath:indexPath];
    cell.selectButton.tag = indexPath.row;
    
    cell.delegate = self;
   
    ZJSyncHPHOHBModel *model = self.dataMutableArray[indexPath.row];
    cell.selectButton.selected = model.isSelect;
    cell.heartBeatLabel.text = model.heartBeat;
    
    if ([model.sp isEqualToString:@"--"]) { // 当血压数据项s无数据时只显示--，不显示成--/--
        cell.bloodPreLabel.text = @"--";
    } else {
        cell.bloodPreLabel.text = [NSString stringWithFormat:@"%@/%@", model.sp, model.dp];
    }
    
    cell.bloodOxygenLabel.text = model.bloodOxygen;
    cell.timeLabel.text = model.time;
    
    NSLog(@"%@", model.heartBeat);
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (void)cellButtonAcion:(UIButton *)button {

    if (!button.selected) {
        button.selected = YES;
        ZJSyncHPHOHBModel *model = _dataMutableArray[button.tag];
        model.isSelect = YES;
        [_selectMutableArray addObject:self.dataMutableArray[button.tag]];
    } else {
        button.selected = NO;
        ZJSyncHPHOHBModel *model = _dataMutableArray[button.tag];
        model.isSelect = NO;
        [_selectMutableArray removeObject:self.dataMutableArray[button.tag]];
    }
    
    NSLog(@"_selectMutableArray - %@", _selectMutableArray);
    
}

/**
 全选按钮
 */
- (void)selectAllButtonAction:(UIButton *)button {
    
    _selectMutableArray = [NSMutableArray new];
    
    if (!button.selected) {
        button.selected = YES;
        for (int i = 0; i<_dataMutableArray.count; i++) {
            ZJSyncHPHOHBModel *model = _dataMutableArray[i];
            model.isSelect = YES;
            [_selectMutableArray addObject:model];
        }
    } else {
        button.selected = NO;
        for (int i = 0; i<_dataMutableArray.count; i++) {
            ZJSyncHPHOHBModel *model = _dataMutableArray[i];
            model.isSelect = NO;
        }
    }
    
    [self.tableView reloadData];
    
}



/**
 上传按钮点击
 */
- (void)uploadBtnAction {
    
    if (kArrayIsEmpty(_selectMutableArray)) {
        [MBProgressHUD showTipMessageInView:@"请选择要上传的数据"];
        return;
    }
    
    NSMutableArray *uploadMutableArray = [NSMutableArray new];
    for (int i = 0; i < _selectMutableArray.count; i++) {
        
        ZJSyncHPHOHBModel *model = _selectMutableArray[i];

        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        
        if (![model.sp isEqualToString:@"--"]) {
            [dic setObject:model.sp forKey:@"sPressure"];
        }
        
        if (![model.dp isEqualToString:@"--"]) {
            [dic setObject:model.dp forKey:@"dPressure"];
        }
        
        if (![model.bloodOxygen isEqualToString:@"--"]) {
            [dic setObject:model.bloodOxygen forKey:@"oxygen"];
        }
        
        if (![model.heartBeat isEqualToString:@"--"]) {
            [dic setObject:model.heartBeat forKey:@"pulse"];
        }

        [dic setObject:@"0" forKey:@"state"];
        [dic setObject:model.time forKey:@"upsaveTime"];
     
        [uploadMutableArray addObject:dic];
    }
    
    // 上传json数据
    NSData *data = [NSJSONSerialization dataWithJSONObject:uploadMutableArray options:NSJSONWritingPrettyPrinted error:nil];
    NSString *json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSData *uploadData = [json dataUsingEncoding:NSUTF8StringEncoding];
    DBXBpOxyHbApi *api = [[DBXBpOxyHbApi alloc] initWithJsonData:uploadData];
    [MBProgressHUD showActivityMessageInView:@"上传中"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        [MBProgressHUD hideHUD];
         NSLog(@"成功 %@", request.responseJSONObject);
        
        if ([[request.responseJSONObject objectForKey:@"state"] integerValue] != 0) {
            [MBProgressHUD showTipMessageInView:[request.responseJSONObject objectForKey:@"message"]];
            return;
        }
        
        // 删除选择的数据项
        [self.dataBase open];
        for (int i = 0; i<self.selectMutableArray.count; i++) {
            ZJSyncHPHOHBModel *model = self.selectMutableArray[i];
            NSString *deleteSql = [NSString stringWithFormat:@"delete from t_recevieInfos where id = '%@'", model.Id];
            BOOL res = [self.dataBase executeUpdate:deleteSql];
            if (res) {
                NSLog(@"删除成功");
                [self.dataMutableArray removeObject:self.selectMutableArray[i]];
            }
        }
        self.selectMutableArray = [NSMutableArray new];
        [self.tableView reloadData];
        [MBProgressHUD showTipMessageInView:@"上传完成"];
        
        
        // 发送通知，刷新首页数据
        [[NSNotificationCenter defaultCenter] postNotificationName:kReloadDataApiAndDataDetailApi object:nil userInfo:nil];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        [MBProgressHUD hideHUD];
        [MBProgressHUD showTipMessageInView:@"请稍后再试"];
        
    }];
}

/**
 删除按钮点击
 */
- (void)deleteBtnAction {
    
    if (kArrayIsEmpty(_selectMutableArray)) {
        [MBProgressHUD showTipMessageInView:@"请选择要删除的数据"];
        return;
    }
    
    [_dataBase open];
    for (int i = 0 ; i<_selectMutableArray.count; i++) {
        ZJSyncHPHOHBModel *model = _selectMutableArray[i];
        [_dataMutableArray removeObject:model];
        // 从数据库中删除该条数据
        NSString *deleteSql = [NSString stringWithFormat:@"delete from t_recevieInfos where id = '%@'", model.Id];
        BOOL res = [_dataBase executeUpdate:deleteSql];
        if (res) {
            NSLog(@"删除成功");
        }
        
    }
    
    _selectMutableArray = [NSMutableArray new];
    [self.tableView reloadData];
    [_dataBase close];
    
    [MBProgressHUD showTipMessageInView:@"删除成功"];
}

@end
