//
//  ZJSyncHPHOHBViewController.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/6.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ZJSyncHPHOHBViewControllerDelegate <NSObject>

- (void)completePersonInfoAlert;

@end

/**
 同步血压血氧心率
 */
@interface ZJSyncHPHOHBViewController : UIViewController

@property (nonatomic, assign) id<ZJSyncHPHOHBViewControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
