//
//  DBXStepNumberViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/27.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXStepNumberViewController.h"
#import "DBXStepNumberTableViewCell.h"

static NSString *stepNumberTableViewCellIdentifier = @"DBXStepNumberTableViewCell";

@interface DBXStepNumberViewController ()<UITableViewDelegate, UITableViewDataSource>

@property(nonatomic, strong) UITableView *tableView;

@end

@implementation DBXStepNumberViewController

#pragma mark - 懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-40-kSafeAreaTopHeight-60) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[DBXStepNumberTableViewCell class] forCellReuseIdentifier:stepNumberTableViewCellIdentifier];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView setEditing:YES animated:YES];
        // 解决滑动视图顶部空出状态栏高度的问题
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)]) {
                self.edgesForExtendedLayout = UIRectEdgeNone;
            }
        }
        
        
    }
    
    return _tableView;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    [self.view addSubview:self.tableView];
}

#pragma mark - tableView Delegate and DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    return self.bloodPressureMarray.count;
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DBXStepNumberTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:stepNumberTableViewCellIdentifier forIndexPath:indexPath];
    
    [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)setupModelOfCell:(DBXStepNumberTableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    cell.title = self.bloodPressureMarray[indexPath.row];
    //    cell.subTitle = self.timeMarray[indexPath.row];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView fd_heightForCellWithIdentifier:stepNumberTableViewCellIdentifier cacheByIndexPath:indexPath configuration:^(id cell) {
        
        [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        
        // 全选
        [self.stepNumberMarray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
        }];
        
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        // 取消全选
        [self.stepNumberMarray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0] animated:NO];
        }];
    }
}

- (UIScrollView *)streachScrollView
{
    return self.tableView;
}


@end
