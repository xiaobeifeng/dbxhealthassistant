//
//  DBXPeripheralViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/20.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXPeripheralViewController.h"
#import "DBXPeripheralManagementViewController.h"
#import "UIColor+Color.h"
#import "DBXCircleView.h"
#import "DBXPeripheralTableViewCell.h"
#import "DBXPeripheralDataSyncViewController.h"
#import "DBXMePersonalinfoApi.h"
#import "DBXMePersonalinfoModel.h"
#import "ZJDeviceSyncViewController.h"
#import "ZJDeviceManageViewController.h"
#import "ZJDeviceTimeViewController.h"

static NSString *cellIdentifier = @"DBXPeripheralTableViewCell";
static CGFloat kTopViewHeight = 250;
static CGFloat kCircleOutImageViewWidth = 170;
static CGFloat kBloodPressViewWidth = 150;
static CGFloat kCircleInImageViewWidth = 120;

@interface DBXPeripheralViewController ()<UITableViewDelegate, UITableViewDataSource>

@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, copy) NSArray *titleArray;
@property(nonatomic, strong) DBXCircleView *circleView;
@property(nonatomic, strong) UILabel *showValueLbl;

@end

@implementation DBXPeripheralViewController

#pragma mark - 懒加载
- (NSArray *)titleArray
{
    if (!_titleArray) {
        _titleArray = @[@"设备管理", @"时间校准", @"数据同步", @"固件升级"];
    }
    return _titleArray;
}

- (UITableView *)tableView
{
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-self.tabBarController.tabBar.height) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[DBXPeripheralTableViewCell class] forCellReuseIdentifier:cellIdentifier];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        // 解决滑动视图顶部空出状态栏高度的问题
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)]) {
                self.edgesForExtendedLayout = UIRectEdgeNone;
            }
        }
        
        [self.view addSubview:_tableView];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, -5000, kScreenWidth, 5000)];
        imageView.backgroundColor = kNavigationBarColor;
        [_tableView addSubview:imageView];
    }
    
    return _tableView;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIView *headerView = [UIView new];
    [headerView setFrame:CGRectMake(0, 0, kScreenWidth, kTopViewHeight)];
    headerView.backgroundColor = kNavigationBarColor;
//    [sysView.layer addSublayer:[UIColor setGradualChangingColor:sysView fromColor:@"6981ef" toColor:@"4c69ee"]];
    self.tableView.tableHeaderView = headerView;
    
    UIImageView *circleOutImageView = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenWidth-kCircleOutImageViewWidth)/2, (kTopViewHeight-kCircleOutImageViewWidth)/2, kCircleOutImageViewWidth, kCircleOutImageViewWidth)];
    kViewBorderRadius(circleOutImageView, kCircleOutImageViewWidth/2, 2, [UIColor whiteColor]);
    circleOutImageView.backgroundColor = [UIColor clearColor];
    [headerView addSubview:circleOutImageView];
    
    _circleView = [[DBXCircleView alloc] initWithFrame:CGRectMake((kCircleOutImageViewWidth-kBloodPressViewWidth)/2, (kCircleOutImageViewWidth-kBloodPressViewWidth)/2, kBloodPressViewWidth, kBloodPressViewWidth) gradientLayerColor:[UIColor whiteColor] circleWidth:15.0f];
//    circleView.progress = 1;
    [circleOutImageView addSubview:_circleView];
    
        UIImageView *powerImageView = [UIImageView new];
        powerImageView.image = [UIImage imageNamed:@"peripheral_energy"];
        [_circleView addSubview:powerImageView];
        [powerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.circleView).with.offset(30);
            make.centerX.equalTo(self.circleView.mas_centerX);
            make.width.mas_equalTo(@15);
            make.height.mas_equalTo(@20);
        }];
    
        _showValueLbl = [UILabel new];
//        showValueLbl.text = @"100%";
        _showValueLbl.textColor = [UIColor whiteColor];
        _showValueLbl.font = kBoldFont(22);
        [_showValueLbl sizeToFit];
        [_circleView addSubview:_showValueLbl];
        [_showValueLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.circleView.mas_centerY);
            make.centerX.equalTo(self.circleView.mas_centerX);
        }];
    
        UIButton *enterBtn = [UIButton new];
        [enterBtn setTitle:@"剩余电量" forState:UIControlStateNormal];
        [enterBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        enterBtn.adjustsImageWhenHighlighted = NO;
        enterBtn.titleLabel.font = kFont(12);
        [enterBtn sizeToFit];
        [self.circleView addSubview:enterBtn];
        [enterBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.circleView).with.offset(-30);
            make.centerX.equalTo(self.circleView.mas_centerX);
        }];
    
    
    [self checkAndMonitorBatteryLevel];
}

#pragma mark - 电池电量获取及监控
-(void)checkAndMonitorBatteryLevel
{
    
    //拿到当前设备
    UIDevice * device = [UIDevice currentDevice];
    
    //是否允许监测电池
    //要想获取电池电量信息和监控电池电量 必须允许
    device.batteryMonitoringEnabled = true;
    
    //1、check
    /*
     获取电池电量
     0 .. 1.0. -1.0 if UIDeviceBatteryStateUnknown
     */
    float level = device.batteryLevel;
    NSLog(@"level = %lf",level);
    
    _circleView.progress = level;
    _showValueLbl.text = [NSString stringWithFormat:@"%i%%", (int)(level*100)];
    
    //2、monitor
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeBatteryLevel:) name:@"UIDeviceBatteryLevelDidChangeNotification" object:device];
    
}

// 电量变化
- (void)didChangeBatteryLevel:(id)sender
{
    //电池电量发生改变时调用
    UIDevice *myDevice = [UIDevice currentDevice];
    [myDevice setBatteryMonitoringEnabled:YES];
    float batteryLevel = [myDevice batteryLevel];
    NSLog(@"电池剩余比例：%@", [NSString stringWithFormat:@"%f",batteryLevel*100]);
    
    _circleView.progress = batteryLevel;
    _showValueLbl.text = [NSString stringWithFormat:@"%i%%", (int)(batteryLevel*100)];
}

#pragma mark - tableView Delegate and DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.titleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DBXPeripheralTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)setupModelOfCell:(DBXPeripheralTableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.title = self.titleArray[indexPath.row];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView fd_heightForCellWithIdentifier:cellIdentifier cacheByIndexPath:indexPath configuration:^(id cell) {
        
        [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) {
        ZJDeviceManageViewController *peripheralManagementVC = [ZJDeviceManageViewController new];
        peripheralManagementVC.navigationItem.title = @"设备管理";
        [self.navigationController pushViewController:peripheralManagementVC animated:YES];
        return;
    }
    
   
    NSString *deviceName = [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_NAME]; // 获取本地保存的设备名
    if (kStringIsEmpty(deviceName)) {
        [MBProgressHUD showInfoMessage:@"请绑定设备后再试"];
        return;
    }
    
    if (indexPath.row == 1) {
        ZJDeviceTimeViewController *deviceTimeVC = [ZJDeviceTimeViewController new];
        [self.navigationController pushViewController:deviceTimeVC animated:YES];
    }
    
    if (indexPath.row == 2) {
        ZJDeviceSyncViewController *deviceSyncVC = [ZJDeviceSyncViewController new];
        [self.navigationController pushViewController:deviceSyncVC animated:YES];
    }
    
    if (indexPath.row == 3) {
        [MBProgressHUD showInfoMessage:@"暂无可升级固件"];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
