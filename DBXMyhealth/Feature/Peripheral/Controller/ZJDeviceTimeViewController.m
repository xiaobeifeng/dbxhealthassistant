//
//  ZJDeviceTimeViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/12.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "ZJDeviceTimeViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import <BabyBluetooth.h>
#import "DBXMePersonalinfoApi.h"
#import "DBXMePersonalinfoModel.h"

@interface ZJDeviceTimeViewController ()
@property (nonatomic, strong) UIImageView *bindingImageView;
@property (nonatomic, strong) UILabel *bindingPeripheralNameLabel;
@property (nonatomic, strong) BabyBluetooth *baby;
@property (nonatomic, strong) CBPeripheral *currPeripheral;
@property (nonatomic, strong) CBCharacteristic *characteristic;
@property (nonatomic, strong) NSTimer *connectTimer;    // 蓝牙搜索计时器
@property (nonatomic, assign) BOOL isPopDisconnect;    // 是否由于pop导致的设备断开连接
@end

@implementation ZJDeviceTimeViewController

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.connectTimer invalidate];     // 移除定时器
    _isPopDisconnect = YES;
    [self.baby cancelAllPeripheralsConnection]; // 取消所有设备连接
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"时间校准";
    self.view.backgroundColor = [UIColor whiteColor];
    _isPopDisconnect = NO;
    
    [self setupUI];

    [self checkPersonInfo];
    
}

- (void)setupUI {
    
    // TableView头部视图
    UIView *headerView = [UIView new];
    headerView.backgroundColor = kNavigationBarColor;
    headerView.frame = CGRectMake(0, 0, kScreenWidth, 250-self.navigationController.navigationBar.height-[UIApplication sharedApplication].statusBarFrame.size.height);
    [self.view addSubview:headerView];
    
    // 头部视图图片
    _bindingImageView = [UIImageView new];
    _bindingImageView.backgroundColor = [UIColor clearColor];
    _bindingImageView.image = [UIImage imageNamed:@"peripheral_unbinding"];
    [headerView addSubview:_bindingImageView];
    [_bindingImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(@60);
        make.height.mas_equalTo(@65);
        make.centerX.mas_equalTo(headerView.mas_centerX);
        make.centerY.mas_equalTo(headerView.mas_centerY).with.offset(-20);
    }];
    
    // 头部视图文本
    _bindingPeripheralNameLabel = [UILabel new];
    _bindingPeripheralNameLabel.text = @"暂无绑定设备";
    _bindingPeripheralNameLabel.textColor = [UIColor whiteColor];
    _bindingPeripheralNameLabel.textAlignment = NSTextAlignmentCenter;
    _bindingPeripheralNameLabel.font = [UIFont systemFontOfSize:14];
    [headerView addSubview:_bindingPeripheralNameLabel];
    [_bindingPeripheralNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kScreenWidth-26);
        make.centerX.mas_equalTo(headerView.mas_centerX);
        make.top.mas_equalTo(self.bindingImageView.mas_bottom).with.offset(20);
    }];
    
}

/**
 设置蓝牙
 */
- (void)setupBlueTooth {
    _baby = [BabyBluetooth shareBabyBluetooth];
    _baby.scanForPeripherals().begin(); // 开始扫描外设
    _connectTimer = [NSTimer scheduledTimerWithTimeInterval:20.0f target:self selector:@selector(connectTimeout) userInfo:nil repeats:NO]; // 开一个定时器监控连接超时的情况
    __weak typeof(self) weakSelf = self;
    [_baby setBlockOnCentralManagerDidUpdateState:^(CBCentralManager *central) {
        if (central.state != CBManagerStatePoweredOn) {
            [MBProgressHUD showTipMessageInView:@"请检查蓝牙是否开启后再试"];
        } else {
            // 开始扫描外设
            [central scanForPeripheralsWithServices:nil options:nil];
        }
    }];
    
    // 设置扫描到设备的委托
    [_baby setBlockOnDiscoverToPeripherals:^(CBCentralManager *central, CBPeripheral *peripheral, NSDictionary *advertisementData, NSNumber *RSSI) {
        NSLog(@"搜索到了设备:%@",peripheral.name);
        
        NSString *deviceName = [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_NAME];
        if ([peripheral.name isEqualToString:deviceName]) {
            weakSelf.bindingPeripheralNameLabel.text = [NSString stringWithFormat:@"当前绑定设备：%@", deviceName];
            weakSelf.bindingImageView.image = [UIImage imageNamed:@"peripheral_binding"];
            weakSelf.baby.having(peripheral).and.connectToPeripherals().begin();    // 开始连接当前外设
        }
        
    }];
    
    // 设置设备连接失败的委托
    [_baby setBlockOnFailToConnect:^(CBCentralManager *central, CBPeripheral *peripheral, NSError *error) {
        [MBProgressHUD hideHUD];
        NSLog(@"设备：%@--连接失败",peripheral.name);
        [MBProgressHUD showTipMessageInView:@"请稍后后再试"];
        [weakSelf.connectTimer invalidate];     // 移除定时器
        [weakSelf.baby cancelScan]; //  取消扫描外设
    }];
    
    [_baby setBlockOnDisconnect:^(CBCentralManager *central, CBPeripheral *peripheral, NSError *error) {
        if (!_isPopDisconnect) {
            [MBProgressHUD hideHUD];
            [MBProgressHUD showTipMessageInView:@"连接已断开，请将设备靠近手机"];
            [weakSelf.connectTimer invalidate];     // 移除定时器
            _baby.scanForPeripherals().begin(); // 开始扫描外设
        }
    }];
    
    // 设置设备连接成功的委托,同一个baby对象，使用不同的channel切换委托回调
    [_baby setBlockOnConnected:^(CBCentralManager *central, CBPeripheral *peripheral) {
        NSLog(@"%@", peripheral.name);
        [weakSelf.connectTimer invalidate];     // 移除定时器
        [weakSelf.baby cancelScan]; //  取消扫描外设
        // 查找服务
        [peripheral discoverServices:nil];
        
    }];
    
    // 设置发现设备的Services的委托
    [_baby setBlockOnDiscoverServices:^(CBPeripheral *peripheral, NSError *error) {
        for (CBService *s in peripheral.services) {
            NSLog(@"UUID - %@", s.UUID);
            if ([s.UUID.UUIDString isEqualToString:@"FFF0"]) {
                [peripheral discoverCharacteristics:nil forService:s];
            }
        }
    }];
    
    // 设置发现设service的Characteristics的委托
    [_baby setBlockOnDiscoverCharacteristics:^(CBPeripheral *peripheral, CBService *service, NSError *error) {
        for (CBCharacteristic *characteristic in service.characteristics) {
            
            if ([characteristic.UUID.UUIDString isEqualToString:@"FFF6"]) {
                weakSelf.currPeripheral = peripheral;
                weakSelf.characteristic = characteristic;
                [peripheral setNotifyValue:YES forCharacteristic:characteristic];
                [weakSelf syncTimeToDevice];    // 开始向外设同步时间
            }
        }
    }];
    
}

/**
 搜索设备超时
 */
- (void)connectTimeout {
    [MBProgressHUD hideHUD];
    [_connectTimer invalidate];     // 移除定时器
    [MBProgressHUD showTipMessageInView:@"未搜索到当前绑定设备，请检查设备后再试"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
    
}


/**
 检查个人信息是否完善
 */
- (void)checkPersonInfo {
    DBXMePersonalinfoApi *api = [DBXMePersonalinfoApi new];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        DBXMePersonalinfoModel *model = [DBXMePersonalinfoModel yy_modelWithJSON:request.responseJSONObject];
        NSArray *array = [NSArray yy_modelArrayWithClass:[DBXMePersonalinfoModelData class] json:model.data];
        if (kArrayIsEmpty(array)) {
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"请完善个人信息后再试" message:@"" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
            [alertVC addAction:okAction];
            
            [self presentViewController:alertVC animated:YES completion:nil];
        } else {
            [MBProgressHUD showActivityMessageInView:@""];
            [self setupBlueTooth];  // 开启蓝牙模块
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        [MBProgressHUD showTipMessageInView:@"请稍后再试"];

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [self.navigationController popViewControllerAnimated:YES];
        });
    }];
}

/**
 给外设同步时间
 */
- (void)syncTimeToDevice {
    
    
    // 获取身高体重
    DBXMePersonalinfoApi *api = [DBXMePersonalinfoApi new];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {

        DBXMePersonalinfoModel *model = [DBXMePersonalinfoModel yy_modelWithJSON:request.responseJSONObject];
        NSArray *array = [NSArray yy_modelArrayWithClass:[DBXMePersonalinfoModelData class] json:model.data];
        if (!kArrayIsEmpty(array)) {
            
            // 获取当前时间
            NSDate *currentDate = [NSDate date];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *currentDateStr = [dateFormatter stringFromDate:currentDate];
            
            // eg.2018-09-05 14:25:46
            NSInteger fullYear = [[currentDateStr substringWithRange:NSMakeRange(0, 4)] integerValue];  // 用于计算年龄
            NSInteger year = [[currentDateStr substringWithRange:NSMakeRange(2, 2)] integerValue];
            NSInteger month = [[currentDateStr substringWithRange:NSMakeRange(5, 2)] integerValue];
            NSInteger day = [[currentDateStr substringWithRange:NSMakeRange(8, 2)] integerValue];
            NSInteger hour = [[currentDateStr substringWithRange:NSMakeRange(11, 2)] integerValue];
            NSInteger minute = [[currentDateStr substringWithRange:NSMakeRange(14, 2)] integerValue];
            NSInteger second = [[currentDateStr substringWithRange:NSMakeRange(17, 2)] integerValue];
            
            // 校验当前时间
            Byte yearByte[2];
            yearByte[0] = 0xcc;
            yearByte[1] = (Byte)year;
            Byte monthByte[2];
            monthByte[0] = 0xcc;
            monthByte[1] = (Byte)month;
            Byte dayByte[2];
            dayByte[0] = 0xcc;
            dayByte[1] = (Byte)day;
            Byte hourByte[2];
            hourByte[0] = 0xcc;
            hourByte[1] = (Byte)hour;
            Byte minuteByte[2];
            minuteByte[0] = 0xcc;
            minuteByte[1] = (Byte)minute;
            Byte secondByte[2];
            secondByte[0] = 0xcc;
            secondByte[1] = (Byte)second;
            
            // 当前星期
            NSTimeInterval interval = [[NSDate date] timeIntervalSince1970];
            NSDate *newDate = [NSDate dateWithTimeIntervalSince1970:interval];
            NSArray *weekday = [NSArray arrayWithObjects: [NSNull null], @"7", @"1", @"2", @"3", @"4", @"5", @"6", nil];
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            NSDateComponents *components = [calendar components:NSCalendarUnitWeekday fromDate:newDate];
            NSString *weekStr = [weekday objectAtIndex:components.weekday];
            Byte weekByte[2];
            weekByte[0] = 0xcc;
            weekByte[1] = (Byte)([weekStr integerValue]);
            
            DBXMePersonalinfoModelData *modelData = array.firstObject;
            // 身高
            NSInteger height = [modelData.hight integerValue];
            Byte heightByte[2];
            heightByte[0] = 0xdd;
            heightByte[1] = (Byte)height;
            
            // 体重
            NSInteger weight = [modelData.weight integerValue];
            Byte weightByte[2];
            weightByte[0] = 0xdd;
            weightByte[1] = (Byte)weight;
            
            // 年龄
            NSString *birth = modelData.birth;
            NSInteger age;
            if (!kStringIsEmpty(birth)) {
                NSArray *birthArray = [birth componentsSeparatedByString:@"-"];
                NSInteger birthYear = [birthArray[0] integerValue];
                age = fullYear - birthYear;
            } else {
                age = 0;
            }
            Byte ageByte[2];
            ageByte[0] = 0xdd;
            ageByte[1] = (Byte)(age);
            
            // 性别
            NSInteger sexInteger;
            if ([modelData.sex isEqualToString:@"男"]) {
                sexInteger = 1;
            } else {
                sexInteger = 0;
            }
            Byte sexByte[2];
            sexByte[0] = 0xdd;
            sexByte[1] = (Byte)sexInteger;
            
            // 开始写入数据
            [_currPeripheral writeValue:[NSData dataWithBytes:&yearByte length:sizeof(yearByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
            [_currPeripheral writeValue:[NSData dataWithBytes:&monthByte length:sizeof(monthByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
            [_currPeripheral writeValue:[NSData dataWithBytes:&dayByte length:sizeof(dayByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
            [_currPeripheral writeValue:[NSData dataWithBytes:&hourByte length:sizeof(hourByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
            [_currPeripheral writeValue:[NSData dataWithBytes:&minuteByte length:sizeof(minuteByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
            [_currPeripheral writeValue:[NSData dataWithBytes:&secondByte length:sizeof(secondByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
            [_currPeripheral writeValue:[NSData dataWithBytes:&weekByte length:sizeof(weekByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
            [_currPeripheral writeValue:[NSData dataWithBytes:&heightByte length:sizeof(heightByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
            [_currPeripheral writeValue:[NSData dataWithBytes:&weightByte length:sizeof(weightByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
            [_currPeripheral writeValue:[NSData dataWithBytes:&sexByte length:sizeof(sexByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
            [_currPeripheral writeValue:[NSData dataWithBytes:&ageByte length:sizeof(ageByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
            [MBProgressHUD hideHUD];
            [self showSuccessAlertVC];
            
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        [MBProgressHUD hideHUD];
        [MBProgressHUD showTipMessageInView:@"请稍后再试"];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [self.navigationController popViewControllerAnimated:YES];
        });
        
    }];
    
}

/**
 绑定成功的AlertVC
 */
- (void)showSuccessAlertVC {
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"时间校准完成" preferredStyle:UIAlertControllerStyleAlert];
    
    __weak __typeof(self)weakSelf = self;
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        [weakSelf.navigationController popViewControllerAnimated:YES];
        
    }];
    
    [alertVC addAction:okAction];
    
    [self presentViewController:alertVC animated:YES completion:nil];
}

@end
