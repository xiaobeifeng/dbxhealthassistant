//
//  DBXPeripheralDataSyncViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/25.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXPeripheralDataSyncViewController.h"
#import "DBXBloodSugarViewController.h"
#import "DBXBloodPressureOxygenHeartbeatViewController.h"
#import "DBXStepNumberViewController.h"
#import "DBXMePersonalinfoApi.h"
#import "DBXMePersonalinfoModel.h"
#import <CoreBluetooth/CoreBluetooth.h>

@interface DBXPeripheralDataSyncViewController ()<CBCentralManagerDelegate, CBPeripheralDelegate>

@property (nonatomic, copy) NSArray *subVCNameArray;

/**
 接收数据
 */
@property (nonatomic, strong) NSMutableArray *receiveDataMarray;
/**
 合并后的接收数据
 */
@property (nonatomic, strong) NSMutableArray *mergeReceiveDataMarray;
/**
 血糖数据
 */
@property (nonatomic, strong) NSMutableArray *bloodSugarDataMarray;
/**
 血氧数据
 */
@property (nonatomic, strong) NSMutableArray *bloodOxygenDataMarray;
/**
 心率数据
 */
@property (nonatomic, strong) NSMutableArray *heartbeatDataMarray;
/**
 高压数据
 */
@property (nonatomic, strong) NSMutableArray *highPressDataMarray;
/**
 低压数据
 */
@property (nonatomic, strong) NSMutableArray *lowPressDataMarray;
/**
 运动步数数据
 */
@property (nonatomic, strong) NSMutableArray *stepNumberDataMarray;
/**
 时间数据
 */
@property (nonatomic, strong) NSMutableArray *timeDataMarray;
/**
 血压时间数据
 */
@property (nonatomic, strong) NSMutableArray *bloodPressureTimeDataMarray;
/**
 血糖时间数据
 */
@property (nonatomic, strong) NSMutableArray *bloodSugarTimeDataMarray;

/**
 底部上传同步栏
 */
@property (nonatomic, strong) UIView *bottomView;

/**
 上传按钮
 */
@property (nonatomic, strong) UIButton *uploadBtn;

/**
 当前滑动页序号
 */
@property (nonatomic, assign) NSInteger currentPageVCIndex;

@property (nonatomic, strong) FMDatabase *dataBase;

@property (nonatomic, strong)  UILabel *peripheralInfoLbl;

/** 系统蓝牙设备管理对象，可以把他理解为主设备，通过他，可以去扫描和链接外设 */
@property (nonatomic, strong) CBCentralManager *manager;
/** 保存外设数组 */
@property (nonatomic, strong) NSMutableArray *peripherals;
/** 当前连接外设 */
@property (nonatomic, strong) CBPeripheral *per;
@property (nonatomic, strong) CBCharacteristic *characteristic;
/** 连接超时的定时器 */
@property (nonatomic, strong) NSTimer *connectTimer;
@end

@implementation DBXPeripheralDataSyncViewController

#pragma mark - 懒加载
- (NSArray *)subVCNameArray
{
    if (!_subVCNameArray) {
        _subVCNameArray = @[@"血压/血氧/心率", @"血糖", @"运动"];
    }
    return _subVCNameArray;
}

/**
 接收到的字符串进行合并的数据
 */
- (NSMutableArray *)mergeReceiveDataMarray
{
    if (!_mergeReceiveDataMarray) {
        _mergeReceiveDataMarray = [NSMutableArray new];
    }
    return _mergeReceiveDataMarray;
}

/**
 接收到的字符串数据
 */
- (NSMutableArray *)receiveDataMarray
{
    if (!_receiveDataMarray) {
        _receiveDataMarray = [NSMutableArray new];
    }
    return _receiveDataMarray;
}

/**
 血糖
 */
- (NSMutableArray *)bloodSugarDataMarray
{
    if (!_bloodSugarDataMarray) {
        _bloodSugarDataMarray = [NSMutableArray new];
    }
    return _bloodSugarDataMarray;
}
/**
 血压
 */
- (NSMutableArray *)highPressDataMarray
{
    if (!_highPressDataMarray) {
        _highPressDataMarray = [NSMutableArray new];
    }
    return _highPressDataMarray;
}
/**
 血氧
 */
- (NSMutableArray *)bloodOxygenDataMarray
{
    if (!_bloodOxygenDataMarray) {
        _bloodOxygenDataMarray = [NSMutableArray new];
    }
    return _bloodOxygenDataMarray;
}
/**
 心率
 */
- (NSMutableArray *)heartbeatDataMarray
{
    if (!_heartbeatDataMarray) {
        _heartbeatDataMarray = [NSMutableArray new];
    }
    return _heartbeatDataMarray;
}
/**
 血压，血氧，心率时间
 */
- (NSMutableArray *)bloodPressureTimeDataMarray
{
    if (!_bloodPressureTimeDataMarray) {
        _bloodPressureTimeDataMarray = [NSMutableArray new];
    }
    return _bloodPressureTimeDataMarray;
}
/**
 血糖时间
 */
- (NSMutableArray *)bloodSugarTimeDataMarray
{
    if (!_bloodSugarTimeDataMarray) {
        _bloodSugarTimeDataMarray = [NSMutableArray new];
    }
    return _bloodSugarTimeDataMarray;
}
/**
 时间
 */
- (NSMutableArray *)timeDataMarray
{
    if (!_timeDataMarray) {
        _timeDataMarray = [NSMutableArray new];
    }
    return _timeDataMarray;
}

/**
 底部功能栏
 */
- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [UIView new];
        _bottomView.frame = CGRectMake(0, kScreenHeight-60, kScreenWidth, 60);
        _bottomView.backgroundColor = [UIColor whiteColor];
        
        UIView *lineView = [UIView new];
        lineView.backgroundColor = kTableSeparatorLineColor;
        [_bottomView addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.bottomView);
            make.height.mas_equalTo(0.5);
            make.width.mas_equalTo(kScreenWidth);
        }];
        
        UIButton *deleteBtn = [UIButton new];
        deleteBtn.titleLabel.font = kFont(14);
        kViewRadius(deleteBtn, 5.0f);
        [deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
        [deleteBtn addTarget:self action:@selector(handleDeleteBtnEvent) forControlEvents:UIControlEventTouchUpInside];
        deleteBtn.backgroundColor = [UIColor redColor];
        [_bottomView addSubview:deleteBtn];
        
        _uploadBtn = [UIButton new];
        _uploadBtn.titleLabel.font = kFont(14);
        kViewRadius(_uploadBtn, 5.0f);
        [_uploadBtn setTitle:@"上传" forState:UIControlStateNormal];
        // 上传功能指向血压
        [_uploadBtn addTarget:self action:@selector(handleUploadBtnEvent) forControlEvents:UIControlEventTouchUpInside];
        _uploadBtn.backgroundColor = kNavigationBarColor;
        [_bottomView addSubview:_uploadBtn];
        
        [@[deleteBtn, _uploadBtn] mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedItemLength:120 leadSpacing:30 tailSpacing:30];
        [@[deleteBtn, _uploadBtn] mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@13);
            make.bottom.equalTo(self.bottomView).with.offset(-13);
        }];
    }
    
    return _bottomView;
}

- (instancetype)init
{
    
    if (self = [super init]) {
        
        self.titleSizeNormal = 13;
        self.titleSizeSelected = 13;
        self.menuViewStyle = WMMenuViewStyleLine;
        self.menuItemWidth = 100;
        self.titleColorSelected = kNavigationBarColor;
        self.titleColorNormal = kNavigationBarColor;
        self.menuViewHeight = 40;

    }
    
    return self;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.bottomView removeFromSuperview];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:self.bottomView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.title = @"数据同步";
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setupPeripheralConnect];

    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 250-kSafeAreaTopHeight)];
    headerView.backgroundColor = kNavigationBarColor;
    [self.view addSubview:headerView];
    
    UIImageView *imageView = [UIImageView new];
    imageView.image = [UIImage imageNamed:@"peripheral_watch"];
    [headerView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(80);
        make.width.mas_equalTo(50);
        make.centerY.equalTo(headerView);
        make.left.equalTo(headerView).with.offset(30);
    }];
    
    _peripheralInfoLbl = [UILabel new];
    _peripheralInfoLbl.font = kFont(14);
    _peripheralInfoLbl.textColor = [UIColor whiteColor];
    _peripheralInfoLbl.numberOfLines = 0;
    [_peripheralInfoLbl sizeToFit];
    [headerView addSubview:_peripheralInfoLbl];
    [_peripheralInfoLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageView.mas_right).with.offset(40);
        make.centerY.equalTo(headerView.mas_centerY);
    }];
    
    
    self.headerViewHeight = headerView.height;
    
    [self startRequestPersonInfo];
}

/**
 获取个人信息
 */
- (void)startRequestPersonInfo
{
    DBXMePersonalinfoApi *api = [[DBXMePersonalinfoApi alloc] init];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        DBXMePersonalinfoModel *model = [DBXMePersonalinfoModel yy_modelWithJSON:request.responseJSONObject];
        NSArray *array = [NSArray yy_modelArrayWithClass:[DBXMePersonalinfoModelData class] json:model.data];
        
        DBXMePersonalinfoModelData *modelData;
        if (!kArrayIsEmpty(array)) {
            modelData = array.firstObject;
            
            if (kStringIsEmpty(modelData.sex) || kStringIsEmpty(modelData.birth)) {
                
                UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"请完善个人信息后再试" message:@"" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    [self.navigationController popViewControllerAnimated:YES];
                    
                }];
                
                [alertVC addAction:okAction];
                
                [self presentViewController:alertVC animated:YES completion:nil];
                
            } else {
                
                // 蓝牙初始化
                self.manager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue()];
                self.peripherals = [NSMutableArray new];
                
            }
            
        } else {
            
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"请完善个人信息后再试" message:@"" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [self.navigationController popViewControllerAnimated:YES];
                
            }];
            
            [alertVC addAction:okAction];
            
            [self presentViewController:alertVC animated:YES completion:nil];
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}


#pragma mark - CBCentralManagerDelegate
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    if (central.state == CBManagerStatePoweredOn) { // 蓝牙已开启可以正常使用
        
        NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:kAccount];
        NSString *key = [NSString stringWithFormat:@"%@_peripheralIdentifier", userName];
        NSString *identifier = [[NSUserDefaults standardUserDefaults] objectForKey:key];
        
        if (identifier.length != 0) {
            NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:identifier];
            NSArray *perArray = [_manager retrievePeripheralsWithIdentifiers:@[uuid]];
            _per = perArray.firstObject;
            _peripheralInfoLbl.text = [NSString stringWithFormat:@"当前绑定设备：%@", _per.name];
            [MBProgressHUD showActivityMessageInWindow:@"设备连接中"];
            [_manager connectPeripheral:_per options:nil];
            // 开一个定时器监控连接超时的情况
            _connectTimer = [NSTimer scheduledTimerWithTimeInterval:15.0f target:self selector:@selector(connectTimeout) userInfo:nil repeats:NO];
            [_manager stopScan];
 
        }
    } else if (central.state == CBManagerStatePoweredOff) { // 蓝牙处于关闭状态
        
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"请到系统设置中打开蓝牙" preferredStyle:UIAlertControllerStyleAlert];
        // 直接跳转到系统设置页
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
           [self.navigationController popViewControllerAnimated:YES];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            // 返回上一级
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [alertVC addAction:okAction];
        [alertVC addAction:cancelAction];
        [self presentViewController:alertVC animated:YES completion:nil];
    }
    
    else {    // 蓝牙未开启或处于不正常状态
        
        [MBProgressHUD showActivityMessageInWindow:@"请检查蓝牙相关设置后再试"];
        // 返回上一级
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (void)connectTimeout
{
    [MBProgressHUD hideHUD];
    [MBProgressHUD showTipMessageInWindow:@"设备连接超时"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.manager cancelPeripheralConnection:self.per];
        [self.navigationController popViewControllerAnimated:YES];
    });
    
    
}

/**
 连接Peripherals成功
 
 @param central 中心设备
 @param peripheral 连接的外设
 */
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    NSLog(@">>>连接到名称为（%@）的设备-成功 %@", peripheral.name, [peripheral identifier]);
    
    [_connectTimer invalidate];
    
    [MBProgressHUD hideHUD];
    
    [peripheral setDelegate:self];
    
    [peripheral discoverServices:nil];
}

#pragma mark - CBPeripheralDelegate
// 扫描到Services
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    //  NSLog(@">>>扫描到服务：%@",peripheral.services);
    if (error) {
        NSLog(@">>>Discovered services for %@ with error: %@", peripheral.name, [error localizedDescription]);
        return;
    }
    
    for (CBService *service in peripheral.services) {
        
        NSLog(@"service —— %@", service);
        CBUUID *uuid6 = [CBUUID UUIDWithString:@"FFF6"];
        [peripheral discoverCharacteristics:@[uuid6] forService:service];
        
    }
    
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    if (error) {
        NSLog(@"error Discovered characteristics for %@ with error: %@", service.UUID, [error localizedDescription]);
        return;
    }
    
    NSLog(@"characteristics - %@", service.characteristics);
    
    if (service.characteristics.count != 0) {
        for (CBCharacteristic *characteristic in service.characteristics) {
            NSLog(@"service:%@ 的 Characteristic: %@",service.UUID,characteristic.UUID);
            
            NSLog(@"properties - %lu", (unsigned long)characteristic.properties);
            if ([characteristic.UUID.UUIDString isEqualToString:@"FFF6"]) {
                
                _per = peripheral;
                _characteristic = characteristic;
                
                [_per readValueForCharacteristic:_characteristic];
                
                [peripheral setNotifyValue:YES forCharacteristic:characteristic];
                
                NSLog(@"%lu", (unsigned long)characteristic.properties);
                
                // 只有 characteristic.properties 有write的权限才可以写
                if(characteristic.properties & CBCharacteristicPropertyWrite) {
                    /*
                     最好一个type参数可以为CBCharacteristicWriteWithResponse或type:CBCharacteristicWriteWithResponse,区别是是否会有反馈
                     */
                    NSLog(@"该字段可写！");
                    
                    // 向外设写入数据
                    // 向外设写入数据
                    [self writeValeToPeripheral:peripheral forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
                    
                } else {
                    
                    NSLog(@"该字段不可写！");
                    
                }
                
            }
            
        }
    }
}

- (void)writeValeToPeripheral:(CBPeripheral *)peripheral
            forCharacteristic:(CBCharacteristic *)characteristic
                         type:(CBCharacteristicWriteType)type
{
    // 获取身高体重
    DBXMePersonalinfoApi *api = [DBXMePersonalinfoApi new];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        DBXMePersonalinfoModel *model = [DBXMePersonalinfoModel yy_modelWithJSON:request.responseJSONObject];
        NSArray *array = [NSArray yy_modelArrayWithClass:[DBXMePersonalinfoModelData class] json:model.data];
        
        DBXMePersonalinfoModelData *modelData;
        if (!kArrayIsEmpty(array)) {
            
            // 获取当前时间
            NSDate *currentDate = [NSDate date];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *currentDateStr = [dateFormatter stringFromDate:currentDate];
            // 2018-09-05 14:25:46
            NSInteger year = [[currentDateStr substringWithRange:NSMakeRange(2, 2)] integerValue];
            NSInteger month = [[currentDateStr substringWithRange:NSMakeRange(5, 2)] integerValue];
            NSInteger day = [[currentDateStr substringWithRange:NSMakeRange(8, 2)] integerValue];
            //    NSInteger day = 17;
            NSInteger hour = [[currentDateStr substringWithRange:NSMakeRange(11, 2)] integerValue];
            NSInteger minute = [[currentDateStr substringWithRange:NSMakeRange(14, 2)] integerValue];
            NSInteger second = [[currentDateStr substringWithRange:NSMakeRange(17, 2)] integerValue];
            
            // 校验当前时间
            // 年
            Byte yearByte[2];
            yearByte[0] = 0xcc;
            yearByte[1] = (Byte)year;
            
            // 月
            Byte monthByte[2];
            monthByte[0] = 0xcc;
            monthByte[1] = (Byte)month;
            
            // 日
            Byte dayByte[2];
            dayByte[0] = 0xcc;
            dayByte[1] = (Byte)day;
            
            // 时
            Byte hourByte[2];
            hourByte[0] = 0xcc;
            hourByte[1] = (Byte)hour;
            
            // 分
            Byte minuteByte[2];
            minuteByte[0] = 0xcc;
            minuteByte[1] = (Byte)minute;
            
            // 秒
            Byte secondByte[2];
            secondByte[0] = 0xcc;
            secondByte[1] = (Byte)second;
            
            // 当前星期
            Byte weekByte[2];
            weekByte[0] = 0xcc;
            weekByte[1] = (Byte)([self getCurrentWeekDay]);
            
            
            modelData = array.firstObject;
            
            NSInteger height = [modelData.hight integerValue];
            NSInteger weight = [modelData.weight integerValue];
            
            NSInteger sexInteger;
            if ([modelData.sex isEqualToString:@"男"]) {
                sexInteger = 1;
            } else {
                sexInteger = 0;
            }
            
            // 身高
            Byte heightByte[2];
            heightByte[0] = 0xdd;
            heightByte[1] = (Byte)height;
            
            // 体重
            Byte weightByte[2];
            weightByte[0] = 0xdd;
            weightByte[1] = (Byte)weight;
            
            // 性别
            Byte sexByte[2];
            sexByte[0] = 0xdd;
            sexByte[1] = (Byte)sexInteger;
            
            // 年龄
            Byte ageByte[2];
            ageByte[0] = 0xdd;
            ageByte[1] = (Byte)20;
            
            // 开始写入数据
            [peripheral writeValue:[NSData dataWithBytes:&yearByte length:sizeof(yearByte)] forCharacteristic:characteristic type:type];
            [peripheral writeValue:[NSData dataWithBytes:&monthByte length:sizeof(monthByte)] forCharacteristic:characteristic type:type];
            [peripheral writeValue:[NSData dataWithBytes:&dayByte length:sizeof(dayByte)] forCharacteristic:characteristic type:type];
            [peripheral writeValue:[NSData dataWithBytes:&hourByte length:sizeof(hourByte)] forCharacteristic:characteristic type:type];
            [peripheral writeValue:[NSData dataWithBytes:&minuteByte length:sizeof(minuteByte)] forCharacteristic:characteristic type:type];
            [peripheral writeValue:[NSData dataWithBytes:&secondByte length:sizeof(secondByte)] forCharacteristic:characteristic type:type];
            [peripheral writeValue:[NSData dataWithBytes:&weekByte length:sizeof(weekByte)] forCharacteristic:characteristic type:type];
            [peripheral writeValue:[NSData dataWithBytes:&heightByte length:sizeof(heightByte)] forCharacteristic:characteristic type:type];
            [peripheral writeValue:[NSData dataWithBytes:&weightByte length:sizeof(weightByte)] forCharacteristic:characteristic type:type];
            [peripheral writeValue:[NSData dataWithBytes:&sexByte length:sizeof(sexByte)] forCharacteristic:characteristic type:type];
            [peripheral writeValue:[NSData dataWithBytes:&ageByte length:sizeof(ageByte)] forCharacteristic:characteristic type:type];
            
            [self syncPeripheralData];
            
        } else {
            
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
       [MBProgressHUD showTipMessageInWindow:@"请稍后再试"];
    }];
    
    
    
}


/**
 同步数据
 */
- (void)syncPeripheralData
{
    [MBProgressHUD showActivityMessageInWindow:@"数据同步中，请将手表靠近手机"];
    // 发送同步数据指令
    Byte startSyncByte[2];
    startSyncByte[0] = 0xee;
    startSyncByte[1] = (Byte)1;
    [_per writeValue:[NSData dataWithBytes:&startSyncByte length:sizeof(startSyncByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithResponse];
    
    // 创建本地数据库存储同步信息
    NSString *fmdbName = [NSString stringWithFormat:@"%@_PeripheralSyncData.sqlite",  [[NSUserDefaults standardUserDefaults] objectForKey:kAccount]];
    NSString *fileName = [kDocumentPath stringByAppendingPathComponent:fmdbName];
    
    FMDatabase *dataBase = [FMDatabase databaseWithPath:fileName];
    
    //使用如下语句，如果打开失败，可能是权限不足或者资源不足。通常打开完操作操作后，需要调用 close 方法来关闭数据库。在和数据库交互 之前，数据库必须是打开的。如果资源或权限不足无法打开或创建数据库，都会导致打开失败。
    if ([dataBase open]) {
        
        // 创建血压表 sPressure, dPressure, pulse, oxygen, state, upsaveTime
        NSString *bpSql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (id integer PRIMARY KEY AUTOINCREMENT, sPressure integer NOT NULL, dPressure integer NOT NULL, pulse integer NOT NULL, oxygen integer NOT NULL, state text NOT NULL, upsaveTime text NOT NULL);", kBloodPressureTableName];
        // 创建血糖表
        NSString *bsSql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (id integer PRIMARY KEY AUTOINCREMENT, mCondition text NOT NULL, bloodGluVal text NOT NULL, upsaveTime text NOT NULL);", kBloodSugarTableName];
        
        BOOL bpResult = [dataBase executeUpdate:bpSql];
        BOOL bsResult = [dataBase executeUpdate:bsSql];
        
        if (bpResult) {
            NSLog(@"创建血压表成功");
        }
        if (bsResult) {
            NSLog(@"创建血糖表成功");
        }
    }
    
    self.dataBase = dataBase;
}

/**
 获取Characteristic的值，读到数据会进入方法
 */
- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    NSLog(@"11111");
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    
    NSLog(@"I'm here!!!!!");
    if([characteristic.UUID.UUIDString isEqualToString:@"FFF6"]) {
        // 获取订阅特征回复的数据
        NSData *value = characteristic.value;
        NSString *str = [[NSString alloc] initWithData:value encoding:NSUTF8StringEncoding];
        NSLog(@"蓝牙回复：%@", str);
        
        if ([str isEqualToString:@"FFFFFFFFFFFFFFFFFFFF"]) {
            
            [MBProgressHUD hideHUD];
            [MBProgressHUD showTipMessageInWindow:@"同步完成"];
            
            NSLog(@"传输完成了哈");
            // 发送结束指令
            Byte byte[2];
            byte[0] = 0xee;
            byte[1] = (Byte)0;
            [_per writeValue:[NSData dataWithBytes:&byte length:sizeof(byte)] forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
            // 处理接收到的全部数据
            for (NSString *mergeReceiveStr in self.mergeReceiveDataMarray) {
                
                // 先校验合并后的接收字符串长度为40
                if (mergeReceiveStr.length == 40) {
                    [self handleMergeReceiveStr:mergeReceiveStr];
                }
                
            }
            
            [self reloadData];
            
        }
        
        // 校验硬件端返回有效数据
        if (str.length > 0 && ![str isEqualToString:@"FFFFFFFFFFFFFFFFFFFF"]) {
            
            if ([str isEqualToString:@"00000000000000000000"]) {
                
                return;
            }
            
            // 得到字符串数据头为 001
            NSString *index = [str substringWithRange:NSMakeRange(0, 3)];
            
            // 将收到的字符串数组2个组成一个字符串 eg 001aaa 002aaa -> 001aaa002aaa
            [self.receiveDataMarray addObject:str];
            if (self.receiveDataMarray.count == 2) {
                NSString *string = [NSString stringWithFormat:@"%@%@", self.receiveDataMarray[0], self.receiveDataMarray[1]];
                [self.mergeReceiveDataMarray addObject:string];
                
                [self.receiveDataMarray removeAllObjects];
            }
            
            // eg. 继续发送请求数据指令 001 -> 002
            Byte byte[2];
            byte[0] = 0xee;
            byte[1] = (Byte)([index intValue] + 1);

            [_per writeValue:[NSData dataWithBytes:&byte length:sizeof(byte)] forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
        }
        
    }
}


/**
 设置与外设间通信
 */
- (void)setupPeripheralConnect
{
    // 写入的数据
//    [[BLECentralManager shareInstance].getFirstConnector setWritePartialDataBlock:^(NSData *writeData) {
//        
//        NSLog(@"writeData - %@", writeData);
//    }];
//    
//    [[BLECentralManager shareInstance].getFirstConnector setReceivePartialDataBlock:^(NSData *receiveData) {
//        
//        NSLog(@"%@", receiveData);
//        
//        NSString *recevieStr = [NSString stringWithFormat:@"%s", [receiveData bytes]];
//        
//        NSLog(@"recevieStr - %@", recevieStr);
//        
//        if ([recevieStr isEqualToString:@"FFFFFFFFFFFFFFFFFFFF"]) {
//            
//            [MBProgressHUD hideHUD];
//            [MBProgressHUD showTipMessageInWindow:@"同步完成"];
//            
//            NSLog(@"传输完成了哈");
//            // 发送结束指令
//            Byte byte[2];
//            byte[0] = 0xee;
//            byte[1] = (Byte)0;
//            [[BLECentralManager shareInstance].getFirstConnector sendData:[NSData dataWithBytes:&byte length:sizeof(byte)]];
//            
//            // 处理接收到的全部数据
//            for (NSString *mergeReceiveStr in self.mergeReceiveDataMarray) {
//                
//                // 先校验合并后的接收字符串长度为40
//                if (mergeReceiveStr.length == 40) {
//                    [self handleMergeReceiveStr:mergeReceiveStr];
//                }
//                
//            }
//            
//            [self reloadData];
//            
//        }
//        
//        // 校验硬件端返回有效数据
//        if (recevieStr.length > 0 && ![recevieStr isEqualToString:@"FFFFFFFFFFFFFFFFFFFF"]) {
//            
//            if ([recevieStr isEqualToString:@"00000000000000000000"]) {
//            
//                return;
//            }
//            
//            // 得到字符串数据头为 001
//            NSString *index = [recevieStr substringWithRange:NSMakeRange(0, 3)];
//            
//            [self continueSendDataWithIndex:index recevieStr:recevieStr];
//            
//        }
//        
//    }];
}

/**
 处理合并后的接收字符串
 
 @param mergeReceiveStr 合并后的接收字符串
 */
- (void)handleMergeReceiveStr:(NSString *)mergeReceiveStr
{
    
    NSString *bloodOxygenString = [mergeReceiveStr substringWithRange:NSMakeRange(6, 3)];
    
    NSString *highPressString = [mergeReceiveStr substringWithRange:NSMakeRange(12, 3)];
  
    NSString *lowPressString = [mergeReceiveStr substringWithRange:NSMakeRange(15, 3)];

    // 校验
    if ((![highPressString isEqualToString:@"NNN"] && ![lowPressString isEqualToString:@"NNN"]) || ![bloodOxygenString isEqualToString:@"NNN"]) {
    
        NSString *sbpString = [NSString stringWithFormat:@"%d/%d", [highPressString intValue], [lowPressString intValue]];
        [self.highPressDataMarray addObject:sbpString];
        
        // 血氧
        [self.bloodOxygenDataMarray addObject:bloodOxygenString];
        
        // 心率
        NSString *heartbeatString = [mergeReceiveStr substringWithRange:NSMakeRange(9, 3)];
        [self.heartbeatDataMarray addObject:heartbeatString];
        
        
        // 时间
        NSString *time = [self timeFormat:[mergeReceiveStr substringWithRange:NSMakeRange(28, 12)]];
        
//        [self.bloodPressureTimeDataMarray addObject:];
        NSLog(@"%ld %@", (long)[heartbeatString integerValue], heartbeatString);
        
        // 血压等数据存入数据库中
        NSString *sql = [NSString stringWithFormat:@"INSERT INTO %@ (sPressure, dPressure, pulse, oxygen, state, upsaveTime) VALUES (?, ?, ?, ?, ?, ?);", kBloodPressureTableName];
        BOOL isSuccess = [self.dataBase executeUpdate:sql, @([highPressString intValue]), @([lowPressString intValue]), @([heartbeatString intValue]), @([bloodOxygenString intValue]), @"0", time];
        
        [self.dataBase lastError];
        
        if (isSuccess) {
            NSLog(@"插入血压等数据成功");
        }
        
    }
    
    // 血糖
    NSString *bloodSugar = [mergeReceiveStr substringWithRange:NSMakeRange(3, 3)];
    // 校验血糖数据
    if (![bloodSugar isEqualToString:@"NNN"]) {
        
//        [self.bloodSugarDataMarray addObject:bloodSugar];
        // 时间
        NSString *time = [mergeReceiveStr substringWithRange:NSMakeRange(28, 12)];
        // 时间格式化
        NSString *newTime = [self timeFormat:time];
//        [self.bloodSugarTimeDataMarray addObject:[self timeFormat:time]];
        
        // 血糖数据存入数据库中
        NSString *sql = [NSString stringWithFormat:@"INSERT INTO %@ (mCondition, bloodGluVal, upsaveTime) VALUES (?, ?, ?);", kBloodSugarTableName];
        BOOL isSuccess = [self.dataBase executeUpdate:sql, @"0", bloodSugar, newTime];
        
        if (isSuccess) {
            NSLog(@"插入血糖数据成功");
        }
        
    }
    

}

/**
 时间字符串转化为上传服务器所要求的格式
 
 @param time 需要格式化的时间字符串
 @return 格式化后的时间字符串
 */
- (NSString *)timeFormat:(NSString *)time
{
    // 时间展示
    NSString *year = [NSString stringWithFormat:@"20%@", [time substringWithRange:NSMakeRange(0, 2)]];
    
    NSString *month = [time substringWithRange:NSMakeRange(2, 2)];
    
    NSString *day = [time substringWithRange:NSMakeRange(4, 2)];
    
    NSString *hour = [time substringWithRange:NSMakeRange(6, 2)];
    
    NSString *minute = [time substringWithRange:NSMakeRange(8, 2)];
    
    NSString *second = [time substringWithRange:NSMakeRange(10, 2)];
    
    NSString *newTime = [NSString stringWithFormat:@"%@-%@-%@ %@:%@:%@", year, month, day, hour, minute, second];
    
    return newTime;
}

/**
 继续发送请求数据指令给硬件
 
 @param index 当前序号 eg.001 002 003
 @param recevieStr 接收到的字符串
 */
- (void)continueSendDataWithIndex:(NSString *)index recevieStr:(NSString *)recevieStr
{
    
}

- (void)handleSycnBtnEvent
{
    
    [MBProgressHUD showActivityMessageInWindow:@"同步中，请保持设备靠近手机"];
    
    // 获取当前时间
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *currentDateStr = [dateFormatter stringFromDate:currentDate];
    // 2018-09-05 14:25:46
    NSInteger year = [[currentDateStr substringWithRange:NSMakeRange(2, 2)] integerValue];
    NSInteger month = [[currentDateStr substringWithRange:NSMakeRange(5, 2)] integerValue];
    NSInteger day = [[currentDateStr substringWithRange:NSMakeRange(8, 2)] integerValue];
    NSInteger hour = [[currentDateStr substringWithRange:NSMakeRange(11, 2)] integerValue];
    NSInteger minute = [[currentDateStr substringWithRange:NSMakeRange(14, 2)] integerValue];
    NSInteger second = [[currentDateStr substringWithRange:NSMakeRange(17, 2)] integerValue];
    
    // 校验当前时间
    // 年
    Byte yearByte[2];
    yearByte[0] = 0xcc;
    yearByte[1] = (Byte)year;
    
    // 月
    Byte monthByte[2];
    monthByte[0] = 0xcc;
    monthByte[1] = (Byte)month;
    
    // 日
    Byte dayByte[2];
    dayByte[0] = 0xcc;
    dayByte[1] = (Byte)day;
    
    // 时
    Byte hourByte[2];
    hourByte[0] = 0xcc;
    hourByte[1] = (Byte)hour;
    
    // 分
    Byte minuteByte[2];
    minuteByte[0] = 0xcc;
    minuteByte[1] = (Byte)minute;
    
    // 秒
    Byte secondByte[2];
    secondByte[0] = 0xcc;
    secondByte[1] = (Byte)second;
    
    // 当前星期
    Byte weekByte[2];
    weekByte[0] = 0xcc;
    weekByte[1] = (Byte)([self getCurrentWeekDay]);
    
//    [[BLECentralManager shareInstance].getFirstConnector sendData:[NSData dataWithBytes:&yearByte length:sizeof(yearByte)]];
//    [[BLECentralManager shareInstance].getFirstConnector sendData:[NSData dataWithBytes:&monthByte length:sizeof(monthByte)]];
//    [[BLECentralManager shareInstance].getFirstConnector sendData:[NSData dataWithBytes:&dayByte length:sizeof(dayByte)]];
//    [[BLECentralManager shareInstance].getFirstConnector sendData:[NSData dataWithBytes:&hourByte length:sizeof(hourByte)]];
//    [[BLECentralManager shareInstance].getFirstConnector sendData:[NSData dataWithBytes:&minuteByte length:sizeof(minuteByte)]];
//    [[BLECentralManager shareInstance].getFirstConnector sendData:[NSData dataWithBytes:&secondByte length:sizeof(secondByte)]];
//    [[BLECentralManager shareInstance].getFirstConnector sendData:[NSData dataWithBytes:&weekByte length:sizeof(weekByte)]];
    
    
    
   
}

- (void)startTranslation
{
    
}


#pragma mark - 滑动切换控制器代理
- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController
{
    return self.subVCNameArray.count;
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index
{
    if (index == 0) {

        DBXBloodPressureOxygenHeartbeatViewController *bloodPressureOxygenHeartbeatVC = [DBXBloodPressureOxygenHeartbeatViewController new];
        bloodPressureOxygenHeartbeatVC.bloodPressureMarray = self.highPressDataMarray;
        bloodPressureOxygenHeartbeatVC.bloodOxygenMarray = self.bloodOxygenDataMarray;
        bloodPressureOxygenHeartbeatVC.heartBeatMarray = self.heartbeatDataMarray;
        bloodPressureOxygenHeartbeatVC.timeMarray = self.bloodPressureTimeDataMarray;
        
        return bloodPressureOxygenHeartbeatVC;
        
    } else if (index == 1) {
    
        DBXBloodSugarViewController *bloodSugarVC = [DBXBloodSugarViewController new];
        bloodSugarVC.bloodSugarDataMarray = self.bloodSugarDataMarray;
        bloodSugarVC.timeDataMarray = self.bloodSugarTimeDataMarray;
        return bloodSugarVC;
        
    } else if (index == 2) {
        
        DBXStepNumberViewController *stepNumberVC = [DBXStepNumberViewController new];
        return stepNumberVC;
        
    } else {
        
        
        return [UIViewController new];
    }
}

- (void)pageController:(WMPageController *)pageController didEnterViewController:(__kindof UIViewController *)viewController withInfo:(NSDictionary *)info
{
    
    NSLog(@"selectIndex - %d", pageController.selectIndex);
    _currentPageVCIndex = pageController.selectIndex;
    
}

- (void)handleDeleteBtnEvent
{
    switch (_currentPageVCIndex) {
        case 0:
        {
            NSLog(@"handleUploadBloodPressEvent");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"handleDeleteBpOxyHbEvent" object:nil];
        }
            break;
        case 1:
        {
            NSLog(@"handleUploadBloodPressSugarEvent");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"handleDeleteBloodSugarEvent" object:nil];
        }
            break;
        case 2:
        {
            NSLog(@"handleUploadStepNumberEvent");
        }
            break;
            
        default:
            break;
    }
    
}

- (void)handleUploadBtnEvent
{
    switch (_currentPageVCIndex) {
        case 0:
        {
            NSLog(@"handleUploadBloodPressEvent");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"handleUploadBpOxyHbEvent" object:nil];
        }
            break;
        case 1:
        {
            NSLog(@"handleUploadBloodPressSugarEvent");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"handleUploadBloodSugarEvent" object:nil];
        }
            break;
        case 2:
        {
            NSLog(@"handleUploadStepNumberEvent");
        }
            break;
            
        default:
            break;
    }
    
    
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index
{
    return self.subVCNameArray[index];
}

- (NSInteger)getWeekDayFordate:(NSTimeInterval)data
{
    NSArray *weekday = [NSArray arrayWithObjects: [NSNull null], @"周日", @"周一", @"周二", @"周三", @"周四", @"周五", @"周六", nil];
    NSDate *newDate = [NSDate dateWithTimeIntervalSince1970:data];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitWeekday fromDate:newDate];
    
    return components.weekday;
}

- (NSInteger)getCurrentWeekDay
{
    NSTimeInterval interval = [[NSDate date] timeIntervalSince1970];
    return [self getWeekDayFordate:interval];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
