//
//  ZJDeviceManageViewController.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/9.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 设备管理
 */
@interface ZJDeviceManageViewController : UIViewController

@property (nonatomic, assign) BOOL isNewUser;   // 是否是新用户

@end

NS_ASSUME_NONNULL_END
