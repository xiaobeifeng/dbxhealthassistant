//
//  ZJSyncBSViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/6.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "ZJSyncBSViewController.h"
#import "ZJSyncBSTableViewCell.h"
#import "ZJSyncBSModel.h"
#import "DBXBloodSugarDataModel.h"
#import "DBXBloodSugarDataSyncApi.h"
#import "BRStringPickerView.h"

@interface ZJSyncBSViewController ()<UITableViewDelegate, UITableViewDataSource, ZJSyncBSTableViewCellDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *selectMutableArray;
@property (nonatomic, strong) FMDatabase *dataBase;
@property (nonatomic, strong) NSMutableArray *dataMutableArray;
@end

@implementation ZJSyncBSViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadView:) name:@"CompleteSync" object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"CompleteSync" object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blueColor];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-kSafeAreaTopHeight-150-40-60) style:UITableViewStylePlain];
    [self.tableView registerClass:[ZJSyncBSTableViewCell class] forCellReuseIdentifier:NSStringFromClass([ZJSyncBSTableViewCell class])];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    
    _dataMutableArray = [NSMutableArray new];
    _selectMutableArray = [NSMutableArray new];
    
    [self setupHeaderView];
    [self setupBottomView];
    
}

- (void)setupBottomView {
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.tableView.height, kScreenWidth, 60)];
    bottomView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bottomView];
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = kTableSeparatorLineColor;
    [bottomView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bottomView);
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(kScreenWidth);
    }];
    
    UIButton *deleteBtn = [UIButton new];
    deleteBtn.titleLabel.font = kFont(14);
    kViewRadius(deleteBtn, 5.0f);
    [deleteBtn setTitle:@"删除" forState:UIControlStateNormal];
    [deleteBtn addTarget:self action:@selector(deleteBtnAction) forControlEvents:UIControlEventTouchUpInside];
    deleteBtn.backgroundColor = [UIColor redColor];
    [bottomView addSubview:deleteBtn];
    
    UIButton *uploadBtn = [UIButton new];
    uploadBtn.titleLabel.font = kFont(14);
    kViewRadius(uploadBtn, 5.0f);
    [uploadBtn setTitle:@"上传" forState:UIControlStateNormal];
    [uploadBtn addTarget:self action:@selector(uploadBtnAction) forControlEvents:UIControlEventTouchUpInside];
    uploadBtn.backgroundColor = kNavigationBarColor;
    [bottomView addSubview:uploadBtn];
    
    [@[deleteBtn, uploadBtn] mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedItemLength:120 leadSpacing:30 tailSpacing:30];
    [@[deleteBtn, uploadBtn] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bottomView.mas_centerY);
        make.bottom.equalTo(bottomView).with.offset(-13);
    }];
}

- (void)setupHeaderView {
    
    UIView *headerView = [UIView new];
    headerView.frame = CGRectMake(0, 0, kScreenWidth, 40);
    self.tableView.tableHeaderView = headerView;
    
    UIButton *selectAllButton = [UIButton new];
    [selectAllButton setImage:[UIImage imageNamed:@"button_cell"] forState:UIControlStateNormal];
    [selectAllButton setImage:[UIImage imageNamed:@"button_cell_select"] forState:UIControlStateSelected];
    [selectAllButton addTarget:self action:@selector(selectAllButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:selectAllButton];
    [selectAllButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(25);
        make.centerY.equalTo(headerView.mas_centerY);
        make.left.equalTo(headerView).with.offset(20);
    }];
    
    UILabel *bloodSugarLabel = [UILabel new];
    bloodSugarLabel.font = [UIFont systemFontOfSize:14];
    bloodSugarLabel.textAlignment = NSTextAlignmentCenter;
    bloodSugarLabel.text = @"血糖";
    [headerView addSubview:bloodSugarLabel];
    
    UILabel *typeLabel = [UILabel new];
    typeLabel.font = [UIFont systemFontOfSize:14];
    typeLabel.textAlignment = NSTextAlignmentCenter;
    typeLabel.text = @"测量条件";
    [headerView addSubview:typeLabel];
    
    UILabel *timeLabel = [UILabel new];
    timeLabel.font = [UIFont systemFontOfSize:14];
    timeLabel.numberOfLines = 0;
    timeLabel.textAlignment = NSTextAlignmentCenter;
    timeLabel.text = @"时间";
    [headerView addSubview:timeLabel];
    
    NSArray *buttons = @[bloodSugarLabel, typeLabel, timeLabel];
    // 水平方向控件间隔固定等间隔
    [buttons mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:2 leadSpacing:65 tailSpacing:20];
    [buttons mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(headerView.mas_centerY);
        make.height.mas_equalTo(40);
    }];
    
    UIView *line = [UIView new];
    line.backgroundColor = kTableSeparatorLineColor;
    [headerView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(headerView.mas_bottom);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(0.5);
    }];
}

- (void)reloadView:(NSNotification *)notification {
    
    // 读取数据库数据
    NSString *fmdbName = [NSString stringWithFormat:@"%@_PeripheralSyncData.sqlite",  [[NSUserDefaults standardUserDefaults] objectForKey:kAccount]];
    NSString *fileName = [kDocumentPath stringByAppendingPathComponent:fmdbName];
    _dataBase = [FMDatabase databaseWithPath:fileName];
    if ([_dataBase open]) {
        NSString * sql = @"SELECT * FROM t_recevieInfos1";
        FMResultSet * rs = [_dataBase executeQuery:sql];
        while ([rs next]) {
            
            NSString *Id = [rs stringForColumn:@"id"];
            NSString *recevice = [rs stringForColumn:@"recevice"];
            
            NSLog(@"%@, %@", Id, recevice);
            
            // 获取数据段的时间 28～40
            NSString *receviceTime = [recevice substringWithRange:NSMakeRange(recevice.length-12, 12)];
            // 血糖
            NSString *bloodSugar = [recevice substringWithRange:NSMakeRange(3, 3)];
            
            // 如果接收到的时间字符中含有N或者血糖字符串为NNN,就舍弃该条接收的字符串
            if (![receviceTime containsString:@"N"]  && ![bloodSugar isEqualToString:@"NNN"]) {
                
                [self insertTableView:bloodSugar receviceTime:receviceTime Id:Id];
                
            } else {
                
            }
            
            NSLog(@"receviceTime - %@", receviceTime);
            
        }
        
        [_dataBase close];
    }
}

/**
 TableView插入接收到的字符串
 */
- (void)insertTableView:(NSString *)bloodSugar receviceTime:(NSString *)receviceTime Id:(NSString *)Id {
    
    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_dataMutableArray.count inSection:0];
    [indexPaths addObject:indexPath];
    
    NSString *bs = [NSString stringWithFormat:@"%.1f", [bloodSugar floatValue]/10];
    
    // 时间
    NSString *year = [NSString stringWithFormat:@"%@", [receviceTime substringWithRange:NSMakeRange(0, 2)]];      // 年
    NSString *month = [NSString stringWithFormat:@"%@", [receviceTime substringWithRange:NSMakeRange(2, 2)]];     // 月
    NSString *day = [NSString stringWithFormat:@"%@", [receviceTime substringWithRange:NSMakeRange(4, 2)]];        // 日
    NSString *hour = [NSString stringWithFormat:@"%@", [receviceTime substringWithRange:NSMakeRange(6, 2)]];        // 时
    NSString *minute = [NSString stringWithFormat:@"%@", [receviceTime substringWithRange:NSMakeRange(8, 2)]];        // 分
    NSString *second = [NSString stringWithFormat:@"%@", [receviceTime substringWithRange:NSMakeRange(10, 2)]];        // 秒
    NSString *time = [NSString stringWithFormat:@"20%@-%@-%@ %@:%@:%@", year, month, day, hour, minute, second];
    
    ZJSyncBSModel *model = [ZJSyncBSModel new];
    model.isSelect = NO;
    model.bloodSugar = bs;
    model.time = time;
    model.Id = Id;
    model.type = 0; // 上传时段默认为0空腹
    [_dataMutableArray addObject:model];
    
    [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataMutableArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZJSyncBSTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZJSyncBSTableViewCell class]) forIndexPath:indexPath];
    cell.selectButton.tag = indexPath.row;
    cell.typeButton.tag = indexPath.row;
    cell.delegate = self;
    ZJSyncBSModel *model = _dataMutableArray[indexPath.row];
    cell.selectButton.selected = model.isSelect;
    cell.bloodSugarLabel.text = model.bloodSugar;
    cell.timeLabel.text = model.time;
    cell.type = model.type;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)cellButtonAcion:(UIButton *)button {
    
    if (!button.selected) {
        button.selected = YES;
        ZJSyncBSModel *model = _dataMutableArray[button.tag];
        model.isSelect = YES;
        [_selectMutableArray addObject:self.dataMutableArray[button.tag]];
    } else {
        button.selected = NO;
        ZJSyncBSModel *model = _dataMutableArray[button.tag];
        model.isSelect = NO;
        [_selectMutableArray removeObject:self.dataMutableArray[button.tag]];
    }
    
    NSLog(@"_selectMutableArray - %@", _selectMutableArray);
    
}


/**
 测量时段选择按钮
 */
- (void)typeButtonAcion:(UIButton *)button {
    
    NSArray *titleArray = @[@"空腹", @"早餐前", @"早餐后", @"午餐前", @"午餐后", @"晚餐前", @"晚餐后", @"睡前"];
    
    [BRStringPickerView showStringPickerWithTitle:@"请选择测量时间段" dataSource:titleArray defaultSelValue:titleArray[0] isAutoSelect:NO themeColor:nil resultBlock:^(id selectValue) {
        
        NSLog(@"%ld", (long)button.tag);
        ZJSyncBSModel *model = self.dataMutableArray[button.tag];
        model.type = [titleArray indexOfObject:selectValue];
        [self.tableView reloadRow:button.tag inSection:0 withRowAnimation:0];
        
    } cancelBlock:^{
        
        NSLog(@"点击了背景视图或取消按钮");
    }];
}

/**
 全选按钮
 */
- (void)selectAllButtonAction:(UIButton *)button {
    
    _selectMutableArray = [NSMutableArray new];
    
    if (!button.selected) {
        button.selected = YES;
        for (int i = 0; i<_dataMutableArray.count; i++) {
            ZJSyncBSModel *model = _dataMutableArray[i];
            model.isSelect = YES;
            [_selectMutableArray addObject:model];
        }
    } else {
        button.selected = NO;
        for (int i = 0; i<_dataMutableArray.count; i++) {
            ZJSyncBSModel *model = _dataMutableArray[i];
            model.isSelect = NO;
        }
    }
    
    [self.tableView reloadData];
    
}

/**
 上传按钮点击
 */
- (void)uploadBtnAction {
    
    if (kArrayIsEmpty(_selectMutableArray)) {
        [MBProgressHUD showTipMessageInView:@"请选择要上传的数据"];
        return;
    }
    
    NSMutableArray *uploadMutableArray = [NSMutableArray new];
    for (int i = 0; i < _selectMutableArray.count; i++) {
        
        ZJSyncBSModel *model = _selectMutableArray[i];
        
        NSDictionary *dic = @{@"mCondition":@(model.type), @"bloodGluVal":model.bloodSugar, @"upsaveTime":model.time};

        [uploadMutableArray addObject:dic];
    }
    
    // 上传json数据
    NSData *data = [NSJSONSerialization dataWithJSONObject:uploadMutableArray options:NSJSONWritingPrettyPrinted error:nil];
    NSString *json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSData *uploadData = [json dataUsingEncoding:NSUTF8StringEncoding];
    DBXBloodSugarDataSyncApi *api = [[DBXBloodSugarDataSyncApi alloc] initWithJsonData:uploadData];
    [MBProgressHUD showActivityMessageInView:@"上传中"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        [MBProgressHUD hideHUD];
        NSLog(@"成功 %@", request.responseJSONObject);
        
        if ([[request.responseJSONObject objectForKey:@"state"] integerValue] != 0) {
            [MBProgressHUD showTipMessageInView:[request.responseJSONObject objectForKey:@"message"]];
            return;
        }
        
        // 删除选择的数据项
        [self.dataBase open];
        for (int i = 0; i<self.selectMutableArray.count; i++) {
            ZJSyncBSModel *model = self.selectMutableArray[i];
            NSString *deleteSql = [NSString stringWithFormat:@"delete from t_recevieInfos1 where id = '%@'", model.Id];
            BOOL res = [self.dataBase executeUpdate:deleteSql];
            if (res) {
                NSLog(@"删除成功");
                [self.dataMutableArray removeObject:self.selectMutableArray[i]];
            }
        }
        self.selectMutableArray = [NSMutableArray new];
        [self.tableView reloadData];
        [MBProgressHUD showTipMessageInView:@"上传完成"];
        
        // 发送通知，刷新首页数据
        [[NSNotificationCenter defaultCenter] postNotificationName:kReloadDataApiAndDataDetailApi object:nil userInfo:nil];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        [MBProgressHUD hideHUD];
        [MBProgressHUD showTipMessageInView:@"请稍后再试"];
        
    }];
}

/**
 删除按钮点击
 */
- (void)deleteBtnAction {
    
    if (kArrayIsEmpty(_selectMutableArray)) {
        [MBProgressHUD showTipMessageInView:@"请选择要删除的数据"];
        return;
    }
    
    [_dataBase open];
    for (int i = 0 ; i<_selectMutableArray.count; i++) {
        ZJSyncBSModel *model = _selectMutableArray[i];
        [_dataMutableArray removeObject:model];
        // 从数据库中删除该条数据
        NSString *deleteSql = [NSString stringWithFormat:@"delete from t_recevieInfos1 where id = '%@'", model.Id];
        BOOL res = [_dataBase executeUpdate:deleteSql];
        if (res) {
            NSLog(@"删除成功");
        }
        
    }
    
    _selectMutableArray = [NSMutableArray new];
    [self.tableView reloadData];
    [_dataBase close];
    
    [MBProgressHUD showTipMessageInView:@"删除成功"];
}

@end
