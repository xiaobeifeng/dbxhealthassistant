//
//  DBXPeripheralManagementViewController.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/23.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 设备管理
 */
@interface DBXPeripheralManagementViewController : UIViewController

@property(nonatomic, assign) BOOL isFirstLink;

/**
 是否是时间校准
 */
@property(nonatomic, assign) BOOL isTimeCorrect;
@end
