//
//  DBXBloodPressureOxygenHeartbeatViewController.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/27.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WMStickyPageViewController.h"

/**
 血压，血氧，心率
 */
@interface DBXBloodPressureOxygenHeartbeatViewController : UIViewController<WMStickyPageViewControllerDelegate>


@property(nonatomic, strong) NSMutableArray *bloodPressureMarray;
@property(nonatomic, strong) NSMutableArray *bloodOxygenMarray;
@property(nonatomic, strong) NSMutableArray *heartBeatMarray;
@property(nonatomic, strong) NSMutableArray *timeMarray;


@end
