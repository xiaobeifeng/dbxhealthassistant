//
//  ZJDeviceSyncViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/6.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "ZJDeviceSyncViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import <BabyBluetooth.h>
#import "DBXScrollView.h"
#import "ZJSyncHPHOHBViewController.h"
#import "ZJSyncBSViewController.h"
#import "ZJSyncSportViewController.h"
#import "DBXMePersonalinfoApi.h"
#import "DBXMePersonalinfoModel.h"

@interface ZJDeviceSyncViewController ()<UIScrollViewDelegate>

@property (nonatomic, strong) DBXScrollView *scrollView;
@property (nonatomic, strong) UIView *titleView;
/** 上一次点击的标题按钮 */
@property (nonatomic, weak) UIButton *previousClickedTitleButton;
/** 标题下划线 */
@property (nonatomic, weak) UIView *titleUnderline;
/** 存储接收的字符信息 */
@property (nonatomic, strong) NSMutableArray *receviceMutableArray;
@property (nonatomic, strong) BabyBluetooth *baby;
/** 当前连接设备 */
@property(strong,nonatomic)CBPeripheral *currPeripheral;
@property (nonatomic, strong) CBCharacteristic *characteristic;
/** 搜索蓝牙计时器 */
@property (nonatomic, strong) NSTimer *connectTimer;

@property (nonatomic, strong) FMDatabase *dataBase;

@property (nonatomic, strong) UILabel *peripheralInfoLbl;

@end

@implementation ZJDeviceSyncViewController

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.connectTimer invalidate];     // 移除定时器
    [_baby cancelAllPeripheralsConnection]; // 取消所有连接
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"数据同步";
    
    UIView *topView = [UIView new];
    topView.frame = CGRectMake(0, 0, kScreenWidth, 150);
    topView.backgroundColor = kNavigationBarColor;
    [self.view addSubview:topView];
    
    UIImageView *imageView = [UIImageView new];
    imageView.image = [UIImage imageNamed:@"peripheral_watch"];
    [topView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(80);
        make.width.mas_equalTo(50);
        make.centerY.equalTo(topView);
        make.left.equalTo(topView).with.offset(30);
    }];
    
 
    _peripheralInfoLbl = [UILabel new];
    _peripheralInfoLbl.font = kFont(14);
    _peripheralInfoLbl.textColor = [UIColor whiteColor];
    _peripheralInfoLbl.numberOfLines = 0;
    [_peripheralInfoLbl sizeToFit];
    [topView addSubview:_peripheralInfoLbl];
    [_peripheralInfoLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageView.mas_right).with.offset(20);
        make.centerY.equalTo(topView.mas_centerY);
    }];
    
    
    _receviceMutableArray = [NSMutableArray new];
    
    [self setupAllChildVCs];
    [self setupTitlesView];
    [self setupScrollView];
    [self setupTitleButtons];
    [self setupTitleUnderline];
    
    [self addChildVcViewIntoScrollView:0];
    
    [self checkPersonInfo];

}

- (void)setupAllChildVCs {
    
    [self addChildViewController:[ZJSyncHPHOHBViewController new]];
    [self addChildViewController:[ZJSyncBSViewController new]];
    [self addChildViewController:[ZJSyncSportViewController new]];
}

- (void)setupScrollView {
    
    self.scrollView = [DBXScrollView new];
    self.scrollView.frame = CGRectMake(0, self.titleView.xmg_y+self.titleView.height, kScreenWidth, kScreenHeight-kSafeAreaTopHeight-150-40);
    self.scrollView.backgroundColor = [UIColor whiteColor];
    self.scrollView.contentSize = CGSizeMake(kScreenWidth * 3, self.scrollView.height);
    self.scrollView.delegate = self;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.bounces = NO;
    self.scrollView.pagingEnabled = YES;
    [self.view addSubview:self.scrollView];
}

- (void)setupTitlesView {
    // 设置顶部栏
    self.titleView = [UIView new];
    self.titleView.frame = CGRectMake(0, 150, kScreenWidth, 40);
    self.titleView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.titleView];
    
}

- (void)setupTitleButtons {
    
    NSArray *titleArray = @[@"血压/血氧/心率", @"血糖", @"运动"];
    for (int i=0; i<titleArray.count; i++) {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(i*kScreenWidth/titleArray.count, 0, kScreenWidth/titleArray.count, 40)];
        [self.titleView addSubview:button];
        button.titleLabel.font = [UIFont systemFontOfSize:14];
        [button setTitle:titleArray[i] forState:UIControlStateNormal];
        button.tag = i;
        [button setTitleColor:kNavigationBarColor forState:UIControlStateNormal];
        [button setTitleColor:kNavigationBarColor forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    UIView *syncHPHOHBView = self.childViewControllers[0].view;
    syncHPHOHBView.frame = CGRectMake(0, 0, kScreenWidth, self.scrollView.height);
    [self.scrollView addSubview:syncHPHOHBView];
    
    UIView *syncBSView = self.childViewControllers[1].view;
    syncBSView.frame = CGRectMake(kScreenWidth, 0, kScreenWidth, self.scrollView.height);
    [self.scrollView addSubview:syncBSView];
    
    UIView *syncSportView = self.childViewControllers[2].view;
    syncSportView.frame = CGRectMake(kScreenWidth*2, 0, kScreenWidth, self.scrollView.height);
    [self.scrollView addSubview:syncSportView];
    
}

/**
 *  标题下划线
 */
- (void)setupTitleUnderline {
    // 标题按钮
    UIButton *firstTitleButton = self.titleView.subviews.firstObject;
    
    // 下划线
    UIView *titleUnderline = [[UIView alloc] init];
    titleUnderline.xmg_height = 2;
    titleUnderline.xmg_y = self.titleView.xmg_height - titleUnderline.xmg_height;
    titleUnderline.backgroundColor = [firstTitleButton titleColorForState:UIControlStateSelected];
    [self.titleView addSubview:titleUnderline];
    self.titleUnderline = titleUnderline;
    
    // 切换按钮状态
    firstTitleButton.selected = YES;
    self.previousClickedTitleButton = firstTitleButton;
    
    [firstTitleButton.titleLabel sizeToFit]; // 让label根据文字内容计算尺寸
    self.titleUnderline.xmg_width = firstTitleButton.xmg_width;
    self.titleUnderline.xmg_centerX = firstTitleButton.xmg_centerX;
}

//- (void)setupBottomView {
//
//    UIView *bottomView = [UIView new];
//    bottomView.backgroundColor = [UIColor greenColor];
//    bottomView.frame = CGRectMake(0, self.scrollView.xmg_y+self.scrollView.xmg_height, kScreenWidth, 60);
//    [self.view addSubview:bottomView];
//
//}

/**
 *  当用户松开scrollView并且滑动结束时调用这个代理方法（scrollView停止滚动的时候）
 */
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    // 求出标题按钮的索引
    NSUInteger index = scrollView.contentOffset.x / scrollView.xmg_width;
    
    // 点击对应的标题按钮
    UIButton *button = self.titleView.subviews[index];
    [self buttonAction:button];
}

- (void)buttonAction:(UIButton *)button {
    self.previousClickedTitleButton.selected = NO;
    button.selected = YES;
    self.previousClickedTitleButton = button;
    
    NSInteger index = button.tag;
    
    [UIView animateWithDuration:0.01 animations:^{
        
        // 处理下划线
        self.titleUnderline.xmg_width = button.xmg_width;
        self.titleUnderline.xmg_centerX = button.xmg_centerX;
        
        // 滚动scrollView
        CGFloat offsetX = self.scrollView.xmg_width * index;
        self.scrollView.contentOffset = CGPointMake(offsetX, self.scrollView.contentOffset.y);
    } completion:^(BOOL finished) {
        [self addChildVcViewIntoScrollView:index];
    }];
    
    // 设置index位置对应的tableView.scrollsToTop = YES， 其他都设置为NO
    for (NSUInteger i = 0; i < self.childViewControllers.count; i++) {
        UIViewController *childVc = self.childViewControllers[i];
        // 如果view还没有被创建，就不用去处理
        if (!childVc.isViewLoaded) continue;
        
        UIScrollView *scrollView = (UIScrollView *)childVc.view;
        if (![scrollView isKindOfClass:[UIScrollView class]]) continue;
        
        scrollView.scrollsToTop = (i == index);
    }
}

/**
 *  添加第index个子控制器的view到scrollView中
 */
- (void)addChildVcViewIntoScrollView:(NSUInteger)index {
    UIViewController *childVc = self.childViewControllers[index];
    
    // 如果view已经被加载过，就直接返回
    if (childVc.isViewLoaded) return;
    
    // 取出index位置对应的子控制器view
    UIView *childVcView = childVc.view;
    
    // 设置子控制器view的frame
    CGFloat scrollViewW = self.scrollView.xmg_width;
    childVcView.frame = CGRectMake(index * scrollViewW, 0, scrollViewW, self.scrollView.xmg_height);
    // 添加子控制器的view到scrollView中
    [self.scrollView addSubview:childVcView];
}

/**
 设置蓝牙
 */
- (void)setupBlueTooth {
    
    _baby = [BabyBluetooth shareBabyBluetooth];
    _baby.scanForPeripherals().begin();
    // 开一个定时器监控连接超时的情况
    _connectTimer = [NSTimer scheduledTimerWithTimeInterval:20.0f target:self selector:@selector(connectTimeout) userInfo:nil repeats:NO];
    [MBProgressHUD showActivityMessageInWindow:@"搜索设备中"];
    
    __weak typeof(self) weakSelf = self;
    [_baby setBlockOnCentralManagerDidUpdateState:^(CBCentralManager *central) {
        if (central.state != CBManagerStatePoweredOn) {
            [MBProgressHUD showTipMessageInView:@"请检查蓝牙是否开启后再试"];
        } else {
            // 开始扫描外设
            [central scanForPeripheralsWithServices:nil options:nil];
        }
    }];
    
    //设置扫描到设备的委托
    [_baby setBlockOnDiscoverToPeripherals:^(CBCentralManager *central, CBPeripheral *peripheral, NSDictionary *advertisementData, NSNumber *RSSI) {
        NSLog(@"搜索到了设备:%@",peripheral.name);
        
        // 获取本地保存绑定的设备名
        NSString *deviceName = [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_NAME];
        if ([peripheral.name isEqualToString:deviceName]) {
            [MBProgressHUD hideHUD];
            [MBProgressHUD showActivityMessageInWindow:@"设备连接中"];
            [weakSelf.baby cancelScan];     // 取消扫描外设
            [weakSelf.connectTimer invalidate];     // 移除定时器
            weakSelf.baby.having(peripheral).and.connectToPeripherals().begin();    // 开始连接当前外设
            weakSelf.peripheralInfoLbl.text = [NSString stringWithFormat:@"当前绑定设备：%@", deviceName];
        }
    }];
    
    // 设置设备连接失败的委托
    [_baby setBlockOnFailToConnect:^(CBCentralManager *central, CBPeripheral *peripheral, NSError *error) {
        [MBProgressHUD hideHUD];
        NSLog(@"设备：%@--连接失败",peripheral.name);
        [MBProgressHUD showTipMessageInView:@"请稍后后再试"];
        [weakSelf.connectTimer invalidate];     // 移除定时器
        [weakSelf.baby cancelScan]; //  取消扫描外设
    }];
    
    [_baby setBlockOnCancelScanBlock:^(CBCentralManager *centralManager) {
        NSLog(@"取消扫描");
        //        [MBProgressHUD hideHUD];
    }];
    
    // 设置设备连接成功的委托,同一个baby对象，使用不同的channel切换委托回调
    [_baby setBlockOnConnected:^(CBCentralManager *central, CBPeripheral *peripheral) {
        NSLog(@"%@", peripheral.name);
        [MBProgressHUD hideHUD];
        [MBProgressHUD showTipMessageInView:@"设备连接成功"];
        // 查找服务
        [peripheral discoverServices:nil];
    }];
    
    // 设置发现设备的Services的委托
    [_baby setBlockOnDiscoverServices:^(CBPeripheral *peripheral, NSError *error) {
        for (CBService *s in peripheral.services) {
            NSLog(@"UUID - %@", s.UUID);
            if ([s.UUID.UUIDString isEqualToString:@"FFF0"]) {
                [peripheral discoverCharacteristics:nil forService:s];
            }
        }
    }];
    
    // 设置发现设service的Characteristics的委托
    [_baby setBlockOnDiscoverCharacteristics:^(CBPeripheral *peripheral, CBService *service, NSError *error) {
        for (CBCharacteristic *characteristic in service.characteristics) {
            
            if ([characteristic.UUID.UUIDString isEqualToString:@"FFF6"]) {
                weakSelf.currPeripheral = peripheral;
                weakSelf.characteristic = characteristic;
                [peripheral setNotifyValue:YES forCharacteristic:characteristic];
                [weakSelf syncTimeToDeviceWithCurrentPeripheral:peripheral characteristic:characteristic];
                
            }
        }
    }];
}


/**
 搜索设备超时
 */
- (void)connectTimeout {
    [MBProgressHUD hideHUD];
    [_connectTimer invalidate];     // 移除定时器
    [MBProgressHUD showTipMessageInView:@"未搜索到当前绑定设备，请检查设备后再试"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
    
}

/**
 同步时间成功，开始发送同步数据指令，同步手表数据到手机
 */
- (void)successSyncTime {
    
    [MBProgressHUD showActivityMessageInWindow:@"数据同步中，请将手表靠近手机"];
    // 发送同步数据指令
    Byte startSyncByte[2];
    startSyncByte[0] = 0xee;
    startSyncByte[1] = (Byte)1;
    [self.currPeripheral writeValue:[NSData dataWithBytes:&startSyncByte length:sizeof(startSyncByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithResponse];
    
    __weak typeof(self) weakSelf = self;
    //    设置读取characteristics的委托
    [_baby setBlockOnReadValueForCharacteristic:^(CBPeripheral *peripheral, CBCharacteristic *characteristic, NSError *error) {
        
        NSData *value = characteristic.value;
        NSString *receiveStr = [[NSString alloc] initWithData:value encoding:NSUTF8StringEncoding];
        
        NSLog(@"%@", receiveStr);
        NSString *stopOrder = @"FFFFFFFFFFFFFFFFFFFF";  // 结束指令
        
        if ([receiveStr isEqualToString:stopOrder]) {
            [MBProgressHUD hideHUD];
            [MBProgressHUD showTipMessageInView:@"同步完成"];
            
            // 发送通知给ZJSyncHPHOHBViewController，读取数据，刷新视图
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CompleteSync" object:@""];
            
            return; // 结束同步了
        }
        
        // 创建数据库
        NSString *fmdbName = [NSString stringWithFormat:@"%@_PeripheralSyncData.sqlite",  [[NSUserDefaults standardUserDefaults] objectForKey:kAccount]];
        NSString *fileName = [kDocumentPath stringByAppendingPathComponent:fmdbName];
        weakSelf.dataBase = [FMDatabase databaseWithPath:fileName];
        if ([weakSelf.dataBase open]) {
            
            // 创建保存接收字符串的表
            NSString *bpSql = @"CREATE TABLE IF NOT EXISTS t_recevieInfos (id integer PRIMARY KEY AUTOINCREMENT, recevice text NOT NULL);";
            // 创建保存接收字符串的表
            NSString *bpSql1 = @"CREATE TABLE IF NOT EXISTS t_recevieInfos1 (id integer PRIMARY KEY AUTOINCREMENT, recevice text NOT NULL);";
            
            BOOL bpResult = [weakSelf.dataBase executeUpdate:bpSql];
            BOOL bpResult1 = [weakSelf.dataBase executeUpdate:bpSql1];
            
            if (bpResult) {
                NSLog(@"创建表成功");
            }
            if (bpResult1) {
                NSLog(@"创建表成功");
            }
        }
        
        
        NSString *receiveIndex = [receiveStr substringWithRange:NSMakeRange(0, 3)]; // 获得接收消息的序号
        NSLog(@"receiveIndex - %@", receiveIndex);
        // 继续发送请求数据指令 001 -> 002
        Byte byte[2];
        byte[0] = 0xee;
        byte[1] = (Byte)([receiveIndex intValue] + 1);
        [peripheral writeValue:[NSData dataWithBytes:&byte length:sizeof(byte)] forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
        
        [weakSelf.receviceMutableArray addObject:receiveStr];   // receviceMutableArray暂时保存接收到的字符串
        if (weakSelf.receviceMutableArray.count == 2) {
            
            NSString *receviceTwoStr = [NSString stringWithFormat:@"%@%@", weakSelf.receviceMutableArray[0], weakSelf.receviceMutableArray[1]];
            
            // 血压等数据存入数据库中
            NSString *sql = @"INSERT INTO t_recevieInfos (recevice) VALUES (?);";
            BOOL isSuccess = [weakSelf.dataBase executeUpdate:sql, receviceTwoStr];
            
            NSString *sql1 = @"INSERT INTO t_recevieInfos1 (recevice) VALUES (?);";
            BOOL isSuccess1 = [weakSelf.dataBase executeUpdate:sql1, receviceTwoStr];
            
            [weakSelf.dataBase lastError];
            
            if (isSuccess) {
                NSLog(@"插入数据成功");
                [weakSelf.receviceMutableArray removeAllObjects];
            }
            if (isSuccess1) {
                NSLog(@"插入数据成功");
            }
            
        }
        
    }];
}

/**
 给外设同步时间
 */
- (void)syncTimeToDeviceWithCurrentPeripheral:(CBPeripheral *)peripheral
                               characteristic:(CBCharacteristic *)characteristic {
    
    [MBProgressHUD hideHUD];
    // 获取身高体重
    DBXMePersonalinfoApi *api = [DBXMePersonalinfoApi new];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        DBXMePersonalinfoModel *model = [DBXMePersonalinfoModel yy_modelWithJSON:request.responseJSONObject];
        NSArray *array = [NSArray yy_modelArrayWithClass:[DBXMePersonalinfoModelData class] json:model.data];
        if (!kArrayIsEmpty(array)) {
            
            // 获取当前时间
            NSDate *currentDate = [NSDate date];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *currentDateStr = [dateFormatter stringFromDate:currentDate];
            
            // eg.2018-09-05 14:25:46
            NSInteger fullYear = [[currentDateStr substringWithRange:NSMakeRange(0, 4)] integerValue];  // 用于计算年龄
            NSInteger year = [[currentDateStr substringWithRange:NSMakeRange(2, 2)] integerValue];
            NSInteger month = [[currentDateStr substringWithRange:NSMakeRange(5, 2)] integerValue];
            NSInteger day = [[currentDateStr substringWithRange:NSMakeRange(8, 2)] integerValue];
            NSInteger hour = [[currentDateStr substringWithRange:NSMakeRange(11, 2)] integerValue];
            NSInteger minute = [[currentDateStr substringWithRange:NSMakeRange(14, 2)] integerValue];
            NSInteger second = [[currentDateStr substringWithRange:NSMakeRange(17, 2)] integerValue];
            
            // 校验当前时间
            Byte yearByte[2];
            yearByte[0] = 0xcc;
            yearByte[1] = (Byte)year;
            Byte monthByte[2];
            monthByte[0] = 0xcc;
            monthByte[1] = (Byte)month;
            Byte dayByte[2];
            dayByte[0] = 0xcc;
            dayByte[1] = (Byte)day;
            Byte hourByte[2];
            hourByte[0] = 0xcc;
            hourByte[1] = (Byte)hour;
            Byte minuteByte[2];
            minuteByte[0] = 0xcc;
            minuteByte[1] = (Byte)minute;
            Byte secondByte[2];
            secondByte[0] = 0xcc;
            secondByte[1] = (Byte)second;
            
            // 当前星期
            NSTimeInterval interval = [[NSDate date] timeIntervalSince1970];
            NSDate *newDate = [NSDate dateWithTimeIntervalSince1970:interval];
            NSArray *weekday = [NSArray arrayWithObjects: [NSNull null], @"7", @"1", @"2", @"3", @"4", @"5", @"6", nil];
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            NSDateComponents *components = [calendar components:NSCalendarUnitWeekday fromDate:newDate];
            NSString *weekStr = [weekday objectAtIndex:components.weekday];
            Byte weekByte[2];
            weekByte[0] = 0xcc;
            weekByte[1] = (Byte)([weekStr integerValue]);
            
            DBXMePersonalinfoModelData *modelData = array.firstObject;
            // 身高
            NSInteger height = [modelData.hight integerValue];
            Byte heightByte[2];
            heightByte[0] = 0xdd;
            heightByte[1] = (Byte)height;
            
            // 体重
            NSInteger weight = [modelData.weight integerValue];
            Byte weightByte[2];
            weightByte[0] = 0xdd;
            weightByte[1] = (Byte)weight;
            
            // 年龄
            NSString *birth = modelData.birth;
            NSInteger age;
            if (!kStringIsEmpty(birth)) {
                NSArray *birthArray = [birth componentsSeparatedByString:@"-"];
                NSInteger birthYear = [birthArray[0] integerValue];
                age = fullYear - birthYear;
            } else {
                age = 0;
            }
            Byte ageByte[2];
            ageByte[0] = 0xdd;
            ageByte[1] = (Byte)(age);
            
            // 性别
            NSInteger sexInteger;
            if ([modelData.sex isEqualToString:@"男"]) {
                sexInteger = 1;
            } else {
                sexInteger = 0;
            }
            Byte sexByte[2];
            sexByte[0] = 0xdd;
            sexByte[1] = (Byte)sexInteger;
            
            // 开始写入数据
            [peripheral writeValue:[NSData dataWithBytes:&yearByte length:sizeof(yearByte)] forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
            [peripheral writeValue:[NSData dataWithBytes:&monthByte length:sizeof(monthByte)] forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
            [peripheral writeValue:[NSData dataWithBytes:&dayByte length:sizeof(dayByte)] forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
            [peripheral writeValue:[NSData dataWithBytes:&hourByte length:sizeof(hourByte)] forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
            [peripheral writeValue:[NSData dataWithBytes:&minuteByte length:sizeof(minuteByte)] forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
            [peripheral writeValue:[NSData dataWithBytes:&secondByte length:sizeof(secondByte)] forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
            [peripheral writeValue:[NSData dataWithBytes:&weekByte length:sizeof(weekByte)] forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
            [peripheral writeValue:[NSData dataWithBytes:&heightByte length:sizeof(heightByte)] forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
            [peripheral writeValue:[NSData dataWithBytes:&weightByte length:sizeof(weightByte)] forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
            [peripheral writeValue:[NSData dataWithBytes:&sexByte length:sizeof(sexByte)] forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
            [peripheral writeValue:[NSData dataWithBytes:&ageByte length:sizeof(ageByte)] forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
            
            // 开始发送同步指令
            [self successSyncTime];
            
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [MBProgressHUD showTipMessageInView:@"同步失败，请稍后再试"];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [self.navigationController popViewControllerAnimated:YES];
        });
        
    }];

}

/**
 检查个人信息是否完善
 */
- (void)checkPersonInfo {
    DBXMePersonalinfoApi *api = [DBXMePersonalinfoApi new];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        DBXMePersonalinfoModel *model = [DBXMePersonalinfoModel yy_modelWithJSON:request.responseJSONObject];
        NSArray *array = [NSArray yy_modelArrayWithClass:[DBXMePersonalinfoModelData class] json:model.data];
        if (kArrayIsEmpty(array)) {
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"请完善个人信息后再试" message:@"" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
            [alertVC addAction:okAction];
            
            [self presentViewController:alertVC animated:YES completion:nil];
        } else {
         
            [self setupBlueTooth];  // 开启蓝牙模块
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        [MBProgressHUD showTipMessageInView:@"请稍后再试"];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [self.navigationController popViewControllerAnimated:YES];
        });
    }];
}


@end
