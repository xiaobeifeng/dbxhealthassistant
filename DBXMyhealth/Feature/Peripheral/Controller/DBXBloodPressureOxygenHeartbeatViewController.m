//
//  DBXBloodPressureOxygenHeartbeatViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/27.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXBloodPressureOxygenHeartbeatViewController.h"
#import "DBXBloodPressureOxygenHeartbeatTableViewCell.h"
#import "DBXBpOxyHbModel.h"
#import "DBXBpOxyHbApi.h"

static NSString *bloodPressureOxygenHeartbeatTableViewCellIdentifier = @"DBXBloodPressureOxygenHeartbeatTableViewCell";

@interface DBXBloodPressureOxygenHeartbeatViewController ()<UITableViewDelegate, UITableViewDataSource>

@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) NSMutableArray *dataMarray;
@property(nonatomic, strong) NSMutableArray *selectDataMarray;
@property(nonatomic, strong) FMDatabase *dataBase;
@property(nonatomic, strong) DBXBpOxyHbModel *bpOxyHbModel;

@end

@implementation DBXBloodPressureOxygenHeartbeatViewController

#pragma mark - 懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-40-kSafeAreaTopHeight-60) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[DBXBloodPressureOxygenHeartbeatTableViewCell class] forCellReuseIdentifier:bloodPressureOxygenHeartbeatTableViewCellIdentifier];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView setEditing:YES animated:YES];
        // 解决滑动视图顶部空出状态栏高度的问题
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)]) {
                self.edgesForExtendedLayout = UIRectEdgeNone;
            }
        }
    }
    
    return _tableView;
}

/**
 数据源
 */
- (NSMutableArray *)dataMarray
{
    if (!_dataMarray) {
        _dataMarray = [NSMutableArray new];
    }
    return _dataMarray;
}

/**
 手动选择的需要同步的数据
 */
- (NSMutableArray *)selectDataMarray
{
    if (!_selectDataMarray) {
        _selectDataMarray = [NSMutableArray new];
    }
    return _selectDataMarray;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uploadBpOxyHb) name:@"handleUploadBpOxyHbEvent" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteBpOxyHb) name:@"handleDeleteBpOxyHbEvent" object:nil];
    
    self.view.backgroundColor = [UIColor orangeColor];
    [self.view addSubview:self.tableView];
    
    [self resultFMDB];
    //    NSLog(@"bloodPressureMarray - %@", self.bloodPressureMarray);
    //
    //    if (_bloodPressureMarray.count > 0) {
    //        [_bloodPressureMarray insertObject:@"血压" atIndex:0];
    //    }
    //
    //    if (_bloodOxygenMarray.count > 0) {
    //         [_bloodOxygenMarray insertObject:@"血氧" atIndex:0];
    //    }
    //
    //    if (_heartBeatMarray.count > 0) {
    //        [_heartBeatMarray insertObject:@"心率" atIndex:0];
    //    }
    //
    //    if (_timeMarray.count > 0) {
    //        [_timeMarray insertObject:@"时间" atIndex:0];
    //    }
    
}

/**
 读取本地数据库存储同步信息, 刷新表格
 */
- (void)resultFMDB
{
    
    NSString *fmdbName = [NSString stringWithFormat:@"%@_PeripheralSyncData.sqlite",  [[NSUserDefaults standardUserDefaults] objectForKey:kAccount]];
    NSString *fileName = [kDocumentPath stringByAppendingPathComponent:fmdbName];
    _dataBase = [FMDatabase databaseWithPath:fileName];
    if ([_dataBase open]) {
        NSString * sql = [NSString stringWithFormat:@"SELECT * FROM %@", kBloodPressureTableName];
        FMResultSet * rs = [_dataBase executeQuery:sql];
        while ([rs next]) {
            
            NSString *Id = [rs stringForColumn:@"id"];
            int sPressure = [rs intForColumn:@"sPressure"];
            int dPressure = [rs intForColumn:@"dPressure"];
            int pulse = [rs intForColumn:@"pulse"];
            int oxygen = [rs intForColumn:@"oxygen"];
            NSString *state = [rs stringForColumn:@"state"];
            NSString *upsaveTime = [rs stringForColumn:@"upsaveTime"];
            
            
            NSDictionary *dic = @{@"Id":Id, @"sPressure":@(sPressure), @"dPressure":@(dPressure), @"pulse":@(pulse), @"oxygen":@(oxygen), @"state":state, @"upsaveTime":upsaveTime};
            
            DBXBpOxyHbModel *model = [DBXBpOxyHbModel yy_modelWithDictionary:dic];
            
            [self.dataMarray addObject:model];
            
        }
        
        [self.tableView reloadData];
        
        NSLog(@"dataMarray.count - %lu", (unsigned long)self.dataMarray.count);
        
        [_dataBase close];
    }
}




#pragma mark - tableView Delegate and DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataMarray.count+1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DBXBloodPressureOxygenHeartbeatTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:bloodPressureOxygenHeartbeatTableViewCellIdentifier forIndexPath:indexPath];
    
    [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)setupModelOfCell:(DBXBloodPressureOxygenHeartbeatTableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0) {
        cell.bloodPressure = @"血压";
        cell.bloodOxygen = @"血氧";
        cell.heartbeat = @"心率";
        cell.time = @"时间";
    } else {
        
        DBXBpOxyHbModel *model = self.dataMarray[indexPath.row-1];
        cell.bloodPressure = [NSString stringWithFormat:@"%ld/%ld", (long)model.sPressure, (long)model.dPressure];
        cell.bloodOxygen = [NSString stringWithFormat:@"%ld", (long)model.oxygen];
        cell.heartbeat = [NSString stringWithFormat:@"%ld", (long)model.pulse];
        cell.time = model.upsaveTime;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView fd_heightForCellWithIdentifier:bloodPressureOxygenHeartbeatTableViewCellIdentifier configuration:^(id cell) {
        [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    }];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        
        // 全选
        [self.dataMarray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:idx+1 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
            
            [self.selectDataMarray addObject:self.dataMarray[idx]];
        }];
        
    } else {
        
        [self.selectDataMarray addObject:self.dataMarray[indexPath.row-1]];
        
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        // 取消全选
        [self.dataMarray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:idx+1 inSection:0] animated:NO];
            
            [self.selectDataMarray removeAllObjects];
            
        }];
    } else {
        
        [self.selectDataMarray removeObject:self.dataMarray[indexPath.row-1]];
    }
}

- (void)deleteBpOxyHb
{
    if (kArrayIsEmpty(self.selectDataMarray)) {
        [MBProgressHUD showTipMessageInView:@"请选择要删除的数据"];
        return;
    }
    
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"警告" message:@"数据删除后无法恢复，确认删除？" preferredStyle:UIAlertControllerStyleAlert];
    
    __weak __typeof(self)weakSelf = self;
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
        // 删除数据
        [_dataBase open];
        
        // 删除本地数据
        for (int i = 0; i < weakSelf.selectDataMarray.count; i++) {
            
            DBXBpOxyHbModel *model = weakSelf.selectDataMarray[i];
            NSString *deleteSql = [NSString stringWithFormat:
                                   @"delete from %@ where %@ = '%@'",
                                   kBloodPressureTableName, @"id", model.Id];
            
            BOOL res = [weakSelf.dataBase executeUpdate:deleteSql];
            
            if (!res) {
                NSLog(@"error when delete db table");
            } else {
                
                NSLog(@"success to delete db table");
                
            }
            
            [weakSelf.dataMarray removeObject:model];
            
        }
        
        // 刷新表格
        [weakSelf.tableView reloadData];
        [_dataBase close];
        
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alertVC addAction:okAction];
    [alertVC addAction:cancelAction];
    [self presentViewController:alertVC animated:YES completion:nil];
    
}

- (void)uploadBpOxyHb
{
   
    
    // 创建上传数据
    NSMutableArray *syncDataMarray = [NSMutableArray new];
    
    if (kArrayIsEmpty(self.selectDataMarray)) {
        [MBProgressHUD showInfoMessage:@"请选择需要上传的数据"];
        return;
    }
    
    for (int i = 0; i < self.selectDataMarray.count; i++) {
        
        DBXBpOxyHbModel *model = self.selectDataMarray[i];
        
        NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
        
        if (model.sPressure != 0) {
            [mDic setObject:@(model.sPressure) forKey:@"sPressure"];
        }
        if (model.dPressure != 0) {
            [mDic setObject:@(model.dPressure) forKey:@"dPressure"];
        }
        if (model.oxygen != 0) {
            [mDic setObject:@(model.oxygen) forKey:@"oxygen"];
        }
        if (model.pulse != 0) {
            [mDic setObject:@(model.pulse) forKey:@"pulse"];
        }
        [mDic setObject:model.state forKey:@"state"];
        [mDic setObject:model.upsaveTime forKey:@"upsaveTime"];
        
        
        [syncDataMarray addObject:mDic];
        
    }
    
    // 上传json数据
    NSData *json = [NSJSONSerialization dataWithJSONObject:syncDataMarray options:NSJSONWritingPrettyPrinted error:nil];
    
    NSString *jsonStr = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
    NSData *rawData = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
    
    DBXBpOxyHbApi *api = [[DBXBpOxyHbApi alloc] initWithJsonData:rawData];
    
    [MBProgressHUD showActivityMessageInView:@"上传中"];
    
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"成功 %@", request.responseJSONObject);
        
        NSDictionary *dictionary = request.responseJSONObject;
        if ([[dictionary allKeys] containsObject:@"state"]) {
            
            // 上传成功
            if ([dictionary[@"state"] integerValue] == 0) {
                
                // 删除本地数据
                for (int i = 0; i < self.selectDataMarray.count; i++) {
                    
                    DBXBpOxyHbModel *model = self.selectDataMarray[i];
                    NSString *deleteSql = [NSString stringWithFormat:
                                           @"delete from %@ where %@ = '%@'",
                                           kBloodPressureTableName, @"id", model.Id];
                    
                    BOOL res = [self.dataBase executeUpdate:deleteSql];
                    
                    if (!res) {
                        NSLog(@"error when delete db table");
                    } else {
                        
                        NSLog(@"success to delete db table");
                        
                    }
                    
                    [self.dataMarray removeObject:model];
                    
                }
                
                // 刷新表格
                [self.tableView reloadData];
                [_dataBase close];
                
                [MBProgressHUD hideHUD];
                [MBProgressHUD showTipMessageInView:@"上传完成"];
                
                // 发送通知
                [[NSNotificationCenter defaultCenter] postNotificationName:kReloadDataApiAndDataDetailApi object:nil userInfo:nil];
            }
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"失败 %@", request.error);
        [MBProgressHUD hideHUD];
    }];
    
    NSLog(@"json - %@", json);
    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReloadDataApiAndDataDetailApi object:nil];
}


- (UIScrollView *)streachScrollView
{
    return self.tableView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
