//
//  DBXBloodSugarViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/26.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXBloodSugarViewController.h"
#import "DBXBloodSugarTableViewCell.h"
#import "DBXBloodSugarDataModel.h"
#import "DBXBloodSugarDataSyncApi.h"

static NSString *bloodSugarTableViewCellIdentifier = @"DBXBloodSugarTableViewCell";

@interface DBXBloodSugarViewController ()<UITableViewDelegate, UITableViewDataSource>

@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) NSMutableArray *dataMarray;
@property(nonatomic, strong) NSMutableArray *selectDataMarray;
@property(nonatomic, strong) FMDatabase *dataBase;
@property(nonatomic, strong) DBXBloodSugarDataModel *bloodSugarDataModel;

@end

@implementation DBXBloodSugarViewController

#pragma mark - 懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-40-kSafeAreaTopHeight-60) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[DBXBloodSugarTableViewCell class] forCellReuseIdentifier:bloodSugarTableViewCellIdentifier];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView setEditing:YES animated:YES];
        // 解决滑动视图顶部空出状态栏高度的问题
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)]) {
                self.edgesForExtendedLayout = UIRectEdgeNone;
            }
        }
        
        
    }
    
    return _tableView;
}

/**
 数据源
 */
- (NSMutableArray *)dataMarray
{
    if (!_dataMarray) {
        _dataMarray = [NSMutableArray new];
    }
    return _dataMarray;
}

/**
 手动选择的需要同步的数据
 */
- (NSMutableArray *)selectDataMarray
{
    if (!_selectDataMarray) {
        _selectDataMarray = [NSMutableArray new];
    }
    return _selectDataMarray;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    
    // 取消全选
    [self.bloodSugarDataMarray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0] animated:NO];
    }];
    
    
}

#pragma mark - tableView Delegate and DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataMarray.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DBXBloodSugarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:bloodSugarTableViewCellIdentifier forIndexPath:indexPath];
    
    [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)setupModelOfCell:(DBXBloodSugarTableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        
        cell.title = @"血糖";
        cell.subTitle = @"时间";
        
    } else {
        
        DBXBloodSugarDataModel *model = self.dataMarray[indexPath.row-1];
        
        cell.title = model.bloodGluVal;
        cell.subTitle = model.upsaveTime;
        
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView fd_heightForCellWithIdentifier:bloodSugarTableViewCellIdentifier cacheByIndexPath:indexPath configuration:^(id cell) {
        
        [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    }];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        
        // 全选
        [self.dataMarray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:idx+1 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    
                [self.selectDataMarray addObject:self.dataMarray[idx]];
        }];
        
    } else {
        [self.selectDataMarray addObject:self.dataMarray[indexPath.row-1]];
        
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        // 取消全选
        [self.dataMarray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:idx+1 inSection:0] animated:NO];
            
            [self.selectDataMarray removeAllObjects];
            
        }];
    } else {
   
        [self.selectDataMarray removeObject:self.dataMarray[indexPath.row-1]];
    }
}

/**
 设置数据源
 */
- (void)setupData
{
//    //1.获得数据库文件的路径
//    NSString *doc =[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES)  lastObject];
//
//    NSString *fileName = [doc stringByAppendingPathComponent:@"BloodSugar.sqlite"];
//
//    //2.获得数据库
//    FMDatabase *db = [FMDatabase databaseWithPath:fileName];
//
//    //获取数据
//
//
//    //3.使用如下语句，如果打开失败，可能是权限不足或者资源不足。通常打开完操作操作后，需要调用 close 方法来关闭数据库。在和数据库交互 之前，数据库必须是打开的。如果资源或权限不足无法打开或创建数据库，都会导致打开失败。
//    if ([db open]) {
//        //4.创表
//        BOOL result = [db executeUpdate:@"CREATE TABLE IF NOT EXISTS t_student (id integer PRIMARY KEY AUTOINCREMENT, mCondition text NOT NULL, bloodGluVal text NOT NULL, upsaveTime text NOT NULL, uploadTime text NOT NULL);"];
//
//        if (result) {
//            NSLog(@"创建表成功");
//        }
//    }
//
//    _dataBase = db;
    
//    [self insert];
    [self test];
}

- (void)test
{
    

}


//// 插入数据
//- (void)insert
//{
//      for (int i = 0; i< self.bloodSugarDataMarray.count; i++) {
//
//          NSString *bloodSugar = self.bloodSugarDataMarray[i];
//          NSString *time = self.timeDataMarray[i];
//           // executeUpdate : 不确定的参数用?来占位
//          BOOL isSuccess = [self.dataBase executeUpdate:@"INSERT INTO t_student (mCondition, bloodGluVal, upsaveTime, uploadTime) VALUES (?, ?, ?, ?);", @"0", bloodSugar, time, time];
//        }
// }



- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uploadSugarData) name:@"handleUploadBloodSugarEvent" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteSugarData) name:@"handleDeleteBloodSugarEvent" object:nil];
 
    [self.view addSubview:self.tableView];
    
    [self resultFMDB];
    
}

/**
 读取本地数据库存储同步信息, 刷新表格
 */
- (void)resultFMDB
{
    NSString *fmdbName = [NSString stringWithFormat:@"%@_PeripheralSyncData.sqlite",  [[NSUserDefaults standardUserDefaults] objectForKey:kAccount]];
    NSString *fileName = [kDocumentPath stringByAppendingPathComponent:fmdbName];
    _dataBase = [FMDatabase databaseWithPath:fileName];
    if ([_dataBase open]) {
        NSString * sql = [NSString stringWithFormat:@"SELECT * FROM %@", kBloodSugarTableName];;
        FMResultSet * rs = [_dataBase executeQuery:sql];
        while ([rs next]) {
            
            NSString *Id = [rs stringForColumn:@"id"];
            NSString *mCondition = [rs stringForColumn:@"mCondition"];
            NSString *bloodGluVal = [NSString stringWithFormat:@"%.1f", (float)[[rs stringForColumn:@"bloodGluVal"] floatValue]/10];
            NSString *upsaveTime = [rs stringForColumn:@"upsaveTime"];
            
            NSDictionary *dic = @{@"Id":Id, @"mCondition":mCondition, @"bloodGluVal":bloodGluVal, @"upsaveTime":upsaveTime};
            
            DBXBloodSugarDataModel *model = [DBXBloodSugarDataModel yy_modelWithDictionary:dic];
            
            [self.dataMarray addObject:model];
            
        }
        
        [self.tableView reloadData];
        
        NSLog(@"dataMarray.count - %lu", (unsigned long)self.dataMarray.count);
        
        [_dataBase close];
    }
}

- (void)deleteSugarData
{
    if (kArrayIsEmpty(self.selectDataMarray)) {
        [MBProgressHUD showTipMessageInView:@"请选择要删除的数据"];
        return;
    }
    
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"警告" message:@"数据删除后无法恢复，确认删除？" preferredStyle:UIAlertControllerStyleAlert];
    
    __weak __typeof(self)weakSelf = self;
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
        // 删除数据
        [_dataBase open];
        
        // 删除本地数据
        for (int i = 0; i < weakSelf.selectDataMarray.count; i++) {
            
            DBXBloodSugarDataModel *model = weakSelf.selectDataMarray[i];
            NSString *deleteSql = [NSString stringWithFormat:
                                   @"delete from %@ where %@ = '%@'",
                                   kBloodSugarTableName, @"id", model.Id];
            
            BOOL res = [weakSelf.dataBase executeUpdate:deleteSql];
            
            if (!res) {
                NSLog(@"error when delete db table");
            } else {
                
                NSLog(@"success to delete db table");
                
            }
            
            [weakSelf.dataMarray removeObject:model];
            
        }
        
        // 刷新表格
        [weakSelf.tableView reloadData];
        [_dataBase close];
        
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alertVC addAction:okAction];
    [alertVC addAction:cancelAction];
    [self presentViewController:alertVC animated:YES completion:nil];
    
}

/**
 上传血糖数据
 */
- (void)uploadSugarData
{
    
    [_dataBase open];
    
    if (kArrayIsEmpty(self.selectDataMarray)) {
        [MBProgressHUD showTipMessageInView:@"请选择要上传的数据"];
        return;
    }
    
    NSMutableArray *syncDataMarray = [NSMutableArray new];
    for (int i = 0; i < self.selectDataMarray.count; i++) {
        
        DBXBloodSugarDataModel *model = self.selectDataMarray[i];
        
        NSDictionary *dic = @{@"mCondition":model.mCondition, @"bloodGluVal":model.bloodGluVal, @"upsaveTime":model.upsaveTime};
        
        [syncDataMarray addObject:dic];
    }
    
    
    // 上传json数据
    NSData *json = [NSJSONSerialization dataWithJSONObject:syncDataMarray options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStr = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
    NSData *rawData = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
    
    DBXBloodSugarDataSyncApi *api = [[DBXBloodSugarDataSyncApi alloc] initWithJsonData:rawData];
    [MBProgressHUD showActivityMessageInView:@"上传中"];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"成功 %@", request.responseJSONObject);
        
        NSDictionary *dictionary = request.responseJSONObject;
        if ([[dictionary allKeys] containsObject:@"state"]) {
            
            // 上传成功
            if ([dictionary[@"state"] integerValue] == 0) {
                
                // 删除本地数据
                for (int i = 0; i < self.selectDataMarray.count; i++) {
                    
                    DBXBloodSugarDataModel *model = self.selectDataMarray[i];
                    NSString *deleteSql = [NSString stringWithFormat:
                                           @"delete from %@ where %@ = '%@'",
                                           kBloodSugarTableName, @"id", model.Id];
                    
                    BOOL res = [self.dataBase executeUpdate:deleteSql];
                    
                    if (!res) {
                        NSLog(@"error when delete db table");
                    } else {
                        
                        NSLog(@"success to delete db table");
                        
                    }
                    
                    [self.dataMarray removeObject:model];
                    
                }
                
                // 刷新表格
                [self.tableView reloadData];
                [self.dataBase close];
                
                [MBProgressHUD hideHUD];
                [MBProgressHUD showTipMessageInView:@"上传完成"];
                // 发送通知
                [[NSNotificationCenter defaultCenter] postNotificationName:kReloadDataApiAndDataDetailApi object:nil userInfo:nil];
            }
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"失败 %@", request.error);
        [MBProgressHUD hideHUD];
    }];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReloadDataApiAndDataDetailApi object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIScrollView *)streachScrollView
{
    return self.tableView;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
