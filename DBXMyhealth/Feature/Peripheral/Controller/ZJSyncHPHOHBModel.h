//
//  ZJSyncHPHOHBModel.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/6.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZJSyncHPHOHBModel : NSObject

@property (nonatomic, assign) BOOL isSelect;
@property (nonatomic, copy) NSString *Id;    
@property (nonatomic, copy) NSString *heartBeat;    // 心率
@property (nonatomic, copy) NSString *sp;         // 高压
@property (nonatomic, copy) NSString *dp;         // 低压
@property (nonatomic, copy) NSString *bloodOxygen;      // 血氧
@property (nonatomic, copy) NSString *time;      // 时间

@end

NS_ASSUME_NONNULL_END
