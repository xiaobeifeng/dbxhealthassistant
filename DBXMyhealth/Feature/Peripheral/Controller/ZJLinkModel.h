//
//  ZJLinkModel.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/12.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZJLinkModel : NSObject

@property (nonatomic , copy) NSString              * message;
@property (nonatomic , copy) NSString              * data;
@property (nonatomic , assign) NSInteger             state;

@end

NS_ASSUME_NONNULL_END
