//
//  ZJDeviceManageViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/9.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "ZJDeviceManageViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import <BabyBluetooth.h>
#import "DBXPeripheralTableViewCell.h"
#import "DBXMePersonalinfoApi.h"
#import "DBXMePersonalinfoModel.h"
#import "DBXLinkyornoneApi.h"
#import "DBXFirstLinkUploadTimeApi.h"
#import "ZJLinkModel.h"
#import "DBXRemovebind.h"
#import "DBXLoginViewController.h"

static NSString *cellIdentifier = @"DBXPeripheralTableViewCell";
static CGFloat const kBindingViewHeight = 80;

@interface ZJDeviceManageViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
/** 底部绑定按钮 */
@property (nonatomic, strong) UIButton *bindingBtn;
/** 头部绑定图片 */
@property (nonatomic, strong) UIImageView *bindingImageView;
/** 头部绑定信息 */
@property (nonatomic, strong) UILabel *bindingPeripheralNameLabel;
@property (nonatomic, strong) BabyBluetooth *baby;
@property (nonatomic, strong) CBPeripheral *currPeripheral;
@property (nonatomic, strong) CBCharacteristic *characteristic;
@property (nonatomic, strong) NSTimer *connectTimer;    // 蓝牙搜索计时器
@property (nonatomic, strong) NSMutableArray *peripheralMutableArray;

@end

@implementation ZJDeviceManageViewController

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.baby cancelAllPeripheralsConnection]; // 取消所有设备连接
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    _peripheralMutableArray = [NSMutableArray new];
    
    if (_isNewUser) {   // 是新用户的话，在左上角显示切换用户按钮
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem titleItem:@"切换账号" target:self action:@selector(changeUserAction)];
    }
    
    [self setupUI];
    
    // 获取本地保存绑定的设备名
    NSLog(@"%@", DEVICE_NAME);
    NSString *deviceName = [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_NAME];
    if (kStringIsEmpty(deviceName)) {   // 当前无绑定设备
        [self setupBlueTooth];  // 开启蓝牙模块
    } else {    // 当前存在绑定的设备
        self.bindingPeripheralNameLabel.text = [NSString stringWithFormat:@"当前绑定设备：%@", deviceName];
        self.bindingImageView.image = [UIImage imageNamed:@"peripheral_binding"];
        self.bindingBtn.selected = YES;
    }
    
}


/**
 切换用户
 */
- (void)changeUserAction {
    DBXBaseNavigationController *baseNavC = [[DBXBaseNavigationController alloc] initWithRootViewController:[DBXLoginViewController new]];
    [UIApplication sharedApplication].keyWindow.rootViewController = baseNavC;
}

/**
 搭建UI界面
 */
- (void)setupUI {
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-kSafeAreaTopHeight-kBindingViewHeight) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView registerClass:[DBXPeripheralTableViewCell class] forCellReuseIdentifier:cellIdentifier];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (@available(iOS 11.0, *)) {
        _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)]) {
            self.edgesForExtendedLayout = UIRectEdgeNone;
        }
    }
    
    // 防止UITableView下拉出现白色背景
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, -5000, kScreenWidth, 5000)];
    imageView.backgroundColor = kNavigationBarColor;
    [_tableView addSubview:imageView];
    [self.view addSubview:_tableView];
    
    // TableView头部视图
    UIView *headerView = [UIView new];
    headerView.backgroundColor = kNavigationBarColor;
    headerView.frame = CGRectMake(0, 0, kScreenWidth, 250-self.navigationController.navigationBar.height-[UIApplication sharedApplication].statusBarFrame.size.height);
    self.tableView.tableHeaderView = headerView;
    
    // 头部视图图片
    _bindingImageView = [UIImageView new];
    _bindingImageView.backgroundColor = [UIColor clearColor];
    _bindingImageView.image = [UIImage imageNamed:@"peripheral_unbinding"];
    [headerView addSubview:_bindingImageView];
    [_bindingImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(@60);
        make.height.mas_equalTo(@65);
        make.centerX.mas_equalTo(headerView.mas_centerX);
        make.centerY.mas_equalTo(headerView.mas_centerY).with.offset(-20);
    }];
    
    // 头部视图文本
    _bindingPeripheralNameLabel = [UILabel new];
    _bindingPeripheralNameLabel.text = @"暂无绑定设备";
    _bindingPeripheralNameLabel.textColor = [UIColor whiteColor];
    _bindingPeripheralNameLabel.textAlignment = NSTextAlignmentCenter;
    _bindingPeripheralNameLabel.font = [UIFont systemFontOfSize:14];
    [headerView addSubview:_bindingPeripheralNameLabel];
    [_bindingPeripheralNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kScreenWidth-26);
        make.centerX.mas_equalTo(headerView.mas_centerX);
        make.top.mas_equalTo(self.bindingImageView.mas_bottom).with.offset(20);
    }];
    
    // 底部功能栏
    UIView *bindingView = [UIView new];
    bindingView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bindingView];
    [bindingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(kBindingViewHeight);
        make.width.mas_equalTo(kScreenWidth);
        make.bottom.mas_equalTo(self.view);
    }];
    _bindingBtn = [UIButton new];
    [_bindingBtn setTitle:@"绑定设备" forState:UIControlStateNormal];
    [_bindingBtn setTitle:@"解绑设备" forState:UIControlStateSelected];
    [_bindingBtn setTitleColor:kNavigationBarColor forState:UIControlStateNormal];
    kViewBorderRadius(_bindingBtn, 10, 1, kTableSeparatorLineColor);
    _bindingBtn.titleLabel.font = kCellTitleFont;
    _bindingBtn.backgroundColor = [UIColor whiteColor];
    [_bindingBtn addTarget:self action:@selector(bindingBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [bindingView addSubview:_bindingBtn];
    [_bindingBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bindingView).with.offset(40);
        make.right.mas_equalTo(bindingView).with.offset(-40);
        make.bottom.mas_equalTo(bindingView).with.offset(-20);
        make.top.mas_equalTo(bindingView).with.offset(13);
    }];
}

/**
 设置蓝牙
 */
- (void)setupBlueTooth {
    _baby = [BabyBluetooth shareBabyBluetooth];
    _baby.scanForPeripherals().begin(); // 开始扫描外设
    //
    __weak typeof(self) weakSelf = self;
    [_baby setBlockOnCentralManagerDidUpdateState:^(CBCentralManager *central) {
        if (central.state != CBManagerStatePoweredOn) {
            [MBProgressHUD showTipMessageInView:@"请检查蓝牙是否开启后再试"];
        } else {
            // 开始扫描外设
            [central scanForPeripheralsWithServices:nil options:nil];
        }
    }];
    
    // 设置扫描到设备的委托
    [_baby setBlockOnDiscoverToPeripherals:^(CBCentralManager *central, CBPeripheral *peripheral, NSDictionary *advertisementData, NSNumber *RSSI) {
        NSLog(@"搜索到了设备:%@",peripheral.name);
        
        if (peripheral.name.length == 17 && [peripheral.name containsString:@":"]) {
            
            if (![weakSelf.peripheralMutableArray containsObject:peripheral]) {
                [weakSelf.peripheralMutableArray addObject:peripheral];
                [weakSelf.tableView reloadData];
            }
            
        }
    }];
    
    // 设置设备连接失败的委托
    [_baby setBlockOnFailToConnect:^(CBCentralManager *central, CBPeripheral *peripheral, NSError *error) {
        [MBProgressHUD hideHUD];
        NSLog(@"设备：%@--连接失败",peripheral.name);
    }];
    
    // 设置设备连接成功的委托,同一个baby对象，使用不同的channel切换委托回调
    [_baby setBlockOnConnected:^(CBCentralManager *central, CBPeripheral *peripheral) {
        NSLog(@"%@", peripheral.name);
        [weakSelf.baby cancelScan]; //  取消扫描外设
        [MBProgressHUD hideHUD];
        // 查找服务
        [peripheral discoverServices:nil];
        
    }];
    
    // 设置发现设备的Services的委托
    [_baby setBlockOnDiscoverServices:^(CBPeripheral *peripheral, NSError *error) {
        for (CBService *s in peripheral.services) {
            NSLog(@"UUID - %@", s.UUID);
            if ([s.UUID.UUIDString isEqualToString:@"FFF0"]) {
                [peripheral discoverCharacteristics:nil forService:s];
            }
        }
    }];
    
    // 设置发现设service的Characteristics的委托
    [_baby setBlockOnDiscoverCharacteristics:^(CBPeripheral *peripheral, CBService *service, NSError *error) {
        for (CBCharacteristic *characteristic in service.characteristics) {
            
            if ([characteristic.UUID.UUIDString isEqualToString:@"FFF6"]) {
                weakSelf.currPeripheral = peripheral;
                weakSelf.characteristic = characteristic;
                [peripheral setNotifyValue:YES forCharacteristic:characteristic];
                [weakSelf syncTimeToDevice];    // 开始向外设同步时间
            }
        }
    }];
    
}

/**
 给外设同步时间
 */
- (void)syncTimeToDevice {
    
    [MBProgressHUD hideHUD];
    // 获取身高体重
    DBXMePersonalinfoApi *api = [DBXMePersonalinfoApi new];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        DBXMePersonalinfoModel *model = [DBXMePersonalinfoModel yy_modelWithJSON:request.responseJSONObject];
        NSArray *array = [NSArray yy_modelArrayWithClass:[DBXMePersonalinfoModelData class] json:model.data];
        
        
        // 获取当前时间
        NSDate *currentDate = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *currentDateStr = [dateFormatter stringFromDate:currentDate];
        
        // eg.2018-09-05 14:25:46
        NSInteger fullYear = [[currentDateStr substringWithRange:NSMakeRange(0, 4)] integerValue];  // 用于计算年龄
        NSInteger year = [[currentDateStr substringWithRange:NSMakeRange(2, 2)] integerValue];
        NSInteger month = [[currentDateStr substringWithRange:NSMakeRange(5, 2)] integerValue];
        NSInteger day = [[currentDateStr substringWithRange:NSMakeRange(8, 2)] integerValue];
        NSInteger hour = [[currentDateStr substringWithRange:NSMakeRange(11, 2)] integerValue];
        NSInteger minute = [[currentDateStr substringWithRange:NSMakeRange(14, 2)] integerValue];
        NSInteger second = [[currentDateStr substringWithRange:NSMakeRange(17, 2)] integerValue];
        
        // 校验当前时间
        Byte yearByte[2];
        yearByte[0] = 0xcc;
        yearByte[1] = (Byte)year;
        Byte monthByte[2];
        monthByte[0] = 0xcc;
        monthByte[1] = (Byte)month;
        Byte dayByte[2];
        dayByte[0] = 0xcc;
        dayByte[1] = (Byte)day;
        Byte hourByte[2];
        hourByte[0] = 0xcc;
        hourByte[1] = (Byte)hour;
        Byte minuteByte[2];
        minuteByte[0] = 0xcc;
        minuteByte[1] = (Byte)minute;
        Byte secondByte[2];
        secondByte[0] = 0xcc;
        secondByte[1] = (Byte)second;
        
        // 当前星期
        NSTimeInterval interval = [[NSDate date] timeIntervalSince1970];
        NSDate *newDate = [NSDate dateWithTimeIntervalSince1970:interval];
        NSArray *weekday = [NSArray arrayWithObjects: [NSNull null], @"7", @"1", @"2", @"3", @"4", @"5", @"6", nil];
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [calendar components:NSCalendarUnitWeekday fromDate:newDate];
        NSString *weekStr = [weekday objectAtIndex:components.weekday];
        Byte weekByte[2];
        weekByte[0] = 0xcc;
        weekByte[1] = (Byte)([weekStr integerValue]);
        
        
        
        NSInteger height; // 身高
        NSInteger weight; // 体重
        NSInteger age; // 年龄
        NSInteger sexInteger; // 性别
        
        if (!kArrayIsEmpty(array)) {
            
            DBXMePersonalinfoModelData *modelData = array.firstObject;
            height = [modelData.hight integerValue];
            weight = [modelData.weight integerValue];
            NSString *birth  = modelData.birth;
            
            if (!kStringIsEmpty(birth)) {
                NSArray *birthArray = [birth componentsSeparatedByString:@"-"];
                NSInteger birthYear = [birthArray[0] integerValue];
                age = fullYear - birthYear;
            } else {
                age = 0;
            }
            
            if ([modelData.sex isEqualToString:@"男"]) {
                sexInteger = 1;
            } else {
                sexInteger = 0;
            }
            
        } else {
            
            height = 170;
            weight = 65;
            age = 0;
            sexInteger = 0;
            
        }
        
        Byte heightByte[2];
        heightByte[0] = 0xdd;
        heightByte[1] = (Byte)height;
        
        Byte weightByte[2];
        weightByte[0] = 0xdd;
        weightByte[1] = (Byte)weight;
        
        Byte ageByte[2];
        ageByte[0] = 0xdd;
        ageByte[1] = (Byte)(age);
        
        Byte sexByte[2];
        sexByte[0] = 0xdd;
        sexByte[1] = (Byte)sexInteger;
        
        // 开始写入数据
        [_currPeripheral writeValue:[NSData dataWithBytes:&yearByte length:sizeof(yearByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
        [_currPeripheral writeValue:[NSData dataWithBytes:&monthByte length:sizeof(monthByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
        [_currPeripheral writeValue:[NSData dataWithBytes:&dayByte length:sizeof(dayByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
        [_currPeripheral writeValue:[NSData dataWithBytes:&hourByte length:sizeof(hourByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
        [_currPeripheral writeValue:[NSData dataWithBytes:&minuteByte length:sizeof(minuteByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
        [_currPeripheral writeValue:[NSData dataWithBytes:&secondByte length:sizeof(secondByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
        [_currPeripheral writeValue:[NSData dataWithBytes:&weekByte length:sizeof(weekByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
        [_currPeripheral writeValue:[NSData dataWithBytes:&heightByte length:sizeof(heightByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
        [_currPeripheral writeValue:[NSData dataWithBytes:&weightByte length:sizeof(weightByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
        [_currPeripheral writeValue:[NSData dataWithBytes:&sexByte length:sizeof(sexByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
        [_currPeripheral writeValue:[NSData dataWithBytes:&ageByte length:sizeof(ageByte)] forCharacteristic:_characteristic type:CBCharacteristicWriteWithoutResponse];
        
        // 检查下是否连接过设备
        NSString *deviceName = self.currPeripheral.name;
        [self isConnectCurrentDevice:deviceName];
        
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [MBProgressHUD showTipMessageInView:@"绑定失败，请稍后再试"];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [self.navigationController popViewControllerAnimated:YES];
        });
        
    }];
    
}

#pragma mark - 接口请求
/**
 检查下是否连接过设备
 */
- (void)isConnectCurrentDevice:(NSString *)deviceName {
    
    NSString *clearDeviceName = [deviceName stringByReplacingOccurrencesOfString:@":" withString:@""];   // 去掉设备名中的冒号
    DBXLinkyornoneApi *api = [[DBXLinkyornoneApi alloc] initWithDeviceCode:clearDeviceName];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [MBProgressHUD hideHUD];
        
        ZJLinkModel *model = [ZJLinkModel yy_modelWithJSON:request.responseJSONObject];
        
        // 设备未激活或未建立链接的情况下，第一次链接上传链接时间
        if (model.state == 1043 || model.state == 1041) {
            [self uploadFirstLinkTimeWithDeviceCode:clearDeviceName deviceName:deviceName];
        }
        
        // 设备是该账号可以绑定的
        if (model.state == 1042) {
            // 保存绑定的设备名
            [[NSUserDefaults standardUserDefaults] setObject:deviceName forKey:DEVICE_NAME];
            [[NSUserDefaults standardUserDefaults] synchronize];
            self.bindingBtn.selected = YES;
            self.bindingPeripheralNameLabel.text = [NSString stringWithFormat:@"当前绑定设备：%@", deviceName];
            self.bindingImageView.image = [UIImage imageNamed:@"peripheral_binding"];
            [self showSuccessAlertVC];
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [MBProgressHUD hideHUD];
        [MBProgressHUD showTipMessageInView:@"请稍后再试"];
        
    }];
    
}

/**
 上传当前连接时间
 */
- (void)uploadFirstLinkTimeWithDeviceCode:(NSString *)clearDeviceName deviceName:(NSString *)deviceName {
    // 获取当前时间
    NSDateFormatter *formatterYearAndMonth = [[NSDateFormatter alloc] init];
    formatterYearAndMonth.dateFormat = @"YYYY-MM-dd HH:mm:ss";
    NSString *firstLinkTime = [formatterYearAndMonth stringFromDate:[NSDate date]];
    DBXFirstLinkUploadTimeApi *api = [[DBXFirstLinkUploadTimeApi alloc] initWithDeviceCode:clearDeviceName firstLinkTime:firstLinkTime];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        ZJLinkModel *model = [ZJLinkModel yy_modelWithJSON:request.responseJSONObject];
        
        if (model.state == 0 || model.state == 1043) {   // 成功
            // 保存绑定的设备名
            [[NSUserDefaults standardUserDefaults] setObject:deviceName forKey:DEVICE_NAME];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self showSuccessAlertVC];
            self.bindingPeripheralNameLabel.text = [NSString stringWithFormat:@"当前绑定设备：%@", deviceName];
            self.bindingImageView.image = [UIImage imageNamed:@"peripheral_binding"];
            self.bindingBtn.selected = YES;
            return;
        }
        
        [MBProgressHUD showTipMessageInView:model.message];
        self.bindingBtn.selected = NO;
        
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [MBProgressHUD showTipMessageInView:@"请稍后再试"];
        self.bindingBtn.selected = NO;
    }];
    
}

#pragma mark - tableView Delegate and DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _peripheralMutableArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DBXPeripheralTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)setupModelOfCell:(DBXPeripheralTableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CBPeripheral *peripheral = _peripheralMutableArray[indexPath.row];
    cell.titleLbl.text = peripheral.name;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [tableView fd_heightForCellWithIdentifier:cellIdentifier cacheByIndexPath:indexPath configuration:^(id cell) {
        
        [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.currPeripheral = _peripheralMutableArray[indexPath.row];
    
    //
    
}


/**
 绑定按钮点击事件
 */
- (void)bindingBtnAction:(UIButton *)button {
    
    // 防止按钮重复点击
    button.enabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        button.enabled = YES;
    });
    
    if (!button.selected) { // 绑定设备
        
        if (!self.currPeripheral) {
            [MBProgressHUD showTipMessageInWindow:@"请选择要绑定的设备"];
            return;
        }
        
        [MBProgressHUD showActivityMessageInWindow:@"设备绑定中"];
        _baby.having(self.currPeripheral).and.connectToPeripherals().begin();    // 开始连接当前外设
        
    } else {    // 解绑设备
        
        // 获取本地保存绑定的设备名
        NSString *saveDeviceName = [[NSUserDefaults standardUserDefaults] objectForKey:DEVICE_NAME];
        [self startDisBindingDevice:saveDeviceName];    // 开始解绑设备
    }
}

/**
 设备解绑
 */
- (void)startDisBindingDevice:(NSString *)deviceName {
    
    NSString *clearDeviceName = [deviceName stringByReplacingOccurrencesOfString:@":" withString:@""];   // 去掉设备名中的冒号
    DBXRemovebind *api = [[DBXRemovebind alloc] initWithDeviceCode:clearDeviceName];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        ZJLinkModel *model = [ZJLinkModel yy_modelWithJSON:request.responseJSONObject];
        
        if (model.state == 0) {
            // 移除当前绑定的设备
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:DEVICE_NAME];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            // 更换成已经未绑定样式
            self.bindingPeripheralNameLabel.text = @"暂无绑定设备";
            self.bindingImageView.image = [UIImage imageNamed:@"peripheral_unbinding"];
            self.bindingBtn.selected = NO;
            
            self.baby = [BabyBluetooth shareBabyBluetooth];
            self.baby.scanForPeripherals().begin(); // 开始扫描外设
        }
        
        [MBProgressHUD showTipMessageInWindow:model.message];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [MBProgressHUD showTipMessageInWindow:@"请稍后再试"];
    }];
    
}

/**
 绑定成功的AlertVC
 */
- (void)showSuccessAlertVC {
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"设备绑定成功" preferredStyle:UIAlertControllerStyleAlert];
    
    __weak __typeof(self)weakSelf = self;
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        if (_isNewUser) {
            [UIApplication sharedApplication].keyWindow.rootViewController = [DBXBaseTabBarController new];
            
        } else {
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
        
    }];
    
    [alertVC addAction:okAction];
    
    [self presentViewController:alertVC animated:YES completion:nil];
}

@end
