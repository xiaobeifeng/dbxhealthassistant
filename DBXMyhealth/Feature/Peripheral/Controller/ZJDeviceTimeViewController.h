//
//  ZJDeviceTimeViewController.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/12.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 同步时间
 */
@interface ZJDeviceTimeViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
