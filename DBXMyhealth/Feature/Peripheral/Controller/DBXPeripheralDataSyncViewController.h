//
//  DBXPeripheralDataSyncViewController.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/25.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WMStickyPageViewController.h"

@interface DBXPeripheralDataSyncViewController : WMStickyPageViewController

@end
