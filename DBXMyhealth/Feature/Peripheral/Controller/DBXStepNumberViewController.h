//
//  DBXStepNumberViewController.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/27.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WMStickyPageViewController.h"

/**
 运动步数
 */
@interface DBXStepNumberViewController : UIViewController<WMStickyPageViewControllerDelegate>

@property(nonatomic, strong) NSMutableArray *stepNumberMarray;
@property(nonatomic, strong) NSMutableArray *calorieMarray;
@property(nonatomic, strong) NSMutableArray *timeMarray;

@end
