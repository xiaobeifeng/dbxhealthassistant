//
//  DBXLinkyornoneApi.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/19.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "BaseRequestApi.h"


@interface DBXLinkyornoneApi : BaseRequestApi

- (id)initWithDeviceCode:(NSString *)deviceCode;

@end


