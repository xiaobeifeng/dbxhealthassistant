//
//  DBXLinkyornoneApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/19.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXLinkyornoneApi.h"

@interface DBXLinkyornoneApi()

@property(nonatomic, copy) NSString *deviceCode;

@end

@implementation DBXLinkyornoneApi
- (id)initWithDeviceCode:(NSString *)deviceCode
{
    
    self = [super init];
    if (self) {
        _deviceCode = deviceCode;
    }
    return self;
}

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return @"/AppService/lohas/health/linkyornone";
}

// 请求方法，某人是GET
- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

// 请求体
- (id)requestArgument {
    return @{
             @"device_code": _deviceCode,
             };
}
@end
