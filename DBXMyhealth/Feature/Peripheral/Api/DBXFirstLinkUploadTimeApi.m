//
//  DBXFirstLinkUploadTimeApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/19.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXFirstLinkUploadTimeApi.h"

@interface DBXFirstLinkUploadTimeApi()

@property(nonatomic, copy) NSString *deviceCode;
@property(nonatomic, copy) NSString *firstLinkTime;

@end

@implementation DBXFirstLinkUploadTimeApi
- (id)initWithDeviceCode:(NSString *)deviceCode firstLinkTime:(NSString *)firstLinkTime
{
    
    self = [super init];
    if (self) {
        _deviceCode = deviceCode;
        _firstLinkTime = firstLinkTime;
    }
    return self;
}

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return @"/AppService/lohas/health/firstlinkquery";
}

// 请求方法，某人是GET
- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

// 请求体
- (id)requestArgument {
    return @{
             @"device_code": _deviceCode,
             @"first_link":_firstLinkTime
             };
}
@end
