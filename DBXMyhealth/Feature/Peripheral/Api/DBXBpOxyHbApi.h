//
//  DBXBpOxyHbApi.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/30.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "YTKBaseRequest.h"

@interface DBXBpOxyHbApi : YTKBaseRequest

- (id)initWithJsonData:(NSData *)jsonData;

@end
