//
//  DBXBpOxyHbApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/30.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXBpOxyHbApi.h"

@interface DBXBpOxyHbApi()
{
    NSData *_jsonData;
}

@end
@implementation DBXBpOxyHbApi

- (id)initWithJsonData:(NSData *)jsonData
{
    self = [super init];
    
    if (self) {
        _jsonData = jsonData;
    }
    return self;
}

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    return @"/AppService/lohas/health/dbxbpsave";
}

// 请求方法
- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (YTKRequestSerializerType)requestSerializerType
{
    return YTKRequestSerializerTypeJSON;
}

- (NSURLRequest *)buildCustomUrlRequest
{
    YTKNetworkConfig *config = [YTKNetworkConfig sharedConfig];
    NSString *url = [NSString stringWithFormat:@"%@%@", config.baseUrl, self.requestUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:kToken] forHTTPHeaderField:kToken];
    [request setHTTPBody:_jsonData];
    return request;
}


@end
