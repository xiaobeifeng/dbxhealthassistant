//
//  DBXRemovebind.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/26.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "BaseRequestApi.h"

NS_ASSUME_NONNULL_BEGIN

@interface DBXRemovebind : BaseRequestApi

- (id)initWithDeviceCode:(NSString *)deviceCode;

@end

NS_ASSUME_NONNULL_END
