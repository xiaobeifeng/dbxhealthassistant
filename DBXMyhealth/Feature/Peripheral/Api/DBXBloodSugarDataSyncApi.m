//
//  DBXBloodSugarDataSyncApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/29.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXBloodSugarDataSyncApi.h"

@interface DBXBloodSugarDataSyncApi()
{
    NSData *_bodyData;
}

@end

@implementation DBXBloodSugarDataSyncApi

- (id)initWithJsonData:(NSData *)jsonData
{
    self = [super init];
    
    if (self) {
        _bodyData = jsonData;
    }
    return self;
}

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    return @"/AppService/lohas/health/dbxbgsave";
}

// 请求方法
- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (YTKRequestSerializerType)requestSerializerType
{
    return YTKRequestSerializerTypeJSON;
}

- (NSURLRequest *)buildCustomUrlRequest
{
    YTKNetworkConfig *config = [YTKNetworkConfig sharedConfig];
    NSString *url = [NSString stringWithFormat:@"%@%@", config.baseUrl, self.requestUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[[NSUserDefaults standardUserDefaults] objectForKey:kToken] forHTTPHeaderField:kToken];
    [request setHTTPBody:_bodyData];
    return request;
}


@end
