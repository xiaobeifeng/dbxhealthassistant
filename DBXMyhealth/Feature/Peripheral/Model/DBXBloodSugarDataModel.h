//
//  DBXBloodSugarDataModel.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/29.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBXBloodSugarDataModel : NSObject

@property(nonatomic, copy) NSString *Id;
@property(nonatomic, copy) NSString *mCondition;
@property(nonatomic, copy) NSString *bloodGluVal;
@property(nonatomic, copy) NSString *upsaveTime;

@end
