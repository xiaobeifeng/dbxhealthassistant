//
//  DBXBpOxyHbModel.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/30.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBXBpOxyHbModel : NSObject

@property(nonatomic, copy) NSString *Id;
@property(nonatomic, assign) NSInteger sPressure;
@property(nonatomic, assign) NSInteger dPressure;
@property(nonatomic, assign) NSInteger pulse;
@property(nonatomic, assign) NSInteger oxygen;
@property(nonatomic, copy) NSString *state;
@property(nonatomic, copy) NSString *upsaveTime;

@end
