//
//  DBXRegisteApi.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/18.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBXRegisteApi : BaseRequestApi

- (id)initWithUsername:(NSString *)username
              password:(NSString *)password
       equipmentNumber:(NSString *)equipmentNumber
            shopNumber:(NSString *)shopNumber;

@end

