//
//  DBXLoginApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/14.
//  Copyright © 2018年 zhoujian. All rights reserved.
//

#import "DBXLoginApi.h"

@interface DBXLoginApi()
{
    NSString *_userName;
    NSString *_password;
}

@end

@implementation DBXLoginApi

- (id)initWithUsername:(NSString *)username password:(NSString *)password {
    
    self = [super init];
    if (self) {
        _userName = username;
        _password = password;
    }
    return self;
}

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return @"/AppService/lohas/user/token";
}

// 请求方法，某人是GET
- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

// 请求体
- (id)requestArgument {
    return @{
             @"username": _userName,
             @"password": _password
             };
}

@end
