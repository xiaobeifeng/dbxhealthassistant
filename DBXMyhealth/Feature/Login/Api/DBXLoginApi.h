//
//  DBXLoginApi.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/14.
//  Copyright © 2018年 zhoujian. All rights reserved.
//

#import "YTKBaseRequest.h"

@interface DBXLoginApi : BaseRequestApi

- (id)initWithUsername:(NSString *)username password:(NSString *)password;

@end
