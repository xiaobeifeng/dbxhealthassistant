//
//  DBXForgotPasswordApi.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/10/8.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "BaseRequestApi.h"

@interface DBXForgotPasswordApi : BaseRequestApi


/**
 请求忘记密码接口初始化

 @param username 用户名
 @param device_code 设备号
 @param newPassword 新密码
 @return 
 */
- (id)initWithUsername:(NSString *)username device_code:(NSString *)device_code newPassword:(NSString *)newPassword;
@end

