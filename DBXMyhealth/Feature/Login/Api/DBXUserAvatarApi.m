//
//  DBXUserAvatarApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/30.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXUserAvatarApi.h"

@implementation DBXUserAvatarApi

//需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    return @"/AppService/lohas/health/avatar";
}

//请求方法，某人是GET
- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

- (instancetype)init
{
    if (self = [super init]) {
        
    }
    return self;
}

@end
