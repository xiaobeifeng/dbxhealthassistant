//
//  DBXRegisteApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/18.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXRegisteApi.h"

@interface DBXRegisteApi()

@property(nonatomic, copy) NSString *userName;
@property(nonatomic, copy) NSString *password;
@property(nonatomic, copy) NSString *equipmentNumber;
@property(nonatomic, copy) NSString *shopNumber;

@end

@implementation DBXRegisteApi
- (id)initWithUsername:(NSString *)username
              password:(NSString *)password
       equipmentNumber:(NSString *)equipmentNumber
            shopNumber:(NSString *)shopNumber
{
    
    self = [super init];
    if (self) {
        _userName = username;
        _password = password;
        _equipmentNumber = equipmentNumber;
        _shopNumber = shopNumber;
    }
    return self;
}

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return @"/AppService/lohas/health/auto/register/state/role/dept";
}

// 请求方法，某人是GET
- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

// 请求体
- (id)requestArgument {
    return @{
             @"username": _userName,
             @"password": _password,
             @"device_code":_equipmentNumber,
             @"shop_name":_shopNumber
             };
}
@end
