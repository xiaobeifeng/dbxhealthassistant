//
//  DBXForgotPasswordApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/10/8.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "DBXForgotPasswordApi.h"

@interface DBXForgotPasswordApi()
{
    NSString *_userName;
    NSString *_device_code;
    NSString *_newPassword;
}


@end

@implementation DBXForgotPasswordApi

- (id)initWithUsername:(NSString *)username device_code:(NSString *)device_code newPassword:(NSString *)newPassword {
    
    self = [super init];
    if (self) {
        _userName = username;
        _device_code = device_code;
        _newPassword = newPassword;
    }
    return self;
}

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return @"/AppService/lohas/health/auto/user/password/reset/dept";
}

// 请求方法
- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

// 请求体
- (id)requestArgument {
    return @{
             @"username": _userName,
             @"device_code": _device_code,
             @"newPassword": _newPassword
             };
}

@end
