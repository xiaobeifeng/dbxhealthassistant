//
//  DBXForgotPasswordViewController.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/10/8.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XLForm/XLForm.h>
/// 忘记密码
@interface DBXForgotPasswordViewController : XLFormViewController

@end
