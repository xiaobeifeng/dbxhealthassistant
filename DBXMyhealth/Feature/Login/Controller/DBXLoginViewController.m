//
//  DBXLoginViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/30.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXLoginViewController.h"
#import "DBXLoginInputView.h"
#import "DBXLoginEncryptInputView.h"
#import "DBXRegisteViewController.h"
#import "DBXLoginApi.h"
#import "DBXUserAvatarApi.h"
#import "DBXBaseTabBarController.h"
#import "DBXLinkyorntwoApi.h"
#import "DBXPeripheralManagementViewController.h"
#import "DBXForgotPasswordViewController.h"
#import "ZJDeviceManageViewController.h"

@interface DBXLoginViewController ()<DBXRegisteViewControllerDelegate, UITextFieldDelegate>

@property(nonatomic, strong) DBXLoginInputView *accountInputView;
@property(nonatomic, strong) DBXLoginEncryptInputView *passwordInputView;
@property(nonatomic, strong) UISwitch *switchView;
@end

@implementation DBXLoginViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
   
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    
}

- (void)loadView
{
    UIImageView *imageView = [UIImageView new];
    imageView.userInteractionEnabled = YES;
    imageView.image = [UIImage imageNamed:@"login_background"];
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
    self.view = imageView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupSubViews];
}

/**
 搭建视图
 */
- (void)setupSubViews
{
    UIImageView *topLogoImageView = [UIImageView new];
    topLogoImageView.image = [UIImage imageNamed:@"login_top"];
    topLogoImageView.userInteractionEnabled = YES;
    [self.view addSubview:topLogoImageView];
    [topLogoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).with.offset(90);
        make.width.mas_equalTo(70);
        make.height.mas_equalTo(60);
        make.centerX.mas_equalTo(self.view.mas_centerX);
    }];

# warning 用户名先不进行校验（之前出现了注册中文名字的现象）
    _accountInputView = [[DBXLoginInputView alloc] initWithIcon:@"login_name" placeholder:@"请输入账号"];
//    _accountInputView.textField.keyboardType = UIKeyboardTypeASCIICapable;
    _accountInputView.textField.delegate = self;
    _accountInputView.textField.tag = 10;
    [self.view addSubview:_accountInputView];
    [_accountInputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(topLogoImageView.mas_bottom).with.offset(40);
        make.width.mas_equalTo(320);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.height.mas_equalTo(40);
    }];
    
    _passwordInputView = [[DBXLoginEncryptInputView alloc] initWithIcon:@"login_pwd" placeholder:@"请输入密码"];
    _passwordInputView.textField.keyboardType = UIKeyboardTypeASCIICapable;
    [self.view addSubview:_passwordInputView];
    [_passwordInputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.accountInputView.mas_bottom).with.offset(10);
        make.width.mas_equalTo(self.accountInputView.mas_width);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.height.mas_equalTo(40);
    }];
    
    // 记住密码开关(默认开启)
    _switchView = [[UISwitch alloc] init];
    [_switchView setOn:YES];
    _switchView.onTintColor = [UIColor colorWithHexString:@"#EBBE6F"];
//    [_switchView addTarget:self action:@selector(switchChange:)forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:_switchView];
    [_switchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.passwordInputView.mas_right).with.offset(-13);
        make.top.mas_equalTo(self.passwordInputView.mas_bottom).with.offset(20);
    }];
    
    // 记住密码
    UILabel *switchLabel = [[UILabel alloc] init];
    switchLabel.text = @"记住密码";
    switchLabel.textColor = [UIColor whiteColor];
    switchLabel.font = [UIFont systemFontOfSize:14];
    [switchLabel sizeToFit];
    [self.view addSubview:switchLabel];
    [switchLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.switchView.mas_left).with.offset(-13);
        make.centerY.mas_equalTo(self.switchView.mas_centerY);
    }];
    
    // 登录按钮
    UIButton *loginBtn = [UIButton new];
    [loginBtn setTitle:@"登 录" forState:UIControlStateNormal];
    [loginBtn addTarget:self action:@selector(handleLoginBtnEvent) forControlEvents:UIControlEventTouchUpInside];
    loginBtn.backgroundColor = [UIColor colorWithHexString:@"#EBBE6F"];
    [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    kViewRadius(loginBtn, 20.0f);
    loginBtn.layer.shadowColor = [UIColor blackColor].CGColor;//shadowColor阴影颜色
    loginBtn.layer.shadowOffset = CGSizeMake(0,5);//shadowOffset阴影偏移，默认(0, -3),这个跟shadowRadius配合使用
    loginBtn.layer.shadowOpacity = 0.3;//阴影透明度，默认0
    loginBtn.layer.masksToBounds = NO;

    [self.view addSubview:loginBtn];
    [loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.switchView.mas_bottom).with.offset(50);
        make.left.mas_equalTo(self.passwordInputView.mas_left).with.offset(13);
        make.right.mas_equalTo(self.passwordInputView.mas_right).with.offset(-13);
        make.height.mas_equalTo(44);
        make.centerX.mas_equalTo(self.view.mas_centerX);
    }];
    
    // 注册按钮
    UIButton *registBtn = [UIButton new];
    [registBtn setTitle:@"注 册" forState:UIControlStateNormal];
    [registBtn addTarget:self action:@selector(handleRegistBtnEvent) forControlEvents:UIControlEventTouchUpInside];
    registBtn.backgroundColor = [UIColor colorWithHexString:@"#7CD2F3"];
    [registBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    kViewRadius(registBtn, 20.0f);
    registBtn.layer.shadowColor = [UIColor blackColor].CGColor;//shadowColor阴影颜色
    registBtn.layer.shadowOffset = CGSizeMake(0,5);//shadowOffset阴影偏移，默认(0, -3),这个跟shadowRadius配合使用
    registBtn.layer.shadowOpacity = 0.3;//阴影透明度，默认0
    registBtn.layer.masksToBounds = NO;
    [self.view addSubview:registBtn];
    [registBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(loginBtn.mas_bottom).with.offset(20);
        make.left.mas_equalTo(self.passwordInputView.mas_left).with.offset(13);
        make.right.mas_equalTo(self.passwordInputView.mas_right).with.offset(-13);
        make.height.mas_equalTo(44);
        make.centerX.mas_equalTo(self.view.mas_centerX);
    }];

    // 忘记密码
    UIButton *forgetPasswordBtn = [UIButton new];
    [forgetPasswordBtn setTitle:@"忘记密码？" forState:UIControlStateNormal];
    forgetPasswordBtn.backgroundColor = [UIColor clearColor];
    forgetPasswordBtn.titleLabel.font = kFont(14);
    forgetPasswordBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [forgetPasswordBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [forgetPasswordBtn addTarget:self action:@selector(handleForgetPasswordBtnEvent) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:forgetPasswordBtn];
    [forgetPasswordBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(registBtn.mas_bottom).with.offset(20);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.width.mas_equalTo(@100);
    }];
    
    NSString *password = [[NSUserDefaults standardUserDefaults] objectForKey:kPassword];
    if (password.length != 0) {
        NSString *account = [[NSUserDefaults standardUserDefaults] objectForKey:kAccount];
        _accountInputView.textField.text = account;
        _passwordInputView.textField.text = password;
    } else {
        _accountInputView.textField.text = @"";
        _passwordInputView.textField.text = @"";
    }
 
   
}

/**
 登录
 */
- (void)handleLoginBtnEvent
{
    // 账号校验
    NSString *accountStr = _accountInputView.textField.text;
    if (!kStringIsEmpty(accountStr)) {
        
        BOOL isLegalAccount = [NSString isLegalAccount:accountStr];
        if (isLegalAccount) {
            
            NSString *accountStrNoSpace = [NSString removeSpaceAndNewline:accountStr];
            
            // 密码校验
            NSString *password = _passwordInputView.textField.text;
            if (!kStringIsEmpty(password)) {
                
                
                // 记住密码开启
                if (_switchView.isOn) {
                    // 本地保存用户名和密码
                    [[NSUserDefaults standardUserDefaults] setObject:accountStrNoSpace forKey:kAccount];
                    [[NSUserDefaults standardUserDefaults] setObject:password forKey:kPassword];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                } else {    // 保存用户名，移除密码
                    [[NSUserDefaults standardUserDefaults] setObject:accountStrNoSpace forKey:kAccount];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kPassword];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                
                
                // 开始进行登录请求
                [self startRequestLoginWithAccount:accountStrNoSpace password:password];
                
            } else {
                [MBProgressHUD showTipMessageInView:@"请输入密码"];
                return;
            }
            
        } else {
            [MBProgressHUD showTipMessageInView:@"账号仅支持数字字母下划线"];
            return;
        }
    } else {
        [MBProgressHUD showTipMessageInView:@"请输入账号"];
        return;
    }
}

/**
 开始进行登录请求
 */
- (void)startRequestLoginWithAccount:(NSString *)account password:(NSString *)password
{
    
    DBXLoginApi *api = [[DBXLoginApi alloc] initWithUsername:account password:password];
    [MBProgressHUD showActivityMessageInWindow:@"登录中..."];
    
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"succeed %@", request.responseJSONObject);
        [MBProgressHUD hideHUD];
        
        NSDictionary *dictionary = request.responseJSONObject[@"data"];
        
        if (!kDictIsEmpty(dictionary)) {
            // 保存token
            NSString *token = dictionary[@"token"];
            [[NSUserDefaults standardUserDefaults] setObject:token forKey:kToken];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            // 判断账号是否激活
            [self requestLinkyorntwoApi];
            
        } else {
            
            [MBProgressHUD showTipMessageInView:request.responseJSONObject[@"message"]];
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [MBProgressHUD hideHUD];
        [MBProgressHUD showTipMessageInView:[NSString stringWithFormat:@"%@", request.error]];
        NSLog(@"%@", request.error);
        
    }];
    
    
}

/**
 查看用户是否建立过外设连接
 */
- (void)requestLinkyorntwoApi
{
    DBXLinkyorntwoApi *api = [[DBXLinkyorntwoApi alloc] init];
    
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        if (request.responseJSONObject) {
            NSDictionary *dic = request.responseJSONObject;
            
            // 设备未建立过连接 或 设备未激活
            if ([dic[@"state"] integerValue] == 1041 || [dic[@"state"] integerValue] == 1043) {
                ZJDeviceManageViewController *deviceManageVC = [ZJDeviceManageViewController new];
                deviceManageVC.navigationItem.title = @"设备管理";
                deviceManageVC.isNewUser = YES;
                DBXBaseNavigationController *baseNavC = [[DBXBaseNavigationController alloc] initWithRootViewController:deviceManageVC];
                [UIApplication sharedApplication].keyWindow.rootViewController = baseNavC;
            }
            
            // 建立过连接，直接进入app
            if ([dic[@"state"] integerValue] == 1042) {
                
                [UIApplication sharedApplication].keyWindow.rootViewController = [DBXBaseTabBarController new];
                
            }
            
            
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
    
}

/**
 注册
 */
- (void)handleRegistBtnEvent
{
    
    DBXRegisteViewController *registVC = [DBXRegisteViewController new];
    registVC.delegate = self;
    [self.navigationController pushViewController:registVC animated:YES];
}


/**
 忘记密码
 */
- (void)handleForgetPasswordBtnEvent
{
    DBXForgotPasswordViewController *forgotPasswordVC = [DBXForgotPasswordViewController new];
    [self.navigationController pushViewController:forgotPasswordVC animated:YES];
}


#pragma mark - DBXRegisteViewControllerDelegate
/**
 注册模块代理：新注册的用户登录

 @param userName
 @param password
 */
- (void)registSuccessWithUserName:(NSString *)userName password:(NSString *)password
{
    self.accountInputView.textField.text = userName;
    self.passwordInputView.textField.text = password;
    
    // 本地保存用户名和密码
    [[NSUserDefaults standardUserDefaults] setObject:userName forKey:kAccount];
    [[NSUserDefaults standardUserDefaults] setObject:password forKey:kPassword];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self startRequestLoginWithAccount:userName password:password];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField.tag == 10) {
        
        //用户名去空格
        NSString *tem = [[string componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]componentsJoinedByString:@""];
        if (![string isEqualToString:tem]) {
            return NO;
        }
       
    }
    
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
