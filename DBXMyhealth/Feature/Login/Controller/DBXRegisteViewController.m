//
//  DBXRegisteViewController.m
//  DBXHealthAssistant
//
//  Created by zhoujian on 2018/8/6.
//  Copyright © 2018年 mc. All rights reserved.
//

#import "DBXRegisteViewController.h"
#import "UIButton+ImageTitleSpacing.h"
#import "DBXRegisteView.h"
#import "DBXPasswordView.h"
#import "DBXPeripheralManagementViewController.h"
#import "DBXRegisteApi.h"
#import "DBXLoginApi.h"
#import "DBXLinkyorntwoApi.h"
#import "DBXPeripheralManagementViewController.h"

// 输入限制-用户名
static NSString *inputLimits_userName = @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
// 输入限制-设备号
static NSString *inputLimits_equipmentNumber = @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789:";

@interface DBXRegisteViewController ()<UITextFieldDelegate>

/** 用户名 */
@property (nonatomic, strong) DBXRegisteView *userNameView;
/** 密码 */
@property (nonatomic, strong) DBXPasswordView *passwordView;
/** 再次输入密码 */
@property (nonatomic, strong) DBXPasswordView *repeatPasswordView;
/** 设备号 */
@property (nonatomic, strong) DBXRegisteView *equipmentView;
/** 店铺代码 */
@property (nonatomic, strong) DBXRegisteView *storeView;

@property (nonatomic, strong) UIScrollView *scrollView;


@end

@implementation DBXRegisteViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
}

- (void)setupUI
{
    UIImageView *backgroudImageView = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    backgroudImageView.userInteractionEnabled = YES;
    backgroudImageView.image = [UIImage imageNamed:@"login_background"];
    [self.view addSubview:backgroudImageView];
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:backgroudImageView.frame];
    [backgroudImageView addSubview:scrollView];
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(backgroudImageView).with.insets(UIEdgeInsetsMake(0,0,0,0));
    }];
    
    UIView *container = [UIView new];
    [scrollView addSubview:container];
    [container mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(scrollView);
        make.width.equalTo(scrollView);
    }];
    
    UILabel *titleLbl = [UILabel new];
    titleLbl.text = @"注册一个新的账号";
    titleLbl.textColor = [UIColor whiteColor];
    titleLbl.font = [UIFont systemFontOfSize:30];
    [container addSubview:titleLbl];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(scrollView).with.offset(30);
        make.centerX.equalTo(scrollView.mas_centerX);
    }];
    
    _userNameView = [[DBXRegisteView alloc] initRegisteViewWithTitle:@"用户名" placeholder:@"请输入您的用户名（仅支持数字、字母）"];
    _userNameView.textField.delegate = self;
    [container addSubview:_userNameView];
    [_userNameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLbl.mas_bottom).with.offset(50);
        make.centerX.equalTo(container.mas_centerX);
        make.left.equalTo(container).with.offset(30);
        make.right.equalTo(container).with.offset(-30);
        make.height.equalTo(@46);
    }];
    
    _passwordView = [[DBXPasswordView alloc] initRegisteViewWithTitle:@"设置密码" placeholder:@"请输入您的密码"];
    _passwordView.textField.delegate = self;
    [container addSubview:_passwordView];
    [_passwordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userNameView.mas_bottom).with.offset(30);
        make.centerX.equalTo(container.mas_centerX);
        make.left.equalTo(container).with.offset(30);
        make.right.equalTo(container).with.offset(-30);
        make.height.equalTo(@46);
    }];
    
    _repeatPasswordView = [[DBXPasswordView alloc] initRegisteViewWithTitle:@"再次输入密码" placeholder:@"请再次输入您的密码"];
    _repeatPasswordView.textField.delegate = self;
    [container addSubview:_repeatPasswordView];
    [_repeatPasswordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.passwordView.mas_bottom).with.offset(30);
        make.centerX.equalTo(container.mas_centerX);
        make.left.equalTo(container).with.offset(30);
        make.right.equalTo(container).with.offset(-30);
        make.height.equalTo(@46);
    }];
    
    _equipmentView = [[DBXRegisteView alloc] initRegisteViewWithTitle:@"设备号" placeholder:@"请输入您的设备号"];
    _equipmentView.textField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;    // 自动大写
    _equipmentView.textField.delegate = self;
    [container addSubview:_equipmentView];
    [_equipmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.repeatPasswordView.mas_bottom).with.offset(30);
        make.centerX.equalTo(container.mas_centerX);
        make.left.equalTo(container).with.offset(30);
        make.right.equalTo(container).with.offset(-30);
        make.height.equalTo(@46);
    }];
    
    _storeView = [[DBXRegisteView alloc] initRegisteViewWithTitle:@"店铺代码" placeholder:@"请输入您的店铺代码"];
    _storeView.textField.delegate = self;
    [container addSubview:_storeView];
    [_storeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.equipmentView.mas_bottom).with.offset(30);
        make.centerX.equalTo(container.mas_centerX);
        make.left.equalTo(container).with.offset(30);
        make.right.equalTo(container).with.offset(-30);
        make.height.equalTo(@46);
    }];
    
    UIButton *nextBtn = [UIButton new];
    [nextBtn setTitle:@"注 册" forState:UIControlStateNormal];
    nextBtn.titleLabel.font = [UIFont systemFontOfSize:17];
    nextBtn.backgroundColor = [UIColor colorWithRed:0.15 green:0.34 blue:0.61 alpha:1.00];
    kViewRadius(nextBtn, 23.0f);
    [nextBtn addTarget:self action:@selector(handleNextBtnEvent:) forControlEvents:UIControlEventTouchUpInside];
    [nextBtn sizeToFit];
    [container addSubview:nextBtn];
    [nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.storeView.mas_bottom).with.offset(40);
        make.centerX.equalTo(scrollView.mas_centerX);
        make.width.equalTo(@140);
        make.height.equalTo(@46);
    }];
    
    [container mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(nextBtn.mas_bottom).with.offset(60);
    }];
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setupUI];
    
    //点击背景收回键盘
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    
}

/**
 注册按钮点击事件
 
 @param button button
 */
- (void)handleNextBtnEvent:(UIButton *)button
{
    // 用户名
    NSString *userName = _userNameView.textField.text;
    // 密码
    NSString *password = _passwordView.textField.text;
    // 再次输入密码
    NSString *passwordAgain = _repeatPasswordView.textField.text;
    // 设备号
    NSString *equipmentNumber = _equipmentView.textField.text;
    // 店铺号
    NSString *shopNumber = _storeView.textField.text;
    
    if (kStringIsEmpty(userName)) {
        [MBProgressHUD showInfoMessage:@"用户名不能为空"];
        return;
    }
    
    if (kStringIsEmpty(password)) {
        [MBProgressHUD showInfoMessage:@"密码不能为空"];
        return;
    }
    
    // 再次输入密码校验
    if (kStringIsEmpty(passwordAgain)) {
        [MBProgressHUD showInfoMessage:@"再次输入密码不能为空"];
        return;
    } else {
        if (![_repeatPasswordView.textField.text isEqualToString:_passwordView.textField.text]) {
            [MBProgressHUD showInfoMessage:@"两次输入的密码不一致，请重新输入"];
            return;
        }
    }
    
    // 设备号
    if (kStringIsEmpty(equipmentNumber)) {
        [MBProgressHUD showInfoMessage:@"设备号不能为空"];
        return;
    }
    // 设备号去掉冒号
    NSString *cleanEquipmentNumber = [equipmentNumber stringByReplacingOccurrencesOfString:@":" withString:@""];
    
    // 店铺号
    if (kStringIsEmpty(shopNumber)) {
        [MBProgressHUD showInfoMessage:@"店铺号不能为空"];
        return;
    }
    
    DBXRegisteApi *api = [[DBXRegisteApi alloc] initWithUsername:userName password:password equipmentNumber:cleanEquipmentNumber shopNumber:shopNumber];
    
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        if (request.responseJSONObject) {
            NSDictionary *dic = request.responseJSONObject;
            // 注册成功
            if ([dic[@"state"] integerValue] == 0) {
                
                [MBProgressHUD showInfoMessage:dic[@"message"]];
                
                [self.navigationController popViewControllerAnimated:YES];
                [self.delegate registSuccessWithUserName:userName password:password];
                
            } else {
                
                [MBProgressHUD showInfoMessage:dic[@"message"]];
                
            }
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
    
}

#pragma mark - textField Delegate
/**
 点击Done 收起键盘
 */
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

/**
 用户输入校验和限制输入长度
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;   // return NO to not change text
{
    // 用户名输入框
    if (textField == _userNameView.textField) {
        
        if (textField.text.length + string.length - range.length > 20) {
            return NO;
        } else {
            NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:inputLimits_userName] invertedSet];
            NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            return [string isEqualToString:filtered];
        }
    }
    
    // 密码输入长度小于等于20字符
    if (textField == _passwordView.textField || textField == _repeatPasswordView.textField) {
        
        if (textField.text.length + string.length - range.length > 20) {
            return NO;
        } else {
            return YES;
        }
    }
    
    // 设备号输入校验
    if (textField == _equipmentView.textField) {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:inputLimits_equipmentNumber] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    
    // 店铺代码输入校验
    if (textField == _storeView.textField) {
        NSString *tem = [[string componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]componentsJoinedByString:@""];
        if (![string isEqualToString:tem]) {
            return NO;
        }
        return YES;
    }
    
    return YES;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
