//
//  DBXRegisteViewController.h
//  DBXHealthAssistant
//
//  Created by zhoujian on 2018/8/6.
//  Copyright © 2018年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DBXRegisteViewControllerDelegate <NSObject>

- (void)registSuccessWithUserName:(NSString *)userName
                         password:(NSString *)password;

@end

/**
 注册页
 */
@interface DBXRegisteViewController : UIViewController

@property(nonatomic, weak) id<DBXRegisteViewControllerDelegate> delegate;

@end
