//
//  DBXForgotPasswordViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/10/8.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "DBXForgotPasswordViewController.h"
#import "DBXForgotPasswordApi.h"

@interface DBXForgotPasswordViewController ()

@end

@implementation DBXForgotPasswordViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self){
        [self initializeForm];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self){
        [self initializeForm];
    }
    return self;
}

- (void)initializeForm
{
    XLFormDescriptor * form;
    XLFormSectionDescriptor * section;
    XLFormRowDescriptor * row;
    
    form = [XLFormDescriptor formDescriptorWithTitle:@"忘记密码"];
    section = [XLFormSectionDescriptor formSection];
    [form addFormSection:section];
    
    // 用户名
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"userName" rowType:XLFormRowDescriptorTypeName title:@"用户名"];
    [row.cellConfig setObject:@"请输入用户名" forKey:@"textField.placeholder"];
    [section addFormRow:row];
    
    // 设备号
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"equipment" rowType:XLFormRowDescriptorTypeAccount title:@"设备号"];
    [row.cellConfig setObject:@"请输入已激活的设备号" forKey:@"textField.placeholder"];
    [section addFormRow:row];
    
    // 新密码
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"newPassword" rowType:XLFormRowDescriptorTypePassword title:@"新密码"];
    [row.cellConfig setObject:@"请输入新密码" forKey:@"textField.placeholder"];
    [section addFormRow:row];
    
    // 再次输入新密码
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"againNewPassword" rowType:XLFormRowDescriptorTypePassword title:@"确认密码"];
    [row.cellConfig setObject:@"请再次输入新密码" forKey:@"textField.placeholder"];
    [section addFormRow:row];
    
    self.form = form;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem titleItem:@"提交" target:self action:@selector(saveBtnClick)];
}


/**
 保存设置
 */
- (void)saveBtnClick
{
    // 获取表单所有到的值
    NSDictionary *values =  [self formValues];
    NSString *userName = values[@"userName"];
    NSString *equipmentNumber = values[@"equipment"];
    NSString *newPassword = values[@"newPassword"];
    NSString *againNewPassword = values[@"againNewPassword"];
    
//    userName = @"yxl";
//    equipmentNumber = @"F0:45:DA:AB:F8:FA";
//    newPassword = @"123456";
    
    // 校验
    if (kObjectIsEmpty(userName)) {
        [MBProgressHUD showInfoMessage:@"请输入用户名"];
        return;
    }

    if (kObjectIsEmpty(equipmentNumber)) {
        [MBProgressHUD showInfoMessage:@"请输入绑定的设备号"];
        return;
    }

    if (newPassword.length > 20) {
        [MBProgressHUD showInfoMessage:@"密码太长啦"];
        return;
    }

    if (!kObjectIsEmpty(newPassword) && !kObjectIsEmpty(againNewPassword)) {

        if (![newPassword isEqualToString:againNewPassword]) {
            [MBProgressHUD showInfoMessage:@"新密码和确认密码不一致，请检查后再试"];
            return;
        }

    } else {

        if (kObjectIsEmpty(newPassword)) {
            [MBProgressHUD showInfoMessage:@"请输入新密码"];
            return;
        } else {
            [MBProgressHUD showInfoMessage:@"请输入确认密码"];
            return;
        }

    }
    
    
    // 设备号去掉冒号
    NSString *cleanEquipmentNumber = [equipmentNumber stringByReplacingOccurrencesOfString:@":" withString:@""];
    
    DBXForgotPasswordApi *api = [[DBXForgotPasswordApi alloc] initWithUsername:userName device_code:cleanEquipmentNumber newPassword:newPassword];
    
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
         NSLog(@"%@", request.responseJSONObject);
        
         if (request.responseJSONObject) {

             NSInteger state = [request.responseJSONObject[@"state"] integerValue];
             if(state == 0) {
                 [MBProgressHUD showInfoMessage:@"密码设置成功，请重新登录"];
                 [self.navigationController popViewControllerAnimated:YES];
             } else {
                 NSString *message = request.responseJSONObject[@"message"];
                 [MBProgressHUD showInfoMessage:message];
             }
             
         }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [MBProgressHUD showInfoMessage:@"密码设置失败，请稍后再试"];
        
    }];
    
}



@end
