//
//  DBXPasswordView.h
//  DBXHealthAssistant
//
//  Created by zhoujian on 2018/8/7.
//  Copyright © 2018年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 密码输入框
 */
@interface DBXPasswordView : UIView

@property(nonatomic, strong) UITextField *textField;
@property(nonatomic, strong) UILabel *titleLbl;

- (instancetype)initRegisteViewWithTitle:(NSString *)title placeholder:(NSString *)placeholder;

@end
