//
//  DBXRegisteView.m
//  DBXHealthAssistant
//
//  Created by zhoujian on 2018/8/6.
//  Copyright © 2018年 mc. All rights reserved.
//

#import "DBXRegisteView.h"

@interface DBXRegisteView()

@end

@implementation DBXRegisteView

- (DBXRegisteView *)initRegisteViewWithTitle:(NSString *)title placeholder:(NSString *)placeholder
{
    if (self == [super init]) {
        
        self.userInteractionEnabled = YES;
        
        _titleLbl = [UILabel new];
        _titleLbl.text = title;
        _titleLbl.font = [UIFont systemFontOfSize:16];
        _titleLbl.textColor = [UIColor whiteColor];
        [self addSubview:_titleLbl];
        [_titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.top.equalTo(self);
            make.height.equalTo(@20);
            make.width.equalTo(self.mas_width);
        }];
        
        _textField = [UITextField new];
        _textField.tintColor = [UIColor whiteColor];
        
        _textField.textColor = [UIColor whiteColor];
        _textField.placeholder = placeholder;
        _textField.autocorrectionType = UITextAutocorrectionTypeNo;
        _textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _textField.keyboardType = UIKeyboardTypeASCIICapable;
        _textField.returnKeyType = UIReturnKeyDone;
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.font = [UIFont systemFontOfSize:14];
        [self addSubview:_textField];
        [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_titleLbl.mas_bottom).with.offset(5);
            make.left.equalTo(_titleLbl.mas_left);
            make.width.equalTo(self.mas_width);
            make.height.equalTo(@20.5);
        }];
        
        UIView *line = [UIView new];
        line.backgroundColor = [UIColor whiteColor];
        [self addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_textField.mas_bottom).with.offset(2);
            make.height.equalTo(@0.5);
            make.width.equalTo(self.mas_width);
            make.left.equalTo(_titleLbl.mas_left);
        }];
        
        
        
    }
    
    return self;
}



@end
