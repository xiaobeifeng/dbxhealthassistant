//
//  DBXLoginEncryptInputView.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/10/19.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 登录模块密码加密输入框
 */
@interface DBXLoginEncryptInputView : UIView

/**
 输入框
 */
@property(nonatomic, strong) UITextField *textField;

/**
 创建输入框
 
 @param icon 输入框icon
 @param placeholder 输入框占位文字
 @return 输入框
 */
- (instancetype)initWithIcon:(NSString *)icon placeholder:(NSString *)placeholder;

@end

NS_ASSUME_NONNULL_END
