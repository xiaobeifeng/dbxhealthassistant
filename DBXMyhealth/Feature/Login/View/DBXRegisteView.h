//
//  DBXRegisteView.h
//  DBXHealthAssistant
//
//  Created by zhoujian on 2018/8/6.
//  Copyright © 2018年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 注册输入框
 */
@interface DBXRegisteView : UIView

@property(nonatomic, strong) UITextField *textField;
@property(nonatomic, strong) UILabel *titleLbl;

- (DBXRegisteView *)initRegisteViewWithTitle:(NSString *)title placeholder:(NSString *)placeholder;

@end
