//
//  DBXPasswordView.m
//  DBXHealthAssistant
//
//  Created by zhoujian on 2018/8/7.
//  Copyright © 2018年 mc. All rights reserved.
//

#import "DBXPasswordView.h"

@implementation DBXPasswordView

- (instancetype)initRegisteViewWithTitle:(NSString *)title placeholder:(NSString *)placeholder
{
    if (self == [super init]) {
        
        self.userInteractionEnabled = YES;
        
        _titleLbl = [UILabel new];
        _titleLbl.text = title;
        _titleLbl.font = [UIFont systemFontOfSize:16];
        _titleLbl.textColor = [UIColor whiteColor];
        [self addSubview:_titleLbl];
        [_titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.top.equalTo(self);
            make.height.equalTo(@20);
            make.width.equalTo(self.mas_width);
        }];
        
        _textField = [UITextField new];
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.secureTextEntry = YES;
        _textField.tintColor = [UIColor whiteColor];
        _textField.textColor = [UIColor whiteColor];
//        _textField.backgroundColor = [UIColor orangeColor];
        _textField.placeholder = placeholder;
        _textField.autocorrectionType = UITextAutocorrectionTypeNo;
        _textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _textField.keyboardType = UIKeyboardTypeASCIICapable;
        _textField.returnKeyType = UIReturnKeyDone;
        _textField.font = [UIFont systemFontOfSize:14];
        [self addSubview:_textField];
        [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_titleLbl.mas_bottom).with.offset(5);
            make.left.equalTo(_titleLbl.mas_left);
            make.width.equalTo(self.mas_width).with.offset(-23);
            make.height.equalTo(@20.5);
        }];
        

        UIButton *encryptBtn = [UIButton new];
        [encryptBtn setImage:[UIImage imageNamed:@"regist_eye"] forState:UIControlStateNormal];
        [encryptBtn setImage:[UIImage imageNamed:@"regist_eye_select"] forState:UIControlStateSelected];
        [encryptBtn addTarget:self action:@selector(handleEncryptBtnEvent:) forControlEvents:UIControlEventTouchUpInside];
        encryptBtn.backgroundColor = [UIColor clearColor];
        [self addSubview:encryptBtn];
        [encryptBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_textField.mas_right).with.offset(3);
            make.width.height.equalTo(@20);
            make.centerY.equalTo(_textField.mas_centerY);
        }];
        
        UIView *line = [UIView new];
        line.backgroundColor = [UIColor whiteColor];
        [self addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_textField.mas_bottom).with.offset(2);
            make.height.equalTo(@0.5);
            make.width.equalTo(self.mas_width);
            make.left.equalTo(_titleLbl.mas_left);
        }];
        
        
        
    }
    
    return self;
}

/**
 密码加密状态切换
 */
- (void)handleEncryptBtnEvent:(UIButton *)button
{
    [_textField resignFirstResponder];  // 避免切换小圆点导致光标位置错误
    
    button.selected = !button.selected;
    
    if (button.selected) { // 按下去了就是明文
        
        NSString *tempPwdStr = _textField.text;
        _textField.secureTextEntry = NO;
        _textField.text = tempPwdStr;
        
    } else { // 暗文
        
        NSString *tempPwdStr = _textField.text;
        _textField.secureTextEntry = YES;
        _textField.text = tempPwdStr;
    }
}


@end
