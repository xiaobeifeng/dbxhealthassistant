//
//  DBXLoginEncryptInputView.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/10/19.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "DBXLoginEncryptInputView.h"

@interface  DBXLoginEncryptInputView()

@property(nonatomic, strong) UIImageView *inputIconImageView;
@property(nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UIButton *encryptBtn;

@end

@implementation DBXLoginEncryptInputView

- (instancetype)initWithIcon:(NSString *)icon placeholder:(NSString *)placeholder
{
    self = [super init];
    
    if (self) {
        
        _inputIconImageView = [UIImageView new];
        _inputIconImageView.image = [UIImage imageNamed:icon];
        _inputIconImageView.backgroundColor = [UIColor clearColor];
        [self addSubview:_inputIconImageView];
        
        _lineView = [UIView new];
        _lineView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_lineView];
        
        _textField = [UITextField new];
        _textField.backgroundColor = [UIColor clearColor];
        _textField.secureTextEntry = YES;
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.font = kFont(14);
        _textField.placeholder = placeholder;
        _textField.autocorrectionType = UITextAutocorrectionTypeNo;
        _textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        //        [_textField setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
        _textField.tintColor = [UIColor whiteColor];
        _textField.textColor = [UIColor whiteColor];
        [self addSubview:_textField];
        
        _encryptBtn = [UIButton new];
        [_encryptBtn setImage:[UIImage imageNamed:@"regist_eye"] forState:UIControlStateNormal];
        [_encryptBtn setImage:[UIImage imageNamed:@"regist_eye_select"] forState:UIControlStateSelected];
        [_encryptBtn addTarget:self action:@selector(handleEncryptBtnEvent:) forControlEvents:UIControlEventTouchUpInside];
//        _encryptBtn.backgroundColor = [UIColor redColor];
        _encryptBtn.backgroundColor = [UIColor clearColor];
        [self addSubview:_encryptBtn];
        
    }
    
    return self;
}

+ (BOOL)requiresConstraintBasedLayout
{
    return YES;
}

- (void)updateConstraints
{
    [_inputIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).with.offset(13);
        make.width.height.mas_equalTo(23);
        make.centerY.mas_equalTo(self.mas_centerY);
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).with.offset(13);
        make.right.mas_equalTo(self).with.offset(-13);
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(self.mas_bottom);
    }];
    
    [_encryptBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).with.offset(-13);
        make.width.height.equalTo(@20);
        make.centerY.equalTo(self.inputIconImageView.mas_centerY);
    }];
    
    [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.inputIconImageView.mas_right).with.offset(13);
        make.right.mas_equalTo(self.encryptBtn.mas_left).with.offset(-13);
        make.height.mas_equalTo(self.inputIconImageView.mas_height);
        make.centerY.mas_equalTo(self.inputIconImageView.mas_centerY);
    }];
    
    [super updateConstraints];
}

/**
 密码加密状态切换
 */
- (void)handleEncryptBtnEvent:(UIButton *)button
{
    [_textField resignFirstResponder];  // 避免切换小圆点导致光标位置错误
    
    button.selected = !button.selected;
    
    if (button.selected) { // 按下去了就是明文
        
        NSString *tempPwdStr = _textField.text;
        _textField.secureTextEntry = NO;
        _textField.text = tempPwdStr;
        
    } else { // 暗文
        
        NSString *tempPwdStr = _textField.text;
        _textField.secureTextEntry = YES;
        _textField.text = tempPwdStr;
    }
}

@end
