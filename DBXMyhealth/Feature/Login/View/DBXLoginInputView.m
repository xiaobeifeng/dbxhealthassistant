//
//  DBXLoginInputView.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/30.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXLoginInputView.h"

@interface DBXLoginInputView()

@property(nonatomic, strong) UIImageView *inputIconImageView;
@property(nonatomic, strong) UIView *lineView;

@end

@implementation DBXLoginInputView

- (instancetype)initWithIcon:(NSString *)icon placeholder:(NSString *)placeholder
{
    self = [super init];
    
    if (self) {
        
        _inputIconImageView = [UIImageView new];
        _inputIconImageView.image = [UIImage imageNamed:icon];
        _inputIconImageView.backgroundColor = [UIColor clearColor];
        [self addSubview:_inputIconImageView];
        
        _lineView = [UIView new];
        _lineView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_lineView];
        
        _textField = [UITextField new];
        _textField.backgroundColor = [UIColor clearColor];
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.font = kFont(14);
        _textField.placeholder = placeholder;
        _textField.autocorrectionType = UITextAutocorrectionTypeNo;
        _textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
//        [_textField setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
        _textField.tintColor = [UIColor whiteColor];
        _textField.textColor = [UIColor whiteColor];
        [self addSubview:_textField];
    }
    
    return self;
}

+ (BOOL)requiresConstraintBasedLayout
{
    return YES;
}

- (void)updateConstraints
{
    [_inputIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).with.offset(13);
        make.width.height.mas_equalTo(23);
        make.centerY.mas_equalTo(self.mas_centerY);
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).with.offset(13);
        make.right.mas_equalTo(self).with.offset(-13);
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(self.mas_bottom);
    }];
    
    [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.inputIconImageView.mas_right).with.offset(13);
        make.right.mas_equalTo(self).with.offset(-13);
        make.height.mas_equalTo(self.inputIconImageView.mas_height);
        make.centerY.mas_equalTo(self.inputIconImageView.mas_centerY);
    }];
    
    [super updateConstraints];
}

@end
