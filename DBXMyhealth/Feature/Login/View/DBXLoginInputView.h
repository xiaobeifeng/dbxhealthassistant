//
//  DBXLoginInputView.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/30.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBXLoginInputView : UIView


/**
 输入框
 */
@property(nonatomic, strong) UITextField *textField;

/**
 创建输入框

 @param icon 输入框icon
 @param placeholder 输入框占位文字
 @return 输入框
 */
- (instancetype)initWithIcon:(NSString *)icon placeholder:(NSString *)placeholder;

@end
