//
//  ZJSetpNumberApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/14.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "ZJSetpNumberApi.h"

@interface ZJSetpNumberApi()
@property(nonatomic, assign) NSInteger count;
@property(nonatomic, assign) NSInteger doa;
@end

@implementation ZJSetpNumberApi

- (id)initWithCount:(NSInteger)count doa:(NSInteger)doa
{
    self = [super init];
    if (self) {
        _count = count;
        _doa = doa;
    }
    return self;
}

- (NSString *)requestUrl {
    
    NSString *url = [NSString stringWithFormat:@"/AppService/lohas/health/step/%ld/%ld", (long)_count, (long)_doa];
    return url;
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
