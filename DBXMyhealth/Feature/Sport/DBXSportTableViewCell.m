//
//  DBXSportTableViewCell.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/17.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXSportTableViewCell.h"

@interface DBXSportTableViewCell()

@property(nonatomic, strong) UIView *lineView;

@end

@implementation DBXSportTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _timeLbl = [UILabel new];
        _timeLbl.font = kFont(12);
        _timeLbl.textAlignment = NSTextAlignmentCenter;
        _timeLbl.numberOfLines = 0;
        [self.contentView addSubview:_timeLbl];
        
        _stepLbl = [UILabel new];
        _stepLbl.font = kFont(12);
        _stepLbl.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_stepLbl];
        
        _calorieLbl = [UILabel new];
        _calorieLbl.font = kFont(12);
        _calorieLbl.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_calorieLbl];
        
        _lineView = [UIView new];
        _lineView.backgroundColor = kTableSeparatorLineColor;
        [self.contentView addSubview:_lineView];
        
    }
    
    return self;
}

+ (BOOL)requiresConstraintBasedLayout
{
    return YES;
}

- (void)updateConstraints
{
    [@[_timeLbl, _stepLbl, _calorieLbl] mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:5 leadSpacing:13 tailSpacing:13];
    
    [@[_timeLbl, _stepLbl, _calorieLbl] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView);
        make.height.equalTo(@39.5);
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(0.5);
        make.width.mas_equalTo(kScreenWidth);
        make.bottom.mas_equalTo(self.contentView.mas_bottom);
    }];
    
    [super updateConstraints];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
