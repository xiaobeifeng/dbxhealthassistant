//
//  ZJStepNumberModel.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/14.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZJStepNumberModelData :NSObject
@property (nonatomic , copy) NSString              * steps;
@property (nonatomic , copy) NSString              * upsavetime;

@end

@interface ZJStepNumberModel :NSObject
@property (nonatomic , copy) NSString              * message;
@property (nonatomic , copy) NSArray<ZJStepNumberModelData *>              * data;
@property (nonatomic , copy) NSString              * state;

@end

NS_ASSUME_NONNULL_END
