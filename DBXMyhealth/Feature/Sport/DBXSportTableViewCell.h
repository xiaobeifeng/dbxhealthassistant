//
//  DBXSportTableViewCell.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/17.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBXSportTableViewCell : UITableViewCell

@property(nonatomic, strong) UILabel *timeLbl;
@property(nonatomic, strong) UILabel *stepLbl;
@property(nonatomic, strong) UILabel *calorieLbl;

@end
