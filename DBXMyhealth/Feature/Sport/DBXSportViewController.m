//
//  DBXSportViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/20.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXSportViewController.h"
#import "DBXSportTableViewCell.h"
#import "ZJSetpNumberApi.h"
#import "ZJStepNumberModel.h"
#import "DBXMePersonalinfoApi.h"
#import "DBXMePersonalinfoModel.h"

static NSString *kTableViewCellIdentifier = @"DBXSportTableViewCell";

@interface DBXSportViewController ()<UITableViewDelegate, UITableViewDataSource>

@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) UIImageView *headerView;
@property (nonatomic, strong) NSMutableArray *datasMarray;
@property (nonatomic, assign) NSInteger height;
@property (nonatomic, assign) NSInteger weight;

@end

@implementation DBXSportViewController

#pragma mark - 懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-kSafeAreaTopHeight) style:UITableViewStylePlain];
        [_tableView registerClass:[DBXSportTableViewCell class] forCellReuseIdentifier:kTableViewCellIdentifier];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

- (UIImageView *)headerView
{
    if (!_headerView) {
        _headerView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 150)];
        _headerView.image = [UIImage imageNamed:@"sport_top"];
    }
    return _headerView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"运动";
    _datasMarray = [NSMutableArray new];
    self.tableView.tableHeaderView = self.headerView;
    
    [self startRequestUserInfo];
    
    
}

- (void)startRequestUserInfo {
    // 获取身高体重
    DBXMePersonalinfoApi *api = [DBXMePersonalinfoApi new];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        DBXMePersonalinfoModel *model = [DBXMePersonalinfoModel yy_modelWithJSON:request.responseJSONObject];
        NSArray *array = [NSArray yy_modelArrayWithClass:[DBXMePersonalinfoModelData class] json:model.data];
        if (!kArrayIsEmpty(array)) {
            DBXMePersonalinfoModelData *modelData = array.firstObject;
            // 身高
            _height = [modelData.hight integerValue];
            // 体重
            _weight = [modelData.weight integerValue];
        } else {
            _height = 170;
            _weight = 0;
        }
        
        [self startRequestStepNumberApi];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
    
    }];
}

- (void)startRequestStepNumberApi {
    
    ZJSetpNumberApi *api = [[ZJSetpNumberApi alloc] initWithCount:1000 doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        ZJStepNumberModel *model = [ZJStepNumberModel yy_modelWithJSON:request.responseJSONObject];
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJStepNumberModelData class] json:model.data];
        if (kArrayIsEmpty(array)) {
            _datasMarray = [NSMutableArray new];
            [_tableView reloadData];
        } else {
            _datasMarray = [[NSMutableArray alloc] initWithArray:array];
            [_tableView reloadData];
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1 + _datasMarray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DBXSportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifier forIndexPath:indexPath];
    
    if (indexPath.row == 0) {
        cell.timeLbl.text = @"时间";
        cell.stepLbl.text = @"步数";
        cell.calorieLbl.text = @"消耗卡路里";
        
        return cell;
    } else {
        
        if (!kArrayIsEmpty(_datasMarray)) {
            ZJStepNumberModelData *modelData = _datasMarray[indexPath.row-1];
            cell.timeLbl.text = modelData.upsavetime;
            cell.stepLbl.text = modelData.steps;
            NSLog(@"%ld", [modelData.steps integerValue]);
            NSInteger calorie = [modelData.steps integerValue]*_height*_weight*1.036/300000;    // 计算卡路里 步数*身高/300000*体重*1.036 身高默认170 体重默认0
            cell.calorieLbl.text = [NSString stringWithFormat:@"%ld", calorie];
        }
        return cell;
    }
         
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
