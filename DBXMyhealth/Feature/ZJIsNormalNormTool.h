//
//  ZJIsNormalNormTool.h
//  Myhealth
//
//  Created by zhoujian on 2018/11/26.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZJIsNormalNormTool : NSObject


/**
 判断血压是否正常

 @param sbp 高压
 @param dbp 低压
 @return 是/否正常
 */
+ (BOOL)zj_isNormalBp:(double)sbp dbp:(double)dbp;

/**
 判断高压是否正常
 
 @param sbp 高压
 @return 是/否正常
 */
+ (BOOL)zj_isNormalSbp:(double)sbp;

/**
 判断低压是否正常

 @param dbp 低压
 @return 是/否正常
 */
+ (BOOL)zj_isNormalDbp:(double)dbp;

/**
 判断血糖是否正常

 @param condition 测量时间段
 @param bs 血糖
 @return 是/否正常
 */
+ (BOOL)zj_isNormalBs:(NSInteger)condition bs:(double)bs;

/**
 判断血氧是否正常
 
 @param bo 血氧
 @return 是/否正常
 */
+ (BOOL)zj_isNormalBo:(double)bo;

/**
 判断心率是否正常
 
 @param hb 心率
 @return 是/否正常
 */
+ (BOOL)zj_isNormalHb:(double)hb;
@end

NS_ASSUME_NONNULL_END
