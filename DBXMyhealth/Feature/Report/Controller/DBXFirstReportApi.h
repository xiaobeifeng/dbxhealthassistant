//
//  DBXFirstReportApi.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/14.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "BaseRequestApi.h"

/**
 获取最新一条数据
 */
@interface DBXFirstReportApi : BaseRequestApi

- (id)initWithCount:(NSInteger)count flag:(NSInteger)flag doa:(NSInteger)doa;

@end
