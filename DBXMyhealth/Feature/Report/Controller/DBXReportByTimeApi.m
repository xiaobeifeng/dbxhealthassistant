//
//  DBXReportByTimeApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/17.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXReportByTimeApi.h"

@interface DBXReportByTimeApi()
@property(nonatomic, copy) NSString *time;
@property(nonatomic, assign) NSInteger flag;
@property(nonatomic, assign) NSInteger order;
@end

@implementation DBXReportByTimeApi
- (id)initWithFlag:(NSInteger)flag
             order:(NSInteger)order
              time:(NSString *)time
{
    self = [super init];
    if (self) {
        _flag = flag;
        _time = time;
        _order = order;
    }
    return self;
}

- (NSString *)requestUrl {
    
    NSString *url = [[NSString stringWithFormat:@"/AppService/lohas/health/report/order/%ld/%ld/%@", (long)_flag, (long)_order,_time] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    return url;
}


- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}
@end
