//
//  DBXReportBpBxHbValueApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/17.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXReportBpBxHbValueApi.h"

@interface DBXReportBpBxHbValueApi()
@property(nonatomic, copy) NSString *startTime;
@property(nonatomic, copy) NSString *endTime;
@end

@implementation DBXReportBpBxHbValueApi
- (id)initWithStratTime:(NSString *)startTime
                endTime:(NSString *)endTime
{
    self = [super init];
    if (self) {
        _startTime = startTime;
        _endTime = endTime;
    }
    return self;
}

- (NSString *)requestUrl {
    
    NSString *url = [[NSString stringWithFormat:@"/AppService/lohas/health/bpbytime/%@/%@", _startTime, _endTime] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    return url;
}


- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}
@end
