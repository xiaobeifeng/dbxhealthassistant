//
//  DBXDataAnalyzeViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/12.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXDataAnalyzeViewController.h"
#import "DBXReportUserInfoView.h"
#import "XMGTitleButton.h"
#import "SCChart.h"
#import "DBXMePersonalinfoApi.h"
#import "DBXMePersonalinfoModel.h"
#import "DBXFirstReportApi.h"
#import "DBXReportModel.h"
#import "DBXReportBpBxHbValueApi.h"
#import "DBXReportBpBxHbValueModel.h"
#import "DBXReportByTimeApi.h"
#import "DBXReportHistoryTableViewCell.h"


static CGFloat kReportUserInfoViewHeight = 180;
static CGFloat kChartTitleViewHeight = 40;
static CGFloat kChartHeight = 100;
static CGFloat kScrollTitlesViewHeight = 25;
static NSString *kTableViewCellIdentifier = @"DBXReportHistoryTableViewCell";

@interface DBXDataAnalyzeViewController ()<UIScrollViewDelegate, SCChartDataSource, UITableViewDelegate, UITableViewDataSource, DBXReportUserInfoViewDelegate>

@property(nonatomic, strong) DBXReportUserInfoView *reportUserInfoView;

/** 用来存放所有子控制器view的scrollView */
@property (nonatomic, weak) UIScrollView *scrollView;
/** 标题栏 */
@property (nonatomic, weak) UIView *titlesView;
/** 标题下划线 */
@property (nonatomic, weak) UIView *titleUnderline;
/** 上一次点击的标题按钮 */
@property (nonatomic, weak) XMGTitleButton *previousClickedTitleButton;

@property(nonatomic, strong) SCChart *bloodPressChartView;
@property(nonatomic, strong) SCChart *bloodOxygenChartView;
@property(nonatomic, strong) SCChart *heartBeatChartView;

@property(nonatomic, strong) UITableView *tableView;


/** 开始时间 */
@property(nonatomic, copy) NSString *startTime;
/** 结束时间 */
@property(nonatomic, copy) NSString *endTime;

/** 血压最大值 */
@property(nonatomic, copy) NSString *maxBloodPress;
/** 血压平均值 */
@property(nonatomic, copy) NSString *averageBloodPress;
/** 血压最小值 */
@property(nonatomic, copy) NSString *minBloodPress;
/** 血氧最大值 */
@property(nonatomic, copy) NSString *maxBloodOxygen;
/** 血氧平均值 */
@property(nonatomic, copy) NSString *averageBloodOxygen;
/** 血氧最小值 */
@property(nonatomic, copy) NSString *minBloodOxygen;
/** 心率最大值 */
@property(nonatomic, copy) NSString *maxHeartBeat;
/** 心率平均值 */
@property(nonatomic, copy) NSString *averageHeartBeat;
/** 心率最小值 */
@property(nonatomic, copy) NSString *minHeartBeat;
/** 各项数值数组 */
@property (nonatomic, strong) NSMutableArray *dataMarray;
/** 高压数组 */
@property (nonatomic, strong) NSMutableArray *spMarray;
/** 低压数组 */
@property (nonatomic, strong) NSMutableArray *dpMarray;
/** 心率数组 */
@property (nonatomic, strong) NSMutableArray *heartBeatMarray;
/** 血氧数组 */
@property (nonatomic, strong) NSMutableArray *bloodOxygenMarray;
@end

@implementation DBXDataAnalyzeViewController

#pragma mark - 懒加载
- (NSMutableArray *)dataMarray
{
    if (!_dataMarray) {
        _dataMarray = [NSMutableArray new];
    }
    return _dataMarray;
}

- (NSMutableArray *)heartBeatMarray
{
    if (!_heartBeatMarray) {
        _heartBeatMarray = [NSMutableArray new];
    }
    return _heartBeatMarray;
}

- (NSMutableArray *)spMarray
{
    if (!_spMarray) {
        _spMarray = [NSMutableArray new];
    }
    return _spMarray;
}

- (NSMutableArray *)dpMarray
{
    if (!_dpMarray) {
        _dpMarray = [NSMutableArray new];
    }
    return _dpMarray;
}

- (NSMutableArray *)bloodOxygenMarray
{
    if (!_bloodOxygenMarray) {
        _bloodOxygenMarray = [NSMutableArray new];
    }
    return _bloodOxygenMarray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    _reportUserInfoView = [[DBXReportUserInfoView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kReportUserInfoViewHeight)];
    _reportUserInfoView.backgroundColor = kNavigationBarColor;
    _reportUserInfoView.delegate = self;
    [self.view addSubview:_reportUserInfoView];
    
    UILabel *chartTitleLbl = [UILabel new];
    chartTitleLbl.frame = CGRectMake(0, _reportUserInfoView.height, kScreenWidth, kChartTitleViewHeight);
    chartTitleLbl.text = @"生理参数分析";
    chartTitleLbl.font = kFont(16);
    chartTitleLbl.textAlignment = NSTextAlignmentCenter;
    chartTitleLbl.backgroundColor = kTableSeparatorLineColor;
    [self.view addSubview:chartTitleLbl];
    
    // scrollView
    [self setupScrollView];
    
    // 标题栏
    [self setupTitlesView];
    
    CGFloat tableTitleLbl_y = kReportUserInfoViewHeight+kChartTitleViewHeight+kScrollTitlesViewHeight+kChartHeight;
    UILabel *tableTitleLbl = [UILabel new];
    tableTitleLbl.frame = CGRectMake(0, tableTitleLbl_y, kScreenWidth, kChartTitleViewHeight);
    tableTitleLbl.text = @"历史数据";
    tableTitleLbl.font = kFont(16);
    tableTitleLbl.textAlignment = NSTextAlignmentCenter;
    tableTitleLbl.backgroundColor = kTableSeparatorLineColor;
    [self.view addSubview:tableTitleLbl];
    
    CGFloat tableView_y = kReportUserInfoViewHeight+kChartTitleViewHeight*2+kScrollTitlesViewHeight+kChartHeight;
    CGFloat tableViewHeight = kScreenHeight - tableView_y - kSafeAreaTopHeight - 60;
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, tableView_y, kScreenWidth, tableViewHeight) style:UITableViewStylePlain];
    [_tableView registerClass:[DBXReportHistoryTableViewCell class] forCellReuseIdentifier:kTableViewCellIdentifier];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.tableFooterView = [UIView new];
    _tableView.bounces = NO;
    [self.view addSubview:_tableView];
    
    
    // 加载个人信息
    [self startRequestPersonInfo];
    
    //
    [self startRequestFirstReportApi];
    
}

/**
 根据出生日期返回年龄
 */
- (NSString *)dateToOld:(NSString *)bornDateString
{
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *bornDate =[dateFormat dateFromString:bornDateString];
    
    //获得当前系统时间
    NSDate *currentDate = [NSDate date];
    //获得当前系统时间与出生日期之间的时间间隔
    NSTimeInterval time = [currentDate timeIntervalSinceDate:bornDate];
    //时间间隔以秒作为单位,求年的话除以60*60*24*356
    int age = ((int)time)/(3600*24*365);
    return [NSString stringWithFormat:@"%d",age];
}

/**
 获取个人信息
 */
- (void)startRequestPersonInfo
{
    DBXMePersonalinfoApi *api = [[DBXMePersonalinfoApi alloc] init];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        DBXMePersonalinfoModel *model = [DBXMePersonalinfoModel yy_modelWithJSON:request.responseJSONObject];
        NSArray *array = [NSArray yy_modelArrayWithClass:[DBXMePersonalinfoModelData class] json:model.data];
        
        DBXMePersonalinfoModelData *modelData;
        if (!kArrayIsEmpty(array)) {
            modelData = array.firstObject;
    
            if (kStringIsEmpty(modelData.sex) || kStringIsEmpty(modelData.birth)) {
                
                UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"请完善个人信息后再试" message:@"" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    [self.navigationController popViewControllerAnimated:YES];
                    
                }];
                
                [alertVC addAction:okAction];
                
                [self presentViewController:alertVC animated:YES completion:nil];
                
            } else {
                self.reportUserInfoView.name = [[NSUserDefaults standardUserDefaults] objectForKey:kAccount];
                self.reportUserInfoView.sex = modelData.sex;
                self.reportUserInfoView.age = [self dateToOld:modelData.birth];
            }
            
        } else {
            
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"请完善个人信息后再试" message:@"" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [self.navigationController popViewControllerAnimated:YES];
                
            }];
            
            [alertVC addAction:okAction];
            
            [self presentViewController:alertVC animated:YES completion:nil];
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

/**
 请求第一条报告数据
 */
- (void)startRequestFirstReportApi
{
    DBXFirstReportApi *api = [[DBXFirstReportApi alloc] initWithCount:1 flag:_flag doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        if (request.responseJSONObject) {
            
            DBXReportModel *model = [DBXReportModel yy_modelWithJSON:request.responseObject];
            
            NSArray *array = [NSArray yy_modelArrayWithClass:[DBXReportModelData class] json:model.data];
            
            if (!kArrayIsEmpty(array)) {
                
                DBXReportModelData *modelData = array.firstObject;
    
                self.maxBloodPress = [NSString stringWithFormat:@"血压：%@/%@ mmHg", modelData.maxsP, modelData.maxdP];
                self.maxBloodOxygen = [NSString stringWithFormat:@"血氧：%@ %%", modelData.maxox];
                self.maxHeartBeat = [NSString stringWithFormat:@"心率：%@ 次/分", modelData.maxpu];
                
                self.averageBloodPress = [NSString stringWithFormat:@"血压：%@/%@ mmHg", modelData.sPavg, modelData.dPavg];
                self.averageBloodOxygen = [NSString stringWithFormat:@"血氧：%@ %%", modelData.oxavg];
                self.averageHeartBeat = [NSString stringWithFormat:@"心率：%@ 次/分", modelData.puavg];
                
                self.minBloodPress = [NSString stringWithFormat:@"血压：%@/%@ mmHg", modelData.minsP, modelData.mindP];
                self.minBloodOxygen = [NSString stringWithFormat:@"血氧：%@ %%", modelData.minox];
                self.minHeartBeat = [NSString stringWithFormat:@"心率：%@ 次/分", modelData.minpu];
                
                
                self.reportUserInfoView.bloodPressLbl.text =  self.maxBloodPress;
                self.reportUserInfoView.bloodOxygenLbl.text =  self.maxBloodOxygen;
                self.reportUserInfoView.heartBeatLbl.text =  self.maxHeartBeat;
                
                NSLog(@"%@ %@", modelData.starttime, modelData.endtime);
                
                
                // 刷新父视图标题
                if (self.delegate && [self.delegate respondsToSelector:@selector(reloadReportDetailVCNavTitleWithStartTime:endTime:)]) {
                    
                    
                    if (!kStringIsEmpty(modelData.starttime)) {
                        
                        self.startTime = modelData.starttime;
                        self.endTime = modelData.endtime;
                        
                        [self startRequestReportBpBxHbValueApi];
                    }
                    
                    [self.delegate reloadReportDetailVCNavTitleWithStartTime:self.startTime endTime:self.endTime];
                   
                }
                
            } else {
                
                [MBProgressHUD showInfoMessage:@"暂无报告"];
                [self.navigationController popViewControllerAnimated:YES];
            }
            
        }
        
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

- (void)startRequestReportBpBxHbValueApi
{
    DBXReportBpBxHbValueApi *api = [[DBXReportBpBxHbValueApi alloc] initWithStratTime:_startTime endTime:_endTime];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            if (!kObjectIsEmpty(request.responseJSONObject)) {
                DBXReportBpBxHbValueModel *model = [DBXReportBpBxHbValueModel yy_modelWithJSON:request.responseJSONObject];
                if (!kArrayIsEmpty(self.dataMarray)) {
                    [self.dataMarray removeAllObjects];
                }
                NSArray *array = [NSArray yy_modelArrayWithClass:[DBXReportBpBxHbValueModelData class] json:model.data];
                
                [self.dataMarray addObjectsFromArray:array];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self reloadChatView];
                    [self.tableView reloadData];
                });
                
                
            }
            
        });
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

/**
 刷新表格视图
 */
- (void)reloadChatView
{
    if (!kArrayIsEmpty(self.spMarray)) {
        [self.spMarray removeAllObjects];
        [self.dpMarray removeAllObjects];
    }
    for (int i=0; i<self.dataMarray.count; i++) {
        DBXReportBpBxHbValueModelData *modelData = self.dataMarray[i];
        [self.spMarray addObject:modelData.sPressure];
        [self.dpMarray addObject:modelData.dPressure];
    }
    [_bloodPressChartView strokeChart];
    
    if (!kArrayIsEmpty(self.heartBeatMarray)) {
        [self.heartBeatMarray removeAllObjects];
    }
    for (int i=0; i<self.dataMarray.count; i++) {
        DBXReportBpBxHbValueModelData *modelData = self.dataMarray[i];
        [self.heartBeatMarray addObject:modelData.pulse];
    }
    [_heartBeatChartView strokeChart];
    
    if (!kArrayIsEmpty(self.bloodOxygenMarray)) {
        [self.bloodOxygenMarray removeAllObjects];
    }
    for (int i=0; i<self.dataMarray.count; i++) {
        DBXReportBpBxHbValueModelData *modelData = self.dataMarray[i];
        [self.bloodOxygenMarray addObject:modelData.oxygen];
    }
    [_bloodOxygenChartView strokeChart];
    
}

- (void)requestReportDataWithFlag:(NSInteger)flag order:(NSInteger)order isNextData:(BOOL)isNextData date:(NSString *)date
{
    NSLog(@"数据分析界面刷新");
    NSString *time;
    if (kStringIsEmpty(date)) {
        if (isNextData) {
            time = _endTime;
        } else {
            time = _startTime;
        }
    } else {
        time = date;
    }
    
    DBXReportByTimeApi *api = [[DBXReportByTimeApi alloc] initWithFlag:flag order:order time:time];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            if (request.responseJSONObject) {
                
                DBXReportModel *model = [DBXReportModel yy_modelWithJSON:request.responseObject];
                
                NSArray *array = [NSArray yy_modelArrayWithClass:[DBXReportModelData class] json:model.data];
                
                if (!kArrayIsEmpty(array)) {
                    
                    DBXReportModelData *modelData = array.firstObject;
                    
                    
                    self.maxBloodPress = [NSString stringWithFormat:@"血压：%@/%@ mmHg", modelData.maxsP, modelData.maxdP];
                    self.maxBloodOxygen = [NSString stringWithFormat:@"血氧：%@ %%", modelData.maxox];
                    self.maxHeartBeat = [NSString stringWithFormat:@"心率：%@ 次/分", modelData.maxpu];
                    
                    self.averageBloodPress = [NSString stringWithFormat:@"血压：%@/%@ mmHg", modelData.sPavg, modelData.dPavg];
                    self.averageBloodOxygen = [NSString stringWithFormat:@"血氧：%@ %%", modelData.oxavg];
                    self.averageHeartBeat = [NSString stringWithFormat:@"心率：%@ 次/分", modelData.puavg];
                    
                    self.minBloodPress = [NSString stringWithFormat:@"血压：%@/%@ mmHg", modelData.minsP, modelData.minsP];
                    self.minBloodOxygen = [NSString stringWithFormat:@"血氧：%@ %%", modelData.minox];
                    self.minHeartBeat = [NSString stringWithFormat:@"心率：%@ 次/分", modelData.minpu];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        self.reportUserInfoView.bloodPressLbl.text =  self.maxBloodPress;
                        self.reportUserInfoView.bloodOxygenLbl.text =  self.maxBloodOxygen;
                        self.reportUserInfoView.heartBeatLbl.text =  self.maxHeartBeat;
                        
                        // 刷新父视图标题
                        if (self.delegate && [self.delegate respondsToSelector:@selector(reloadReportDetailVCNavTitleWithStartTime:endTime:)]) {
                            
                            
                            if (!kStringIsEmpty(modelData.starttime)) {
                                
                                self.startTime = [modelData.starttime substringWithRange:NSMakeRange(0, 10)];
                                self.endTime = [modelData.endtime substringWithRange:NSMakeRange(0, 10)];
                                
                                [self startRequestReportBpBxHbValueApi];
                            }
                            
                            [self.delegate reloadReportDetailVCNavTitleWithStartTime:self.startTime endTime:self.endTime];
                            
                        }
                    });
                    
                } else {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD showInfoMessage:@"没有更多啦～"];
                    });
                    
                }
            }
            
        });
        
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

#pragma mark - reportUserInfoView delegate
- (void)handleAverageBtnEvent
{
    self.reportUserInfoView.bloodPressLbl.text =  self.averageBloodPress;
    self.reportUserInfoView.bloodOxygenLbl.text =  self.averageBloodOxygen;
    self.reportUserInfoView.heartBeatLbl.text =  self.averageHeartBeat;
}

- (void)handleMaxBtnEvent
{
    self.reportUserInfoView.bloodPressLbl.text =  self.maxBloodPress;
    self.reportUserInfoView.bloodOxygenLbl.text =  self.maxBloodOxygen;
    self.reportUserInfoView.heartBeatLbl.text =  self.maxHeartBeat;
}

- (void)handleMinBtnEvent
{
    self.reportUserInfoView.bloodPressLbl.text =  self.minBloodPress;
    self.reportUserInfoView.bloodOxygenLbl.text =  self.minBloodOxygen;
    self.reportUserInfoView.heartBeatLbl.text =  self.minHeartBeat;
}

/**
 *  标题栏
 */
- (void)setupTitlesView
{
    UIView *titlesView = [[UIView alloc] init];
    titlesView.backgroundColor = [UIColor whiteColor];
    titlesView.frame = CGRectMake(0, kReportUserInfoViewHeight+kChartTitleViewHeight, self.view.xmg_width, kScrollTitlesViewHeight);
    [self.view addSubview:titlesView];
    self.titlesView = titlesView;
    
    // 标题栏按钮
    [self setupTitleButtons];
    
    // 标题下划线
    [self setupTitleUnderline];
}

/**
 *  scrollView
 */
- (void)setupScrollView
{
    
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.backgroundColor = [UIColor whiteColor];
    scrollView.frame = CGRectMake(0, kReportUserInfoViewHeight+kChartTitleViewHeight+kScrollTitlesViewHeight, kScreenWidth, kChartHeight);
    scrollView.delegate = self;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.pagingEnabled = YES;
    [self.view addSubview:scrollView];
    self.scrollView = scrollView;
    
    // 添加子控制器的view
    NSUInteger count = self.childViewControllers.count;
    CGFloat scrollViewW = scrollView.xmg_width;
    CGFloat scrollViewH = scrollView.xmg_height;
    
    _bloodPressChartView = [[SCChart alloc] initwithSCChartDataFrame:CGRectMake(10, 0, scrollViewW-20, scrollViewH)
                                                withSource:self
                                                 withStyle:SCChartLineStyle];
    [_bloodPressChartView strokeChart];
    
    _bloodOxygenChartView = [[SCChart alloc] initwithSCChartDataFrame:CGRectMake(scrollViewW+10, 0, scrollViewW-20, scrollViewH)
                                                          withSource:self
                                                           withStyle:SCChartLineStyle];
    [_bloodOxygenChartView strokeChart];
    
    _heartBeatChartView = [[SCChart alloc] initwithSCChartDataFrame:CGRectMake(scrollViewW*2+10, 0, scrollViewW-20, scrollViewH)
                                                           withSource:self
                                                            withStyle:SCChartLineStyle];
    [_heartBeatChartView strokeChart];
  
    [scrollView addSubview:_bloodOxygenChartView];
    [scrollView addSubview:_bloodPressChartView];
    [scrollView addSubview:_heartBeatChartView];
    
    scrollView.contentSize = CGSizeMake(count * scrollViewW, 0);
    
}

#pragma mark - SCChartDataSource
// 横坐标标题数组
- (NSArray *)SCChart_xLableArray:(SCChart *)chart
{
    if (chart == _bloodPressChartView) {
        
        NSMutableArray *xMarray = [NSMutableArray new];
        for (int i = 0; i < self.spMarray.count; i++) {
            
            NSString *x = [NSString stringWithFormat:@"%d", i];
            [xMarray addObject:x];
        }
        
        return xMarray;
        
    }
    
    if (chart == _heartBeatChartView) {
        
        NSMutableArray *xMarray = [NSMutableArray new];
        for (int i = 0; i < self.heartBeatMarray.count; i++) {
            
            NSString *x = [NSString stringWithFormat:@"%d", i];
            [xMarray addObject:x];
        }
        
        return xMarray;
        
    }
    
    if (chart == _bloodOxygenChartView) {
        
        NSMutableArray *xMarray = [NSMutableArray new];
        for (int i = 0; i < self.bloodOxygenMarray.count; i++) {
            
            NSString *x = [NSString stringWithFormat:@"%d", i];
            [xMarray addObject:x];
        }
        
        return xMarray;
        
    }
    
    return @[@"1", @"2", @"3", @"4"];
}

//数值多重数组
- (NSArray *)SCChart_yValueArray:(SCChart *)chart
{
    if (chart == _bloodPressChartView) {
        return @[self.spMarray, self.dpMarray];
    }
    
    if (chart == _heartBeatChartView) {
        return @[self.heartBeatMarray];
    }
    
    if (chart == _bloodOxygenChartView) {
        return @[self.bloodOxygenMarray];
    }
    
    return @[@[@"0", @"0", @"0", @"0"]];
}

// 颜色数组
- (NSArray *)SCChart_ColorArray:(SCChart *)chart
{
    return @[SCBlue,SCRed];
}

// 判断显示横线条
- (BOOL)SCChart:(SCChart *)chart ShowHorizonLineAtIndex:(NSInteger)index {
    return YES;
}

//判断显示最大最小值
- (BOOL)SCChart:(SCChart *)chart ShowMaxMinAtIndex:(NSInteger)index
{
    return NO;
}

/**
 *  标题栏按钮
 */
- (void)setupTitleButtons
{
    // 文字
    NSArray *titles = @[@"血压", @"心率", @"血氧"];
    NSUInteger count = titles.count;
    
    // 标题按钮的尺寸
    CGFloat titleButtonW = self.titlesView.xmg_width / count;
    CGFloat titleButtonH = self.titlesView.xmg_height;
    
    // 创建5个标题按钮
    for (NSUInteger i = 0; i < count; i++) {
        XMGTitleButton *titleButton = [[XMGTitleButton alloc] init];
        titleButton.titleLabel.font = kFont(12);
        titleButton.tag = i;
        [titleButton addTarget:self action:@selector(titleButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.titlesView addSubview:titleButton];
        // frame
        titleButton.frame = CGRectMake(i * titleButtonW, 0, titleButtonW, titleButtonH);
        // 文字
        [titleButton setTitle:titles[i] forState:UIControlStateNormal];
    }
}

/**
 *  标题下划线
 */
- (void)setupTitleUnderline
{
    // 标题按钮
    XMGTitleButton *firstTitleButton = self.titlesView.subviews.firstObject;
    
    // 下划线
    UIView *titleUnderline = [[UIView alloc] init];
    titleUnderline.xmg_height = 2;
    titleUnderline.xmg_y = self.titlesView.xmg_height - titleUnderline.xmg_height;
    titleUnderline.backgroundColor = [firstTitleButton titleColorForState:UIControlStateSelected];
    [self.titlesView addSubview:titleUnderline];
    self.titleUnderline = titleUnderline;
    
    // 切换按钮状态
    firstTitleButton.selected = YES;
    self.previousClickedTitleButton = firstTitleButton;
    
    [firstTitleButton.titleLabel sizeToFit]; // 让label根据文字内容计算尺寸
    self.titleUnderline.xmg_width = firstTitleButton.titleLabel.xmg_width + 10;
    self.titleUnderline.xmg_centerX = firstTitleButton.xmg_centerX;
}

#pragma mark - 监听
/**
 *  点击标题按钮
 */
- (void)titleButtonClick:(XMGTitleButton *)titleButton
{
    // 切换按钮状态
    self.previousClickedTitleButton.selected = NO;
    titleButton.selected = YES;
    self.previousClickedTitleButton = titleButton;
    
    [UIView animateWithDuration:0.25 animations:^{
        // 处理下划线
        self.titleUnderline.xmg_width = titleButton.titleLabel.xmg_width + 10;
        self.titleUnderline.xmg_centerX = titleButton.xmg_centerX;
        
        CGFloat offsetX = self.scrollView.xmg_width * titleButton.tag;
        self.scrollView.contentOffset = CGPointMake(offsetX, self.scrollView.contentOffset.y);
    }];
    
    
}


#pragma mark - UITableViewDelegate and DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataMarray.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    DBXReportHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifier forIndexPath:indexPath];
                                           
    [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];

    // 设置cell颜色
    if (indexPath.row % 2 == 0) {
        cell.backgroundColor = [UIColor colorWithHexString:@"C4CBFC"];
    }else{
        cell.backgroundColor = [UIColor colorWithHexString:@"FFFFFF"];
    }
    
    return cell;
}

- (void)setupModelOfCell:(DBXReportHistoryTableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0) {
        cell.timeLbl.text = @"时间";
        cell.bloodPressLbl.text = @"血压";
        cell.bloodOxygenLbl.text = @"血氧";
        cell.heartBeatLbl.text = @"心率";
        cell.bloodPressLevelLbl.text = @"血压级别";
    } else {
        DBXReportBpBxHbValueModelData *model = self.dataMarray[indexPath.row-1];
        cell.timeLbl.text = kStringIsEmpty(model.upsaveTime)?@"-":[model.upsaveTime substringWithRange:NSMakeRange(5, 11)];
        
        if (kStringIsEmpty(model.sPressure)&&kStringIsEmpty(model.dPressure)) {
            cell.bloodPressLbl.text = @"-";
        } else {
           cell.bloodPressLbl.text = [NSString stringWithFormat:@"%@/%@", model.sPressure, model.dPressure];
        }
        
        
        cell.bloodOxygenLbl.text = kStringIsEmpty(model.oxygen)?@"-":model.oxygen;
        cell.heartBeatLbl.text = kStringIsEmpty(model.pulse)?@"-":model.pulse;
        
        if (kStringIsEmpty(model.sPressure)&&kStringIsEmpty(model.dPressure)) {
            cell.bloodPressLevelLbl.text = @"-";
        } else {
            NSInteger sPressure = [model.sPressure integerValue];
            NSInteger dPressure = [model.dPressure integerValue];
            if (sPressure < 120 && dPressure < 80) {
                cell.bloodPressLevelLbl.text = @"正常血压";
            }else if((sPressure >= 120 && sPressure < 140) && (dPressure >= 80 && dPressure < 90)){
                cell.bloodPressLevelLbl.text = @"正常高值";
            }else if(sPressure >= 140 && dPressure < 90){
                cell.bloodPressLevelLbl.text = @"单纯收缩期高血压";
            }else if((sPressure >= 140 && sPressure < 160) || (dPressure >= 90 && dPressure < 100)){
                cell.bloodPressLevelLbl.text = @"1级高血压(轻度)";
            }else if((sPressure >= 160 && sPressure < 180) || (dPressure >= 100 && dPressure < 110)){
                cell.bloodPressLevelLbl.text = @"2级高血压(中度)";
            }else if(sPressure >= 180 || dPressure >= 110){
                cell.bloodPressLevelLbl.text = @"3级高血压(重度)";
            }else{
                cell.bloodPressLevelLbl.text = @"-";
            }
        }
        
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return [tableView fd_heightForCellWithIdentifier:kTableViewCellIdentifier cacheByIndexPath:indexPath configuration:^(id cell) {
        
        [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
