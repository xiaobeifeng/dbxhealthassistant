//
//  DBXReportDetailViewController.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/12.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBXReportDetailViewController : UIViewController

/** 报告周期 */
@property(nonatomic, assign) NSInteger flag;


@end
