//
//  DBXDataReportViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/12.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXDataReportViewController.h"
#import "DBXReportButton.h"
#import "DBXDataReportTableViewCell.h"
#import "DBXFirstReportApi.h"
#import "DBXReportModel.h"
#import "DBXReportByTimeApi.h"

static NSString *tableViewCellIdentifier = @"DBXDataReportTableViewCell";
static NSString *kFooterTitleInfo = @"以上评估报告和指导意见仅供参考，不作为诊断标准和治疗依据。如有相关疾病，请及时就医，谨遵医嘱！";

@interface DBXDataReportViewController ()<UITableViewDelegate, UITableViewDataSource>

@property(nonatomic, strong) DBXReportButton *evaluateBtn;
@property(nonatomic, strong) DBXReportButton *opinionBtn;
@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) NSMutableArray *dataMarray;
/** 开始时间 */
@property(nonatomic, copy) NSString *startTime;
/** 结束时间 */
@property(nonatomic, copy) NSString *endTime;

@end

@implementation DBXDataReportViewController

#pragma mark - 懒加载
- (DBXReportButton *)evaluateBtn
{
    if (!_evaluateBtn) {
        _evaluateBtn = [DBXReportButton buttonWithType:UIButtonTypeCustom];
        _evaluateBtn.path = [self getPath:1];
        [_evaluateBtn setTitle:@"综合评价" forState:UIControlStateNormal];
        [_evaluateBtn addTarget:self action:@selector(evaluateBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        _evaluateBtn.backgroundColor = kNavigationBarColor;
        _evaluateBtn.titleLabel.font = kFont(14);
        [self.view addSubview:_evaluateBtn];
    }
    return _evaluateBtn;
}


- (DBXReportButton *)opinionBtn
{
    if (!_opinionBtn) {
        _opinionBtn = [DBXReportButton buttonWithType:UIButtonTypeCustom];
        _opinionBtn.path = [self getPath:2];
        [_opinionBtn setTitle:@"指导意见" forState:UIControlStateNormal];
        [_opinionBtn addTarget:self action:@selector(opinionBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        _opinionBtn.backgroundColor = kNavigationBarColor;
        _opinionBtn.titleLabel.font = kFont(14);
        [self.view addSubview:_opinionBtn];
    }
    return _opinionBtn;
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 100, kScreenWidth, kScreenHeight-kSafeAreaTopHeight-60-100) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.sectionHeaderHeight = CGFLOAT_MIN;
        _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
//        _tableView.bounces = NO;
        [_tableView registerClass:[DBXDataReportTableViewCell class] forCellReuseIdentifier:tableViewCellIdentifier];
        _tableView.tableFooterView = [UIView new];
        
    }
    return _tableView;
}

- (NSMutableArray *)dataMarray
{
    if (!_dataMarray) {
        _dataMarray = [NSMutableArray new];
    }
    return _dataMarray;
}

- (void)evaluateBtnAction:(UIButton *)button
{
    
}

- (void)opinionBtnAction:(UIButton *)button
{
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.view.backgroundColor = [UIColor whiteColor];

    [self.evaluateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(@150);
        make.height.mas_equalTo(@50);
        make.centerX.mas_equalTo(self.view.mas_centerX).with.offset(-70);
        make.top.mas_equalTo(20);
    }];
    
    [self.opinionBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(@150);
        make.height.mas_equalTo(@50);
        make.centerX.mas_equalTo(self.view.mas_centerX).with.offset(70);
        make.top.mas_equalTo(20);
    }];
    
    [self.view addSubview:self.tableView];
    
    [self startRequestFirstReportApi];

}

- (void)requestReportDataWithFlag:(NSInteger)flag order:(NSInteger)order isNextData:(BOOL)isNextData date:(NSString *)date
{
    NSString *time;
    if (kStringIsEmpty(date)) {
        if (isNextData) {
            time = _endTime;
        } else {
            time = _startTime;
        }
    } else {
        time = date;
    }
    
    DBXReportByTimeApi *api = [[DBXReportByTimeApi alloc] initWithFlag:_flag order:order time:time];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            if (request.responseJSONObject) {
                
                DBXReportModel *model = [DBXReportModel yy_modelWithJSON:request.responseObject];
                
                NSArray *array = [NSArray yy_modelArrayWithClass:[DBXReportModelData class] json:model.data];
                
                if (!kArrayIsEmpty(array)) {
                    
                    DBXReportModelData *modelData = array.firstObject;
                    
                    NSString *evaluate = modelData.strpinggu;
                    NSString *opinion = modelData.strsuggest;
                    
                    if (kStringIsEmpty(evaluate)) {
                        evaluate = @"暂无综合评价";
                    }
                    
                    if (kStringIsEmpty(opinion)) {
                        opinion = @"暂无指导意见";
                    }
                    
                    if (!kArrayIsEmpty(self.dataMarray)) {
                        [self.dataMarray removeAllObjects];
                    }
                    
                    [self.dataMarray addObject:evaluate];
                    [self.dataMarray addObject:opinion];
                    
                    NSLog(@"%@", self.dataMarray);
                    
                    if (!kStringIsEmpty(modelData.starttime)) {
                        
                        self.startTime = [modelData.starttime substringWithRange:NSMakeRange(0, 10)];
                        self.endTime = [modelData.endtime substringWithRange:NSMakeRange(0, 10)];
                        
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                    });
                    
                }
            }
            
        });
        
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}
- (void)reloadViewData
{
    NSLog(@"数据报告界面刷新");
    
}

/**
 请求第一条报告数据
 */
- (void)startRequestFirstReportApi
{
    DBXFirstReportApi *api = [[DBXFirstReportApi alloc] initWithCount:1 flag:_flag doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        if (request.responseJSONObject) {
            
            DBXReportModel *model = [DBXReportModel yy_modelWithJSON:request.responseObject];
            
            NSArray *array = [NSArray yy_modelArrayWithClass:[DBXReportModelData class] json:model.data];
            
            if (!kArrayIsEmpty(array)) {
                
                DBXReportModelData *modelData = array.firstObject;
                
                NSString *evaluate = modelData.strpinggu;
                NSString *opinion = modelData.strsuggest;
                
                if (kStringIsEmpty(evaluate)) {
                    evaluate = @"暂无综合评价";
                }
                
                if (kStringIsEmpty(opinion)) {
                    opinion = @"暂无指导意见";
                }
                    
                [self.dataMarray addObject:evaluate];
                [self.dataMarray addObject:opinion];
                
                if (!kStringIsEmpty(modelData.starttime)) {
                    
                    self.startTime = [modelData.starttime substringWithRange:NSMakeRange(0, 10)];
                    self.endTime = [modelData.endtime substringWithRange:NSMakeRange(0, 10)];
                    
                }
                
                NSLog(@"%@", self.dataMarray);
                
                [self.tableView reloadData];
                
            }
            
        }
        
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}


// 获取遮罩path
- (UIBezierPath *)getPath:(NSInteger)num
{
    switch (num) {
        case 1:
        {
            UIBezierPath *path = [UIBezierPath bezierPath];
            
            [path moveToPoint:CGPointMake(0, 0)];
            [path addLineToPoint:CGPointMake(0, 50)];
            [path addLineToPoint:CGPointMake(130, 50)];
            [path addLineToPoint:CGPointMake(150, 0)];
            [path addLineToPoint:CGPointMake(0, 0)];
            return path;
            
        }
        break;
            
        case 2:
        {
            UIBezierPath *path = [UIBezierPath bezierPath];
            
            [path moveToPoint:CGPointMake(0, 50)];
            [path addLineToPoint:CGPointMake(150, 50)];
            [path addLineToPoint:CGPointMake(150, 0)];
            [path addLineToPoint:CGPointMake(20, 0)];
            [path addLineToPoint:CGPointMake(0, 50)];
            return path;
          
        }
        break;

        default:
            return nil;
        
            break;
    }
}

#pragma mark - UITableViewDelegate and DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataMarray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DBXDataReportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tableViewCellIdentifier forIndexPath:indexPath];
    
    [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)setupModelOfCell:(DBXDataReportTableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    cell.content = self.dataMarray[indexPath.row];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return [tableView fd_heightForCellWithIdentifier:tableViewCellIdentifier cacheByIndexPath:indexPath configuration:^(id cell) {
            
            [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
        }];
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"以上评估报告和指导意见仅供参考，不作为诊断标准和治疗依据。如有相关疾病，请及时就医，谨遵医嘱！";
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    NSString *sectionTitle = [self tableView:tableView titleForFooterInSection:section];
    if (sectionTitle == nil) {
        return nil;
    }
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(13, 0, kScreenWidth-26, 50);
    label.numberOfLines = 0;
    label.text = kFooterTitleInfo;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor lightGrayColor];
    label.font = kFont(12);
    label.text = sectionTitle;
    UIView *view = [[UIView alloc] init];
    [view addSubview:label];
    
    return view;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
