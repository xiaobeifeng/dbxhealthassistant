//
//  DBXReportUserInfoView.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/12.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DBXReportUserInfoViewDelegate <NSObject>

- (void)handleMaxBtnEvent;
- (void)handleAverageBtnEvent;
- (void)handleMinBtnEvent;

@end


/**
 报告所属人信息
 */
@interface DBXReportUserInfoView : UIView

@property(nonatomic, copy) NSString *name;
@property(nonatomic, copy) NSString *sex;
@property(nonatomic, copy) NSString *age;

@property(nonatomic, strong) UILabel *bloodPressLbl;
@property(nonatomic, strong) UILabel *bloodOxygenLbl;
@property(nonatomic, strong) UILabel *heartBeatLbl;
@property(nonatomic, weak) id<DBXReportUserInfoViewDelegate> delegate;

@end
