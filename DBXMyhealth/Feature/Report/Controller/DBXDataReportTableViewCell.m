//
//  DBXDataReportTableViewCell.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/13.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXDataReportTableViewCell.h"

@interface DBXDataReportTableViewCell()

@property(nonatomic, strong) UILabel *contentLbl;
@property(nonatomic, strong) UIView *view;
@end

@implementation DBXDataReportTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _contentLbl = [UILabel new];
        _contentLbl.backgroundColor = [UIColor whiteColor];
        _contentLbl.numberOfLines = 0;
        [_contentLbl sizeToFit];
        [self.contentView addSubview:_contentLbl];
        
        _view = [UIView new];
        _view.backgroundColor = [UIColor whiteColor];
        kViewBorderRadius(_view, 5.0f, 1, kNavigationBarColor);
        [self.contentView addSubview:_view];
        
        [self.contentView bringSubviewToFront:_contentLbl];
    }
    
    return self;
}

+ (BOOL)requiresConstraintBasedLayout
{
    return YES;
}

- (void)updateConstraints
{
    [_contentLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).with.offset(-13);
        make.bottom.mas_equalTo(self.contentView).with.offset(-13);
        make.top.mas_equalTo(self.contentView).with.offset(13);
        make.left.mas_equalTo(self.contentView).with.offset(13);
    }];
    
    [_view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).with.offset(-5);
        make.bottom.mas_equalTo(self.contentView).with.offset(-5);
        make.top.mas_equalTo(self.contentView).with.offset(5);
        make.left.mas_equalTo(self.contentView).with.offset(5);
    }];
    
    [super updateConstraints];
}

- (void)setContent:(NSString *)content
{
    _contentLbl.text = content;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
