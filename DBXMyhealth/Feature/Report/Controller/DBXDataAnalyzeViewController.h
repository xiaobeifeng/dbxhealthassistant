//
//  DBXDataAnalyzeViewController.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/12.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DBXDataAnalyzeViewControllerDelegate <NSObject>

- (void)reloadReportDetailVCNavTitleWithStartTime:(NSString *)startTime
                                          endTime:(NSString *)endTime;

@end

@interface DBXDataAnalyzeViewController : UIViewController

@property(nonatomic, weak) id<DBXDataAnalyzeViewControllerDelegate> delegate;

/**
 请求上一条/下一条报告

 @param flag 报告周期 1周，2月，3年
 @param order 0上一条 1下一条
 @param isNextData 是：请求下一条数据 否：请求上一条
 */
- (void)requestReportDataWithFlag:(NSInteger)flag
                            order:(NSInteger)order
                       isNextData:(BOOL)isNextData
                             date:(NSString *)date;
/** 报告周期 */
@property(nonatomic, assign) NSInteger flag;

@end
