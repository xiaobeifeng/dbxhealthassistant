//
//  DBXReportBpBxHbValueApi.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/17.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "BaseRequestApi.h"

/**
 获取指定时间的血压，血氧，心率数据
 */
@interface DBXReportBpBxHbValueApi : BaseRequestApi
- (id)initWithStratTime:(NSString *)startTime
                endTime:(NSString *)endTime;
@end
