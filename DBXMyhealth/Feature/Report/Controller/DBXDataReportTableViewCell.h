//
//  DBXDataReportTableViewCell.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/13.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBXDataReportTableViewCell : UITableViewCell

@property(nonatomic, copy) NSString *content;

@end
