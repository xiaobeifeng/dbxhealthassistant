//
//  DBXReportButton.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/13.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 报告模块数据报告不规则按钮
 */
@interface DBXReportButton : UIButton

@property(nonatomic, strong) UIBezierPath *path;

@end
