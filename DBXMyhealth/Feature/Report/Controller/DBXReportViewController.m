//
//  DBXReportViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/20.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXReportViewController.h"
#import "DBXReportDetailViewController.h"

@interface DBXReportViewController ()<UITableViewDelegate, UITableViewDataSource>

@property(nonatomic, strong) UITableView *tableView;

@end

@implementation DBXReportViewController

#pragma mark - 懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-self.tabBarController.tabBar.height) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |
        UIViewAutoresizingFlexibleWidth;
        // 解决滑动视图顶部空出状态栏高度的问题
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)]) {
                self.edgesForExtendedLayout = UIRectEdgeNone;
            }
        }
        
        _tableView.tableFooterView = [UIView new];
        
    }
    
    return _tableView;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
//    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.view.backgroundColor = kNavigationBarColor;
    
    self.navigationItem.title = @"报告";
    
    // 周报告
    UIButton *weekBtn = [UIButton new];
    [weekBtn setImage:[UIImage imageNamed:@"report_week"] forState:UIControlStateNormal];
    [weekBtn addTarget:self action:@selector(weekBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:weekBtn];
    UIImageView *weekImageView = [UIImageView new];
    weekImageView.image = [UIImage imageNamed:@"report_week_text"];
    [self.view addSubview:weekImageView];
    
    [weekBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(30);
        make.width.height.mas_equalTo(120);
        make.centerX.mas_equalTo(self.view.mas_centerX).with.offset(-100);
    }];
    [weekImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(weekBtn.mas_centerY);
        make.width.mas_equalTo(140);
        make.height.mas_equalTo(45);
        make.left.mas_equalTo(weekBtn.mas_right).with.offset(20);
    }];
    
    // 月报告
    UIButton *monthBtn = [UIButton new];
    [monthBtn setImage:[UIImage imageNamed:@"report_month"] forState:UIControlStateNormal];
    [monthBtn addTarget:self action:@selector(monthBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:monthBtn];
    UIImageView *monthImageView = [UIImageView new];
    monthImageView.image = [UIImage imageNamed:@"ireport_month_text"];
    [self.view addSubview:monthImageView];
    
    [monthBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(weekBtn.mas_bottom).with.offset(40);
        make.width.height.mas_equalTo(120);
        make.centerX.mas_equalTo(self.view.mas_centerX).with.offset(100);
    }];
    [monthImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(monthBtn.mas_centerY);
        make.width.mas_equalTo(140);
        make.height.mas_equalTo(45);
        make.right.mas_equalTo(monthBtn.mas_left).with.offset(-20);
    }];
    
    UIButton *yearBtn = [UIButton new];
    [yearBtn setImage:[UIImage imageNamed:@"report_year"] forState:UIControlStateNormal];
    [yearBtn addTarget:self action:@selector(yearBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:yearBtn];
    UIImageView *yearImageView = [UIImageView new];
    yearImageView.image = [UIImage imageNamed:@"report_year_text"];
    [self.view addSubview:yearImageView];
    
    [yearBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(monthBtn.mas_bottom).with.offset(40);
        make.width.height.mas_equalTo(120);
        make.centerX.mas_equalTo(self.view.mas_centerX).with.offset(-100);
    }];
    
    [yearImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(yearBtn.mas_centerY);
        make.width.mas_equalTo(140);
        make.height.mas_equalTo(45);
        make.left.mas_equalTo(yearBtn.mas_right).with.offset(20);
    }];
    

}


- (void)weekBtnAction
{
    DBXReportDetailViewController *reportDetailVC = [DBXReportDetailViewController new];
    reportDetailVC.flag = 1;
    [self.navigationController pushViewController:reportDetailVC animated:YES];
}

- (void)monthBtnAction
{
    DBXReportDetailViewController *reportDetailVC = [DBXReportDetailViewController new];
    reportDetailVC.flag = 2;
    [self.navigationController pushViewController:reportDetailVC animated:YES];
}

- (void)yearBtnAction
{
    DBXReportDetailViewController *reportDetailVC = [DBXReportDetailViewController new];
    reportDetailVC.flag = 3;
    [self.navigationController pushViewController:reportDetailVC animated:YES];
}

#pragma mark - tableView Delegate and DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"CELLID";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DBXReportDetailViewController *reportDetailVC = [DBXReportDetailViewController new];
    [self.navigationController pushViewController:reportDetailVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
