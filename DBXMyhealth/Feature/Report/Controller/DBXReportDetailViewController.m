//
//  DBXReportDetailViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/12.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXReportDetailViewController.h"
#import "XMGTitleButton.h"
#import "DBXDataAnalyzeViewController.h"
#import "DBXDataReportViewController.h"
#import "DBXScrollView.h"
#import "DBXReportCalendarViewController.h"

@interface DBXReportDetailViewController ()<UIScrollViewDelegate, DBXDataAnalyzeViewControllerDelegate, DBXReportCalendarViewControllerDelegate>
/** 用来存放所有子控制器view的scrollView */
@property (nonatomic, weak) UIScrollView *scrollView;
/** 标题栏 */
@property (nonatomic, weak) UIView *titlesView;
/** 标题下划线 */
@property (nonatomic, weak) UIView *titleUnderline;
/** 上一次点击的标题按钮 */
@property (nonatomic, weak) UIButton *previousClickedTitleButton;
/** 下拉菜单 */
@property (nonatomic, strong) FFDropDownMenuView *dropDownMenu;

@end

@implementation DBXReportDetailViewController

#pragma mark - 懒加载


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithimage:[UIImage imageNamed:@"report_calendarBtn"] selImage:[UIImage imageNamed:@"report_calendarBtn"] target:self action:@selector(rightBarButtonItemAction)];
//    self.navigationItem.title = [self getWeekTime];
    
    // 初始化子控制器
    [self setupAllChildVcs];
    
    // scrollView
    [self setupScrollView];
    
    // 标题栏
    [self setupTitlesView];
    
    // 添加第0个子控制器的view
    [self addChildVcViewIntoScrollView:0];
    
    [self setupDropDownMenu];
}

- (void)setupDropDownMenu
{
    NSArray *modelsArray = [self getMenuModelsArray];
    
    self.dropDownMenu = [FFDropDownMenuView ff_DefaultStyleDropDownMenuWithMenuModelsArray:modelsArray menuWidth:FFDefaultFloat eachItemHeight:FFDefaultFloat menuRightMargin:FFDefaultFloat triangleRightMargin:FFDefaultFloat];
    
    self.dropDownMenu.ifShouldScroll = NO;
    
    self.dropDownMenu.iconSize = CGSizeMake(0, 0);
    self.dropDownMenu.menuWidth = 100;
    
    [self.dropDownMenu setup];
}

/** 获取菜单模型数组 */
- (NSArray *)getMenuModelsArray
{
    __weak typeof(self) weakSelf = self;
    
    FFDropDownMenuModel *menuModel0 = [FFDropDownMenuModel ff_DropDownMenuModelWithMenuItemTitle:@"上一条" menuItemIconName:@""  menuBlock:^{
        
        DBXDataAnalyzeViewController *dataAnalyze = weakSelf.childViewControllers[0];
        [dataAnalyze requestReportDataWithFlag:weakSelf.flag order:0 isNextData:NO date:nil];
        
        DBXDataReportViewController *dataReportVC = weakSelf.childViewControllers[1];
        [dataReportVC requestReportDataWithFlag:weakSelf.flag order:0 isNextData:NO date:nil];
        
    }];
    
    FFDropDownMenuModel *menuModel1 = [FFDropDownMenuModel ff_DropDownMenuModelWithMenuItemTitle:@"下一条" menuItemIconName:@"" menuBlock:^{
        DBXDataAnalyzeViewController *dataAnalyze = weakSelf.childViewControllers[0];
        [dataAnalyze requestReportDataWithFlag:weakSelf.flag order:1 isNextData:YES date:nil];
        
        DBXDataReportViewController *dataReportVC = weakSelf.childViewControllers[1];
        [dataReportVC requestReportDataWithFlag:weakSelf.flag order:1 isNextData:YES date:nil];
    }];
    
    FFDropDownMenuModel *menuModel2 = [FFDropDownMenuModel ff_DropDownMenuModelWithMenuItemTitle:@"更多" menuItemIconName:@"" menuBlock:^{
        DBXReportCalendarViewController *reportCalendarVC = [DBXReportCalendarViewController new];
        reportCalendarVC.flag = weakSelf.flag;
        reportCalendarVC.delegate = self;
        reportCalendarVC.navigationItem.title = weakSelf.navigationItem.title;
        [self.navigationController pushViewController:reportCalendarVC animated:YES];
    }];
    
    NSArray *menuModelArr = @[menuModel0, menuModel1, menuModel2];
    return menuModelArr;
}

- (void)reloadViewWithCalendarDate:(NSString *)date
{
    DBXDataAnalyzeViewController *dataAnalyze = self.childViewControllers[0];
    [dataAnalyze requestReportDataWithFlag:self.flag order:0 isNextData:NO date:date];
}


- (void)rightBarButtonItemAction
{
    // 显示下拉界面
    [self.dropDownMenu showMenu];
}

/**
 *  初始化子控制器
 */
- (void)setupAllChildVcs
{
    DBXDataAnalyzeViewController *dataAnalyzeVC = [[DBXDataAnalyzeViewController alloc] init];
    dataAnalyzeVC.delegate = self;
    dataAnalyzeVC.flag = _flag;
    [self addChildViewController:dataAnalyzeVC];
    
    DBXDataReportViewController *dataReportVC = [DBXDataReportViewController new];
    dataReportVC.flag = _flag;
    [self addChildViewController:dataReportVC];
}

- (void)reloadReportDetailVCNavTitleWithStartTime:(NSString *)startTime endTime:(NSString *)endTime
{
    if (!kStringIsEmpty(startTime) && !kStringIsEmpty(endTime)) {
        
        NSString *nStartTime = [startTime substringWithRange:NSMakeRange(0, 10)];
        NSString *nEndTime = [endTime substringWithRange:NSMakeRange(0, 10)];
        
        self.navigationItem.title = [NSString stringWithFormat:@"%@~%@", nStartTime, nEndTime];
    }
}

/**
 *  scrollView
 */
- (void)setupScrollView
{
    // 不允许自动修改UIScrollView的内边距
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    DBXScrollView *scrollView = [[DBXScrollView alloc] init];
    scrollView.bounces = NO;
    scrollView.backgroundColor = [UIColor whiteColor];
    scrollView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight-kSafeAreaTopHeight-60);
    scrollView.delegate = self;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.pagingEnabled = YES;
    [self.view addSubview:scrollView];
    self.scrollView = scrollView;
    
    // 添加子控制器的view
    NSUInteger count = self.childViewControllers.count;
    CGFloat scrollViewW = scrollView.xmg_width;
        CGFloat scrollViewH = scrollView.xmg_height;
    
        for (NSUInteger i = 0; i < count; i++) {
            // 取出i位置子控制器的view
            UIView *childVcView = self.childViewControllers[i].view;
            childVcView.frame = CGRectMake(i * scrollViewW, 0, scrollViewW, scrollViewH);
            [scrollView addSubview:childVcView];
        }
    
    scrollView.contentSize = CGSizeMake(count * scrollViewW, 0);
}

/**
 *  标题栏
 */
- (void)setupTitlesView
{
    UIView *titlesView = [[UIView alloc] init];
    titlesView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    titlesView.frame = CGRectMake(0, kScreenHeight-kSafeAreaTopHeight-60, self.view.xmg_width, 60);
    [self.view addSubview:titlesView];
    self.titlesView = titlesView;
    
    // 标题栏按钮
    [self setupTitleButtons];
    
    // 标题下划线
//    [self setupTitleUnderline];
}

/**
 *  标题栏按钮
 */
- (void)setupTitleButtons
{

    NSArray *titles = @[@"数据分析", @"数据报告"];
    NSArray *images = @[@"report_ evaluate_normal", @"report_opinion_normal"];
    NSArray *selectImages = @[@"report_ evaluate_select", @"report_opinion_select"];
    NSUInteger count = titles.count;
    
    // 标题按钮的尺寸
    CGFloat titleButtonW = self.titlesView.xmg_width / count;
    CGFloat titleButtonH = self.titlesView.xmg_height;
    
    // 创建5个标题按钮
    for (NSUInteger i = 0; i < count; i++) {
        UIButton *titleButton = [[UIButton alloc] init];
        titleButton.tag = i;
        [titleButton addTarget:self action:@selector(titleButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.titlesView addSubview:titleButton];
        [titleButton setTitleColor:[UIColor colorWithHexString:@"127CDE"] forState:UIControlStateNormal];
        [titleButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [titleButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"449CED"]] forState:UIControlStateNormal];
        [titleButton setBackgroundImage:[UIImage imageWithColor:kNavigationBarColor] forState:UIControlStateSelected];
        titleButton.titleLabel.font = kFont(14);
        titleButton.frame = CGRectMake(i * titleButtonW, 0, titleButtonW, titleButtonH);
 
        [titleButton setTitle:titles[i] forState:UIControlStateNormal];

        [titleButton setImage:[UIImage imageNamed:images[i]] forState:UIControlStateNormal];
        [titleButton setImage:[UIImage imageNamed:selectImages[i]] forState:UIControlStateSelected];
        [titleButton layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleTop imageTitleSpace:5];
    }
    
    // 默认首按钮选中
    UIButton *firstTitleButton = self.titlesView.subviews.firstObject;
    firstTitleButton.selected = YES;
    self.previousClickedTitleButton = firstTitleButton;
}

/**
 *  标题下划线
 */
- (void)setupTitleUnderline
{
    // 标题按钮
    XMGTitleButton *firstTitleButton = self.titlesView.subviews.firstObject;
    
    // 下划线
    UIView *titleUnderline = [[UIView alloc] init];
    titleUnderline.xmg_height = 2;
    titleUnderline.xmg_y = self.titlesView.xmg_height - titleUnderline.xmg_height;
    titleUnderline.backgroundColor = [firstTitleButton titleColorForState:UIControlStateSelected];
    [self.titlesView addSubview:titleUnderline];
    self.titleUnderline = titleUnderline;
    
    // 切换按钮状态
    firstTitleButton.selected = YES;
    self.previousClickedTitleButton = firstTitleButton;
    
    [firstTitleButton.titleLabel sizeToFit]; // 让label根据文字内容计算尺寸
    self.titleUnderline.xmg_width = firstTitleButton.titleLabel.xmg_width + 10;
    self.titleUnderline.xmg_centerX = firstTitleButton.xmg_centerX;
}

#pragma mark - 监听
/**
 *  点击标题按钮
 */
- (void)titleButtonClick:(XMGTitleButton *)titleButton
{
    // 切换按钮状态
    self.previousClickedTitleButton.selected = NO;
    titleButton.selected = YES;
    self.previousClickedTitleButton = titleButton;
    
    NSUInteger index = titleButton.tag;
    [UIView animateWithDuration:0.25 animations:^{
        // 处理下划线
        self.titleUnderline.xmg_width = titleButton.titleLabel.xmg_width + 10;
        self.titleUnderline.xmg_centerX = titleButton.xmg_centerX;
        
        // 滚动scrollView
        CGFloat offsetX = self.scrollView.xmg_width * index;
        self.scrollView.contentOffset = CGPointMake(offsetX, self.scrollView.contentOffset.y);
    } completion:^(BOOL finished) {
        // 添加子控制器的view
        [self addChildVcViewIntoScrollView:index];
    }];
}



#pragma mark - <UIScrollViewDelegate>
/**
 *  当用户松开scrollView并且滑动结束时调用这个代理方法（scrollView停止滚动的时候）
 */
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    // 求出标题按钮的索引
    NSUInteger index = scrollView.contentOffset.x / scrollView.xmg_width;
    
    // 点击对应的标题按钮
    XMGTitleButton *titleButton = self.titlesView.subviews[index];
    [self titleButtonClick:titleButton];
}

#pragma mark - 其他
/**
 *  添加第index个子控制器的view到scrollView中
 */
- (void)addChildVcViewIntoScrollView:(NSUInteger)index
{
    //取出按钮索引对应的控制器
    UIViewController *childVc = self.childViewControllers[index];
    
    // 如果view已经被加载过，就直接返回
    if (childVc.isViewLoaded) return;
    
    // 取出index位置对应的子控制器view
    UIView *childVcView = childVc.view;
    //    if (childVcView.superview) return;
    //    if (childVcView.window) return;
    
    // 设置子控制器view的frame
    CGFloat scrollViewW = self.scrollView.xmg_width;
    childVcView.frame = CGRectMake(index * scrollViewW, 0, scrollViewW, self.scrollView.xmg_height);
    // 添加子控制器的view到scrollView中
    [self.scrollView addSubview:childVcView];
}


///**
// 获取当前周
//
// @return getWeekTime
// */
//- (NSString *)getWeekTime
//{
//    NSDate *nowDate = [NSDate date];
//    NSCalendar *calendar = [NSCalendar currentCalendar];
//    NSDateComponents *comp = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday  fromDate:nowDate];
//    // 获取今天是周几
//    NSInteger weekDay = [comp weekday];
//    // 获取几天是几号
//    NSInteger day = [comp day];
//    NSLog(@"%ld----%ld",(long)weekDay,(long)day);
//
//    // 计算当前日期和本周的星期一和星期天相差天数
//    long firstDiff,lastDiff;
//    //    weekDay = 1; weekDay == 1 == 周日
//    if (weekDay == 1)
//    {
//        firstDiff = -6;
//        lastDiff = 0;
//    }
//    else
//    {
//        firstDiff = [calendar firstWeekday] - weekDay + 1;
//        lastDiff = 8 - weekDay;
//    }
//    NSLog(@"firstDiff: %ld   lastDiff: %ld",firstDiff,lastDiff);
//
//    // 在当前日期(去掉时分秒)基础上加上差的天数
//    NSDateComponents *baseDayComp = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay  fromDate:nowDate];
//
//    //获取周一日期
//    [baseDayComp setDay:day + firstDiff];
//    NSDate *firstDayOfWeek = [calendar dateFromComponents:baseDayComp];
//
//    //获取周末日期
//    [baseDayComp setDay:day + lastDiff];
//    NSDate *lastDayOfWeek = [calendar dateFromComponents:baseDayComp];
//
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"YYYY.MM.dd"];
//    NSString *firstDay = [formatter stringFromDate:firstDayOfWeek];
//    NSString *lastDay = [formatter stringFromDate:lastDayOfWeek];
//    NSLog(@"%@=======%@",firstDay,lastDay);
//
//    NSString *dateStr = [NSString stringWithFormat:@"%@ ～ %@",firstDay,lastDay];
//
//    return dateStr;
//
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
