//
//  DBXReportButton.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/13.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXReportButton.h"

@implementation DBXReportButton

- (void)drawRect:(CGRect)rect {
    // Drawing code
    CAShapeLayer *shapLayer = [CAShapeLayer layer];
    
    shapLayer.path = self.path.CGPath;
    
    self.layer.mask = shapLayer;
}

// 覆盖方法，点击时判断点是否在path内，YES则响应，NO则不响应
- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    BOOL res = [super pointInside:point withEvent:event];
    if (res) {
        
        if ([self.path containsPoint:point]) {
            
            return YES;
        }
        
        return NO;
    }
    return NO;
}


@end
