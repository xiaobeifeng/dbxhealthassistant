//
//  DBXReportUserInfoView.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/9/12.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "DBXReportUserInfoView.h"

@interface DBXReportUserInfoView()

@property(nonatomic, strong) UILabel *nameLbl;
@property(nonatomic, strong) UILabel *sexLbl;
@property(nonatomic, strong) UILabel *ageLbl;
@property(nonatomic, strong) UIButton *maxValueBtn;
@property(nonatomic, strong) UIButton *averageValueBtn;
@property(nonatomic, strong) UIButton *minValueBtn;
@end

@implementation DBXReportUserInfoView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        _nameLbl = [UILabel new];
        _nameLbl.text = @"姓名：-";
        _nameLbl.textAlignment = NSTextAlignmentCenter;
        _nameLbl.textColor = [UIColor whiteColor];
        _nameLbl.font = kFont(14);
        [self addSubview:_nameLbl];
        
        _sexLbl = [UILabel new];
        _sexLbl.text = @"性别：-";
        _sexLbl.textAlignment = NSTextAlignmentCenter;
        _sexLbl.textColor = [UIColor whiteColor];
        _sexLbl.font = kFont(14);
        [self addSubview:_sexLbl];
        
        _ageLbl = [UILabel new];
        _ageLbl.text = @"年龄：-";
        _ageLbl.textAlignment = NSTextAlignmentCenter;
        _ageLbl.textColor = [UIColor whiteColor];
        _ageLbl.font = kFont(14);
        [self addSubview:_ageLbl];
        
        [@[_nameLbl, _sexLbl, _ageLbl] mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:10 leadSpacing:13 tailSpacing:13];
        
        [@[_nameLbl, _sexLbl, _ageLbl] mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.height.equalTo(@35);
        }];
        
        _bloodPressLbl = [UILabel new];
        _bloodPressLbl.text = @"血压：-/-mmHg";
        _bloodPressLbl.textColor = [UIColor whiteColor];
        _bloodPressLbl.font = kFont(14);
        [_bloodOxygenLbl sizeToFit];
        [self addSubview:_bloodPressLbl];
        [_bloodPressLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.nameLbl.mas_bottom).with.offset(10);
            make.centerX.mas_equalTo(self.mas_centerX);
            
        }];
        
        _bloodOxygenLbl = [UILabel new];
        _bloodOxygenLbl.text = @"血氧：- %";
        _bloodOxygenLbl.textColor = [UIColor whiteColor];
        _bloodOxygenLbl.font = kFont(14);
        [self addSubview:_bloodOxygenLbl];
        [_bloodOxygenLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.bloodPressLbl.mas_bottom).with.offset(10);
            make.left.mas_equalTo(self.bloodPressLbl.mas_left);
            
        }];
        
        _heartBeatLbl = [UILabel new];
        _heartBeatLbl.text = @"心率：- 次/分";
        _heartBeatLbl.textColor = [UIColor whiteColor];
        _heartBeatLbl.font = kFont(14);
        [self addSubview:_heartBeatLbl];
        [_heartBeatLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.bloodOxygenLbl.mas_bottom).with.offset(10);
            make.left.mas_equalTo(self.bloodPressLbl.mas_left);
        }];
        
        _maxValueBtn = [UIButton new];
        _maxValueBtn.tag = 100;
        [_maxValueBtn addTarget:self action:@selector(handleMaxBtnEvent:) forControlEvents:UIControlEventTouchUpInside];
        [_maxValueBtn setTitle:@"最大值" forState:UIControlStateNormal];
        [_maxValueBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [_maxValueBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
        _maxValueBtn.titleLabel.font = kFont(12);
        _maxValueBtn.backgroundColor = kNavigationBarColor;
        kViewBorderRadius(_maxValueBtn, 8.0f, 1, [UIColor whiteColor]);
//        _maxValueBtn.selected = YES;
//        kViewBorderRadius(_maxValueBtn, 8.0f, 1, [UIColor whiteColor]);
        [self addSubview:_maxValueBtn];
        
        _averageValueBtn = [UIButton new];
        _averageValueBtn.tag = 200;
        [_averageValueBtn setTitle:@"平均值" forState:UIControlStateNormal];
        [_averageValueBtn addTarget:self action:@selector(handleAverageBtnEvent:) forControlEvents:UIControlEventTouchUpInside];
        _averageValueBtn.titleLabel.font = kFont(12);
        [_averageValueBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [_averageValueBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
        _averageValueBtn.backgroundColor = kNavigationBarColor;
//        kViewBorderRadius(_averageValueBtn, 8.0f, 1, [UIColor whiteColor]);
        [self addSubview:_averageValueBtn];
        
        _minValueBtn = [UIButton new];
        _minValueBtn.tag = 300;
        [_minValueBtn setTitle:@"最小值" forState:UIControlStateNormal];
        [_minValueBtn addTarget:self action:@selector(handleMinBtnEvent:) forControlEvents:UIControlEventTouchUpInside];
        _minValueBtn.titleLabel.font = kFont(12);
        [_minValueBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [_minValueBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
        _minValueBtn.backgroundColor = kNavigationBarColor;
//        kViewBorderRadius(_minValueBtn, 8.0f, 1, [UIColor whiteColor]);
        [self addSubview:_minValueBtn];
        
        [@[_maxValueBtn, _averageValueBtn, _minValueBtn] mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedItemLength:70 leadSpacing:50 tailSpacing:50];
        
        [@[_maxValueBtn, _averageValueBtn, _minValueBtn] mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.heartBeatLbl.mas_bottom).with.offset(20);
            make.height.equalTo(@25);
        }];
        
        
    }
    
    return self;
}

- (void)setName:(NSString *)name
{
    if (kStringIsEmpty(name)) {
        _nameLbl.text = @"姓名：-";
    } else {
        _nameLbl.text = [NSString stringWithFormat:@"姓名：%@", name];
    }
}

- (void)setAge:(NSString *)age
{
    if (kStringIsEmpty(age)) {
        _ageLbl.text = @"年龄：-";
    } else {
        _ageLbl.text = [NSString stringWithFormat:@"年龄：%@", age];
    }
}

- (void)setSex:(NSString *)sex
{
    if (kStringIsEmpty(sex)) {
        _sexLbl.text = @"性别：-";
    } else {
        _sexLbl.text = [NSString stringWithFormat:@"性别：%@", sex];
    }
}



- (void)handleMaxBtnEvent:(UIButton *)button
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(handleMaxBtnEvent)]) {
        
        kViewBorderRadius(_maxValueBtn, 8.0f, 1, [UIColor whiteColor]);
        kViewBorderRadius(_averageValueBtn, 0.0f, 0, [UIColor whiteColor]);
        kViewBorderRadius(_minValueBtn, 0.0f, 0, [UIColor whiteColor]);
//        _maxValueBtn.selected = YES;
//        _averageValueBtn.selected = NO;
//        _minValueBtn.selected = NO;
        [self.delegate handleMaxBtnEvent];
    }
}

- (void)handleAverageBtnEvent:(UIButton *)button
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(handleAverageBtnEvent)]) {
        
//        _maxValueBtn.selected = NO;
//        _averageValueBtn.selected = YES;
//        _minValueBtn.selected = NO;
        kViewBorderRadius(_maxValueBtn, 0.0f, 0, [UIColor whiteColor]);
        kViewBorderRadius(_averageValueBtn, 8.0f, 1, [UIColor whiteColor]);
        kViewBorderRadius(_minValueBtn, 0.0f, 0, [UIColor whiteColor]);
        [self.delegate handleAverageBtnEvent];
    }
}

- (void)handleMinBtnEvent:(UIButton *)button
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(handleMinBtnEvent)]) {
        
//        _maxValueBtn.selected = NO;
//        _averageValueBtn.selected = NO;
//        _minValueBtn.selected = YES;
        kViewBorderRadius(_maxValueBtn, 0.0f, 0, [UIColor whiteColor]);
        kViewBorderRadius(_averageValueBtn, 0.0f, 0, [UIColor whiteColor]);
        kViewBorderRadius(_minValueBtn, 8.0f, 1, [UIColor whiteColor]);
        [self.delegate handleMinBtnEvent];
    }
}


@end
