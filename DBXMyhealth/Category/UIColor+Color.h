//
//  UIColor+Color.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/8/20.
//  Copyright © 2018年 zhoujian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Color)

/** 绘制渐变色颜色的方法 */
+ (CAGradientLayer *)setGradualChangingColor:(UIView *)view fromColor:(NSString *)fromHexColorStr toColor:(NSString *)toHexColorStr;

/** 获取16进制颜色的方法 */
+ (UIColor *)colorWithHex:(NSString *)hexColor;
@end
