//
//  NSString+String.h
//  SMKJSmartDiagnose
//
//  Created by zhoujian on 2018/7/6.
//  Copyright © 2018年 zhoujian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (String)


/**
 判断是否为合法账号（仅包含数字字母下划线）

 @param string 账号
 @return YES or NO
 */
+ (BOOL)isLegalAccount:(NSString*)string;

+ (NSString *)removeSpaceAndNewline:(NSString *)str;
@end
