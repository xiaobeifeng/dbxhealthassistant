//
//  NSString+String.m
//  SMKJSmartDiagnose
//
//  Created by zhoujian on 2018/7/6.
//  Copyright © 2018年 zhoujian. All rights reserved.
//

#import "NSString+String.h"

@implementation NSString (String)

+ (BOOL)isLegalAccount:(NSString*)string
{
    NSString *regex = @"^[_a-zA-Z0-9]+$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self MATCHES %@", string];
    return [predicate evaluateWithObject:string];
}


/**
 去掉字符串中所有的空格及回车
 */
+ (NSString *)removeSpaceAndNewline:(NSString *)str
{
    NSString *temp = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return temp;
}

@end
