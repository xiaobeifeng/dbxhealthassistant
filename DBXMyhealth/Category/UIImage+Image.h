//
//  UIImage+Image.h
//  wietec
//
//  Created by zhoujian-vino on 2017/11/2.
//  Copyright © 2017年 vino. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Image)

+ (instancetype)imageWithOriginal:(NSString *)imageName;
+ (UIImage *)resizeImageWithName:(NSString *)name;

@end
