//
//  UIImage+Image.m
//  wietec
//
//  Created by zhoujian-vino on 2017/11/2.
//  Copyright © 2017年 vino. All rights reserved.
//

#import "UIImage+Image.h"

@import Accelerate;

@implementation UIImage (Image)

/**
 *  设置图片不被系统渲染
 */
+ (instancetype)imageWithOriginal:(NSString *)imageName
{
    UIImage *image = [UIImage imageNamed:imageName];
    
    return [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
}

/**
 *  返回一张自由拉伸的图片
 */
+ (UIImage *)resizeImageWithName:(NSString *)name
{
    UIImage *image = [UIImage imageNamed:name];
    
    return [image stretchableImageWithLeftCapWidth:image.size.width * 0.5 topCapHeight:image.size.height * 0.5];
}


@end
