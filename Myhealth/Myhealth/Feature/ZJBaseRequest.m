//
//  ZJBaseRequest.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/14.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJBaseRequest.h"

@implementation ZJBaseRequest

/**
 默认请求体是自身转json
 */
- (id)requestArgument {
    
    return [self yy_modelToJSONObject];
}

/**
 请求失败过滤器
 */
- (void)requestFailedFilter {
    //失败处理器
    NSLog(@"失败了 - %@", self.error);
    [MBProgressHUD hideHUD];
    [MBProgressHUD showErrorMessage:TRAY_AGAIN_LATER];
    
}

- (void)requestCompleteFilter {
    
    [MBProgressHUD hideHUD];
    
    if (self.responseJSONObject) {
        self.result = self.responseJSONObject;
        NSLog(@"返回结果 - %@", [self.result jsonPrettyStringEncoded]); // YYKit
    } else {
//        [MBProgressHUD showErrorMessage:TRAY_AGAIN_LATER];
    }
    
    if ([self code] == 2) {
        
        [MBProgressHUD showWarnMessage:@"token失效，请重新登录"];
        [self stop];
        
        ZJNavigationController *navC = [[ZJNavigationController alloc] initWithRootViewController:[ZJLoginViewController new]];
        [UIApplication sharedApplication].keyWindow.rootViewController = navC;
        
    }
    
}

- (id)jsonValidator
{
    return @{
             @"message":[NSString class],
             @"data":[NSObject class],
             @"state":[NSNumber class]
             };
}

- (NSString *)message {
    if (self.error) {
        return self.error.localizedDescription;
    }
    NSString *message = [NSString stringWithFormat:@"%@",self.result[@"message"]];
    return message;
}

- (NSInteger)code {
    NSString *code = [NSString stringWithFormat:@"%@",self.result[@"state"]];
    return [code integerValue];
}

- (NSDictionary<NSString *,NSString *> *)requestHeaderFieldValueDictionary {
    
    NSLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:UD_KEY_TOKEN]);
    NSString *token = [[NSUserDefaults standardUserDefaults] zj_objcetForKey:UD_KEY_TOKEN];
    if (token.length > 0) {
        return @{@"token":token};
    } else {
        return nil;
    }
    
}

- (BOOL)statusCodeValidator {
    
    
    NSInteger statusCode = [self responseStatusCode];
    
    if (statusCode == 1) {
        
        return NO;
    } else {
        
        return YES;
    }
    
}

- (NSTimeInterval)requestTimeoutInterval {
    return 15;
}

@end
