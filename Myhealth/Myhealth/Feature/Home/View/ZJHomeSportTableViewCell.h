
//  Created by zhoujian on 2018/10/31.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 首页运动自定义cell
 */
@interface ZJHomeSportTableViewCell : UITableViewCell

/** 消耗卡路里值 */
@property (nonatomic, strong) UILabel *kcalValueLable;
/** 步数值 */
@property (nonatomic, strong) UILabel *stepValueLable;
/** 记录时间 */
@property (nonatomic, strong) UILabel *timeLable;

@end

NS_ASSUME_NONNULL_END
