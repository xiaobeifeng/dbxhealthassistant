
//  Created by zhoujian on 2018/8/31.
//  Copyright © 2018年 mc. All rights reserved.
//

#import "ZJHistoryBPBOHBTableViewCell.h"
@interface ZJHistoryBPBOHBTableViewCell()

@property(nonatomic, strong) UIView *lineView;
@property(nonatomic, strong) UIView *pointView;
@property(nonatomic, strong) UIView *contentBackgroundView;
@property (nonatomic, strong) UIButton *stateButton;

@end

@implementation ZJHistoryBPBOHBTableViewCell

-  (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.contentView.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.96 alpha:1.00];
        
        _lineView = [UIView new];
        _lineView.backgroundColor = COLOR_PICTONBLUE;
        [self.contentView addSubview:_lineView];
        
        _pointView = [UIView new];
        _pointView.backgroundColor = COLOR_PICTONBLUE;
        kViewRadius(_pointView, 5);
        [_lineView addSubview:_pointView];
        
        _contentBackgroundView = [UIView new];
        _contentBackgroundView.backgroundColor = [UIColor whiteColor];
        kViewRadius(_contentBackgroundView, 5.0f);
        [self.contentView addSubview:_contentBackgroundView];
        
        _dateLbl = [UILabel new];
        _dateLbl.backgroundColor = [UIColor whiteColor];
        _dateLbl.textColor = COLOR_PICTONBLUE;
        _dateLbl.font = [UIFont systemFontOfSize:12];
        [_contentBackgroundView addSubview:_dateLbl];
        
        _sbpLabel = [UILabel new];
        _sbpLabel.backgroundColor = [UIColor whiteColor];
        _sbpLabel.textColor = COLOR_PICTONBLUE;
        _sbpLabel.font = [UIFont systemFontOfSize:14];
        [_contentBackgroundView addSubview:_sbpLabel];
        
        _dbpLabel = [UILabel new];
        _dbpLabel.backgroundColor = [UIColor whiteColor];
        _dbpLabel.textColor = COLOR_PICTONBLUE;
        _dbpLabel.font = [UIFont systemFontOfSize:14];
        [_contentBackgroundView addSubview:_dbpLabel];
        
        _heartBeatLbl = [UILabel new];
        _heartBeatLbl.backgroundColor = [UIColor whiteColor];
        _heartBeatLbl.textColor = COLOR_PICTONBLUE;
        _heartBeatLbl.font = [UIFont systemFontOfSize:14];
        [_contentBackgroundView addSubview:_heartBeatLbl];
        
        _oxyLbl = [UILabel new];
        _oxyLbl.backgroundColor = [UIColor whiteColor];
        _oxyLbl.textColor = COLOR_PICTONBLUE;
        _oxyLbl.font = [UIFont systemFontOfSize:14];
        [_contentBackgroundView addSubview:_oxyLbl];
        
        _stateButton = [UIButton new];
        _stateButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [_stateButton setTitle:@"血压正常，请继续保持" forState:UIControlStateNormal];
        [_stateButton setTitleColor:COLOR_PICTONBLUE forState:UIControlStateNormal];
        [_stateButton setTitle:@"血压异常，请注意监测并咨询专业医生" forState:UIControlStateSelected];
        [_stateButton setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
        _stateButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [_contentBackgroundView addSubview:_stateButton];
        
    }
    return self;
}

+ (BOOL)requiresConstraintBasedLayout
{
    return YES;
}

- (void)updateConstraints
{
    
    [_contentBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).with.offset(13);
        make.left.mas_equalTo(self.contentView).with.offset(70);
        make.right.mas_equalTo(self.contentView).with.offset(-13);
        make.height.mas_equalTo(165);
        make.bottom.mas_equalTo(self.contentView).with.offset(-13);
    }];
    
    [@[_dateLbl, _sbpLabel, _dbpLabel, _heartBeatLbl, _oxyLbl, _stateButton] mas_distributeViewsAlongAxis:MASAxisTypeVertical withFixedSpacing:5 leadSpacing:13 tailSpacing:13];
    
    [@[_dateLbl, _sbpLabel, _dbpLabel, _heartBeatLbl, _oxyLbl, _stateButton] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentBackgroundView).with.offset(13);
        make.right.mas_equalTo(self.contentBackgroundView).with.offset(-13);
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView);
        make.bottom.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView).with.offset(34);
        make.width.mas_equalTo(2);
    }];
    [_pointView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.lineView.mas_centerY);
        make.centerX.mas_equalTo(self.lineView.mas_centerX);
        make.width.height.mas_equalTo(10);
    }];
    
    
    [super updateConstraints];
}

- (void)setSbpValue:(NSInteger)sbpValue {
    _sbpValue = sbpValue;
}

- (void)setDbpValue:(NSInteger)dbpValue {
    _dbpValue = dbpValue;
    
    BOOL isNormal = [ZJIsNormalNormTool zj_isNormalBp:_sbpValue dbp:_dbpValue];
    if (isNormal) {
        _stateButton.selected = NO;
    } else {
        _stateButton.selected = YES;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
