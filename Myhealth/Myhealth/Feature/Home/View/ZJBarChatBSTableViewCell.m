
//  Created by zhoujian on 2018/11/3.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJBarChatBSTableViewCell.h"

@interface ZJBarChatBSTableViewCell()
/** 白色背景 */
@property (nonatomic, strong) UIView *view;


@end

@implementation ZJBarChatBSTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // 设置cell样式
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = COLOR_GALLERY;
        
        self.view = [UIView new];
        self.view.backgroundColor = [UIColor whiteColor];
        kViewRadius(self.view, 5.0f);
        [self.contentView addSubview:self.view];
        
        self.iconImageView = [UIImageView new];
        self.iconImageView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.iconImageView];
        
        self.titleLabel = [UILabel new];
        self.titleLabel.font = [UIFont systemFontOfSize:14];
//        self.titleLabel.textColor = [UIColor colorWithRed:0.99 green:0.36 blue:0.46 alpha:1.00];
        [self.titleLabel sizeToFit];
        [self.view addSubview:self.titleLabel];
        
        self.subTitleLabel = [UILabel new];
        self.subTitleLabel.font = [UIFont systemFontOfSize:14];
//        self.subTitleLabel.textColor = [UIColor colorWithRed:0.99 green:0.36 blue:0.46 alpha:1.00];
        [self.subTitleLabel sizeToFit];
        [self.view addSubview:self.subTitleLabel];
        [self.subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.titleLabel.mas_right).with.offset(13);
            make.centerY.equalTo(self.titleLabel.mas_centerY);
        }];
        
        self.timeLabel = [UILabel new];
        self.timeLabel.font = [UIFont systemFontOfSize:12];
        self.timeLabel.textColor = [UIColor lightGrayColor];
        self.timeLabel.text = @"暂无时间";
        [self.timeLabel sizeToFit];
        [self.view addSubview:self.timeLabel];
        
        self.lineImageView = [UIImageView new];
        self.lineImageView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.lineImageView];
   
        self.stateView = [UIButton new];
        self.stateView.backgroundColor = [UIColor greenColor];
        self.stateView.backgroundColor = [UIColor clearColor];
        self.stateView.titleLabel.font = [UIFont systemFontOfSize:10];
        [self.stateView setTitle:@"数据正常" forState:UIControlStateNormal];
        [self.stateView setTitleColor:COLOR_PICTONBLUE forState:UIControlStateNormal];
        [self.stateView setImage:[UIImage imageNamed:@"button_normal"] forState:UIControlStateNormal];
        
        [self.stateView setTitle:@"数据异常" forState:UIControlStateSelected];
        [self.stateView setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
        [self.stateView setImage:[UIImage imageNamed:@"button_warn"] forState:UIControlStateSelected];
        [self.stateView sizeToFit];
        [self.view addSubview:self.stateView];
        
        [self.view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.mas_equalTo(13);
            make.right.bottom.mas_equalTo(-13);
        }];
        
        [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.equalTo(self.view).with.offset(13);
            make.width.height.mas_equalTo(20);
        }];
        
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.iconImageView.mas_right).with.offset(13);
            make.centerY.equalTo(self.iconImageView.mas_centerY);
            
        }];
        
        [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.iconImageView.mas_left);
            make.top.equalTo(self.iconImageView.mas_bottom).with.offset(13);
            
        }];
        
        [self.lineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.timeLabel.mas_bottom).with.offset(20);
            make.left.equalTo(self.view).with.offset(50);
            make.right.equalTo(self.view).with.offset(-50);
            make.height.mas_equalTo(10);
            make.bottom.equalTo(self.view).with.offset(-13);
        }];
        
        [self.stateView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.lineImageView.mas_top);
//            make.width.mas_equalTo(20);
//            make.height.mas_equalTo(10);
            make.left.equalTo(self.lineImageView.mas_left).with.offset(-5);
        }];
        
        UIImageView *detailArrow = [UIImageView new];
        detailArrow.image = [UIImage imageNamed:@"detail_arrow"];
        [self.view addSubview:detailArrow];
        [detailArrow mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(20);
            make.centerY.equalTo(self.titleLabel.mas_centerY);
            make.right.equalTo(self.view).with.offset(-13);
        }];
    }
    
    return self;
}

- (void)setBsValue:(double)bsValue {
    
    float barLength = kScreenWidth-13*2-50*2;   // 条形图长度
    _bsValue = bsValue;
        
    NSInteger leftPadding = barLength/33 * bsValue - 5;     // 血糖最大值33
    [self.stateView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.lineImageView.mas_left).with.offset(leftPadding);
    }];
    
}

- (void)setBsCondition:(NSInteger)bsCondition {

    BOOL isNormal = [ZJIsNormalNormTool zj_isNormalBs:bsCondition bs:_bsValue];
    if (isNormal) {
        _stateView.selected = NO;
    } else {
        _stateView.selected = YES;
    }
    
}

- (void)setBoValue:(NSInteger)boValue {
    float barLength = kScreenWidth-13*2-50*2;   // 条形图长度
    _boValue = boValue;
    
    NSInteger leftPadding = barLength/100 * boValue - 5;
    [self.stateView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.lineImageView.mas_left).with.offset(leftPadding);
    }];
        
    BOOL isNormal = [ZJIsNormalNormTool zj_isNormalBo:boValue];
    if (isNormal) {
        _stateView.selected = NO;
    } else {
        _stateView.selected = YES;
    }
}


//- (void)setTitleLabel:(UILabel *)titleLabel {
//    
//    if ([titleLabel.text isEqualToString:@"心率"]) {
//        [self.lineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.height.mas_equalTo(40);
//        }];
//    } else {
//        [_lineImageView mas_updateConstraints:^(MASConstraintMaker *make) {
//            make.height.mas_equalTo(10);
//        }];
//    }
//    
//}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
