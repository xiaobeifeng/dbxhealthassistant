
//  Created by zhoujian on 2018/9/6.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJDataBarBPDetailTableViewCell.h"

@interface ZJDataBarBPDetailTableViewCell()


@property(nonatomic, strong) UIButton *stateButton;     // 血压状态按钮
@property(nonatomic, strong) UIView *contantBackgroundView;
@property(nonatomic, strong) UIView *lineView;
@property(nonatomic, strong) UIView *pointView;

@end

@implementation ZJDataBarBPDetailTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.contentView.backgroundColor = COLOR_GALLERY;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _contantBackgroundView = [UIView new];
        kViewRadius(_contantBackgroundView, 5.0f);
        _contantBackgroundView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_contantBackgroundView];
        
        _dateLbl = [UILabel new];
        _dateLbl.font = [UIFont systemFontOfSize:12];
        _dateLbl.textColor = COLOR_PICTONBLUE;
        [self.contentView addSubview:_dateLbl];
        
        _sPressValueLbl = [UILabel new];
        _sPressValueLbl.backgroundColor = [UIColor clearColor];
        _sPressValueLbl.textColor = COLOR_PICTONBLUE;
        _sPressValueLbl.textAlignment = NSTextAlignmentCenter;
        [_contantBackgroundView addSubview:_sPressValueLbl];
        
        _dPressValueLbl = [UILabel new];
        _dPressValueLbl.backgroundColor = [UIColor clearColor];
        _dPressValueLbl.textColor = COLOR_PICTONBLUE;
        _dPressValueLbl.textAlignment = NSTextAlignmentCenter;
        [_contantBackgroundView addSubview:_dPressValueLbl];
        
        _stateButton = [UIButton new];
        _stateButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [_stateButton setTitle:@"血压正常，请继续保持" forState:UIControlStateNormal];
        [_stateButton setTitleColor:COLOR_PICTONBLUE forState:UIControlStateNormal];
        [_stateButton setTitle:@"血压异常，请注意监测并咨询专业医生" forState:UIControlStateSelected];
        [_stateButton setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
        [_contantBackgroundView addSubview:_stateButton];
        
        _lineView = [UIView new];
        _lineView.backgroundColor = COLOR_PICTONBLUE;
        [self.contentView addSubview:_lineView];
        
        _pointView = [UIView new];
        kViewRadius(_pointView, 5);
        _pointView.backgroundColor = COLOR_PICTONBLUE;
        [self.contentView addSubview:_pointView];
        
    }
    
    return self;
}

+ (BOOL)requiresConstraintBasedLayout
{
    return YES;
}

- (void)updateConstraints
{
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView);
        make.bottom.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView).with.offset(34);
        make.width.mas_equalTo(2);
    }];
    
    [_pointView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(10);
        make.centerX.mas_equalTo(self.lineView.mas_centerX);
        make.centerY.mas_equalTo(self.lineView.mas_centerY);
    }];
    
    [_contantBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(13);
        make.left.mas_equalTo(70);
        make.right.mas_equalTo(-13);
        make.height.mas_equalTo(80);
        make.bottom.mas_equalTo(-13);
    }];
    
    [_dateLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contantBackgroundView);
        make.width.mas_equalTo(self.contantBackgroundView.mas_width).multipliedBy(0.9);
        make.height.mas_equalTo(20);
        make.centerX.mas_equalTo(self.contantBackgroundView.mas_centerX);
    }];
    
    [_sPressValueLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.dateLbl.mas_bottom);
        make.width.mas_equalTo(self.contantBackgroundView.mas_width).multipliedBy(0.5);
        make.height.mas_equalTo(40);
        make.left.mas_equalTo(self.contantBackgroundView.mas_left);
    }];
    
    [_dPressValueLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.dateLbl.mas_bottom);
        make.width.mas_equalTo(self.contantBackgroundView.mas_width).multipliedBy(0.5);;
        make.height.mas_equalTo(40);
        make.left.mas_equalTo(self.sPressValueLbl.mas_right);
    }];
    
    [_stateButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.sPressValueLbl.mas_bottom);
        make.width.mas_equalTo(self.contantBackgroundView.mas_width).multipliedBy(0.9);
        make.height.mas_equalTo(20);
        make.centerX.mas_equalTo(self.contantBackgroundView.mas_centerX);
    }];
    
    [super updateConstraints];
}

- (void)setSPressValue:(NSString *)sPressValue {
    
    _sPressValue = sPressValue;
    
    // 设置显示值
    NSMutableAttributedString *mValue = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", sPressValue]];
    [mValue addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:NSMakeRange(0, sPressValue.length)];
    NSDictionary *dictionary = @{NSForegroundColorAttributeName:COLOR_PICTONBLUE, NSFontAttributeName:[UIFont systemFontOfSize:12]};
    NSMutableAttributedString *mUnit = [[NSMutableAttributedString alloc] initWithString:@" mmHg"];
    [mUnit addAttributes:dictionary range:NSMakeRange(0, 5)];
    [mValue appendAttributedString:mUnit];
    _sPressValueLbl.attributedText = mValue;
    
}

- (void)setDPressValue:(NSString *)dPressValue {
   
    // 设置显示值
    NSMutableAttributedString *mValue = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", dPressValue]];
    [mValue addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:NSMakeRange(0, dPressValue.length)];
    NSDictionary *dictionary = @{NSForegroundColorAttributeName:COLOR_PICTONBLUE, NSFontAttributeName:[UIFont systemFontOfSize:12]};
    NSMutableAttributedString *mUnit = [[NSMutableAttributedString alloc] initWithString:@" mmHg"];
    [mUnit addAttributes:dictionary range:NSMakeRange(0, 5)];
    [mValue appendAttributedString:mUnit];
    _dPressValueLbl.attributedText = mValue;
    
    NSInteger sbpValue = [_sPressValue integerValue];
    NSInteger dbpValue = [dPressValue integerValue];
    
    BOOL isNormal = [ZJIsNormalNormTool zj_isNormalBp:sbpValue dbp:dbpValue];
    if (isNormal) {
        _stateButton.selected = NO;
    } else {
        _stateButton.selected = YES;
    }
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
