
//  Created by zhoujian on 2018/8/31.
//  Copyright © 2018年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZJHistoryBPBOHBTableViewCell : UITableViewCell

@property(nonatomic, strong) UILabel *dateLbl;
@property(nonatomic, strong) UILabel *sbpLabel;     // 收缩压
@property(nonatomic, strong) UILabel *dbpLabel;   // 舒张压
@property(nonatomic, strong) UILabel *heartBeatLbl;
@property(nonatomic, strong) UILabel *oxyLbl;
@property(nonatomic, assign) NSInteger sbpValue;
@property(nonatomic, assign) NSInteger dbpValue;


@end
