
//  Created by zhoujian on 2018/11/2.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


/**
 数据模块 血压条形图
 */
@interface ZJBarChatBPTableViewCell : UITableViewCell

/** 图标 */
@property (nonatomic, strong) UIImageView *iconImageView;
/** 标题 */
@property (nonatomic, strong) UILabel *titleLabel;
/** 子标题 */
@property (nonatomic, strong) UILabel *subTitleLabel;
/** 时间 */
@property (nonatomic, strong) UILabel *timeLabel;
/** 收缩压数值 */
@property (nonatomic, assign) NSInteger sbpValue;
/** 收缩压数值 */
@property (nonatomic, assign) NSInteger dbpValue;
/** 收缩压正常或者异常的状态视图 */
@property (nonatomic, strong) UIButton *sbpStateButton;
/** 舒张压正常或者异常的状态视图 */
@property (nonatomic, strong) UIButton *dbpStateButton;
@end

NS_ASSUME_NONNULL_END
