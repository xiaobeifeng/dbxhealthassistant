
//  Created by zhoujian on 2018/9/13.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJDataReportTableViewCell.h"

@interface ZJDataReportTableViewCell()

@property(nonatomic, strong) UILabel *contentLbl;
@property(nonatomic, strong) UIView *view;
@end

@implementation ZJDataReportTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        
        _contentLbl = [UILabel new];
        _contentLbl.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _contentLbl.font = [UIFont systemFontOfSize:14];
        _contentLbl.numberOfLines = 0;
        [_contentLbl sizeToFit];
        [self.contentView addSubview:_contentLbl];
        
        _view = [UIView new];
        _view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        kViewRadiusBorder(_view, 5.0f, 1, COLOR_PICTONBLUE);
        [self.contentView addSubview:_view];
        
        [self.contentView bringSubviewToFront:_contentLbl];
    }
    
    return self;
}

+ (BOOL)requiresConstraintBasedLayout
{
    return YES;
}

- (void)updateConstraints
{
    [_contentLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).with.offset(-13);
        make.bottom.mas_equalTo(self.contentView).with.offset(-13);
        make.top.mas_equalTo(self.contentView).with.offset(13);
        make.left.mas_equalTo(self.contentView).with.offset(13);
    }];
    
    [_view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView).with.offset(-5);
        make.bottom.mas_equalTo(self.contentView).with.offset(-5);
        make.top.mas_equalTo(self.contentView).with.offset(5);
        make.left.mas_equalTo(self.contentView).with.offset(5);
    }];
    
    [super updateConstraints];
}

- (void)setContent:(NSString *)content
{
    _contentLbl.text = content;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
