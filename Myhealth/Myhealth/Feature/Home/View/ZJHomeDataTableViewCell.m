
//  Created by zhoujian on 2018/10/30.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJHomeDataTableViewCell.h"
#import "ZJHomeCellTitleButton.h"
#import "ZZCircleProgress.h"

@interface ZJHomeDataTableViewCell()


@property (nonatomic, strong) UIView *view;

@property (nonatomic, strong) ZJHomeCellTitleButton *titleButton;   // 标题View
@property (nonatomic, strong) ZZCircleProgress *bpProgressView;
@property (nonatomic, strong) ZZCircleProgress *bsProgressView;
@property (nonatomic, strong) UILabel *bpTitleLabel;    // 血压标题
@property (nonatomic, strong) UILabel *bsTitleLabel;    // 血糖标题
@property (nonatomic, strong) UIButton *detailButton;    // 查看详情按钮
@end

@implementation ZJHomeDataTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        // 设置cell样式
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = COLOR_GALLERY;
        
        self.view = [UIView new];
        self.view.backgroundColor = [UIColor whiteColor];
        kViewRadius(self.view, 5.0f);
        [self.contentView addSubview:self.view];
        
        self.titleButton = [[ZJHomeCellTitleButton alloc] initWithTitle:@"数据"];
        [self.view addSubview:self.titleButton];
        
        _bpProgressView = [[ZZCircleProgress alloc] initWithFrame:CGRectMake(0, 0, 100, 100) pathBackColor:COLOR_GALLERY pathFillColor:[UIColor colorWithRed:0.31 green:0.59 blue:1.00 alpha:1.00] startAngle:-90 strokeWidth:8];
        _bpProgressView.progressLabel.hidden = YES;
        _bpProgressView.showPoint = NO;
        _bpProgressView.prepareToShow = YES;//设置完进度条相关属性后需要设置此属性为YES，否则设置progress之前进度条不显示。
        [self.view addSubview:_bpProgressView];
        
        _bsProgressView = [[ZZCircleProgress alloc] initWithFrame:CGRectMake(0, 0, 100, 100) pathBackColor:COLOR_GALLERY pathFillColor:[UIColor colorWithRed:1.00 green:0.47 blue:0.49 alpha:1.00] startAngle:-90 strokeWidth:8];
        _bsProgressView.progressLabel.hidden = YES;
        _bsProgressView.showPoint = NO;
        _bsProgressView.prepareToShow = YES;//设置完进度条相关属性后需要设置此属性为YES，否则设置progress之前进度条不显示。
        [self.view addSubview:_bsProgressView];
        
        _bpTitleLabel = [UILabel new];
        _bpTitleLabel.text = @"血压（mmHg）";
        _bpTitleLabel.font = [UIFont systemFontOfSize:8];
        _bpTitleLabel.textColor = COLOR_BLACKGRAY;
        [_bpTitleLabel sizeToFit];
        [_bpProgressView addSubview:_bpTitleLabel];
        [_bpTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_bpProgressView.mas_centerY).with.offset(-15);
            make.centerX.equalTo(_bpProgressView.mas_centerX);
        }];
        
        _bpValueLabel = [UILabel new];
        _bpValueLabel.text = @"--";
        _bpValueLabel.font = [UIFont boldSystemFontOfSize:17];
        _bpValueLabel.textColor = COLOR_BLACKGRAY;
        [_bpValueLabel sizeToFit];
        [_bpProgressView addSubview:_bpValueLabel];
        [_bpValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_bpProgressView.mas_centerY).with.offset(5);
            make.centerX.equalTo(_bpProgressView.mas_centerX);
        }];
        
        _bsTitleLabel = [UILabel new];
        _bsTitleLabel.text = @"血糖（mmol/L）";
        _bsTitleLabel.font = [UIFont systemFontOfSize:8];
        _bsTitleLabel.textColor = COLOR_BLACKGRAY;
        [_bsTitleLabel sizeToFit];
        [_bsProgressView addSubview:_bsTitleLabel];
        [_bsTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_bsProgressView.mas_centerY).with.offset(-15);
            make.centerX.equalTo(_bsProgressView.mas_centerX);
        }];
        
        _bsValueLabel = [UILabel new];
        _bsValueLabel.text = @"--";
        _bsValueLabel.font = [UIFont boldSystemFontOfSize:17];
        _bsValueLabel.textColor = COLOR_BLACKGRAY;
        [_bsValueLabel sizeToFit];
        [_bsProgressView addSubview:_bsValueLabel];
        [_bsValueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_bsProgressView.mas_centerY).with.offset(5);
            make.centerX.equalTo(_bsProgressView.mas_centerX);
        }];
        
        _bpStateButton = [UIButton new];
        [_bpStateButton setTitle:@"数据正常" forState:UIControlStateNormal];
        [_bpStateButton setTitleColor:COLOR_PICTONBLUE forState:UIControlStateNormal];
        [_bpStateButton setTitle:@"数据异常" forState:UIControlStateSelected];
        [_bpStateButton setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
        _bpStateButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [_bpStateButton sizeToFit];
        [self.view addSubview:_bpStateButton];
        
        _bsStateButton = [UIButton new];
        [_bsStateButton setTitle:@"数据正常" forState:UIControlStateNormal];
        [_bsStateButton setTitleColor:COLOR_PICTONBLUE forState:UIControlStateNormal];
        [_bsStateButton setTitle:@"数据异常" forState:UIControlStateSelected];
        [_bsStateButton setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
        _bsStateButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [_bsStateButton sizeToFit];
        [self.view addSubview:_bsStateButton];
        
        self.detailButton = [UIButton new];
        [self.detailButton setTitle:@"查看详情" forState:UIControlStateNormal];
        [self.detailButton addTarget:self action:@selector(detailButtonAction) forControlEvents:UIControlEventTouchUpInside];
        [self.detailButton setTitleColor:COLOR_BLACKGRAY forState:UIControlStateNormal];
        self.detailButton.titleLabel.font = [UIFont systemFontOfSize:14];
        self.detailButton.adjustsImageWhenHighlighted = NO;
        [self.detailButton sizeToFit];
        [self.view addSubview:self.detailButton];
        
    }
    
    return self;
}


+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    
    [self.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(13);
        make.bottom.equalTo(self.contentView).with.offset(-13);
        make.left.equalTo(self.contentView).with.offset(13);
        make.right.equalTo(self.contentView).with.offset(-13);
    }];
    
    [self.titleButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(20);
        make.left.equalTo(self.view).with.offset(13);
        make.top.equalTo(self.view).with.offset(13);
    }];
    
    [_bpProgressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleButton.mas_bottom).with.offset(10);
        make.width.height.mas_equalTo(100);
        make.centerX.equalTo(self.view).with.offset(-70);
    }];
    
    [_bsProgressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleButton.mas_bottom).with.offset(10);
        make.width.height.mas_equalTo(100);
        make.centerX.equalTo(self.view).with.offset(70);
    }];
    
    
    [_bpStateButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_bsProgressView.mas_bottom).with.offset(5);
        make.centerX.equalTo(_bpProgressView.mas_centerX);
//        make.bottom.equalTo(self.view).with.offset(-13);
    }];
    [_bsStateButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_bsProgressView.mas_bottom).with.offset(5);
        make.centerX.equalTo(_bsProgressView.mas_centerX);
    }];
    
    [self.detailButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_bsStateButton.mas_bottom).with.offset(5);
        make.bottom.equalTo(self.view).with.offset(-13);
        make.centerX.equalTo(self.view.mas_centerX);
    }];
    
    [super updateConstraints];
}

- (void)detailButtonAction
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(homeDataTableViewCellDetailButtonAction)]) {
        [self.delegate homeDataTableViewCellDetailButtonAction];
    }
}

/**
 高压数值

 @param sbpValue 高压数值
 */
- (void)setSbpValue:(NSInteger)sbpValue {
    _sbpValue = sbpValue;
    _bpProgressView.progress = (double)sbpValue/300;
}

/**
 低压数值

 @param dbpValue 低压数值
 */
- (void)setDbpValue:(NSInteger)dbpValue {
    _dbpValue = dbpValue;
    
    BOOL isNormal = [ZJIsNormalNormTool zj_isNormalBp:_sbpValue dbp:_dbpValue];
    if (isNormal) {
        _bpStateButton.selected = NO;
    } else {
        _bpStateButton.selected = YES;
    }
}

/**
 血糖值

 @param bsValue 血糖值
 */
- (void)setBsValue:(double)bsValue {
    
    _bsValue = bsValue;
    
    _bsProgressView.progress = bsValue/33.0;
}

- (void)setBsCondition:(NSInteger)bsCondition {
    
    BOOL isNormal = [ZJIsNormalNormTool zj_isNormalBs:bsCondition bs:_bsValue];
    
    if (isNormal) {
        _bsStateButton.selected = NO;
    } else {
        _bsStateButton.selected = YES;
    }
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
