
//  Created by zhoujian on 2018/11/2.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJBarChatBPTableViewCell.h"

@interface ZJBarChatBPTableViewCell()
/** 白色背景 */
@property (nonatomic, strong) UIView *view;
/** 收缩压条形图 */
@property (nonatomic, strong) UIImageView *sbpImageView;
/** 舒张压条形图 */
@property (nonatomic, strong) UIImageView *dbpImageView;


@end

@implementation ZJBarChatBPTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // 设置cell样式
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = COLOR_GALLERY;
        
        self.view = [UIView new];
        self.view.backgroundColor = [UIColor whiteColor];
        kViewRadius(self.view, 5.0f);
        [self.contentView addSubview:self.view];
        
        self.iconImageView = [UIImageView new];
        self.iconImageView.backgroundColor = [UIColor clearColor];
        self.iconImageView.image = [UIImage imageNamed:@"health_bp_icon"];
        [self.view addSubview:self.iconImageView];
        
        self.titleLabel = [UILabel new];
        self.titleLabel.text = @"血压";
        self.titleLabel.font = [UIFont systemFontOfSize:14];
        self.titleLabel.textColor = [UIColor colorWithRed:0.39 green:0.63 blue:1.00 alpha:1.00];
        [self.titleLabel sizeToFit];
        [self.view addSubview:self.titleLabel];
        
        self.subTitleLabel = [UILabel new];
        self.subTitleLabel.text = @"-- mmHg";
        self.subTitleLabel.font = [UIFont systemFontOfSize:14];
        self.subTitleLabel.textColor = [UIColor colorWithRed:0.39 green:0.63 blue:1.00 alpha:1.00];
        [self.subTitleLabel sizeToFit];
        [self.view addSubview:self.subTitleLabel];
        [self.subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.titleLabel.mas_right).with.offset(13);
            make.centerY.equalTo(self.titleLabel.mas_centerY);
        }];
        
        self.timeLabel = [UILabel new];
        self.timeLabel.text = @"暂无时间";
        self.timeLabel.font = [UIFont systemFontOfSize:12];
        self.timeLabel.textColor = [UIColor lightGrayColor];
        [self.timeLabel sizeToFit];
        [self.view addSubview:self.timeLabel];
        
        self.sbpImageView = [UIImageView new];
        self.sbpImageView.backgroundColor = [UIColor clearColor];
        self.sbpImageView.image = [UIImage imageNamed:@"health_bp_line"];
        [self.view addSubview:self.sbpImageView];
        
        self.sbpStateButton = [UIButton new];
        self.sbpStateButton.backgroundColor = [UIColor clearColor];
        self.sbpStateButton.titleLabel.font = [UIFont systemFontOfSize:10];
        
        [self.sbpStateButton setTitle:@"数据正常" forState:UIControlStateNormal];
        [self.sbpStateButton setTitleColor:COLOR_PICTONBLUE forState:UIControlStateNormal];
        [self.sbpStateButton setImage:[UIImage imageNamed:@"button_normal"] forState:UIControlStateNormal];
        
        [self.sbpStateButton setTitle:@"数据异常" forState:UIControlStateSelected];
        [self.sbpStateButton setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
        [self.sbpStateButton setImage:[UIImage imageNamed:@"button_warn"] forState:UIControlStateSelected];
        
        [self.sbpStateButton sizeToFit];
        [self.view addSubview:self.sbpStateButton];
        
        self.dbpImageView = [UIImageView new];
        self.dbpImageView.backgroundColor = [UIColor clearColor];
        self.dbpImageView.image = [UIImage imageNamed:@"health_bp_line"];
        [self.view addSubview:self.dbpImageView];
        
        self.dbpStateButton = [UIButton new];
        self.dbpStateButton.backgroundColor = [UIColor clearColor];
        self.dbpStateButton.titleLabel.font = [UIFont systemFontOfSize:10];
        [self.dbpStateButton setTitle:@"数据正常" forState:UIControlStateNormal];
        [self.dbpStateButton setTitleColor:COLOR_PICTONBLUE forState:UIControlStateNormal];
        [self.dbpStateButton setImage:[UIImage imageNamed:@"button_normal"] forState:UIControlStateNormal];
        
        [self.dbpStateButton setTitle:@"数据异常" forState:UIControlStateSelected];
        [self.dbpStateButton setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
        [self.dbpStateButton setImage:[UIImage imageNamed:@"button_warn"] forState:UIControlStateSelected];
        
        [self.dbpStateButton sizeToFit];
        [self.view addSubview:self.dbpStateButton];
        
        // 设置约束
        [self.view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.mas_equalTo(13);
            make.right.bottom.mas_equalTo(-13);
        }];
        
        [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.equalTo(self.view).with.offset(13);
            make.width.height.mas_equalTo(20);
        }];
        
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.iconImageView.mas_right).with.offset(13);
            make.centerY.equalTo(self.iconImageView.mas_centerY);
            
        }];
        
        [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.iconImageView.mas_left);
            make.top.equalTo(self.iconImageView.mas_bottom).with.offset(13);
            
        }];
        
        [self.sbpImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.timeLabel.mas_bottom).with.offset(20);
            make.left.equalTo(self.view).with.offset(50);
            make.right.equalTo(self.view).with.offset(-50);
            make.height.mas_equalTo(10);
        }];
        
        [self.sbpStateButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.sbpImageView.mas_top);
            make.left.equalTo(self.sbpImageView.mas_left).with.offset(-5);
        }];
        
        [self.dbpImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.sbpImageView.mas_bottom).with.offset(30);
            make.left.equalTo(self.view).with.offset(50);
            make.right.equalTo(self.view).with.offset(-50);
            make.height.mas_equalTo(10);
            make.bottom.equalTo(self.view).with.offset(-13);
        }];
        
        [self.dbpStateButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.dbpImageView.mas_top);
            make.left.equalTo(self.sbpImageView.mas_left).with.offset(-5);
        }];
        
        UIImageView *detailArrow = [UIImageView new];
        detailArrow.image = [UIImage imageNamed:@"detail_arrow"];
        [self.view addSubview:detailArrow];
        [detailArrow mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(20);
            make.centerY.equalTo(self.titleLabel.mas_centerY);
            make.right.equalTo(self.view).with.offset(-13);
        }];
  
    }
    
    return self;
}

- (void)setSbpValue:(NSInteger)sbpValue {
    
    _sbpValue = sbpValue;
    
    float barLength = kScreenWidth-13*2-50*2;   // 条形图长度

    NSInteger leftPadding = barLength/300 * sbpValue - 5;       // sbp最大值300
    [self.sbpStateButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.sbpImageView.mas_left).with.offset(leftPadding);
    }];
    
    
    BOOL isNormal = [ZJIsNormalNormTool zj_isNormalSbp:_sbpValue];
    if (isNormal) {
        _sbpStateButton.selected = NO;
    } else {
        _sbpStateButton.selected = YES;
    }
    

}

- (void)setDbpValue:(NSInteger)dbpValue {
    
    _dbpValue = dbpValue;
    
    float barLength = kScreenWidth-13*2-50*2;  // 条形图长度
    
    NSInteger leftPadding = barLength/300 * dbpValue - 5;   // dbp最大值300， 但都用300，不然显示出来的两条数据显示有问题
    [self.dbpStateButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.sbpImageView.mas_left).with.offset(leftPadding);
    }];
    
    BOOL isNormal = [ZJIsNormalNormTool zj_isNormalDbp:dbpValue];
    if (isNormal) {
        _dbpStateButton.selected = NO;
    } else {
        _dbpStateButton.selected = YES;
    }
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
