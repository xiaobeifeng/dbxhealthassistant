//
//  Created by zhoujian on 2018/9/17.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZJReportHistoryTableViewCell : UITableViewCell

/** 时间 */
@property(nonatomic, strong) UILabel *timeLbl;
/** 血压 */
@property(nonatomic, strong) UILabel *bloodPressLbl;
/** 血氧 */
@property(nonatomic, strong) UILabel *bloodOxygenLbl;
/** 心率 */
@property(nonatomic, strong) UILabel *heartBeatLbl;
/** 血压等级 */
@property(nonatomic, strong) UILabel *bloodPressLevelLbl;

@end
