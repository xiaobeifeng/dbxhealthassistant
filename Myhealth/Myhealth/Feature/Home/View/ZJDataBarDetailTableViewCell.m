
//  Created by zhoujian on 2018/9/5.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJDataBarDetailTableViewCell.h"

@interface ZJDataBarDetailTableViewCell()


@property(nonatomic, strong) UILabel *valueLbl;
@property(nonatomic, strong) UIButton *stateButton;
@property(nonatomic, strong) UIView *contantBackgroundView;
@property(nonatomic, strong) UIView *lineView;
@property(nonatomic, strong) UIView *pointView;

@end

@implementation ZJDataBarDetailTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.contentView.backgroundColor = COLOR_GALLERY;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _contantBackgroundView = [UIView new];
        _contantBackgroundView.backgroundColor = [UIColor whiteColor];
        kViewRadius(_contantBackgroundView, 5.0f);
        [self.contentView addSubview:_contantBackgroundView];
        
        _dateLbl = [UILabel new];
        _dateLbl.font = [UIFont systemFontOfSize:12];
        _dateLbl.textColor = COLOR_PICTONBLUE;
        [self.contentView addSubview:_dateLbl];
        
        _valueLbl = [UILabel new];
        _valueLbl.backgroundColor = [UIColor clearColor];
        _valueLbl.textColor = COLOR_PICTONBLUE;
        _valueLbl.textAlignment = NSTextAlignmentCenter;
        [_contantBackgroundView addSubview:_valueLbl];
        
        _stateButton = [UIButton new];
        _stateButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [_contantBackgroundView addSubview:_stateButton];
        
        _lineView = [UIView new];
        _lineView.backgroundColor = COLOR_PICTONBLUE;
        [self.contentView addSubview:_lineView];
        
        _pointView = [UIView new];
        kViewRadius(_pointView, 5);
        _pointView.backgroundColor = COLOR_PICTONBLUE;
        [self.contentView addSubview:_pointView];
        
    }
    
    return self;
}

+ (BOOL)requiresConstraintBasedLayout
{
    return YES;
}

- (void)updateConstraints
{
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView);
        make.bottom.mas_equalTo(self.contentView);
        make.left.mas_equalTo(self.contentView).with.offset(34);
        make.width.mas_equalTo(2);
    }];
    
    [_pointView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(10);
        make.centerX.mas_equalTo(self.lineView.mas_centerX);
        make.centerY.mas_equalTo(self.lineView.mas_centerY);
    }];
    
    [_contantBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(13);
        make.left.mas_equalTo(70);
        make.right.mas_equalTo(-13);
        make.height.mas_equalTo(80);
        make.bottom.mas_equalTo(-13);
    }];
    
    [_dateLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contantBackgroundView);
        make.width.mas_equalTo(self.contantBackgroundView.mas_width).multipliedBy(0.9);
        make.height.mas_equalTo(20);
        make.centerX.mas_equalTo(self.contantBackgroundView.mas_centerX);
    }];
    
    [_valueLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.dateLbl.mas_bottom);
        make.width.mas_equalTo(self.contantBackgroundView.mas_width).multipliedBy(0.9);
        make.height.mas_equalTo(40);
        make.centerX.mas_equalTo(self.contantBackgroundView.mas_centerX);
    }];
    
    [_stateButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.valueLbl.mas_bottom);
        make.width.mas_equalTo(self.contantBackgroundView.mas_width).multipliedBy(0.9);
        make.height.mas_equalTo(20);
        make.centerX.mas_equalTo(self.contantBackgroundView.mas_centerX);
    }];
    
    [super updateConstraints];
}


/**
 心率值

 @param pulseValue 心率值
 */
- (void)setPulseValue:(NSString *)pulseValue
{
  
    // 设置显示值
    NSMutableAttributedString *mValue = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", pulseValue]];
    [mValue addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:NSMakeRange(0, pulseValue.length)];
    NSMutableAttributedString *mUnit = [[NSMutableAttributedString alloc] initWithString:@" bpm"];
    [mUnit addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(0, 4)];
    [mValue appendAttributedString:mUnit];
    _valueLbl.attributedText = mValue;
    
    [_stateButton setTitle:@"心率正常，请继续保持" forState:UIControlStateNormal];
    [_stateButton setTitleColor:COLOR_PICTONBLUE forState:UIControlStateNormal];
    [_stateButton setTitle:@"心率异常，请注意监测并咨询专业医生" forState:UIControlStateSelected];
    [_stateButton setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
    
 
    BOOL isNoraml = [ZJIsNormalNormTool zj_isNormalHb:[pulseValue integerValue]];
    if (isNoraml) {
        _stateButton.selected = NO;
    } else {
        _stateButton.selected = YES;
    }

}


- (void)setOxygenValue:(NSString *)oxygenValue
{
    // 设置显示值
    NSMutableAttributedString *mValue = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", oxygenValue]];
    [mValue addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:NSMakeRange(0, oxygenValue.length)];
    NSMutableAttributedString *mUnit = [[NSMutableAttributedString alloc] initWithString:@" %"];
    [mUnit addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(0, 2)];
    [mValue appendAttributedString:mUnit];
    _valueLbl.attributedText = mValue;

    [_stateButton setTitle:@"血氧正常，请继续保持" forState:UIControlStateNormal];
    [_stateButton setTitleColor:COLOR_PICTONBLUE forState:UIControlStateNormal];
    [_stateButton setTitle:@"血氧异常，请注意监测并咨询专业医生" forState:UIControlStateSelected];
    [_stateButton setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
    
    BOOL isNoraml = [ZJIsNormalNormTool zj_isNormalBo:[oxygenValue integerValue]];
    if (isNoraml) {
        _stateButton.selected = NO;
    } else {
        _stateButton.selected = YES;
    }
}

- (void)setBloodSugarValue:(NSString *)bloodSugarValue
{
    _bloodSugarValue = bloodSugarValue;

    NSString *reSetBloodSugar = [NSString stringWithFormat:@"%.1f", [bloodSugarValue doubleValue]];
    NSMutableAttributedString *mValue = [[NSMutableAttributedString alloc] initWithString:reSetBloodSugar];
    [mValue addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:NSMakeRange(0, reSetBloodSugar.length)];
    NSMutableAttributedString *mUnit = [[NSMutableAttributedString alloc] initWithString:@" mmol/L"];
    [mUnit addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(0, 7)];
    [mValue appendAttributedString:mUnit];
    _valueLbl.attributedText = mValue;
    
}

- (void)setBsCondition:(NSInteger)bsCondition {
   
    double bsValue = [_bloodSugarValue doubleValue];
    
    
    [_stateButton setTitle:@"血糖正常，请继续保持" forState:UIControlStateNormal];
    [_stateButton setTitleColor:COLOR_PICTONBLUE forState:UIControlStateNormal];
    [_stateButton setTitle:@"血糖异常，请注意监测并咨询专业医生" forState:UIControlStateSelected];
    [_stateButton setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
    
    BOOL isNoraml = [ZJIsNormalNormTool zj_isNormalBs:bsCondition bs:bsValue];
    if (isNoraml) {
        _stateButton.selected = NO;
    } else {
        _stateButton.selected = YES;
    }
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
