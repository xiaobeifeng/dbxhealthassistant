//
//  ZJBarChatHBTableViewCell.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/21.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJBarChatHBTableViewCell.h"
@interface ZJBarChatHBTableViewCell()
@property (nonatomic, strong) UIView *view;

@end

@implementation ZJBarChatHBTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // 设置cell样式
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = COLOR_GALLERY;
        
        self.view = [UIView new];
        self.view.backgroundColor = [UIColor whiteColor];
        kViewRadius(self.view, 5.0f);
        [self.contentView addSubview:self.view];
        
        self.iconImageView = [UIImageView new];
        self.iconImageView.backgroundColor = [UIColor clearColor];
        self.iconImageView.image = [UIImage imageNamed:@"health_pulse_icon"];
        [self.view addSubview:self.iconImageView];
        
        self.titleLabel = [UILabel new];
        self.titleLabel.text = @"心率";
        self.titleLabel.font = [UIFont systemFontOfSize:14];
        self.titleLabel.textColor = [UIColor colorWithRed:0.99 green:0.36 blue:0.46 alpha:1.00];
        [self.titleLabel sizeToFit];
        [self.view addSubview:self.titleLabel];
        
        self.subTitleLabel = [UILabel new];
        self.subTitleLabel.text = @"- bpm";
        self.subTitleLabel.font = [UIFont systemFontOfSize:14];
        self.subTitleLabel.textColor = [UIColor colorWithRed:0.99 green:0.36 blue:0.46 alpha:1.00];
        [self.subTitleLabel sizeToFit];
        [self.view addSubview:self.subTitleLabel];
        [self.subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.titleLabel.mas_right).with.offset(13);
            make.centerY.equalTo(self.titleLabel.mas_centerY);
        }];
        
        self.timeLabel = [UILabel new];
        self.timeLabel.font = [UIFont systemFontOfSize:12];
        self.timeLabel.textColor = [UIColor lightGrayColor];
        self.timeLabel.text = @"暂无时间";
        [self.timeLabel sizeToFit];
        [self.view addSubview:self.timeLabel];
        
        self.lineImageView = [UIImageView new];
        self.lineImageView.image = [UIImage imageNamed:@"health_pulse_line"];
        self.lineImageView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.lineImageView];
        
//        self.stateView = [UIButton new];
//        self.stateView.backgroundColor = [UIColor greenColor];
//        [self.view addSubview:self.stateView];
        
        [self.view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.mas_equalTo(13);
            make.right.bottom.mas_equalTo(-13);
        }];
        
        [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.equalTo(self.view).with.offset(13);
            make.width.height.mas_equalTo(20);
        }];
        
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.iconImageView.mas_right).with.offset(13);
            make.centerY.equalTo(self.iconImageView.mas_centerY);
            
        }];
        
        [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.iconImageView.mas_left);
            make.top.equalTo(self.iconImageView.mas_bottom).with.offset(13);
            
        }];
        
        [self.lineImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.timeLabel.mas_bottom).with.offset(10);
            make.left.equalTo(self.view).with.offset(50);
            make.right.equalTo(self.view).with.offset(-50);
            make.height.mas_equalTo(25);
            make.bottom.equalTo(self.view).with.offset(-13);
        }];
        
        UIImageView *detailArrow = [UIImageView new];
        detailArrow.image = [UIImage imageNamed:@"detail_arrow"];
        [self.view addSubview:detailArrow];
        [detailArrow mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(20);
            make.centerY.equalTo(self.titleLabel.mas_centerY);
            make.right.equalTo(self.view).with.offset(-13);
        }];
    }
    
    return self;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
