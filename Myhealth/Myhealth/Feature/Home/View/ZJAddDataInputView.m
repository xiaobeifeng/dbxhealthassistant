
//  Created by zhoujian on 2018/9/11.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJAddDataInputView.h"

@implementation ZJAddDataInputView

- (instancetype)initWithTitle:(NSString *)title unit:(NSString *)unit
{
    if (self = [super init]) {
        
        _textField = [UITextField new];
        _textField.backgroundColor = [UIColor whiteColor];
        _textField.keyboardType = UIKeyboardTypeDecimalPad;
        kViewRadiusBorder(_textField, 5.0f, 1, COLOR_GALLERY);
        UIImageView *leftView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 8, 8)];
        leftView.backgroundColor = [UIColor whiteColor];
        _textField.leftView = leftView;
        _textField.leftViewMode = UITextFieldViewModeAlways;
        [self addSubview:_textField];
        [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.mas_centerX);
            make.width.mas_equalTo(200);
            make.height.mas_equalTo(self.mas_height);
            make.centerY.mas_equalTo(self.mas_centerY);
        }];
        
        _titleLbl = [UILabel new];
        _titleLbl.backgroundColor = [UIColor whiteColor];
        _titleLbl.font = [UIFont systemFontOfSize:14];
        _titleLbl.text = title;
        [_titleLbl sizeToFit];
        [self addSubview:_titleLbl];
        [_titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.textField.mas_left);
            make.centerY.mas_equalTo(self.mas_centerY);
        }];
        
        _unitLbl = [UILabel new];
        _unitLbl.backgroundColor = [UIColor whiteColor];
        _unitLbl.font = [UIFont systemFontOfSize:14];
        _unitLbl.text = unit;
        [_unitLbl sizeToFit];
        [self addSubview:_unitLbl];
        [_unitLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.textField.mas_right).with.offset(5);
            make.centerY.mas_equalTo(self.mas_centerY);
        }];
        
    }
    return self;
}

@end
