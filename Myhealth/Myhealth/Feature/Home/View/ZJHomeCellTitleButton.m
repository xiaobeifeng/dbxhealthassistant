
//  Created by zhoujian on 2018/10/30.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJHomeCellTitleButton.h"

@interface ZJHomeCellTitleButton()

/** 深蓝色竖线 */
@property (nonatomic, strong) UIView *deepBlueLine;

@end

@implementation ZJHomeCellTitleButton


- (instancetype)initWithTitle:(NSString *)title {
    
    self = [super init];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        [self setTitle:title forState:UIControlStateNormal];
        [self setTitleColor:COLOR_BLACKGRAY forState:UIControlStateNormal];
        self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        self.userInteractionEnabled = NO;
        self.titleLabel.font = [UIFont systemFontOfSize:14];
        
        _deepBlueLine = [UIView new];
        _deepBlueLine.backgroundColor = COLOR_PICTONBLUE;
        kViewRadius(_deepBlueLine, 2.0f);
        [self addSubview:_deepBlueLine];
        [_deepBlueLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(5);
            make.height.mas_equalTo(self.mas_height);
            make.left.equalTo(self);
            make.centerY.equalTo(self.mas_centerY);
        }];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
