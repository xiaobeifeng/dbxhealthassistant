
//  Created by zhoujian on 2018/9/6.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZJDataBarBPDetailTableViewCell : UITableViewCell

@property(nonatomic, copy) NSString *sPressValue;
@property(nonatomic, copy) NSString *dPressValue;
@property(nonatomic, strong) UILabel *dateLbl;
@property(nonatomic, strong) UILabel *sPressValueLbl;
@property(nonatomic, strong) UILabel *dPressValueLbl;

@end
