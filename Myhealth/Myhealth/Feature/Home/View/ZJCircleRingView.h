
//  Created by zhoujian on 2018/10/30.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 圆环指示图
 */
@interface ZJCircleRingView : UIView

@property (nonatomic, assign) double toValue;
@property (nonatomic, strong) NSString *content;

- (instancetype)initWithTitle:(NSString *)title
             fromGradualColor:(NSString *)fromGradualColor
               toGradualColor:(NSString *)toGradualColor;
@end

NS_ASSUME_NONNULL_END
