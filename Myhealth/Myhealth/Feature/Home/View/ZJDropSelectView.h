//
//  ZJDropSelectView.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/13.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ZJDropSelectViewDelegate <NSObject>

- (void)selectIndex:(NSInteger)index title:(NSString *)title;

@end

@interface ZJDropSelectView : UIView

@property (nonatomic, assign) id<ZJDropSelectViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
