//
//  SMKJSelectDateView.m
//  SMKJSmartDiagnose
//
//  Created by zhoujian on 2018/7/11.
//  Copyright © 2018年 zhoujian. All rights reserved.
//

#import "SMKJSelectDateView.h"

@interface SMKJSelectDateView()

@property (nonatomic, strong) UILabel *dateLbl;

@end

@implementation SMKJSelectDateView


- (instancetype)initWithDate:(NSDate *)date
{
    if (self = [super init]) {
        
        UIButton *fromMonthBtn = [UIButton new];
//        fromMonthBtn.backgroundColor = [UIColor colorWithRed:0.89 green:0.89 blue:0.90 alpha:1.00];
        [fromMonthBtn setImage:[UIImage imageNamed:@"button_date_left"] forState:UIControlStateNormal];
        fromMonthBtn.tag = 100;
        [fromMonthBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:fromMonthBtn];
        [fromMonthBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).with.offset(20);
            make.width.equalTo(@40);
            make.height.equalTo(@40);
            make.centerY.equalTo(self.mas_centerY);
        }];
        
        UIButton *toMonthBtn = [UIButton new];
//        toMonthBtn.backgroundColor = [UIColor colorWithRed:0.89 green:0.89 blue:0.90 alpha:1.00];
        toMonthBtn.tag = 101;
        [toMonthBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [toMonthBtn setImage:[UIImage imageNamed:@"button_date_right"] forState:UIControlStateNormal];
        [self addSubview:toMonthBtn];
        [toMonthBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self).with.offset(-20);
            make.width.equalTo(@40);
            make.height.equalTo(@40);
            make.centerY.equalTo(self.mas_centerY);
        }];
        
        
        NSDateFormatter *formatterYearAndMonth = [[NSDateFormatter alloc] init];
        formatterYearAndMonth.dateFormat = @"YYYY年MM月";
        NSString *chooseDate = [formatterYearAndMonth stringFromDate:date];
        _dateLbl = [UILabel new];
        _dateLbl.text = chooseDate;
//        _dateLbl.characterSpace = 2.0f;
//        _dateLbl.font = kYaHeiFont(15);
        _dateLbl.textColor = [UIColor whiteColor];
        [_dateLbl sizeToFit];
        [self addSubview:_dateLbl];
        [_dateLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.mas_centerY);
            make.centerX.equalTo(self.mas_centerX);
        }];
        
//        UIButton *calendarBtn = [UIButton new];
//        calendarBtn.tag = 200;
//        calendarBtn.backgroundColor = [UIColor colorWithRed:0.68 green:0.68 blue:0.69 alpha:1.00];
//        [calendarBtn setImage:[UIImage imageNamed:@"calendar"] forState:UIControlStateNormal];
//        [calendarBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
//        kViewRadius(calendarBtn, 5.0f);
//        [self addSubview:calendarBtn];
//        [calendarBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(self.dateLbl.mas_right).with.offset(20);
//            make.centerY.equalTo(self.mas_centerY);
//            make.width.height.equalTo(@30);
//        }];
        
    }
    
    return self;
}

- (void)btnClick:(UIButton *)button
{
    NSLog(@"tag - %ld", button.tag);
    
    switch (button.tag) {
        case 100:
        {
            [self.delegate selectDateViewClick:MonthReduce];
        }
            break;
        case 101:
        {
            [self.delegate selectDateViewClick:MonthAdd];
        }
            break;
        case 200:
        {
            [self.delegate selectDateViewClick:CalendarShow];
        }
            break;
            
        default:
            break;
    }
    
}

- (void)setSelectDateString:(NSString *)selectDateString
{
    _dateLbl.text = selectDateString;
}


@end
