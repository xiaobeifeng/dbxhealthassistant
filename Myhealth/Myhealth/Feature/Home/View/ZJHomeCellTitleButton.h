
//  Created by zhoujian on 2018/10/30.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 首页 cell上的标题View
 */
@interface ZJHomeCellTitleButton : UIButton

- (instancetype)initWithTitle:(NSString *)title;

@end

NS_ASSUME_NONNULL_END
