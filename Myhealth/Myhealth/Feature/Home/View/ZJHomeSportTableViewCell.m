
//  Created by zhoujian on 2018/10/31.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJHomeSportTableViewCell.h"
#import "ZJHomeCellTitleButton.h"

@interface ZJHomeSportTableViewCell()
/** 圆角背景视图 */
@property (nonatomic, strong) UIView *view;
/** 标题View */
@property (nonatomic, strong) ZJHomeCellTitleButton *titleButton;
/** 步数标题 */
@property (nonatomic, strong) UILabel *stepTitleLable;

/** 消耗卡路里标题 */
@property (nonatomic, strong) UILabel *kcalTitleLable;

/** 进入详情页箭头 */
@property (nonatomic, strong) UIImageView *detailImageView;

@end

@implementation ZJHomeSportTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // 设置cell样式
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = COLOR_GALLERY;
        
        self.view = [UIView new];
        self.view.backgroundColor = [UIColor whiteColor];
        kViewRadius(self.view, 5.0f);
        [self.contentView addSubview:self.view];
        
        self.titleButton = [[ZJHomeCellTitleButton alloc] initWithTitle:@"运动"];
        [self.view addSubview:self.titleButton];
        
        self.stepTitleLable = [UILabel new];
        self.stepTitleLable.text = @"步数";
        self.stepTitleLable.textAlignment = NSTextAlignmentCenter;
        self.stepTitleLable.textColor = COLOR_BLACKGRAY;
        self.stepTitleLable.font = [UIFont systemFontOfSize:14];
        [self.view addSubview:self.stepTitleLable];
        
        self.kcalTitleLable = [UILabel new];
        self.kcalTitleLable.text = @"消耗卡路里";
        self.kcalTitleLable.textAlignment = NSTextAlignmentCenter;
        self.kcalTitleLable.textColor = COLOR_BLACKGRAY;
        self.kcalTitleLable.font = [UIFont systemFontOfSize:14];
        [self.view addSubview:self.kcalTitleLable];
        
        self.stepValueLable = [UILabel new];
        self.stepValueLable.text = @"--";
        self.stepValueLable.font = [UIFont boldSystemFontOfSize:25];
        self.stepValueLable.textColor = [UIColor colorWithHexString:@"#3BE2A5"];
        [self.stepValueLable sizeToFit];
        [self.view addSubview:self.stepValueLable];
        
        self.kcalValueLable = [UILabel new];
        self.kcalValueLable.text = @"--";
        self.kcalValueLable.textColor = [UIColor colorWithHexString:@"#FFA72A"];
        self.kcalValueLable.font = [UIFont boldSystemFontOfSize:25];
        [self.kcalValueLable sizeToFit];
        [self.view addSubview:self.kcalValueLable];
        
        self.timeLable = [UILabel new];
        self.timeLable.font = [UIFont systemFontOfSize:12];
        self.timeLable.textColor = COLOR_BLACKGRAY;
        self.timeLable.text = @"";
        [self.timeLable sizeToFit];
        [self.view addSubview:self.timeLable];
        
//        self.detailImageView = [UIImageView new];
//        self.detailImageView.backgroundColor = [UIColor redColor];
//        self.detailImageView.image = [UIImage imageNamed:@"detail_arrow"];
//        [self.view addSubview:self.detailImageView];
        
    }
    return self;
}

+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints
{
    [self.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(13);
        make.bottom.equalTo(self.contentView).with.offset(-13);
        make.left.equalTo(self.contentView).with.offset(13);
        make.right.equalTo(self.contentView).with.offset(-13);
    }];
    
    [self.titleButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(20);
        make.left.equalTo(self.view).with.offset(13);
        make.top.equalTo(self.view).with.offset(13);
    }];
    
    [@[_stepTitleLable, _kcalTitleLable] mas_distributeViewsAlongAxis:MASAxisTypeHorizontal
                           withFixedSpacing:20
                                leadSpacing:20
                                tailSpacing:20];
    [@[_stepTitleLable, _kcalTitleLable] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleButton.mas_bottom).with.offset(10);
    }];
    
    [self.stepValueLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.stepTitleLable.mas_centerX);
        make.top.equalTo(self.stepTitleLable.mas_bottom).with.offset(5);
        make.width.mas_lessThanOrEqualTo(self.stepTitleLable.mas_width);
    }];
    
    [self.kcalValueLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.kcalTitleLable.mas_centerX);
        make.top.equalTo(self.kcalTitleLable.mas_bottom).with.offset(5);
        make.width.mas_lessThanOrEqualTo(self.kcalTitleLable.mas_width);
    }];
    
    [self.timeLable mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.kcalValueLable.mas_bottom).with.offset(13);
        make.bottom.equalTo(self.view).with.offset(-13);
        make.right.equalTo(self.view).with.offset(-13);
    }];
    
//    [self.detailImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(self.view);
//        make.right.equalTo(self.view).with.offset(-13);
//        make.width.height.mas_equalTo(20);
//    }];
    
    [super updateConstraints];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
