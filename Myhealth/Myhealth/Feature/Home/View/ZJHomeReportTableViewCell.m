//
//  AIDEHomeReportTableViewCell.m
//  HealthAide
//
//  Created by zhoujian on 2018/10/31.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJHomeReportTableViewCell.h"
#import "ZJHomeCellTitleButton.h"

@interface ZJHomeReportTableViewCell()

/** 圆角背景视图 */
@property (nonatomic, strong) UIView *view;
/** 标题View */
@property (nonatomic, strong) ZJHomeCellTitleButton *titleButton;
@property (nonatomic, strong) UIButton *weekReportButton;
@property (nonatomic, strong) UIButton *monthReportButton;
@property (nonatomic, strong) UIButton *yearReportButton;

@end

@implementation ZJHomeReportTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // 设置cell样式
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = COLOR_GALLERY;
        
        self.view = [UIView new];
        self.view.backgroundColor = [UIColor whiteColor];
        kViewRadius(self.view, 5.0f);
        [self.contentView addSubview:self.view];
        
        self.titleButton = [[ZJHomeCellTitleButton alloc] initWithTitle:@"报告"];
        [self.view addSubview:self.titleButton];
        
        self.weekReportButton = [UIButton new];
        [self.weekReportButton setTitle:@"周报告" forState:UIControlStateNormal];
        [self.weekReportButton setImage:[UIImage imageNamed:@"button_week_nor"] forState:UIControlStateNormal];
        [self.weekReportButton setTitleColor:COLOR_PICTONBLUE forState:UIControlStateNormal];
        self.weekReportButton.titleLabel.font = [UIFont systemFontOfSize:14];
        self.weekReportButton.tag = 100;
        [self.weekReportButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.weekReportButton layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleLeft imageTitleSpace:10];
        
        self.monthReportButton = [UIButton new];
        [self.monthReportButton setTitle:@"月报告" forState:UIControlStateNormal];
        [self.monthReportButton setImage:[UIImage imageNamed:@"button_month_nor"] forState:UIControlStateNormal];
        [self.monthReportButton setTitleColor:COLOR_PICTONBLUE forState:UIControlStateNormal];
        self.monthReportButton.titleLabel.font = [UIFont systemFontOfSize:14];
        self.monthReportButton.tag = 101;
        [self.monthReportButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.monthReportButton layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleLeft imageTitleSpace:10];
        
        self.yearReportButton = [UIButton new];
        [self.yearReportButton setTitle:@"年报告" forState:UIControlStateNormal];
        [self.yearReportButton setImage:[UIImage imageNamed:@"button_year_nor"] forState:UIControlStateNormal];
        [self.yearReportButton setTitleColor:COLOR_PICTONBLUE forState:UIControlStateNormal];
        self.yearReportButton.tag = 102;
        [self.yearReportButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        self.yearReportButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [self.yearReportButton layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleLeft imageTitleSpace:10];
        
        // 取消点击高亮
        self.weekReportButton.adjustsImageWhenHighlighted = NO;
        self.monthReportButton.adjustsImageWhenHighlighted = NO;
        self.yearReportButton.adjustsImageWhenHighlighted = NO;
        
        [self.view addSubview:self.weekReportButton];
        [self.view addSubview:self.monthReportButton];
        [self.view addSubview:self.yearReportButton];
        
    }
    
    return self;
}

+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints
{
    [self.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(13);
        make.bottom.equalTo(self.contentView).with.offset(-13);
        make.left.equalTo(self.contentView).with.offset(13);
        make.right.equalTo(self.contentView).with.offset(-13);
    }];
    
    [self.titleButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(20);
        make.left.equalTo(self.view).with.offset(13);
        make.top.equalTo(self.view).with.offset(13);
    }];
    
    NSArray *buttons = @[self.weekReportButton, self.monthReportButton, self.yearReportButton];
    // 水平方向控件间隔固定等间隔
    [buttons mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:15 leadSpacing:20 tailSpacing:20];
    [buttons mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleButton.mas_bottom).with.offset(10);
        make.height.mas_equalTo(40);
        make.bottom.equalTo(self.view).with.offset(-13);
    }];
    
    [super updateConstraints];
}

- (void)buttonAction:(UIButton *)button {
    [self.delegate homeReportDataTableViewCellDetailButtonAction:button];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
