//
//  ZJBarChatHBTableViewCell.h
//  Myhealth
//
//  Created by zhoujian on 2018/11/21.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 心率cell
 */
@interface ZJBarChatHBTableViewCell : UITableViewCell

/** 图标 */
@property (nonatomic, strong) UIImageView *iconImageView;
/** 标题 */
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *subTitleLabel;
/** 条形图 */
@property (nonatomic, strong) UIImageView *lineImageView;
/** 时间 */
@property (nonatomic, strong) UILabel *timeLabel;

@end

NS_ASSUME_NONNULL_END
