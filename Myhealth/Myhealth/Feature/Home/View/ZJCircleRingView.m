
//  Created by zhoujian on 2018/10/30.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJCircleRingView.h"

/** 圆环半径 */
static const float CircleRingRadius = 45;
/** 圆环线宽 */
static const float CircleRingLineWidth = 8;
/** 圆环占位背景色 */
#define CirclePlaceholderColor [UIColor colorWithHexString:@"#DCE3E9"]

@interface ZJCircleRingView()

@property (nonatomic, strong) CAShapeLayer *shapelayer;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) NSString *fromGradualColor;
@property (nonatomic, strong) NSString *toGradualColor;
@property (nonatomic, strong) NSMutableArray *leftLayerColors;
@property (nonatomic, strong) NSMutableArray *rightLayerColors;
@end

@implementation ZJCircleRingView

- (NSMutableArray *)leftLayerColors
{
    if (!_leftLayerColors) {
        _leftLayerColors = [NSMutableArray new];
    }
    return _leftLayerColors;
}

- (NSMutableArray *)rightLayerColors
{
    if (!_rightLayerColors) {
        _rightLayerColors = [NSMutableArray new];
    }
    return _rightLayerColors;
}

- (instancetype)initWithTitle:(NSString *)title
             fromGradualColor:(NSString *)fromGradualColor
               toGradualColor:(NSString *)toGradualColor {
    
    self = [super init];
    if (self) {

        _titleLabel = [UILabel new];
        _titleLabel.text = title;
        _titleLabel.font = [UIFont systemFontOfSize:9];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.textColor = COLOR_BLACKGRAY;
        [_titleLabel sizeToFit];
        [self addSubview:_titleLabel];
        
        _contentLabel = [UILabel new];
        _contentLabel.text = @"-";
        _contentLabel.font = [UIFont systemFontOfSize:18];
        _contentLabel.textAlignment = NSTextAlignmentCenter;
        _contentLabel.textColor = COLOR_BLACKGRAY;
        [_contentLabel sizeToFit];
        [self addSubview:_contentLabel];
        
        _fromGradualColor = fromGradualColor;
        _toGradualColor = toGradualColor;
        
        [self.leftLayerColors addObject:(id)[UIColor colorWithHexString:_toGradualColor].CGColor];
        [self.leftLayerColors addObject:(id)[UIColor colorWithHexString:_toGradualColor].CGColor];
        
        [self.rightLayerColors addObject:(id)[UIColor colorWithHexString:_fromGradualColor].CGColor];
        [self.rightLayerColors addObject:(id)[UIColor colorWithHexString:_toGradualColor].CGColor];
        
    }
    return self;
}

- (void)layoutSubviews {
    
    [super layoutSubviews];

    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.centerY.equalTo(self).with.offset(-10);
    }];
    
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.centerY.equalTo(self).with.offset(10);
    }];
}

- (void)drawRect:(CGRect)rect {
    
    CAShapeLayer *layer = [CAShapeLayer layer];
    layer.frame = self.bounds;
    [self.layer addSublayer:layer];
    
    // 绘制圆环
    CGFloat width = CGRectGetWidth(self.bounds);
    CGFloat height = CGRectGetHeight(self.bounds);
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(width/2, height/2) radius:CircleRingRadius startAngle:0 endAngle:2*M_PI clockwise:YES];
    layer.path = path.CGPath;
    layer.fillColor = [UIColor clearColor].CGColor;
    layer.strokeColor = CirclePlaceholderColor.CGColor;
    layer.lineWidth = CircleRingLineWidth;
    layer.lineCap = kCALineCapRound;
    
    [self drawColorLayer];
}

// 设置整个view渐变色
- (void)drawColorLayer {
    CAShapeLayer *layer = [CAShapeLayer layer];
    layer.frame = self.bounds;
    [self.layer addSublayer:layer];
    
    CGFloat width = CGRectGetWidth(self.bounds);
    CGFloat height = CGRectGetHeight(self.bounds);
    CAGradientLayer *leftLayer = [CAGradientLayer layer];
    leftLayer.frame = CGRectMake(0, 0, width/2, height);
    leftLayer.locations = @[@0.0, @1.0];
    leftLayer.colors = self.leftLayerColors;
    [layer addSublayer:leftLayer];
    
    CAGradientLayer *rightLayer = [CAGradientLayer layer];
    rightLayer.frame = CGRectMake(width/2, 0, width/2, height);
    rightLayer.locations = @[@0.0, @1.0];
    rightLayer.colors = self.rightLayerColors;
    [layer addSublayer:rightLayer];
    
    CAShapeLayer *maskLayer = [self buildMaskLayer];
    layer.mask = maskLayer;
}

// 创建一个渐变色的环形的遮罩
- (CAShapeLayer *)buildMaskLayer {
    self.shapelayer = [CAShapeLayer layer];
    self.shapelayer.frame = self.bounds;
    self.shapelayer.lineWidth = CircleRingLineWidth;
    CGFloat width = CGRectGetWidth(self.bounds);
    CGFloat height = CGRectGetHeight(self.bounds);
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(width/2, height/2) radius:CircleRingRadius startAngle:-0.5f*M_PI endAngle:1.5f*M_PI clockwise:YES];
    self.shapelayer.path = path.CGPath;
    self.shapelayer.fillColor = [UIColor clearColor].CGColor;
    self.shapelayer.strokeColor = [UIColor blueColor].CGColor;
//    self.shapelayer.lineCap = kCALineCapSquare;
    
    CABasicAnimation *pathAnima = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnima.duration = 2.0f;
    pathAnima.fromValue = @0;
    pathAnima.toValue = @(self.toValue);
    pathAnima.removedOnCompletion = NO;
    pathAnima.fillMode = kCAFillModeForwards;
    [self.shapelayer addAnimation:pathAnima forKey:@"strokeEndAnimation"];
    
    return self.shapelayer;
}

- (void)setToValue:(double)toValue
{
    _toValue = toValue;
    
    [self setNeedsDisplay];
}

- (void)setContent:(NSString *)content
{
    if (content) {
        _content = content;
        _contentLabel.text = content;
    }
}
@end
