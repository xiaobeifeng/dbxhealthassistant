
//  Created by zhoujian on 2018/9/5.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZJDataBarDetailTableViewCell : UITableViewCell

@property(nonatomic, copy) NSString *pulseValue;
@property(nonatomic, copy) NSString *oxygenValue;
@property(nonatomic, copy) NSString *bloodSugarValue;
@property(nonatomic, assign) NSInteger bsCondition;
@property(nonatomic, strong) UILabel *dateLbl;
@end
