
//  Created by zhoujian on 2018/10/31.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 首页报告自定义cell
 */

@protocol ZJHomeReportTableViewCellDelegate <NSObject>

@required
- (void)homeReportDataTableViewCellDetailButtonAction:(UIButton *)button;

@end

@interface ZJHomeReportTableViewCell : UITableViewCell

@property (nonatomic, assign) id<ZJHomeReportTableViewCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
