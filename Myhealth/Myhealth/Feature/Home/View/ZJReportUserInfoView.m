
//  Created by zhoujian on 2018/9/12.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJReportUserInfoView.h"

@interface ZJReportUserInfoView()

@property(nonatomic, strong) UILabel *nameLbl;
@property(nonatomic, strong) UILabel *sexLbl;
@property(nonatomic, strong) UILabel *ageLbl;

@end

@implementation ZJReportUserInfoView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.userInteractionEnabled = YES;
        
        _dateButton = [[UIButton alloc] init];
        _dateButton.backgroundColor = [UIColor clearColor];
        _dateButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_dateButton setImage:[UIImage imageNamed:@"button_ calendar"] forState:UIControlStateNormal];
        [self addSubview:_dateButton];
        [_dateButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.height.mas_equalTo(40);
            make.width.equalTo(self);
        }];
        
        _bloodPressLbl = [UILabel new];
        _bloodPressLbl.text = @"血压：-- mmHg";
        _bloodPressLbl.textColor = [UIColor whiteColor];
        _bloodPressLbl.font = [UIFont systemFontOfSize:14];
        [_bloodOxygenLbl sizeToFit];
        [self addSubview:_bloodPressLbl];
        [_bloodPressLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.dateButton.mas_bottom).with.offset(10);
            make.centerX.mas_equalTo(self.mas_centerX);
            
        }];
        
        _bloodOxygenLbl = [UILabel new];
        _bloodOxygenLbl.text = @"血氧：-- %";
        _bloodOxygenLbl.textColor = [UIColor whiteColor];
        _bloodOxygenLbl.font = [UIFont systemFontOfSize:14];
        [self addSubview:_bloodOxygenLbl];
        [_bloodOxygenLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.bloodPressLbl.mas_bottom).with.offset(10);
            make.left.mas_equalTo(self.bloodPressLbl.mas_left);
            
        }];
        
        _heartBeatLbl = [UILabel new];
        _heartBeatLbl.text = @"心率：-- 次/分";
        _heartBeatLbl.textColor = [UIColor whiteColor];
        _heartBeatLbl.font = [UIFont systemFontOfSize:14];
        [self addSubview:_heartBeatLbl];
        [_heartBeatLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.bloodOxygenLbl.mas_bottom).with.offset(10);
            make.left.mas_equalTo(self.bloodPressLbl.mas_left);
        }];
        
        _maxValueBtn = [UIButton new];
        _maxValueBtn.tag = 100;
//        [_maxValueBtn addTarget:self action:@selector(handleMaxBtnEvent:) forControlEvents:UIControlEventTouchUpInside];
        [_maxValueBtn setTitle:@"最大值" forState:UIControlStateNormal];
        [_maxValueBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_maxValueBtn setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor]] forState:UIControlStateNormal];
        [_maxValueBtn setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"#4083C2"]] forState:UIControlStateSelected];
        kViewRadius(_maxValueBtn, 5.0f);
        _maxValueBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        
        
        _maxValueBtn.selected = YES;

        [self addSubview:_maxValueBtn];
        
        _averageValueBtn = [UIButton new];
        _averageValueBtn.tag = 200;
        [_averageValueBtn setTitle:@"平均值" forState:UIControlStateNormal];
//        [_averageValueBtn addTarget:self action:@selector(handleAverageBtnEvent:) forControlEvents:UIControlEventTouchUpInside];
        _averageValueBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [_averageValueBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_averageValueBtn setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor]] forState:UIControlStateNormal];
        [_averageValueBtn setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"#4083C2"]] forState:UIControlStateSelected];
        kViewRadius(_averageValueBtn, 5.0f);
        [self addSubview:_averageValueBtn];
        
        _minValueBtn = [UIButton new];
        _minValueBtn.tag = 300;
        [_minValueBtn setTitle:@"最小值" forState:UIControlStateNormal];
//        [_minValueBtn addTarget:self action:@selector(handleMinBtnEvent:) forControlEvents:UIControlEventTouchUpInside];
        _minValueBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [_minValueBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_minValueBtn setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor]] forState:UIControlStateNormal];
        [_minValueBtn setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"#4083C2"]] forState:UIControlStateSelected];
        kViewRadius(_minValueBtn, 5.0f);
        [self addSubview:_minValueBtn];
        
        [@[_maxValueBtn, _averageValueBtn, _minValueBtn] mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedItemLength:70 leadSpacing:20 tailSpacing:20];
        
        [@[_maxValueBtn, _averageValueBtn, _minValueBtn] mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.heartBeatLbl.mas_bottom).with.offset(40);
            make.height.equalTo(@25);
        }];
        
        
    }
    
    return self;
}

- (void)setName:(NSString *)name
{
    if (kStringIsEmpty(name)) {
        _nameLbl.text = @"姓名：-";
    } else {
        _nameLbl.text = [NSString stringWithFormat:@"姓名：%@", name];
    }
}

- (void)setAge:(NSString *)age
{
    if (kStringIsEmpty(age)) {
        _ageLbl.text = @"年龄：-";
    } else {
        _ageLbl.text = [NSString stringWithFormat:@"年龄：%@", age];
    }
}

- (void)setSex:(NSString *)sex
{
    if (kStringIsEmpty(sex)) {
        _sexLbl.text = @"性别：-";
    } else {
        _sexLbl.text = [NSString stringWithFormat:@"性别：%@", sex];
    }
}



- (void)handleMaxBtnEvent:(UIButton *)button
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(handleMaxBtnEvent)]) {
        
        kViewRadiusBorder(_maxValueBtn, 8.0f, 1, [UIColor whiteColor]);
        kViewRadiusBorder(_averageValueBtn, 0.0f, 0, [UIColor whiteColor]);
        kViewRadiusBorder(_minValueBtn, 0.0f, 0, [UIColor whiteColor]);
//        _maxValueBtn.selected = YES;
//        _averageValueBtn.selected = NO;
//        _minValueBtn.selected = NO;
        [self.delegate handleMaxBtnEvent];
    }
}

- (void)handleAverageBtnEvent:(UIButton *)button
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(handleAverageBtnEvent)]) {
        
//        _maxValueBtn.selected = NO;
//        _averageValueBtn.selected = YES;
//        _minValueBtn.selected = NO;
        kViewRadiusBorder(_maxValueBtn, 0.0f, 0, [UIColor whiteColor]);
        kViewRadiusBorder(_averageValueBtn, 8.0f, 1, [UIColor whiteColor]);
        kViewRadiusBorder(_minValueBtn, 0.0f, 0, [UIColor whiteColor]);
        [self.delegate handleAverageBtnEvent];
    }
}

- (void)handleMinBtnEvent:(UIButton *)button
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(handleMinBtnEvent)]) {
        
//        _maxValueBtn.selected = NO;
//        _averageValueBtn.selected = NO;
//        _minValueBtn.selected = YES;
        kViewRadiusBorder(_maxValueBtn, 0.0f, 0, [UIColor whiteColor]);
        kViewRadiusBorder(_averageValueBtn, 0.0f, 0, [UIColor whiteColor]);
        kViewRadiusBorder(_minValueBtn, 8.0f, 1, [UIColor whiteColor]);
        [self.delegate handleMinBtnEvent];
    }
}


@end
