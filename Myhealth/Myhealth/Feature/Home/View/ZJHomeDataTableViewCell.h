
//  Created by zhoujian on 2018/10/30.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ZJHomeDataTableViewCellDelegate <NSObject>

@required
- (void)homeDataTableViewCellDetailButtonAction;


@end

/**
 首页数据自定义cell
 */
@interface ZJHomeDataTableViewCell : UITableViewCell

@property (nonatomic, assign) id<ZJHomeDataTableViewCellDelegate> delegate;

@property (nonatomic, strong) UILabel *bpValueLabel;    // 血压数值Label
@property (nonatomic, strong) UILabel *bsValueLabel;    // 血糖数值Label
@property (nonatomic, assign) NSInteger sbpValue;    // 高压数值
@property (nonatomic, assign) NSInteger dbpValue;    // 低压数值
@property (nonatomic, assign) double bsValue;    // 血糖数值
@property (nonatomic, assign) NSInteger bsCondition;    // 血糖测量时间点
@property (nonatomic, strong) UIButton *bpStateButton;   // 血压状态
@property (nonatomic, strong) UIButton *bsStateButton;   // 血糖状态

@end

NS_ASSUME_NONNULL_END
