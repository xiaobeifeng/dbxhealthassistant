//
//  ZJDropSelectView.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/13.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "ZJDropSelectView.h"
#import "ZJDropSelectTableViewCell.h"

@interface ZJDropSelectView()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, copy) NSArray *titleArray;

@end

@implementation ZJDropSelectView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        _titleArray = @[@"空腹", @"早餐前", @"早餐后", @"午餐前", @"午餐后", @"晚餐前", @"晚餐后", @"睡前"];
        
        _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerClass:[ZJDropSelectTableViewCell class] forCellReuseIdentifier:NSStringFromClass([ZJDropSelectTableViewCell class])];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_tableView];
        
    }
    return self;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _titleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZJDropSelectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZJDropSelectTableViewCell class]) forIndexPath:indexPath];
    cell.titleLabel.text = _titleArray[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%ld", (long)indexPath.row);
    [self.delegate selectIndex:indexPath.row title:_titleArray[indexPath.row]];
}

@end
