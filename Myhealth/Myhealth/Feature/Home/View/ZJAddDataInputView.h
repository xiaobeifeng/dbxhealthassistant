
//  Created by zhoujian on 2018/9/11.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZJAddDataInputView : UIView

- (instancetype)initWithTitle:(NSString *)title unit:(NSString *)unit;

@property(nonatomic, strong) UITextField *textField;
@property(nonatomic, strong) UILabel *titleLbl;
@property(nonatomic, strong) UILabel *unitLbl;

@end
