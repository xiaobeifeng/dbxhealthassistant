
//  Created by zhoujian on 2018/11/3.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZJBarChatBSTableViewCell : UITableViewCell

/** 图标 */
@property (nonatomic, strong) UIImageView *iconImageView;
/** 标题 */
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *subTitleLabel;
/** 条形图 */
@property (nonatomic, strong) UIImageView *lineImageView;
/** 时间 */
@property (nonatomic, strong) UILabel *timeLabel;
/** 数值 */
@property (nonatomic, assign) double bsValue;
@property (nonatomic, assign) NSInteger bsCondition;  // 血糖测量时间段

@property (nonatomic, assign) NSInteger boValue;
@property (nonatomic, strong) UIButton *stateView;
@end

NS_ASSUME_NONNULL_END
