//
//  SMKJSelectDateView.h
//  SMKJSmartDiagnose
//
//  Created by zhoujian on 2018/7/11.
//  Copyright © 2018年 zhoujian. All rights reserved.
//

#import <UIKit/UIKit.h>


/**
 月份切换栏
 */
typedef NS_ENUM(NSInteger, SMKJSelectDateBtnType) {
    MonthReduce,    // 月减少
    MonthAdd,        // 月增加
    CalendarShow    // 日历显示
};

@protocol SMKJSelectDateViewDelegate <NSObject>

/**
 按钮点击

 @param type 按钮所对应的功能
 */
- (void)selectDateViewClick:(SMKJSelectDateBtnType)type;

@end

@interface SMKJSelectDateView : UIView

@property (nonatomic, assign) id<SMKJSelectDateViewDelegate> delegate;

/// 日期展示（NSString）
@property (nonatomic, copy) NSString *selectDateString;

/**
 初始化

 @param date 当前日期（NSDate）
 @return SMKJSelectDateView
 */
- (instancetype)initWithDate:(NSDate *)date;



@end
