
//  Created by zhoujian on 2018/8/31.
//  Copyright © 2018年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZJHistoryBSTableViewCell : UITableViewCell

@property(nonatomic, strong) UILabel *dateLbl;
@property(nonatomic, assign) double bsValue;
@property(nonatomic, assign) NSInteger condition;
@property(nonatomic, strong) UILabel *bloodSugarLbl;

@end
