
//  Created by zhoujian on 2018/9/17.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJReportHistoryTableViewCell.h"

@implementation ZJReportHistoryTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _timeLbl = [UILabel new];
        _timeLbl.font = [UIFont systemFontOfSize:10];
        [self.contentView addSubview:_timeLbl];
        
        _bloodPressLbl = [UILabel new];
        _bloodPressLbl.font = [UIFont systemFontOfSize:10];
        [self.contentView addSubview:_bloodPressLbl];
        
        _bloodOxygenLbl = [UILabel new];
        _bloodOxygenLbl.font = [UIFont systemFontOfSize:10];
        [self.contentView addSubview:_bloodOxygenLbl];
        
        _heartBeatLbl = [UILabel new];
        _heartBeatLbl.font = [UIFont systemFontOfSize:10];
        [self.contentView addSubview:_heartBeatLbl];
        
        _bloodPressLevelLbl = [UILabel new];
        _bloodPressLevelLbl.numberOfLines = 0;
        _bloodPressLevelLbl.font = [UIFont systemFontOfSize:10];
        [self.contentView addSubview:_bloodPressLevelLbl];
        
//        _timeLbl.backgroundColor = [UIColor orangeColor];
//        _bloodPressLbl.backgroundColor = [UIColor orangeColor];
//        _bloodOxygenLbl.backgroundColor = [UIColor orangeColor];
//        _heartBeatLbl.backgroundColor = [UIColor orangeColor];
//        _bloodPressLevelLbl.backgroundColor = [UIColor orangeColor];
        
        _timeLbl.textAlignment = NSTextAlignmentCenter;
        _bloodPressLbl.textAlignment = NSTextAlignmentCenter;
        _bloodOxygenLbl.textAlignment = NSTextAlignmentCenter;
        _heartBeatLbl.textAlignment = NSTextAlignmentCenter;
        _bloodPressLevelLbl.textAlignment = NSTextAlignmentCenter;

    }
    return self;
}

+ (BOOL)requiresConstraintBasedLayout
{
    return YES;
}

- (void)updateConstraints
{
    [@[_timeLbl, _bloodPressLbl, _bloodOxygenLbl, _heartBeatLbl, _bloodPressLevelLbl] mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:5 leadSpacing:13 tailSpacing:13];
    
    [_bloodPressLevelLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.contentView).with.offset(13);
        make.bottom.mas_equalTo(self.contentView).with.offset(-13);
    }];
    
    [@[_timeLbl, _bloodPressLbl, _bloodOxygenLbl, _heartBeatLbl] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.bloodPressLevelLbl.mas_centerY);
    }];
    
    [super updateConstraints];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
