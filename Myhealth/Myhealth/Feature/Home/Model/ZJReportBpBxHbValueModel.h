
//  Created by zhoujian on 2018/9/17.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZJReportBpBxHbValueModelData :NSObject
@property (nonatomic , copy) NSString              * upsaveTime;
@property (nonatomic , copy) NSString              * pulse;
@property (nonatomic , copy) NSString              * sPressure;
@property (nonatomic , copy) NSString              * dPressure;
@property (nonatomic , copy) NSString              * oxygen;

@end

@interface ZJReportBpBxHbValueModel :NSObject
@property (nonatomic , copy) NSString              * message;
@property (nonatomic , copy) NSArray<ZJReportBpBxHbValueModelData *>              * data;
@property (nonatomic , copy) NSString              * state;

@end
