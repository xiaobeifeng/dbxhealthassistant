//
//  DBXBpBsHbHistoryModel.h
//  DBXHealthAssistant
//
//  Created by zhoujian on 2018/8/31.
//  Copyright © 2018年 mc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZJHistoryBPBSHBModelData :NSObject
@property (nonatomic , copy) NSString              * upsaveTime;
@property (nonatomic , copy) NSString              * Id;
@property (nonatomic , copy) NSString              * pulse;
@property (nonatomic , copy) NSString              * sPressure;
@property (nonatomic , copy) NSString              * dPressure;
@property (nonatomic , copy) NSString              * oxygen;

@end

@interface ZJHistoryBPBSHBModel :NSObject
@property (nonatomic , copy) NSString              * message;
@property (nonatomic , copy) NSArray<ZJHistoryBPBSHBModelData *>              * data;
@property (nonatomic , copy) NSString              * state;

@end
