
//  Created by zhoujian on 2018/8/31.
//  Copyright © 2018年 mc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZJBloodSugarModelData :NSObject
@property (nonatomic , copy) NSString              * upsaveTime;
@property (nonatomic , copy) NSString              * Id;
@property (nonatomic , copy) NSString              * bloodGluVal;
@property (nonatomic , copy) NSString              * mCondition;

@end

@interface ZJBloodSugarModel :NSObject
@property (nonatomic , copy) NSString              * message;
@property (nonatomic , copy) NSArray<ZJBloodSugarModelData *>              * data;
@property (nonatomic , copy) NSString              * state;

@end
