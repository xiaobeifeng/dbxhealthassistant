//
//  ZJCarouselModel.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/9.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ZJCarouselModelData :NSObject
@property (nonatomic , copy) NSString              * path;
@property (nonatomic , copy) NSString              * news_id;
@end

@interface ZJCarouselModel :NSObject
@property (nonatomic , copy) NSString              * message;
@property (nonatomic , copy) NSArray<ZJCarouselModelData *>              * data;
@property (nonatomic , copy) NSString              * state;

@end
