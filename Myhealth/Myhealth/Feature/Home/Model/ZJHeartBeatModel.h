
//  Created by zhoujian on 2018/9/13.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <Foundation/Foundation.h>

/** 心率数据模型 */
@interface ZJHeartBeatModelData :NSObject
@property (nonatomic , copy) NSString              * id;
@property (nonatomic , copy) NSString              * pulse;
@property (nonatomic , copy) NSString              * upsaveTime;

@end

@interface ZJHeartBeatModel :NSObject
@property (nonatomic , copy) NSString              * message;
@property (nonatomic , copy) NSArray<ZJHeartBeatModelData *>              * data;
@property (nonatomic , copy) NSString              * state;

@end
