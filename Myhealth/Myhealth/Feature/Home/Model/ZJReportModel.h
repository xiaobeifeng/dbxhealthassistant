
//  Created by zhoujian on 2018/9/14.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZJReportModelData :NSObject
@property (nonatomic , copy) NSString              * dPavg;
@property (nonatomic , copy) NSString              * maxdP;
@property (nonatomic , copy) NSString              * minox;
@property (nonatomic , copy) NSString              * maxpu;
@property (nonatomic , copy) NSString              * oxavg;
@property (nonatomic , copy) NSString              * starttime;
@property (nonatomic , copy) NSString              * minsP;
@property (nonatomic , copy) NSString              * endtime;
@property (nonatomic , copy) NSString              * sPavg;
@property (nonatomic , copy) NSString              * strpinggu;
@property (nonatomic , copy) NSString              * strsuggest;
@property (nonatomic , copy) NSString              * weavg;
@property (nonatomic , copy) NSString              * puavg;
@property (nonatomic , copy) NSString              * mindP;
@property (nonatomic , copy) NSString              * minpu;
@property (nonatomic , copy) NSString              * maxox;
@property (nonatomic , copy) NSString              * rnumber;
@property (nonatomic , copy) NSString              * bmr;
@property (nonatomic , copy) NSString              * bmi;
@property (nonatomic , copy) NSString              * upsaveTime;
@property (nonatomic , copy) NSString              * maxsP;

@end

@interface ZJReportModel :NSObject
@property (nonatomic , copy) NSString              * message;
@property (nonatomic , copy) NSArray<ZJReportModelData *>              * data;
@property (nonatomic , copy) NSString              * state;

@end
