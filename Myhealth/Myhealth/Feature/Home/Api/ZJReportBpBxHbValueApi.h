
//  Created by zhoujian on 2018/9/17.
//  Copyright © 2018年 曙马科技. All rights reserved.
//



/**
 获取指定时间的血压，血氧，心率数据
 */
@interface ZJReportBpBxHbValueApi : ZJBaseRequest
- (id)initWithStratTime:(NSString *)startTime
                endTime:(NSString *)endTime;
@end
