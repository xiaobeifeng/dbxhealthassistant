
//  Created by zhoujian on 2018/9/12.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJBSUploadApi.h"

@interface ZJBSUploadApi()
{
    double _bloodGluVal;
    NSInteger _mCondition;
}

@end

@implementation ZJBSUploadApi

- (id)initWithBloodGluVal:(double)bloodGluVal
              mCondition:(NSInteger)mCondition
{
    
    self = [super init];
    if (self) {
        _bloodGluVal = bloodGluVal;
        _mCondition = mCondition;
    }
    return self;
}

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return @"glusave";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

// 请求体
- (id)requestArgument {
    return @{
             @"bloodGluVal": @(_bloodGluVal),
             @"mCondition": @(_mCondition),
             };
}

@end
