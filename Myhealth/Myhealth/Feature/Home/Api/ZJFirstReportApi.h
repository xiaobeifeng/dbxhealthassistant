
//  Created by zhoujian on 2018/9/14.
//  Copyright © 2018年 曙马科技. All rights reserved.
//



/**
 获取最新一条数据
 */
@interface ZJFirstReportApi : ZJBaseRequest

- (id)initWithCount:(NSInteger)count flag:(NSInteger)flag doa:(NSInteger)doa;

@end
