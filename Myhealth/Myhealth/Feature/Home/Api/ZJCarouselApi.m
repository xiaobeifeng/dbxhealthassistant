
//  Created by zhoujian on 2018/11/9.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "ZJCarouselApi.h"

@implementation ZJCarouselApi

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return @"getpicture";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
