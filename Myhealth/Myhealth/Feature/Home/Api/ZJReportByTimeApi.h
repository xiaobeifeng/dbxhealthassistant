
//  Created by zhoujian on 2018/9/17.
//  Copyright © 2018年 曙马科技. All rights reserved.
//


@interface ZJReportByTimeApi : ZJBaseRequest

- (id)initWithFlag:(NSInteger)flag
             order:(NSInteger)order
              time:(NSString *)time;

@end
