
//  Created by zhoujian on 2018/9/11.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJBpHbBoUploadApi.h"

@interface ZJBpHbBoUploadApi()
{
    int _sPress;
    int _dPress;
    int _heartBeat;
    int _bloodOxygen;
}

@end

@implementation ZJBpHbBoUploadApi

- (id)initWithSPress:(int)sPress
              dPress:(int)dPress
           heartBeat:(int)heartBeat
         bloodOxygen:(int)bloodOxygen
{
    
    self = [super init];
    if (self) {
        _sPress = sPress;
        _dPress = dPress;
        _heartBeat = heartBeat;
        _bloodOxygen = bloodOxygen;
    }
    return self;
}

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return @"bpsave";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

// 请求体
- (id)requestArgument {
    return @{
             @"sPressure": @(_sPress),
             @"dPressure": @(_dPress),
             @"pulse": @(_heartBeat),
             @"oxygen": @(_bloodOxygen)
             };
}

@end
