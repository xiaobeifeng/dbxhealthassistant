
//  Created by zhoujian on 2018/9/4.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJBloodPressApi.h"

@interface ZJBloodPressApi()

@property(nonatomic, assign) NSInteger count;
@property(nonatomic, assign) NSInteger doa;

@end

@implementation ZJBloodPressApi

- (id)initWithCount:(NSInteger)count doa:(NSInteger)doa
{
    self = [super init];
    if (self) {
        _count = count;
        _doa = doa;
    }
    return self;
}

- (NSString *)requestUrl {
    
    NSString *url = [NSString stringWithFormat:@"bp/%ld/%ld", (long)_count, (long)_doa];
    return url;
}


- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}




@end
