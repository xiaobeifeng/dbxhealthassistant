
//  Created by zhoujian on 2018/9/12.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

@interface ZJHistoryBSApi : ZJBaseRequest

- (id)initWithSelectTime:(NSString *)selectTime
             currentPage:(NSInteger)currentPage;

@end
