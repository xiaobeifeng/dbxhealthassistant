
//  Created by zhoujian on 2018/9/12.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJDeleteHistoryBSApi.h"

@interface ZJDeleteHistoryBSApi()

@property(nonatomic, copy) NSString *Id;

@end

@implementation ZJDeleteHistoryBSApi

- (id)initWithId:(NSString *)Id
{
    
    self = [super init];
    if (self) {
        _Id = Id;
    }
    return self;
}

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return @"gluremovebyid";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

// 请求体
- (id)requestArgument {
    return @{
             @"id": _Id
             };
}

@end
