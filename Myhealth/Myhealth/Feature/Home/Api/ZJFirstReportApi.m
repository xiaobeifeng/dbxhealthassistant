
//  Created by zhoujian on 2018/9/14.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJFirstReportApi.h"

@interface ZJFirstReportApi()

@property(nonatomic, assign) NSInteger count;
@property(nonatomic, assign) NSInteger flag;
@property(nonatomic, assign) NSInteger doa;

@end

@implementation ZJFirstReportApi
- (id)initWithCount:(NSInteger)count
               flag:(NSInteger)flag
                doa:(NSInteger)doa
{
    self = [super init];
    if (self) {
        _count = count;
        _doa = doa;
        _flag = flag;
    }
    return self;
}

- (NSString *)requestUrl {
    
    NSString *url = [NSString stringWithFormat:@"report/%ld/%ld/%ld", (long)_count, (long)_flag,(long)_doa];
    return url;
}


- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}
@end
