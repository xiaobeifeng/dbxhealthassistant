
//  Created by zhoujian on 2018/9/12.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJAddDataBSViewController.h"
#import "SCChart.h"
#import "ZJAddDataInputView.h"
#import <BRPickerView/BRPickerView.h>
#import "ZJBloodSugarApi.h"
#import "ZJBloodSugarModel.h"
#import "ZJBSUploadApi.h"

@interface ZJAddDataBSViewController ()<UITextFieldDelegate>

@property(nonatomic, strong) SCCircleChart *bloodSugarChartView;
@property(nonatomic, strong) ZJAddDataInputView *timeInputView;
@property(nonatomic, strong) ZJAddDataInputView *bloodSugarInputView;
@property(nonatomic, copy) NSArray *timeArray;

@end

@implementation ZJAddDataBSViewController

- (NSArray *)timeArray
{
    if (!_timeArray) {
        _timeArray = @[@"空腹", @"早餐前", @"早餐后", @"午餐前", @"午餐后", @"晚餐前", @"晚餐后", @"睡前"];
    }
    return _timeArray;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)loadView
{
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, kSafeAreaToHeight, kScreenWidth, kScreenHeight-kSafeAreaToHeight)];
    scrollView.contentSize = CGSizeMake(kScreenWidth, kScreenHeight-kSafeAreaToHeight);
    self.view = scrollView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"添加数据";
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIView *bpView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 150)];
    bpView.backgroundColor = COLOR_GALLERY;
    [self.view addSubview:bpView];
    
    _bloodSugarChartView = [[SCCircleChart alloc] initWithFrame:CGRectMake((kScreenWidth-80)/2, (150-80)/2, 80, 80.0)
                                                      total:@33.3
                                                    current:@0
                                                  clockwise:YES
                                                     shadow:YES
                                                shadowColor:[UIColor whiteColor]];
    [_bloodSugarChartView setStrokeColor:SCBlue];
    _bloodSugarChartView.chartType = SCChartFormatTypeNone;
    _bloodSugarChartView.format = @"血糖\n%.1f mmol/L";
    [_bloodSugarChartView strokeChart];
    [bpView addSubview:_bloodSugarChartView];
    
    _timeInputView = [[ZJAddDataInputView alloc] initWithTitle:@"测量时间：" unit:@""];
    _timeInputView.textField.delegate = self;
    _timeInputView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_timeInputView];
    [_timeInputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).with.offset(200);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(40);
    }];
    
    UIButton *timeBtn = [UIButton new];
    [timeBtn addTarget:self action:@selector(handleTimeBtnEvent) forControlEvents:UIControlEventTouchUpInside];
    [_timeInputView addSubview:timeBtn];
    [timeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.timeInputView.mas_centerX);
        make.centerY.mas_equalTo(self.timeInputView.mas_centerY);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(40);
    }];
    
    _bloodSugarInputView = [[ZJAddDataInputView alloc] initWithTitle:@"血糖：" unit:@"mmol/L"];
    _bloodSugarInputView.textField.delegate = self;
    _bloodSugarInputView.textField.tag = 20;
    _bloodSugarInputView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_bloodSugarInputView];
    [_bloodSugarInputView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.timeInputView.mas_bottom).with.offset(20);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(40);
    }];
    
    UIButton *saveBtn = [UIButton new];
    saveBtn.backgroundColor = COLOR_PICTONBLUE;
    [saveBtn setTitle:@"保 存" forState:UIControlStateNormal];
    [saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    kViewRadius(saveBtn, 5.0f);
    saveBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [saveBtn addTarget:self action:@selector(handleSaveBtnEvent) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:saveBtn];
    [saveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bloodSugarInputView.mas_bottom).with.offset(40);
        make.width.mas_equalTo(200);
        make.height.mas_equalTo(40);
        make.centerX.mas_equalTo(self.view.mas_centerX);
    }];
    
    [self startRequestApi];

}

/**
 保存
 */
- (void)handleSaveBtnEvent
{
    NSString *time = _timeInputView.textField.text;
    NSString *bloodSugar = _bloodSugarInputView.textField.text;
    
    NSLog(@"%.1f", [bloodSugar doubleValue]);
    
    // 时间段校验
    if (kStringIsEmpty(time)) {
        [MBProgressHUD showInfoMessage:@"请选择测量时间段"];
        return;
    }
    
    // 血糖校验
    if (!kStringIsEmpty(bloodSugar)) {
        if ([bloodSugar doubleValue] < 0 || [bloodSugar doubleValue] > 33.3) {
            [MBProgressHUD showInfoMessage:@"请填写正确的血糖数值"];
            return;
        }
    } else {
        [MBProgressHUD showInfoMessage:@"请填写血糖数值"];
        return;
    }
    
    
    [_bloodSugarChartView updateChartByCurrent:@([bloodSugar doubleValue])];
    
    NSInteger mCondition = [self.timeArray indexOfObject:time];
    ZJBSUploadApi *api = [[ZJBSUploadApi alloc] initWithBloodGluVal:[bloodSugar doubleValue] mCondition:mCondition];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        [MBProgressHUD hideHUD];
        [MBProgressHUD showTipMessageInView:@"保存成功"];
        
        self.timeInputView.textField.text = @"";
        self.bloodSugarInputView.textField.text = @"";
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
    
}

- (void)startRequestApi
{
    ZJBloodSugarApi *api = [[ZJBloodSugarApi alloc] initWithCount:1 doa:0];
    
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        ZJBloodSugarModel *model = [ZJBloodSugarModel yy_modelWithJSON:request.responseJSONObject];
        
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJBloodSugarModelData class] json:model.data];
        
        if (!kArrayIsEmpty(array)) {
            
            ZJBloodSugarModelData *modelData = array.firstObject;
            [self.bloodSugarChartView updateChartByCurrent:[NSNumber numberWithDouble:[modelData.bloodGluVal doubleValue]]];

        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

- (void)handleTimeBtnEvent
{
    [BRStringPickerView showStringPickerWithTitle:@"请选择测量时间段" dataSource:self.timeArray defaultSelValue:self.timeArray[0] isAutoSelect:NO themeColor:nil resultBlock:^(id selectValue) {
        
        self.timeInputView.textField.text = selectValue;
        
    } cancelBlock:^{
        
        NSLog(@"点击了背景视图或取消按钮");
        
    }];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == _timeInputView.textField) {
        return NO;
    } else {
        return YES;
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.tag == 20) {
        NSMutableString * futureString = [NSMutableString stringWithString:textField.text];
        [futureString  insertString:string atIndex:range.location];
        NSInteger flag=0;
        const NSInteger limited = 1;//小数点后需要限制的个数
        for (int i = (int)(futureString.length - 1); i>=0; i--) {
            if ([futureString characterAtIndex:i] == '.') {
                if (flag > limited) {
                    return NO;
                }
                break;
            }
            flag++;
        }
        return YES;
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
