
//  Created by zhoujian on 2018/8/31.
//  Copyright © 2018年 mc. All rights reserved.
//

#import "ZJHistoryBSViewController.h"
#import "ZJHistoryBSTableViewCell.h"
#import "ZJBloodSugarModel.h"
#import "SMKJSelectDateView.h"
#import "ZJBloodSugarModel.h"
#import "ZJHistoryBSApi.h"
#import "ZJDeleteHistoryBSApi.h"

static NSString * const kBloodSugarHistoryTableViewCellIdentifier = @"DBXBloodSugarHistoryTableViewCell";

@interface ZJHistoryBSViewController ()<UITableViewDelegate, UITableViewDataSource, SMKJSelectDateViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) SMKJSelectDateView *selectDateView;
@property (nonatomic, strong) NSDate *chooseDate;
@property (nonatomic, copy) NSString *selectDateStr;
@property(nonatomic, assign) NSInteger currentPage;
@property (nonatomic, strong) NSMutableArray *dataSourceArray;
@property (nonatomic, strong) NSArray *conditionArray;
@end

@implementation ZJHistoryBSViewController

#pragma mark - 懒加载
- (NSMutableArray *)dataSourceArray {
    if (!_dataSourceArray) {
        _dataSourceArray = [NSMutableArray array];
    }
    return _dataSourceArray;
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 50, kScreenWidth, kScreenHeight-kSafeAreaToHeight-50) style:UITableViewStylePlain];
        [_tableView registerClass:[ZJHistoryBSTableViewCell class] forCellReuseIdentifier:kBloodSugarHistoryTableViewCellIdentifier];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.96 alpha:1.00];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(handleHeaderEvent)];
        _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(handleFooterEvent)];
        _tableView.tableFooterView = [UIView new];
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.emptyDataSetSource = self;
        _tableView.emptyDataSetDelegate = self;
    
    }
    return _tableView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"查询历史";
    
    _conditionArray = @[@"空腹", @"早餐前", @"早餐后", @"午餐前", @"午餐后", @"晚餐前", @"晚餐后", @"睡前"];
    
    _chooseDate = [NSDate date];
    _selectDateView = [[SMKJSelectDateView alloc] initWithDate:_chooseDate];
    _selectDateView.backgroundColor = COLOR_PICTONBLUE;
    _selectDateView.delegate = self;
    _selectDateView.frame = CGRectMake(0, 0, kScreenWidth, 50);
    [self.view addSubview: _selectDateView];
    
    [self.view addSubview:self.tableView];
    
    [self handleHeaderEvent];
    
}

/**
 下拉刷新
 */
- (void)handleHeaderEvent {
    _currentPage = 0;
    [self requestListDataWithDate:[self dateFormatterWithDate:_chooseDate]];
}

/**
 上拉刷新
 */
- (void)handleFooterEvent {
    [self requestListDataWithDate:[self dateFormatterWithDate:_chooseDate]];
}

/**
 将日期格式化指定样式的字符串
 NO:YYYY-MM
 
 @param date 需要转成字符串的日期
 @return 指定样式字符串
 */
- (NSString *)dateFormatterWithDate:(NSDate *)date {
    
    
    NSDateFormatter *formatterYearAndMonth = [[NSDateFormatter alloc] init];
    
    formatterYearAndMonth.dateFormat = @"YYYY-MM";
    
    NSString *dateStr = [formatterYearAndMonth stringFromDate:date];
    return dateStr;
}

- (void)requestListDataWithDate:(NSString *)dateStr
{
    ZJHistoryBSApi *api = [[ZJHistoryBSApi alloc] initWithSelectTime:dateStr currentPage:_currentPage];
    
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        if (request.responseJSONObject) {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                ZJBloodSugarModel *model = [ZJBloodSugarModel yy_modelWithJSON:request.responseJSONObject];
                
                // 请求成功
                if ([model.state integerValue] == 0) {
                    
                    // currentPage为0
                    if (self.currentPage == 0) {
                        [self.dataSourceArray removeAllObjects];
                    }
                    [self.dataSourceArray addObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZJBloodSugarModelData class] json:model.data]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (self.currentPage != 0) {
                            
                            [self.tableView reloadData];
                            [self.tableView.mj_footer endRefreshing];
                            
                        } else {
                            
                            [self.tableView reloadData];
                            [self.tableView.mj_header endRefreshing];
                            
                        }
                        
                        if (model.data.count != 0) {
                            self.currentPage += 1;
                        }
                        
                        // TODO: 根据数据源控制上拉刷新显隐
                        if (self.dataSourceArray.count < 10) {
                            self.tableView.mj_footer.hidden = YES;
                        } else {
                            self.tableView.mj_footer.hidden = NO;
                        }
                        
                        
                    });
                    
                }
            });
            
        }
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
    
}

# pragma mark - selectDateView Delegate
- (void)selectDateViewClick:(SMKJSelectDateBtnType)type
{
    [self.tableView  scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    
    if (type == MonthReduce) {
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        dateComponents.month = -1;
        NSDate *fromMonth = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:_chooseDate options:0];
        _chooseDate = fromMonth;
        NSDateFormatter *formatterYearAndMonth = [[NSDateFormatter alloc] init];
        formatterYearAndMonth.dateFormat = @"YYYY年MM月";
        NSString *chooseDate = [formatterYearAndMonth stringFromDate:_chooseDate];
        _selectDateView.selectDateString = chooseDate;
        
        NSDateFormatter *formatterYearAndMonth1 = [[NSDateFormatter alloc] init];
        formatterYearAndMonth1.dateFormat = @"YYYY-MM";
        _selectDateStr = [formatterYearAndMonth1 stringFromDate:_chooseDate];
        [self handleHeaderEvent];
    }
    
    if (type == MonthAdd) {
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        dateComponents.month = +1;
        NSDate *fromMonth = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:_chooseDate options:0];
        _chooseDate = fromMonth;
        NSDateFormatter *formatterYearAndMonth = [[NSDateFormatter alloc] init];
        formatterYearAndMonth.dateFormat = @"YYYY年MM月";
        NSString *chooseDate = [formatterYearAndMonth stringFromDate:_chooseDate];
        _selectDateView.selectDateString = chooseDate;
        
        NSDateFormatter *formatterYearAndMonth1 = [[NSDateFormatter alloc] init];
        formatterYearAndMonth1.dateFormat = @"YYYY-MM";
        _selectDateStr = [formatterYearAndMonth1 stringFromDate:_chooseDate];
        [self handleHeaderEvent];
    }
    
    if (type == CalendarShow) {
        //        SMKJCalendarView *calendarView = [[SMKJCalendarView alloc] initWithCurrentSelectDate:_chooseDate];
        //        calendarView.delegate = self;
        //        [calendarView show];
    }
}

#pragma mark - UITableView代理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZJHistoryBSTableViewCell *bloodSugarHistoryTableViewCell = [tableView dequeueReusableCellWithIdentifier:kBloodSugarHistoryTableViewCellIdentifier forIndexPath:indexPath];
    
    [self setupModelOfCell:bloodSugarHistoryTableViewCell cellForRowAtIndexPath:indexPath];
    
    return bloodSugarHistoryTableViewCell;
    
}

- (void)setupModelOfCell:(ZJHistoryBSTableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZJBloodSugarModelData *model = self.dataSourceArray[indexPath.row];
    
    NSString *time = model.upsaveTime;
    double bsValue = [model.bloodGluVal doubleValue];
    NSInteger condition = [model.mCondition integerValue];
    
    cell.dateLbl.text = time;
    cell.bloodSugarLbl.text = [NSString stringWithFormat:@"血糖：%.1f mmol/L %@", bsValue, _conditionArray[condition]];
    cell.bsValue = bsValue;
    cell.condition = condition;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView fd_heightForCellWithIdentifier:kBloodSugarHistoryTableViewCellIdentifier configuration:^(id cell) {
        [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    }];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath*)indexPath
{
    // 执行删除操作
    ZJBloodSugarModelData *model = self.dataSourceArray[indexPath.row];
    
    ZJDeleteHistoryBSApi *api = [[ZJDeleteHistoryBSApi alloc] initWithId:model.Id];
    
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        [self.dataSourceArray removeObjectAtIndex:indexPath.row];
        
        if (kArrayIsEmpty(_dataSourceArray)) {
            [self.tableView reloadData];
        } else {
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:indexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }
    
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        [self.tableView reloadData];
        [MBProgressHUD showErrorMessage:@"请稍后再试"];
    }];
}

 /** 删除 cell (UITableView 方法). */
- (void)deleteRowsAtIndexPaths:(NSArray *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation
{
    
}

- (void)dealloc
{
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReloadDataApiAndDataDetailApi object:nil];
}

# pragma mark - emptyDataSet
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"无数据";
    UIFont *font = [UIFont systemFontOfSize:14.0];
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:text];
    [attStr addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, text.length)];
    return attStr;
}
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    UIImage *image = [UIImage imageNamed:@"ic_no_data"];
    return image;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
