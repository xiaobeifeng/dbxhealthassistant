//
//  ZJReportDetailViewController.m
//  ZJMyhealth
//
//  Created by zhoujian on 2018/11/7.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "ZJReportDetailViewController.h"
#import "ZJScrollView.h"
#import "ZJDataAnalysisViewController.h"
#import "ZJReportHistoryViewController.h"
#import "ZJReportShowViewController.h"
#import "ZJReportUserInfoView.h"
#import "ZJFirstReportApi.h"
#import "ZJReportModel.h"
#import "ZJReportCalendarViewController.h"
#import "ZJReportByTimeApi.h"
#import "ZJProfileInfoApi.h"
#import "ZJProfileInfoModel.h"

@interface ZJReportDetailViewController ()<UIScrollViewDelegate, ZJReportCalendarViewControllerDelegate>

@property (nonatomic, strong) ZJScrollView *scrollView;
@property (nonatomic, strong) UIView *titleView;
@property (nonatomic, weak) UIButton *previousClickedTitleButton;
@property (nonatomic, weak) UIView *titleUnderline;
@property (nonatomic, strong) UIView *backgroundView; // 背景框
@property (nonatomic, strong) ZJReportUserInfoView *topImageView; // 顶部视图

@property (nonatomic, strong) UIButton *dateButton;
@property (nonatomic, strong) UILabel *bloodPreLabel;
@property (nonatomic, strong) UILabel *bloodOxygenLabel;
@property (nonatomic, strong) UILabel *heartBeatLabel;

@property (nonatomic, copy) NSString *startTime;  // 开始时间
@property (nonatomic, copy) NSString *endTime;    // 结束时间

@property (nonatomic, copy) NSString *maxBloodPreSp;    // SP血压最大值
@property (nonatomic, copy) NSString *maxBloodPreDp;    // DP血压最大值
@property (nonatomic, copy) NSString *maxBloodOxygen;    // 血氧最大值
@property (nonatomic, copy) NSString *maxHeartBeat;    // 心率最大值
@property (nonatomic, copy) NSString *minBloodPreSp;    // 血压最小值
@property (nonatomic, copy) NSString *minBloodPreDp;    // 血压最小值
@property (nonatomic, copy) NSString *minBloodOxygen;    // 血氧最小值
@property (nonatomic, copy) NSString *minHeartBeat;    // 心率最小值
@property (nonatomic, copy) NSString *averageBloodPreSp;    // 血压最小值
@property (nonatomic, copy) NSString *averageBloodPreDp;    // 血压最小值
@property (nonatomic, copy) NSString *averageBloodOxygen;    // 血氧平均值
@property (nonatomic, copy) NSString *averageHeartBeat;    // 心率平均值

@end

@implementation ZJReportDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#E1E1E1"];
    
    self.backgroundView = [UIView new];
    self.backgroundView.frame = CGRectMake(13, 13, kScreenWidth-26, kScreenHeight-kSafeAreaToHeight-13-20);
    self.backgroundView.backgroundColor = [UIColor whiteColor];
    kViewRadius(self.backgroundView, 10.0f);
    [self.view addSubview:self.backgroundView];
    
    self.topImageView = [ZJReportUserInfoView new];
    self.topImageView.frame = CGRectMake(0, 0, kScreenWidth-26, 250);
    self.topImageView.image = [UIImage imageNamed:@"report_top_background"];
    self.topImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.topImageView.backgroundColor = [UIColor clearColor];
    [self.backgroundView addSubview:self.topImageView];
    [self.topImageView.maxValueBtn addTarget:self action:@selector(maxValueBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.topImageView.minValueBtn addTarget:self action:@selector(minValueBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.topImageView.averageValueBtn addTarget:self action:@selector(averageValueAction) forControlEvents:UIControlEventTouchUpInside];
    [self.topImageView.dateButton addTarget:self action:@selector(dateButtonAction:) forControlEvents:UIControlEventTouchUpInside];

    [self setupAllChildVCs];
    [self setupScrollView];
    [self setupTitlesView];
    [self setupTitleButtons];
//    [self setupTitleUnderline];
    [self addChildVcViewIntoScrollView:0];
    
    // 请求数据
    [self startRequestPersonInfo];
    
}

- (void)dateButtonAction:(UIButton *)button {
    
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"" message:@"获取更多报告" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *beforeAction =  [UIAlertAction actionWithTitle:@"上一条" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self requestBeforeApi];
        
    }];
    
    UIAlertAction *nextAction =  [UIAlertAction actionWithTitle:@"下一条" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self requestNextApi];
    }];
    
    UIAlertAction *moreAction =  [UIAlertAction actionWithTitle:@"更多" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        ZJReportCalendarViewController *calenderVC = [ZJReportCalendarViewController new];
        calenderVC.delegate = self;
        [self.navigationController pushViewController:calenderVC animated:YES];
        
    }];
    
    UIAlertAction *cancelAction =  [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alertVC addAction:beforeAction];
    [alertVC addAction:nextAction];
    [alertVC addAction:moreAction];
    [alertVC addAction:cancelAction];
    
    [self presentViewController:alertVC animated:YES completion:nil];
    
}

- (void)reloadViewWithCalendarDate:(NSString *)date
{
//    ZJDataAnalyzeViewController *dataAnalyze = self.childViewControllers[0];
//    [dataAnalyze requestReportDataWithFlag:self.flag order:0 isNextData:NO date:date];
    
    // 发送通知，更新子视图
    self.startTime = date;
    [self requestBeforeApi];
}

- (void)maxValueBtnAction {
    self.topImageView.maxValueBtn.selected = YES;
    self.topImageView.minValueBtn.selected = NO;
    self.topImageView.averageValueBtn.selected = NO;
    
    NSString *sp = kStringIsEmpty(self.maxBloodPreSp)?@"--":self.maxBloodPreSp;
    NSString *dp = kStringIsEmpty(self.maxBloodPreDp)?@"--":self.maxBloodPreDp;
    NSString *bloodOxygen = kStringIsEmpty(self.maxBloodOxygen)?@"--":self.maxBloodOxygen;
    NSString *heartBeat = kStringIsEmpty(self.maxHeartBeat)?@"--":self.maxHeartBeat;
    self.topImageView.bloodPressLbl.text = [NSString stringWithFormat:@"血压：%@/%@ mmHg", sp, dp];
    self.topImageView.bloodOxygenLbl.text = [NSString stringWithFormat:@"血氧：%@ %%", bloodOxygen];
    self.topImageView.heartBeatLbl.text = [NSString stringWithFormat:@"心率：%@ 次/分",heartBeat];
    
}

- (void)minValueBtnAction {
    self.topImageView.maxValueBtn.selected = NO;
    self.topImageView.minValueBtn.selected = YES;
    self.topImageView.averageValueBtn.selected = NO;
    
    NSString *sp = kStringIsEmpty(self.minBloodPreSp)?@"--":self.minBloodPreSp;
    NSString *dp = kStringIsEmpty(self.minBloodPreDp)?@"--":self.minBloodPreDp;
    NSString *bloodOxygen = kStringIsEmpty(self.minBloodOxygen)?@"--":self.minBloodOxygen;
    NSString *heartBeat = kStringIsEmpty(self.minHeartBeat)?@"--":self.minHeartBeat;
    
    self.topImageView.bloodPressLbl.text = [NSString stringWithFormat:@"血压：%@/%@ mmHg", sp, dp];
    self.topImageView.bloodOxygenLbl.text = [NSString stringWithFormat:@"血氧：%@ %%", bloodOxygen];
    self.topImageView.heartBeatLbl.text = [NSString stringWithFormat:@"心率：%@ 次/分",heartBeat];
}

- (void)averageValueAction {
    self.topImageView.maxValueBtn.selected = NO;
    self.topImageView.minValueBtn.selected = NO;
    self.topImageView.averageValueBtn.selected = YES;
    
    NSString *sp = kStringIsEmpty(self.averageBloodPreSp)?@"--":self.averageBloodPreSp;
    NSString *dp = kStringIsEmpty(self.averageBloodPreDp)?@"--":self.averageBloodPreDp;
    NSString *bloodOxygen = kStringIsEmpty(self.averageBloodOxygen)?@"--":self.averageBloodOxygen;
    NSString *heartBeat = kStringIsEmpty(self.averageHeartBeat)?@"--":self.averageHeartBeat;
    self.topImageView.bloodPressLbl.text = [NSString stringWithFormat:@"血压：%@/%@ mmHg", sp, dp];
    self.topImageView.bloodOxygenLbl.text = [NSString stringWithFormat:@"血氧：%@ %%", bloodOxygen];
    self.topImageView.heartBeatLbl.text = [NSString stringWithFormat:@"心率：%@ 次/分",heartBeat];
}

/**
 获取个人信息
 */
- (void)startRequestPersonInfo
{
    ZJProfileInfoApi *api = [[ZJProfileInfoApi alloc] init];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
     
        ZJProfileInfoModel *model = [ZJProfileInfoModel yy_modelWithJSON:api.result];
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJProfileInfoModelData class] json:model.data];
        
        if (kArrayIsEmpty(array)) {
           
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"请完善个人信息后再试" message:@"" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [self.navigationController popViewControllerAnimated:YES];
                
            }];
            
            [alertVC addAction:okAction];
            
            [self presentViewController:alertVC animated:YES completion:nil];
        } else {
            [self startRequestFirstReportApi];
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}


#pragma mark 请求第一条报告
- (void)startRequestFirstReportApi {
    
    ZJFirstReportApi *api = [[ZJFirstReportApi alloc] initWithCount:1 flag:_reportStyle doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
            
        ZJReportModel *model = [ZJReportModel yy_modelWithJSON:request.responseObject];
            
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJReportModelData class] json:model.data];
            
        if (!kArrayIsEmpty(array)) {
                
            ZJReportModelData *modelData = array.firstObject;
            NSString *startTime = [modelData.starttime substringWithRange:NSMakeRange(0, 10)];
            NSString *endTime = [modelData.endtime substringWithRange:NSMakeRange(0, 10)];
            
            [self.topImageView.dateButton setTitle:[NSString stringWithFormat:@"%@ ～ %@", startTime, endTime] forState:UIControlStateNormal];
            [self.topImageView.dateButton layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleRight imageTitleSpace:10];
            
            self.startTime = modelData.starttime;
            self.endTime = modelData.endtime;
       
            self.maxBloodPreSp = modelData.maxsP;
            self.maxBloodPreDp = modelData.maxdP;
            self.maxBloodOxygen = modelData.maxox;
            self.maxHeartBeat = modelData.maxpu;
            
            self.minBloodPreSp = modelData.minsP;
            self.minBloodPreDp = modelData.mindP;
            self.minBloodOxygen = modelData.minox;
            self.minHeartBeat = modelData.minpu;
            
            self.averageBloodPreSp = modelData.sPavg;
            self.averageBloodPreDp = modelData.dPavg;
            self.averageBloodOxygen = modelData.oxavg;
            self.averageHeartBeat = modelData.puavg;
            
        } else {
            
            [MBProgressHUD showTipMessageInWindow:@"暂无报告"];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
           
        }
        
        [self maxValueBtnAction];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [MBProgressHUD showTipMessageInWindow:@"请稍后再试"];
        [self maxValueBtnAction];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
        
    }];
}

- (void)requestBeforeApi {
    
    NSInteger order = 0;
    ZJReportByTimeApi *api = [[ZJReportByTimeApi alloc] initWithFlag:_reportStyle order:order time:_startTime];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        
        if (request.responseJSONObject) {
            
            ZJReportModel *model = [ZJReportModel yy_modelWithJSON:request.responseObject];
            
            NSArray *array = [NSArray yy_modelArrayWithClass:[ZJReportModelData class] json:model.data];
            
            if (!kArrayIsEmpty(array)) {
                ZJReportModelData *modelData = array.firstObject;
                NSString *startTime = [modelData.starttime substringWithRange:NSMakeRange(0, 10)];
                NSString *endTime = [modelData.endtime substringWithRange:NSMakeRange(0, 10)];
                
                [self.topImageView.dateButton setTitle:[NSString stringWithFormat:@"%@ ～ %@", startTime, endTime] forState:UIControlStateNormal];
                [self.topImageView.dateButton layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleRight imageTitleSpace:10];
                
                self.startTime = modelData.starttime;
                self.endTime = modelData.endtime;
                
                self.maxBloodPreSp = modelData.maxsP;
                self.maxBloodPreDp = modelData.maxdP;
                self.maxBloodOxygen = modelData.maxox;
                self.maxHeartBeat = modelData.maxpu;
                
                self.minBloodPreSp = modelData.minsP;
                self.minBloodPreDp = modelData.mindP;
                self.minBloodOxygen = modelData.minox;
                self.minHeartBeat = modelData.minpu;
                
                self.averageBloodPreSp = modelData.sPavg;
                self.averageBloodPreDp = modelData.dPavg;
                self.averageBloodOxygen = modelData.oxavg;
                self.averageHeartBeat = modelData.puavg;
 
                // 发送通知，更新子视图
                NSDictionary *dic = [NSMutableDictionary dictionary];
                [dic setValue:self.startTime forKey:@"STARTTIME"];
                [dic setValue:self.endTime forKey:@"ENDTIME"];
                [dic setValue:@(order) forKey:@"ORDER"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOAD_CHILDVIEWS" object:nil userInfo:dic];
                
            } else {
                [MBProgressHUD showTipMessageInWindow:@"没有更多报告了"];
            }
            
            [self maxValueBtnAction];
        }
        
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

- (void)requestNextApi {
    
    NSInteger order = 1;
    ZJReportByTimeApi *api = [[ZJReportByTimeApi alloc] initWithFlag:_reportStyle order:order time:_endTime];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        
        if (request.responseJSONObject) {
            
            ZJReportModel *model = [ZJReportModel yy_modelWithJSON:request.responseObject];
            
            NSArray *array = [NSArray yy_modelArrayWithClass:[ZJReportModelData class] json:model.data];
            
            if (!kArrayIsEmpty(array)) {
                ZJReportModelData *modelData = array.firstObject;
                NSString *startTime = [modelData.starttime substringWithRange:NSMakeRange(0, 10)];
                NSString *endTime = [modelData.endtime substringWithRange:NSMakeRange(0, 10)];
                
                [self.topImageView.dateButton setTitle:[NSString stringWithFormat:@"%@ ～ %@", startTime, endTime] forState:UIControlStateNormal];
                [self.topImageView.dateButton layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleRight imageTitleSpace:10];
                
                self.startTime = modelData.starttime;
                self.endTime = modelData.endtime;
                
                self.maxBloodPreSp = modelData.maxsP;
                self.maxBloodPreDp = modelData.maxdP;
                self.maxBloodOxygen = modelData.maxox;
                self.maxHeartBeat = modelData.maxpu;
                
                self.minBloodPreSp = modelData.minsP;
                self.minBloodPreDp = modelData.mindP;
                self.minBloodOxygen = modelData.minox;
                self.minHeartBeat = modelData.minpu;
                
                self.averageBloodPreSp = modelData.sPavg;
                self.averageBloodPreDp = modelData.dPavg;
                self.averageBloodOxygen = modelData.oxavg;
                self.averageHeartBeat = modelData.puavg;
                
                // 发送通知，更新子视图
                NSDictionary *dic = [NSMutableDictionary dictionary];
                [dic setValue:self.startTime forKey:@"STARTTIME"];
                [dic setValue:self.endTime forKey:@"ENDTIME"];
                [dic setValue:@(order) forKey:@"ORDER"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOAD_CHILDVIEWS" object:nil userInfo:dic];
                
            } else {
                [MBProgressHUD showTipMessageInWindow:@"没有更多报告了"];
            }
            
            [self maxValueBtnAction];
        }
        
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}


- (void)setupAllChildVCs {
    
    ZJDataAnalysisViewController *dataAnalysisVC = [ZJDataAnalysisViewController new];
    dataAnalysisVC.reportStyle = _reportStyle;
    [self addChildViewController:dataAnalysisVC];
    
    ZJReportHistoryViewController *reportHistoryVC = [ZJReportHistoryViewController new];
    reportHistoryVC.reportStyle = _reportStyle;
    [self addChildViewController:reportHistoryVC];
    
    ZJReportShowViewController *reportShowVC = [ZJReportShowViewController new];
    reportShowVC.reportStyle = _reportStyle;
    [self addChildViewController:reportShowVC];

}

- (void)setupScrollView {
    
    self.scrollView = [ZJScrollView new];
    self.scrollView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    self.scrollView.frame = CGRectMake(0, self.topImageView.height, self.topImageView.width, kScreenHeight-kSafeAreaToHeight-13-20-50-self.topImageView.height);
    self.scrollView.contentSize = CGSizeMake(kScreenWidth * 3, self.scrollView.height);
    self.scrollView.delegate = self;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.bounces = NO;
    self.scrollView.pagingEnabled = YES;
    [self.backgroundView addSubview:self.scrollView];
  
    
}


- (void)setupTitlesView {
    // 设置顶部栏
    self.titleView = [UIView new];
    self.titleView.frame = CGRectMake(0, self.scrollView.zj_y+self.scrollView.height, self.topImageView.width, 50);
    self.titleView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.backgroundView addSubview:self.titleView];
    
    
}

- (void)setupTitleButtons
{
    NSArray *titleArray = @[@"生理数据分析", @"历史数据", @"数据报告"];
    for (int i=0; i<titleArray.count; i++) {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(i*self.topImageView.width/titleArray.count, 0, self.topImageView.width/titleArray.count, 50)];
        [self.titleView addSubview:button];
        button.titleLabel.font = [UIFont systemFontOfSize:14];
        [button setTitle:titleArray[i] forState:UIControlStateNormal];
        button.tag = i;
        [button setTitleColor:[UIColor colorWithRed:0.39 green:0.49 blue:0.93 alpha:0.5] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor colorWithRed:0.39 green:0.49 blue:0.93 alpha:1.00] forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    UIView *syncHPHOHBView = self.childViewControllers[0].view;
    syncHPHOHBView.frame = CGRectMake(0, 0, kScreenWidth-26, self.scrollView.height);
    [self.scrollView addSubview:syncHPHOHBView];
    
    UIView *syncBSView = self.childViewControllers[1].view;
    syncBSView.frame = CGRectMake(syncHPHOHBView.width, 0, syncHPHOHBView.width, self.scrollView.height);
    [self.scrollView addSubview:syncBSView];
    
    UIView *syncSportView = self.childViewControllers[2].view;
    syncSportView.frame = CGRectMake(syncHPHOHBView.width*2, 0, syncHPHOHBView.width, self.scrollView.height);
    [self.scrollView addSubview:syncSportView];
    
    
    // 标题按钮
    UIButton *firstTitleButton = self.titleView.subviews.firstObject;
    // 切换按钮状态
    firstTitleButton.selected = YES;
    self.previousClickedTitleButton = firstTitleButton;
}

/**
 *  标题下划线
 */
//- (void)setupTitleUnderline {
//    // 标题按钮
//    UIButton *firstTitleButton = self.titleView.subviews.firstObject;
//
//    // 下划线
//    UIView *titleUnderline = [[UIView alloc] init];
//    titleUnderline.xmg_height = 2;
//    titleUnderline.xmg_y = self.titleView.xmg_height - titleUnderline.xmg_height;
//    titleUnderline.backgroundColor = [firstTitleButton titleColorForState:UIControlStateSelected];
//    [self.titleView addSubview:titleUnderline];
//    self.titleUnderline = titleUnderline;
//
//    // 切换按钮状态
//    firstTitleButton.selected = YES;
//    self.previousClickedTitleButton = firstTitleButton;
//
//    [firstTitleButton.titleLabel sizeToFit]; // 让label根据文字内容计算尺寸
//    self.titleUnderline.xmg_width = firstTitleButton.xmg_width;
//    self.titleUnderline.xmg_centerX = firstTitleButton.xmg_centerX;
//}

//- (void)setupBottomView {
//
//    UIView *bottomView = [UIView new];
//    bottomView.backgroundColor = [UIColor greenColor];
//    bottomView.frame = CGRectMake(0, self.scrollView.xmg_y+self.scrollView.xmg_height, kScreenWidth, 60);
//    [self.view addSubview:bottomView];
//
//}

/**
 *  当用户松开scrollView并且滑动结束时调用这个代理方法（scrollView停止滚动的时候）
 */
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    // 求出标题按钮的索引
    NSUInteger index = scrollView.contentOffset.x / scrollView.zj_width;
    
    // 点击对应的标题按钮
    UIButton *button = self.titleView.subviews[index];
    [self buttonAction:button];
}

- (void)buttonAction:(UIButton *)button {
    self.previousClickedTitleButton.selected = NO;
    button.selected = YES;
    self.previousClickedTitleButton = button;
    
    NSInteger index = button.tag;
    
    [UIView animateWithDuration:0.01 animations:^{
        
        // 处理下划线
        self.titleUnderline.zj_width = button.zj_width;
        self.titleUnderline.zj_centerX = button.zj_centerX;
        
        // 滚动scrollView
        CGFloat offsetX = self.scrollView.zj_width * index;
        self.scrollView.contentOffset = CGPointMake(offsetX, self.scrollView.contentOffset.y);
    } completion:^(BOOL finished) {
        [self addChildVcViewIntoScrollView:index];
    }];
    
    // 设置index位置对应的tableView.scrollsToTop = YES， 其他都设置为NO
    for (NSUInteger i = 0; i < self.childViewControllers.count; i++) {
        UIViewController *childVc = self.childViewControllers[i];
        // 如果view还没有被创建，就不用去处理
        if (!childVc.isViewLoaded) continue;
        
        UIScrollView *scrollView = (UIScrollView *)childVc.view;
        if (![scrollView isKindOfClass:[UIScrollView class]]) continue;
        
        scrollView.scrollsToTop = (i == index);
    }
}

/**
 *  添加第index个子控制器的view到scrollView中
 */
- (void)addChildVcViewIntoScrollView:(NSUInteger)index {
    UIViewController *childVc = self.childViewControllers[index];
    
    // 如果view已经被加载过，就直接返回
    if (childVc.isViewLoaded) return;
    
    // 取出index位置对应的子控制器view
    UIView *childVcView = childVc.view;
    
    // 设置子控制器view的frame
    CGFloat scrollViewW = self.scrollView.zj_width;
    childVcView.frame = CGRectMake(index * scrollViewW, 0, scrollViewW, self.scrollView.zj_height);
    // 添加子控制器的view到scrollView中
    [self.scrollView addSubview:childVcView];
}

@end
