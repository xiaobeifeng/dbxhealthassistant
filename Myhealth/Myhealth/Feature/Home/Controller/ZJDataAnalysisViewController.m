//
//  ZJDataAnalysisViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/8.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "ZJDataAnalysisViewController.h"
#import "ZJReportUserInfoView.h"
#import "XMGTitleButton.h"
#import "SCChart.h"
#import "ZJProfileInfoModel.h"
#import "ZJProfileInfoApi.h"
#import "ZJFirstReportApi.h"
#import "ZJReportModel.h"
#import "ZJReportBpBxHbValueApi.h"
#import "ZJReportBpBxHbValueModel.h"
#import "ZJReportByTimeApi.h"
#import "ZJReportHistoryTableViewCell.h"


static CGFloat kReportUserInfoViewHeight = 180;
static CGFloat kChartTitleViewHeight = 40;
static CGFloat kChartHeight = 100;
static CGFloat kScrollTitlesViewHeight = 25;
static NSString *kTableViewCellIdentifier = @"DBXReportHistoryTableViewCell";

@interface ZJDataAnalysisViewController ()<UIScrollViewDelegate, SCChartDataSource>
/** 用来存放所有子控制器view的scrollView */
@property (nonatomic, weak) UIScrollView *scrollView;
/** 标题栏 */
@property (nonatomic, weak) UIView *titlesView;
/** 标题下划线 */
@property (nonatomic, weak) UIView *titleUnderline;
/** 上一次点击的标题按钮 */
@property (nonatomic, weak) XMGTitleButton *previousClickedTitleButton;
@property(nonatomic, strong) SCChart *bloodPressChartView;
@property(nonatomic, strong) SCChart *bloodOxygenChartView;
@property(nonatomic, strong) SCChart *heartBeatChartView;
@property(nonatomic, strong) UITableView *tableView;
/** 血压最大值 */
@property(nonatomic, copy) NSString *maxBloodPress;
/** 血压平均值 */
@property(nonatomic, copy) NSString *averageBloodPress;
/** 血压最小值 */
@property(nonatomic, copy) NSString *minBloodPress;
/** 血氧最大值 */
@property(nonatomic, copy) NSString *maxBloodOxygen;
/** 血氧平均值 */
@property(nonatomic, copy) NSString *averageBloodOxygen;
/** 血氧最小值 */
@property(nonatomic, copy) NSString *minBloodOxygen;
/** 心率最大值 */
@property(nonatomic, copy) NSString *maxHeartBeat;
/** 心率平均值 */
@property(nonatomic, copy) NSString *averageHeartBeat;
/** 心率最小值 */
@property(nonatomic, copy) NSString *minHeartBeat;
/** 各项数值数组 */
@property (nonatomic, strong) NSMutableArray *dataMarray;
/** 高压数组 */
@property (nonatomic, strong) NSMutableArray *spMarray;
/** 低压数组 */
@property (nonatomic, strong) NSMutableArray *dpMarray;
/** 心率数组 */
@property (nonatomic, strong) NSMutableArray *heartBeatMarray;
/** 血氧数组 */
@property (nonatomic, strong) NSMutableArray *bloodOxygenMarray;
@property (nonatomic, strong) UIView *noDataView;

@end

@implementation ZJDataAnalysisViewController

//- (NSMutableArray *)dataMarray
//{
//    if (!_dataMarray) {
//        _dataMarray = [NSMutableArray new];
//    }
//    return _dataMarray;
//}

//- (NSMutableArray *)heartBeatMarray
//{
//    if (!_heartBeatMarray) {
//        _heartBeatMarray = [NSMutableArray new];
//    }
//    return _heartBeatMarray;
//}
//
//- (NSMutableArray *)spMarray
//{
//    if (!_spMarray) {
//        _spMarray = [NSMutableArray new];
//    }
//    return _spMarray;
//}
//
//- (NSMutableArray *)dpMarray
//{
//    if (!_dpMarray) {
//        _dpMarray = [NSMutableArray new];
//    }
//    return _dpMarray;
//}
//
//- (NSMutableArray *)bloodOxygenMarray
//{
//    if (!_bloodOxygenMarray) {
//        _bloodOxygenMarray = [NSMutableArray new];
//    }
//    return _bloodOxygenMarray;
//}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    UILabel *chartTitleLbl = [UILabel new];
    chartTitleLbl.frame = CGRectMake(0, 0, kScreenWidth-26, 40);
    chartTitleLbl.text = @"生理参数分析";
    chartTitleLbl.font = [UIFont systemFontOfSize:14];
    chartTitleLbl.textAlignment = NSTextAlignmentCenter;
    chartTitleLbl.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.view addSubview:chartTitleLbl];
    
    // scrollView
    [self setupScrollView];
    
    // 标题栏
    [self setupTitlesView];
    
    
    // 请求数据
    [self startRequestFirstReportApi];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadView:) name:@"RELOAD_CHILDVIEWS" object:nil];
}

- (void)reloadView:(NSNotification *)notification {
    
    NSLog(@"%@", notification);
    
    NSString *startTime = [notification.userInfo objectForKey:@"STARTTIME"];
    NSString *endTime = [notification.userInfo objectForKey:@"ENDTIME"];
    
    [self startRequestReportBpBxHbValueApiWithStartTime:startTime endTime:endTime];
    
}

#pragma mark 请求第一条报告
- (void)startRequestFirstReportApi {
    
    ZJFirstReportApi *api = [[ZJFirstReportApi alloc] initWithCount:1 flag:_reportStyle doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        ZJReportModel *model = [ZJReportModel yy_modelWithJSON:request.responseObject];
        
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJReportModelData class] json:model.data];
        
        if (!kArrayIsEmpty(array)) {
            
            ZJReportModelData *modelData = array.firstObject;
            
            [self startRequestReportBpBxHbValueApiWithStartTime:modelData.starttime endTime:modelData.endtime];
            
        } else {
            
            
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}


- (void)startRequestReportBpBxHbValueApiWithStartTime:(NSString *)startTime endTime:(NSString *)endTime {
    
    
    
    
    ZJReportBpBxHbValueApi *api = [[ZJReportBpBxHbValueApi alloc] initWithStratTime:startTime endTime:endTime];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        // 每次重新加载都先移除视图
        [_noDataView removeFromSuperview];
        
        ZJReportBpBxHbValueModel *model = [ZJReportBpBxHbValueModel yy_modelWithJSON:request.responseJSONObject];
        
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJReportBpBxHbValueModelData class] json:model.data];
        
        if (!kArrayIsEmpty(array)) {
            
            self.dataMarray = [[NSMutableArray alloc] initWithArray:array];
            [self reloadChatView];
            
        } else {
            
            _noDataView = [[UIView alloc] initWithFrame:CGRectMake(0, 40, kScreenWidth-26, 240.5)];
            _noDataView.backgroundColor = [UIColor whiteColor];
            [self.view addSubview:_noDataView];
            
            UIImageView *imageView = [UIImageView new];
            imageView.image = [UIImage imageNamed:@"ic_no_data"];
            [_noDataView addSubview:imageView];
            [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.height.mas_equalTo(80);
                make.centerY.equalTo(_noDataView.mas_centerY).with.offset(-10);
                make.centerX.equalTo(_noDataView.mas_centerX);
            }];
            
            UILabel *label = [UILabel new];
            label.text = @"无数据";
            label.font = [UIFont systemFontOfSize:14];
            [label sizeToFit];
            label.textColor = [UIColor lightGrayColor];
            [_noDataView addSubview:label];
            [label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(_noDataView.mas_centerX);
                make.top.equalTo(imageView.mas_bottom).with.offset(10);
            }];
           
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [_noDataView removeFromSuperview];
    }];
}

/**
 刷新表格视图
 */
- (void)reloadChatView
{

    // 初始化数组
    _spMarray = [NSMutableArray new];
    _dpMarray = [NSMutableArray new];
    _bloodOxygenMarray = [NSMutableArray new];
    _heartBeatMarray = [NSMutableArray new];
    
    NSLog(@"_dataMarray - %@", _dataMarray);
    
    for (int i=0; i<self.dataMarray.count; i++) {
        ZJReportBpBxHbValueModelData *modelData = self.dataMarray[i];
        [_spMarray addObject:modelData.sPressure];
        [_dpMarray addObject:modelData.dPressure];
    }
    [_bloodPressChartView strokeChart];
    
    
    for (int i=0; i<self.dataMarray.count; i++) {
        ZJReportBpBxHbValueModelData *modelData = self.dataMarray[i];
        [self.heartBeatMarray addObject:modelData.pulse];
    }
    [_heartBeatChartView strokeChart];
    

    for (int i=0; i<self.dataMarray.count; i++) {
        ZJReportBpBxHbValueModelData *modelData = self.dataMarray[i];
        [self.bloodOxygenMarray addObject:modelData.oxygen];
    }
    [_bloodOxygenChartView strokeChart];
    
}

- (void)requestBeforeApi {
    
    ZJReportByTimeApi *api = [[ZJReportByTimeApi alloc] initWithFlag:_reportStyle order:0 time:_startTime];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        
        if (request.responseJSONObject) {
            
            ZJReportModel *model = [ZJReportModel yy_modelWithJSON:request.responseObject];
            
            NSArray *array = [NSArray yy_modelArrayWithClass:[ZJReportModelData class] json:model.data];
            
            if (!kArrayIsEmpty(array)) {
                ZJReportModelData *modelData = array.firstObject;
                [self startRequestReportBpBxHbValueApiWithStartTime:modelData.starttime endTime:modelData.endtime];
            } else {
                [MBProgressHUD showInfoMessage:@"没有更多报告了"];
            }
            
        }
        
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

/**
 *  标题栏
 */
- (void)setupTitlesView
{
    UIView *titlesView = [[UIView alloc] init];
    titlesView.backgroundColor = [UIColor whiteColor];
    titlesView.frame = CGRectMake(0, 40, kScreenWidth-26, 40);
    [self.view addSubview:titlesView];
    self.titlesView = titlesView;
    
    // 标题栏按钮
    [self setupTitleButtons];
    
    // 标题下划线
    [self setupTitleUnderline];
}


/**
 *  scrollView
 */
- (void)setupScrollView
{
    
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    scrollView.backgroundColor = [UIColor whiteColor];
    scrollView.frame = CGRectMake(0, 80.5, kScreenWidth-26, 200);
    scrollView.delegate = self;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.pagingEnabled = YES;
    [self.view addSubview:scrollView];
    self.scrollView = scrollView;
    
    // 添加子控制器的view
    NSUInteger count = self.childViewControllers.count;
    CGFloat scrollViewW = scrollView.zj_width;
    CGFloat scrollViewH = scrollView.zj_height;
    
    
    // 初始化数组
    _spMarray = [NSMutableArray new];
    _dpMarray = [NSMutableArray new];
    _bloodOxygenMarray = [NSMutableArray new];
    _heartBeatMarray = [NSMutableArray new];
    
    _bloodPressChartView = [[SCChart alloc] initwithSCChartDataFrame:CGRectMake(10, 0, scrollViewW-20, scrollViewH)
                                                          withSource:self
                                                           withStyle:SCChartLineStyle];
    [_bloodPressChartView strokeChart];
    
    _bloodOxygenChartView = [[SCChart alloc] initwithSCChartDataFrame:CGRectMake(scrollViewW+10, 0, scrollViewW-20, scrollViewH)
                                                           withSource:self
                                                            withStyle:SCChartLineStyle];
    [_bloodOxygenChartView strokeChart];
    
    _heartBeatChartView = [[SCChart alloc] initwithSCChartDataFrame:CGRectMake(scrollViewW*2+10, 0, scrollViewW-20, scrollViewH)
                                                         withSource:self
                                                          withStyle:SCChartLineStyle];
    [_heartBeatChartView strokeChart];
    
    [scrollView addSubview:_bloodOxygenChartView];
    [scrollView addSubview:_bloodPressChartView];
    [scrollView addSubview:_heartBeatChartView];
    
    scrollView.contentSize = CGSizeMake(count * scrollViewW, 0);
    
}

#pragma mark - SCChartDataSource
// 横坐标标题数组
- (NSArray *)SCChart_xLableArray:(SCChart *)chart
{
    if (chart == _bloodPressChartView) {
        
        NSMutableArray *xMarray = [NSMutableArray new];
        for (int i = 0; i < self.spMarray.count; i++) {
            
            NSString *x = [NSString stringWithFormat:@"%d", i];
            [xMarray addObject:x];
        }
        
        return xMarray;
        
    }
    
    if (chart == _heartBeatChartView) {
        
        NSMutableArray *xMarray = [NSMutableArray new];
        for (int i = 0; i < self.heartBeatMarray.count; i++) {
            
            NSString *x = [NSString stringWithFormat:@"%d", i];
            [xMarray addObject:x];
        }
        
        return xMarray;
        
    }
    
    if (chart == _bloodOxygenChartView) {
        
        NSMutableArray *xMarray = [NSMutableArray new];
        for (int i = 0; i < self.bloodOxygenMarray.count; i++) {
            
            NSString *x = [NSString stringWithFormat:@"%d", i];
            [xMarray addObject:x];
        }
        
        return xMarray;
        
    }
    
    return @[@"1", @"2", @"3", @"4"];
}

//数值多重数组
- (NSArray *)SCChart_yValueArray:(SCChart *)chart
{
    if (chart == _bloodPressChartView) {
        return @[self.spMarray, self.dpMarray];
    }
    
    if (chart == _heartBeatChartView) {
        return @[self.heartBeatMarray];
    }
    
    if (chart == _bloodOxygenChartView) {
        return @[self.bloodOxygenMarray];
    }
    
    return @[@[@"0", @"0", @"0", @"0"]];
}

// 颜色数组
- (NSArray *)SCChart_ColorArray:(SCChart *)chart
{
    return @[SCBlue,SCRed];
}

// 判断显示横线条
- (BOOL)SCChart:(SCChart *)chart ShowHorizonLineAtIndex:(NSInteger)index {
    return YES;
}

//判断显示最大最小值
- (BOOL)SCChart:(SCChart *)chart ShowMaxMinAtIndex:(NSInteger)index
{
    return NO;
}

/**
 *  标题栏按钮
 */
- (void)setupTitleButtons
{
    // 文字
    NSArray *titles = @[@"血压", @"心率", @"血氧"];
    NSUInteger count = titles.count;
    
    // 标题按钮的尺寸
    CGFloat titleButtonW = self.titlesView.zj_width / count;
    CGFloat titleButtonH = self.titlesView.zj_height;
    
    // 创建5个标题按钮
    for (NSUInteger i = 0; i < count; i++) {
        XMGTitleButton *titleButton = [[XMGTitleButton alloc] init];
        titleButton.titleLabel.font = [UIFont systemFontOfSize:12];
        titleButton.tag = i;
        [titleButton addTarget:self action:@selector(titleButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.titlesView addSubview:titleButton];
        // frame
        titleButton.frame = CGRectMake(i * titleButtonW, 0, titleButtonW, titleButtonH);
        // 文字
        [titleButton setTitle:titles[i] forState:UIControlStateNormal];
    }
}

/**
 *  标题下划线
 */
- (void)setupTitleUnderline
{
    // 标题按钮
    XMGTitleButton *firstTitleButton = self.titlesView.subviews.firstObject;
    
    // 下划线
    UIView *titleUnderline = [[UIView alloc] init];
    titleUnderline.zj_height = 2;
    titleUnderline.zj_y = self.titlesView.zj_height - titleUnderline.zj_height;
    titleUnderline.backgroundColor = [firstTitleButton titleColorForState:UIControlStateSelected];
    [self.titlesView addSubview:titleUnderline];
    self.titleUnderline = titleUnderline;
    
    // 切换按钮状态
    firstTitleButton.selected = YES;
    self.previousClickedTitleButton = firstTitleButton;
    
    [firstTitleButton.titleLabel sizeToFit]; // 让label根据文字内容计算尺寸
    self.titleUnderline.zj_width = firstTitleButton.titleLabel.zj_width + 10;
    self.titleUnderline.zj_centerX = firstTitleButton.zj_centerX;
}

#pragma mark - 监听
/**
 *  点击标题按钮
 */
- (void)titleButtonClick:(XMGTitleButton *)titleButton
{
    // 切换按钮状态
    self.previousClickedTitleButton.selected = NO;
    titleButton.selected = YES;
    self.previousClickedTitleButton = titleButton;
    
    [UIView animateWithDuration:0.25 animations:^{
        // 处理下划线
        self.titleUnderline.zj_width = titleButton.titleLabel.zj_width + 10;
        self.titleUnderline.zj_centerX = titleButton.zj_centerX;
        
        CGFloat offsetX = self.scrollView.zj_width * titleButton.tag;
        self.scrollView.contentOffset = CGPointMake(offsetX, self.scrollView.contentOffset.y);
    }];
    
    
}

@end
