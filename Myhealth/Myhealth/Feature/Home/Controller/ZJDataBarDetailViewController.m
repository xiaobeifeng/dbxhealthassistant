//
//  ZJDataBarDetailViewController.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/21.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJDataBarDetailViewController.h"
#import "SCChart.h"
#import "ZJDataBarDetailTableViewCell.h"
#import "ZJHeartBeatModel.h"
#import "ZJHeartbeatApi.h"
#import "ZJBloodOxygenModel.h"
#import "ZJBloodOxygenApi.h"
#import "ZJBloodSugarModel.h"
#import "ZJBloodSugarApi.h"
#import "ZJAddDataBpHbBoViewController.h"
#import "ZJHistoryBSViewController.h"
#import "ZJAddDataBSViewController.h"
#import "ZJHistoryBPBOHBViewController.h"
#import "ZJBloodPressApi.h"
#import "ZJBloodPressModel.h"
#import "ZJDataBarBPDetailTableViewCell.h"
#import "ZJBloodSugarConditionApi.h"
#import "ZJDropSelectView.h"

@interface ZJDataBarDetailViewController ()<UITableViewDelegate, UITableViewDataSource, SCChartDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, ZJDropSelectViewDelegate, UIGestureRecognizerDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *datasMarray;  // 用于存放数据模型
@property (nonatomic, strong) NSMutableArray *valuesMarray;
@property(nonatomic, strong) SCChart *chartView;
@property (nonatomic, strong) FFDropDownMenuView *dropDownMenu;
@property (nonatomic, strong) ZJDropSelectView *dropSelectView;
@property (nonatomic, strong) UIView *dropBackgroundView;   // 下拉视图的背景图
@property (nonatomic, strong) UIButton *navButton;  // 导航栏按钮
@property (nonatomic, assign) NSInteger mCondition; // 当前选择的时间段

@end

@implementation ZJDataBarDetailViewController

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    _datasMarray = [NSMutableArray new];
    
    if(_tag == 100) {
        [self startReuqestBpApi];
    }
    if(_tag == 200) {
        [self startRequestHBApi];
    }
    if(_tag == 300) {
        [self startRequestBoApi];
    }
    if(_tag == 500) {
        [self startRequestBsApi:_mCondition];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem itemWithimage:[UIImage imageNamed:@"nav_rightBar_more"] selImage:[UIImage imageNamed:@"nav_rightBar_more"] target:self action:@selector(rightBarButtonClick)];
    
    [self setupUI];
    
    _mCondition = 0;

}

- (void)setupUI {
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight-kSafeAreaToHeight) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.emptyDataSetSource = self;
    _tableView.emptyDataSetDelegate = self;
    _tableView.backgroundColor = COLOR_GALLERY;
    [_tableView registerClass:[ZJDataBarDetailTableViewCell class] forCellReuseIdentifier:NSStringFromClass([ZJDataBarDetailTableViewCell class])];
    [_tableView registerClass:[ZJDataBarBPDetailTableViewCell class] forCellReuseIdentifier:NSStringFromClass([ZJDataBarBPDetailTableViewCell class])];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    
    _chartView = [[SCChart alloc] initwithSCChartDataFrame:CGRectMake(0, 0, kScreenWidth, 150)
                                                withSource:self
                                                 withStyle:SCChartLineStyle];
    _chartView.backgroundColor = [UIColor whiteColor];
    self.tableView.tableHeaderView = _chartView;
    
    
    [self setupDropDownMenu];
    
    
    if (_tag == 500) {
        _navButton = [UIButton new];
        _navButton.frame = CGRectMake(0, 0, 200, 40);
        [_navButton setTitle:@"血糖-空腹" forState:UIControlStateNormal];
        [_navButton setTitleColor:COLOR_PICTONBLUE forState:UIControlStateNormal];
        [_navButton setImage:[UIImage imageNamed:@"nav_down"] forState:UIControlStateNormal];
        [_navButton layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleRight imageTitleSpace:5];
        [_navButton addTarget:self action:@selector(navigationButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.titleView = _navButton;
    }
}

/**
 导航栏标题按钮点击
 */
- (void)navigationButtonClick:(UIButton *)button {
    
    if (!button.selected) {
        button.selected = YES;
        _dropBackgroundView = [UIView new];
        _dropBackgroundView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
        _dropBackgroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
        [self.view addSubview:_dropBackgroundView];
        [_dropBackgroundView.superview bringSubviewToFront:_dropBackgroundView];
        
        _dropSelectView = [[ZJDropSelectView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 320)];
        _dropSelectView.backgroundColor = [UIColor lightGrayColor];
        _dropBackgroundView.userInteractionEnabled = YES;
        _dropSelectView.delegate = self;
        [_dropBackgroundView addSubview:_dropSelectView]; //这是是自己要做的事情一般是添加一个View 这个需要自己写
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeDropBackgroundView)];
        tap.delegate = self;
        [_dropBackgroundView addGestureRecognizer:tap];
        
    } else {
        button.selected = NO;
        [_dropBackgroundView removeFromSuperview];
    }
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    CGPoint p = [touch locationInView:_dropSelectView];
    
    if(CGRectContainsPoint (_dropSelectView.frame, p)){
        return NO;
    }
    return YES;
    
}

/**
 移除下拉视图的背景图
 */
- (void)removeDropBackgroundView {
    [_dropBackgroundView removeFromSuperview];
}



- (void)rightBarButtonClick {
    [self.dropDownMenu showMenu];
}

#pragma mark - 搭建dropDownMenu
- (void)setupDropDownMenu
{
    NSArray *modelsArray = [self getMenuModelsArray];
    
    self.dropDownMenu = [FFDropDownMenuView ff_DefaultStyleDropDownMenuWithMenuModelsArray:modelsArray menuWidth:FFDefaultFloat eachItemHeight:FFDefaultFloat menuRightMargin:FFDefaultFloat triangleRightMargin:FFDefaultFloat];
    self.dropDownMenu.ifShouldScroll = NO;
    
    self.dropDownMenu.iconSize = CGSizeMake(0, 0);
    self.dropDownMenu.menuWidth = 120;
    
    [self.dropDownMenu setup];
}

/** 获取菜单模型数组 */
- (NSArray *)getMenuModelsArray
{
    __weak typeof(self) weakSelf = self;
    
    FFDropDownMenuModel *menuModel0 = [FFDropDownMenuModel ff_DropDownMenuModelWithMenuItemTitle:@"添加" menuItemIconName:@""  menuBlock:^{
        
        if (_tag == 500) {
            ZJAddDataBSViewController *addDataBSVC = [ZJAddDataBSViewController new];
            [self.navigationController pushViewController:addDataBSVC animated:YES];
        } else {
            ZJAddDataBpHbBoViewController *addDataVC = [ZJAddDataBpHbBoViewController new];
            [weakSelf.navigationController pushViewController:addDataVC animated:YES];
        }
        
    }];
    
    FFDropDownMenuModel *menuModel1 = [FFDropDownMenuModel ff_DropDownMenuModelWithMenuItemTitle:@"查询历史" menuItemIconName:@"" menuBlock:^{
        
        if (_tag == 500) {
            ZJHistoryBSViewController *historyBSVC = [ZJHistoryBSViewController new];
            [self.navigationController pushViewController:historyBSVC animated:YES];
        } else {
            ZJHistoryBPBOHBViewController *historyBPBOHBVC = [ZJHistoryBPBOHBViewController new];
            [weakSelf.navigationController pushViewController:historyBPBOHBVC animated:YES];
        }
        
    }];
    
    NSArray *menuModelArr = @[menuModel0, menuModel1];
    return menuModelArr;
}

#pragma mark - 请求接口

/**
 血压接口
 */
- (void)startReuqestBpApi {
    
    ZJBloodPressApi *api = [[ZJBloodPressApi alloc] initWithCount:10 doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        ZJBloodPressModel *model = [ZJBloodPressModel yy_modelWithJSON:api.result];
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJBloodPressModelData class] json:model.data];
        
        if (!kArrayIsEmpty(array)) {
            
            _datasMarray = [NSMutableArray arrayWithArray:array];
            _chartView.hidden = NO;
        } else {
            _chartView.hidden = YES;
        }
        
        [_tableView reloadData];
        [_chartView strokeChart];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        _chartView.hidden = YES;
        [_tableView reloadData];
    }];
}


/**
 心率接口
 */
- (void)startRequestHBApi {
    
    ZJHeartbeatApi *api = [[ZJHeartbeatApi alloc] initWithCount:10 doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        ZJHeartBeatModel *model = [ZJHeartBeatModel yy_modelWithJSON:api.result];
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJHeartBeatModelData class] json:model.data];
        if (!kArrayIsEmpty(array)) {
            [_datasMarray addObjectsFromArray:array];
            _chartView.hidden = NO;
        } else {
            _chartView.hidden = YES;
        }
        
        [_tableView reloadData];
        [_chartView strokeChart];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        _chartView.hidden = YES;
        [_tableView reloadData];
    }];
    
}

/**
 请求获取血糖接口
 */
- (void)startRequestBsApi:(NSInteger)mCondtition {
 
    ZJBloodSugarConditionApi *api = [[ZJBloodSugarConditionApi alloc] initWithCount:10 doa:0 mCondition:_mCondition];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        ZJBloodSugarModel *model = [ZJBloodSugarModel yy_modelWithJSON:request.responseJSONObject];
            
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJBloodSugarModelData class] json:model.data];
        if (!kArrayIsEmpty(model.data)) {
            [_datasMarray addObjectsFromArray:array];
            _chartView.hidden = NO;
        } else {
            _chartView.hidden = YES;
        }
        
        [_tableView reloadData];
        [_chartView strokeChart];
    
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        _chartView.hidden = YES;
        [_tableView reloadData];
        [_chartView strokeChart];
    }];
}


- (void)startRequestBoApi {
   
    ZJBloodOxygenApi *api = [[ZJBloodOxygenApi alloc] initWithCount:10 doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        ZJBloodOxygenModel *model = [ZJBloodOxygenModel yy_modelWithJSON:api.result];
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJBloodOxygenModelData class] json:model.data];
        if (!kArrayIsEmpty(array)) {
            [_datasMarray addObjectsFromArray:array];
            _chartView.hidden = NO;
        } else {
            _chartView.hidden = YES;
        }
        
        [_tableView reloadData];
        [_chartView strokeChart];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        [_tableView reloadData];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _datasMarray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_tag == 100) {
        ZJDataBarBPDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZJDataBarBPDetailTableViewCell class]) forIndexPath:indexPath];
        [self setupBPModelOfCell:cell cellForRowAtIndexPath:indexPath];
        return cell;
    }
    
    ZJDataBarDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZJDataBarDetailTableViewCell class]) forIndexPath:indexPath];
    [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    return cell;
}

- (void)setupBPModelOfCell:(ZJDataBarBPDetailTableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZJBloodPressModelData *modelData = _datasMarray[indexPath.row];
    NSString *sbp = modelData.sPressure;
    NSString *dbp = modelData.dPressure;;
    NSString *time = modelData.upsaveTime;
    
    cell.sPressValue = sbp;
    cell.dPressValue = dbp;
    cell.dateLbl.text = time;
    
}

- (void)setupModelOfCell:(ZJDataBarDetailTableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_tag == 200) {
        ZJHeartBeatModelData *modelData = _datasMarray[indexPath.row];
        cell.pulseValue = modelData.pulse;
        cell.dateLbl.text = modelData.upsaveTime;
    }
    
    if (_tag == 300) {
        ZJBloodOxygenModelData *modelData = _datasMarray[indexPath.row];
        cell.oxygenValue = modelData.oxygen;
        cell.dateLbl.text = modelData.upsaveTime;
    }
    
    if (_tag == 500) {
        ZJBloodSugarModelData *modelData = _datasMarray[indexPath.row];
        cell.bloodSugarValue = modelData.bloodGluVal;
        cell.bsCondition = [modelData.mCondition integerValue];
        cell.dateLbl.text = modelData.upsaveTime;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_tag == 100) {
        return [tableView fd_heightForCellWithIdentifier:NSStringFromClass([ZJDataBarBPDetailTableViewCell class]) cacheByIndexPath:indexPath configuration:^(id cell) {
            [self setupBPModelOfCell:cell cellForRowAtIndexPath:indexPath];
        }];
    }
    
    return [tableView fd_heightForCellWithIdentifier:NSStringFromClass([ZJDataBarDetailTableViewCell class]) cacheByIndexPath:indexPath configuration:^(id cell) {
        [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    }];
}

#pragma mark - SCChartDataSource
// 横坐标标题数组
- (NSArray *)SCChart_xLableArray:(SCChart *)chart
{
    return @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10"];
}

// 数值多重数组
- (NSArray *)SCChart_yValueArray:(SCChart *)chart
{
    if (_tag == 100) {
        NSMutableArray *array1 = [NSMutableArray new];
        NSMutableArray *array2 = [NSMutableArray new];
        
        for (ZJBloodPressModelData *modelData in _datasMarray) {
            
            NSInteger sbp = [modelData.sPressure integerValue];
            NSInteger dbp = [modelData.dPressure integerValue];
            
            [array1 addObject:@(sbp)];
            [array2 addObject:@(dbp)];
        }
        
        return @[array1, array2];
    }
    
    if (_tag == 200) {
        NSMutableArray *array = [NSMutableArray new];
       
        for (ZJHeartBeatModelData *modelData in _datasMarray) {
            NSInteger hb = [modelData.pulse integerValue];
            [array addObject:@(hb)];
        }
        
        return @[array];
    }
    
    if (_tag == 300) {
        NSMutableArray *array = [NSMutableArray new];
        
        for (ZJBloodOxygenModelData *modelData in _datasMarray) {
            NSInteger hb = [modelData.oxygen integerValue];
            [array addObject:@(hb)];
        }
        
        return @[array];
    }
    
    if (_tag == 500) {
        NSMutableArray *array = [NSMutableArray new];
        
        for (ZJBloodSugarModelData *modelData in _datasMarray) {
            double hb = [modelData.bloodGluVal doubleValue];
            [array addObject:@(hb)];
        }
        
        return @[array];
    }
    
    return nil;
}

// 颜色数组
- (NSArray *)SCChart_ColorArray:(SCChart *)chart
{
    if (_tag == 100) {
        return @[COLOR_PICTONBLUE, [UIColor redColor]];
    }
    return @[COLOR_PICTONBLUE];
}

// 判断显示横线条
- (BOOL)SCChart:(SCChart *)chart ShowHorizonLineAtIndex:(NSInteger)index {
    return YES;
}

//判断显示最大最小值
- (BOOL)SCChart:(SCChart *)chart ShowMaxMinAtIndex:(NSInteger)index
{
    return NO;
}

# pragma mark - emptyDataSet
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"无数据";
    UIFont *font = [UIFont systemFontOfSize:14.0];
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:text];
    [attStr addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, text.length)];
    return attStr;
}
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    UIImage *image = [UIImage imageNamed:@"ic_no_data"];
    return image;
}

# pragma mark ZJDropSelectViewDelegate
- (void)selectIndex:(NSInteger)index title:(NSString *)title {
    NSLog(@"%ld", (long)index);
    _mCondition = index;
    [_navButton setTitle:[NSString stringWithFormat:@"血糖-%@", title] forState:UIControlStateNormal];
    [_navButton layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleRight imageTitleSpace:5];
    _datasMarray = [NSMutableArray new];
    [self startRequestBsApi:index];
    [_dropBackgroundView removeFromSuperview];
}

@end
