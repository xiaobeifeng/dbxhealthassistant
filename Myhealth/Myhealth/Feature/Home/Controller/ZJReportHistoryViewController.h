//
//  ZJReportHistoryViewController.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/8.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZJReportDetailViewController.h"

NS_ASSUME_NONNULL_BEGIN

/**
 历史报告
 */
@interface ZJReportHistoryViewController : UIViewController
@property (nonatomic, assign) ReportStyle reportStyle;

@end

NS_ASSUME_NONNULL_END
