//
//  ZJReportHistoryViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/8.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "ZJReportHistoryViewController.h"
#import "ZJReportHistoryTableViewCell.h"
#import "ZJProfileInfoApi.h"
#import "ZJProfileInfoModel.h"
#import "ZJFirstReportApi.h"
#import "ZJReportModel.h"
#import "ZJReportBpBxHbValueApi.h"
#import "ZJReportBpBxHbValueModel.h"
#import "ZJReportByTimeApi.h"
#import "ZJReportHistoryTableViewCell.h"

static NSString *kTableViewCellIdentifier = @"DBXReportHistoryTableViewCell";
@interface ZJReportHistoryViewController ()<UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@property(nonatomic, strong) UITableView *tableView;
/** 各项数值数组 */
@property (nonatomic, strong) NSMutableArray *dataMarray;

@end

@implementation ZJReportHistoryViewController

- (NSMutableArray *)dataMarray
{
    if (!_dataMarray) {
        _dataMarray = [NSMutableArray new];
    }
    return _dataMarray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    [self setupUI];

    [self startRequestFirstReportApi];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadView:) name:@"RELOAD_CHILDVIEWS" object:nil];
}

- (void)setupUI {
    UILabel *chartTitleLbl = [UILabel new];
    chartTitleLbl.frame = CGRectMake(0, 0, kScreenWidth-26, 40);
    chartTitleLbl.text = @"历史数据";
    chartTitleLbl.font = [UIFont systemFontOfSize:14];
    chartTitleLbl.textAlignment = NSTextAlignmentCenter;
    chartTitleLbl.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.view addSubview:chartTitleLbl];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 40, kScreenWidth-26, kScreenHeight-kSafeAreaToHeight-13-250-40-50-20) style:UITableViewStylePlain];
    [_tableView registerClass:[ZJReportHistoryTableViewCell class] forCellReuseIdentifier:kTableViewCellIdentifier];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.emptyDataSetSource = self;
    _tableView.emptyDataSetDelegate = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.tableFooterView = [UIView new];
    [self.view addSubview:_tableView];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 40, kScreenWidth-26, 44)];
    headerView.backgroundColor = COLOR_GALLERY;
    UILabel *timeLbl = [UILabel new];
    timeLbl.font = [UIFont systemFontOfSize:10];
    [headerView addSubview:timeLbl];
    
    UILabel *bloodPressLbl = [UILabel new];
    bloodPressLbl.font = [UIFont systemFontOfSize:10];
    [headerView addSubview:bloodPressLbl];
    
    UILabel *bloodOxygenLbl = [UILabel new];
    bloodOxygenLbl.font = [UIFont systemFontOfSize:10];
    [headerView addSubview:bloodOxygenLbl];
    
    UILabel *heartBeatLbl = [UILabel new];
    heartBeatLbl.font = [UIFont systemFontOfSize:10];
    [headerView addSubview:heartBeatLbl];
    
    UILabel *bloodPressLevelLbl = [UILabel new];
    bloodPressLevelLbl.numberOfLines = 0;
    bloodPressLevelLbl.font = [UIFont systemFontOfSize:10];
    [headerView addSubview:bloodPressLevelLbl];

    timeLbl.textAlignment = NSTextAlignmentCenter;
    bloodPressLbl.textAlignment = NSTextAlignmentCenter;
    bloodOxygenLbl.textAlignment = NSTextAlignmentCenter;
    heartBeatLbl.textAlignment = NSTextAlignmentCenter;
    bloodPressLevelLbl.textAlignment = NSTextAlignmentCenter;
    
    timeLbl.text = @"时间";
    bloodPressLbl.text = @"血压";
    bloodOxygenLbl.text = @"血氧";
    heartBeatLbl.text = @"心率";
    bloodPressLevelLbl.text = @"血压级别";
    
    [@[timeLbl, bloodPressLbl, bloodOxygenLbl, heartBeatLbl, bloodPressLevelLbl] mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:5 leadSpacing:13 tailSpacing:13];
    
    [@[timeLbl, bloodPressLbl, bloodOxygenLbl, heartBeatLbl, bloodPressLevelLbl] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(headerView.mas_centerY);
    }];
    
    _tableView.tableHeaderView = headerView;
}

- (void)reloadView:(NSNotification *)notification {
    
    NSLog(@"%@", notification);
    
    NSString *startTime = [notification.userInfo objectForKey:@"STARTTIME"];
    NSString *endTime = [notification.userInfo objectForKey:@"ENDTIME"];
    
    [self startRequestReportBpBxHbValueApiWithStartTime:startTime endTime:endTime];
    
}

#pragma mark 请求第一条报告
- (void)startRequestFirstReportApi {
    
    ZJFirstReportApi *api = [[ZJFirstReportApi alloc] initWithCount:1 flag:_reportStyle doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        ZJReportModel *model = [ZJReportModel yy_modelWithJSON:request.responseObject];
        
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJReportModelData class] json:model.data];
        
        if (!kArrayIsEmpty(array)) {
            
            ZJReportModelData *modelData = array.firstObject;
            
            [self startRequestReportBpBxHbValueApiWithStartTime:modelData.starttime endTime:modelData.endtime];
            
        } else {
            
            
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

- (void)startRequestReportBpBxHbValueApiWithStartTime:(NSString *)startTime endTime:(NSString *)endTime {
    
    ZJReportBpBxHbValueApi *api = [[ZJReportBpBxHbValueApi alloc] initWithStratTime:startTime endTime:endTime];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            if (!kObjectIsEmpty(request.responseJSONObject)) {
                ZJReportBpBxHbValueModel *model = [ZJReportBpBxHbValueModel yy_modelWithJSON:request.responseJSONObject];
             
                [self.dataMarray removeAllObjects];
               
                NSArray *array = [NSArray yy_modelArrayWithClass:[ZJReportBpBxHbValueModelData class] json:model.data];
                
                self.dataMarray = [[NSMutableArray alloc] initWithArray:array];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.tableView reloadData];
            });
            
        });
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        [self.tableView reloadData];
    }];
}

#pragma mark - UITableViewDelegate and DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataMarray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ZJReportHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifier forIndexPath:indexPath];
    
    [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    
    // 设置cell颜色
    if (indexPath.row % 2 == 0) {
        cell.backgroundColor = [UIColor colorWithHexString:@"C4CBFC"];
    }else{
        cell.backgroundColor = [UIColor colorWithHexString:@"FFFFFF"];
    }
    
    return cell;
}

- (void)setupModelOfCell:(ZJReportHistoryTableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        ZJReportBpBxHbValueModelData *model = self.dataMarray[indexPath.row];
        cell.timeLbl.text = kStringIsEmpty(model.upsaveTime)?@"--":[model.upsaveTime substringWithRange:NSMakeRange(5, 11)];
        
        if (kStringIsEmpty(model.sPressure)&&kStringIsEmpty(model.dPressure)) {
            cell.bloodPressLbl.text = @"--";
        } else {
            cell.bloodPressLbl.text = [NSString stringWithFormat:@"%@/%@", model.sPressure, model.dPressure];
        }
        
        
        cell.bloodOxygenLbl.text = kStringIsEmpty(model.oxygen)?@"--":model.oxygen;
        cell.heartBeatLbl.text = kStringIsEmpty(model.pulse)?@"--":model.pulse;
        
        if (kStringIsEmpty(model.sPressure)&&kStringIsEmpty(model.dPressure)) {
            cell.bloodPressLevelLbl.text = @"--";
        } else {
            NSInteger sPressure = [model.sPressure integerValue];
            NSInteger dPressure = [model.dPressure integerValue];
            if (sPressure < 120 && dPressure < 80) {
                cell.bloodPressLevelLbl.text = @"正常血压";
            }else if((sPressure >= 120 && sPressure < 140) && (dPressure >= 80 && dPressure < 90)){
                cell.bloodPressLevelLbl.text = @"正常高值";
            }else if(sPressure >= 140 && dPressure < 90){
                cell.bloodPressLevelLbl.text = @"单纯收缩期高血压";
            }else if((sPressure >= 140 && sPressure < 160) || (dPressure >= 90 && dPressure < 100)){
                cell.bloodPressLevelLbl.text = @"1级高血压(轻度)";
            }else if((sPressure >= 160 && sPressure < 180) || (dPressure >= 100 && dPressure < 110)){
                cell.bloodPressLevelLbl.text = @"2级高血压(中度)";
            }else if(sPressure >= 180 || dPressure >= 110){
                cell.bloodPressLevelLbl.text = @"3级高血压(重度)";
            }else{
                cell.bloodPressLevelLbl.text = @"--";
            }
        }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView fd_heightForCellWithIdentifier:kTableViewCellIdentifier cacheByIndexPath:indexPath configuration:^(id cell) {
        
        [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    }];
}

# pragma mark - emptyDataSet
/**
 *  返回标题文字
 */
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"无数据";
    UIFont *font = [UIFont systemFontOfSize:14.0];
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:text];
    [attStr addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, text.length)];
    return attStr;
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    UIImage *image = [UIImage imageNamed:@"ic_no_data"];
    return image;
}

/**
 *  处理空白区域的点击事件
 */
- (void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view {
    NSLog(@"%s",__FUNCTION__);
}


@end
