//
//  ZJHomeViewController.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/13.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJHomeViewController.h"
#import "ZJHomeDataTableViewCell.h"
#import "ZJHomeReportTableViewCell.h"
#import "ZJHomeSportTableViewCell.h"
#import "NewPagedFlowView.h"
#import "PGIndexBannerSubiew.h"
#import "ZJReportShowViewController.h"
#import "ZJBloodPressApi.h"
#import "ZJBloodPressModel.h"
#import "ZJDataBarChatController.h"
#import "ZJCarouselApi.h"
#import "ZJCarouselModel.h"
#import "ZJNewsDetailViewController.h"
#import "ZJSetpNumberApi.h"
#import "ZJStepNumberModel.h"
#import "ZJProfileInfoApi.h"
#import "ZJProfileInfoModel.h"
#import "ZJBloodSugarModel.h"
#import "ZJBloodSugarApi.h"

#define autoRollView_height 180

@interface ZJHomeViewController ()<UITableViewDelegate, UITableViewDataSource, NewPagedFlowViewDelegate, NewPagedFlowViewDataSource, ZJHomeReportTableViewCellDelegate, ZJHomeDataTableViewCellDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, copy) NSString *bloodPre;
@property (nonatomic, assign) double sbpBloodPre;
@property (nonatomic, copy) NSString *bloodPreUploadTime;
@property (nonatomic, assign) BOOL isBloodPreWarn;

@property (nonatomic, strong) ZJStepNumberModelData *stepNumberModelData;

@property (nonatomic, assign) NSInteger height;
@property (nonatomic, assign) NSInteger weight;
@property (nonatomic, assign) double bsValue;            // 血糖值
@property (nonatomic, copy) NSString *bsUpSaveTime;     // 血糖上传时间
@property (nonatomic, assign) NSInteger bsCondition;    // 血糖测量时段
@property (nonatomic, strong) NSMutableArray *carouselModelArray;   // 轮播图数据模型
@property (nonatomic, strong) NewPagedFlowView *autoRollView;   // 轮播图
@property (nonatomic, strong) UIPageControl *pageControl;        // 轮播图分页视图
@property (nonatomic, strong) ZJBloodPressModelData *bpModelData;   // 血压数据模型
@property (nonatomic, strong) ZJBloodSugarModelData *bsModelData;   // 血糖数据模型
@end

@implementation ZJHomeViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tableView  scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];   // 自动滚动到顶部
    
    [self startRequsetBPApi];   // 请求血压数据
    [self startRequestStepNumberApi];   // 请求步数
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // 检查版本更新
    [XDUpdateManager CheckVersionUpadateWithForce:NO];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshHeaderAction) name:@"reload_HomeVC" object:nil];
    
    [self setupUI];
    
    [self startRequsetCarouselApi]; // 请求轮播图api
    
}

/**
 搭建界面
 */
- (void)setupUI {
    
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    _tableView.backgroundColor = COLOR_GALLERY;
    [_tableView registerClass:[ZJHomeDataTableViewCell class] forCellReuseIdentifier:NSStringFromClass([ZJHomeDataTableViewCell class])];
    [_tableView registerClass:[ZJHomeReportTableViewCell class] forCellReuseIdentifier:NSStringFromClass([ZJHomeReportTableViewCell class])];
    [_tableView registerClass:[ZJHomeSportTableViewCell class] forCellReuseIdentifier:NSStringFromClass([ZJHomeSportTableViewCell class])];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.tableFooterView = [UIView new];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshHeaderAction)];
    [self.view addSubview:_tableView];
    
    // 轮播图
    _autoRollView = [[NewPagedFlowView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 0)];
    _autoRollView.delegate = self;
    _autoRollView.dataSource = self;
    _autoRollView.minimumPageAlpha = 0.1;
    _autoRollView.isCarousel = YES;
    _autoRollView.leftRightMargin = 5;
    _autoRollView.topBottomMargin = 20;
    _autoRollView.orientation = NewPagedFlowViewOrientationHorizontal;
    _autoRollView.isOpenAutoScroll = YES;
    self.tableView.tableHeaderView = _autoRollView;
    
    // 初始化pageControl
    _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 8)];
    _autoRollView.pageControl = _pageControl;
    [_autoRollView addSubview:_pageControl];
    
}

- (void)refreshHeaderAction {

    [self startRequsetCarouselApi]; // 请求轮播图api
    [self startRequsetBPApi];   // 请求血压数据
    [self startRequestStepNumberApi];   // 请求步数
}

#pragma mark NewPagedFlowView Datasource and Delegate
- (CGSize)sizeForPageInFlowView:(NewPagedFlowView *)flowView {
    return CGSizeMake(kScreenWidth - 20, autoRollView_height);
}

- (void)didSelectCell:(UIView *)subView withSubViewIndex:(NSInteger)subIndex {
    
    ZJCarouselModelData *modelData = _carouselModelArray[subIndex];
    ZJNewsDetailViewController *newsDetailVC = [ZJNewsDetailViewController new];
    newsDetailVC.ID = modelData.news_id;
    [self.navigationController pushViewController:newsDetailVC animated:YES];
    
}

- (NSInteger)numberOfPagesInFlowView:(NewPagedFlowView *)flowView {
    return _carouselModelArray.count;
}

- (PGIndexBannerSubiew *)flowView:(NewPagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index{
    PGIndexBannerSubiew *bannerView = [flowView dequeueReusableCell];
    if (!bannerView) {
        bannerView = [[PGIndexBannerSubiew alloc] init];
        bannerView.tag = index;
        bannerView.layer.cornerRadius = 4;
        bannerView.layer.masksToBounds = YES;
    }
    
    ZJCarouselModelData *modelData = _carouselModelArray[index];
    //在这里下载网络图片
    [bannerView.mainImageView sd_setImageWithURL:[NSURL URLWithString:URL_BANNER(modelData.path)] placeholderImage:[UIImage imageNamed:@"view_load_error"]];
    
    return bannerView;
}


#pragma mark  请求接口
- (void)startRequsetCarouselApi {
    
    ZJCarouselApi *api = [[ZJCarouselApi alloc] init];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        ZJCarouselModel *model = [ZJCarouselModel yy_modelWithJSON:request.responseJSONObject];
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJCarouselModelData class] json:model.data];
        if (!kArrayIsEmpty(array)) {

            _carouselModelArray = [[NSMutableArray alloc] initWithArray:array];
            _autoRollView.height = autoRollView_height;
            _pageControl.zj_y = autoRollView_height - 20;
            [self.autoRollView reloadData];
        }
     
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}


/**
 请求第一条血压数据
 */
- (void)startRequsetBPApi {
    
    ZJBloodPressApi *api = [[ZJBloodPressApi alloc] initWithCount:1 doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        ZJBloodPressModel *model = [ZJBloodPressModel yy_modelWithJSON:api.result];
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJBloodPressModelData class] json:model.data];
        if (!kArrayIsEmpty(array)) {
            _bpModelData = array[0];
        } else {
            _bpModelData = nil;
        }
        
        [self startRequsetBSApi];   // 请求血糖数据
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        _bpModelData = nil;
        
        [self startRequsetBSApi];   // 请求血糖数据
    }];
    
}

/**
 请求第一条血糖数据
 */
- (void)startRequsetBSApi {
    
    ZJBloodSugarApi *api = [[ZJBloodSugarApi alloc] initWithCount:1 doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", api.result);
        
        ZJBloodSugarModel *model = [ZJBloodSugarModel yy_modelWithJSON:api.result];
        
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJBloodSugarModelData class] json:model.data];
        
        if (!kArrayIsEmpty(array)) {
            _bsModelData = array[0];
        } else {
            _bsModelData = nil;
        }
        
        // 刷新cell
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
        [self.tableView.mj_header endRefreshing];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        _bsModelData = nil;
        
        // 刷新cell
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
        [self.tableView.mj_header endRefreshing];
    }];
}

/**
 获取步数api
 */
- (void)startRequestStepNumberApi{
    
    ZJSetpNumberApi *api = [[ZJSetpNumberApi alloc] initWithCount:1 doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        ZJStepNumberModel *model = [ZJStepNumberModel yy_modelWithJSON:request.responseJSONObject];
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJStepNumberModelData class] json:model.data];
        if (!kArrayIsEmpty(array)) {
            _stepNumberModelData =  array[0];
        } else {
            _stepNumberModelData = nil;
        }
        
        [self startRequestUserInfo];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        _stepNumberModelData = nil;
        
        // 刷新cell
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
        [self.tableView.mj_header endRefreshing];
    }];
    
}

/**
 请求个人资料信息
 */
- (void)startRequestUserInfo {
    
    // 获取身高体重
    ZJProfileInfoApi *api = [ZJProfileInfoApi new];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        ZJProfileInfoModel *model = [ZJProfileInfoModel yy_modelWithJSON:request.responseJSONObject];
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJProfileInfoModelData class] json:model.data];
        
        if (!kArrayIsEmpty(array)) {
            ZJProfileInfoModelData *modelData = array.firstObject;
            // 身高
            _height = [modelData.hight integerValue];
            // 体重
            _weight = [modelData.weight integerValue];
        } else {
            _height = 170;
            _weight = 0;
        }
        
        // 刷新cell
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
        [self.tableView.mj_header endRefreshing];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        _height = 170;
        _weight = 0;
        
        // 刷新cell
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
        [self.tableView.mj_header endRefreshing];
    }];
}


#pragma mark tableView Delegate and DateSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        ZJHomeDataTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZJHomeDataTableViewCell class]) forIndexPath:indexPath];
        cell.delegate = self;
        [self setupDataModelOfCell:cell atIndexPath:indexPath];
        return cell;
    }
    
    if (indexPath.row == 1) {
        ZJHomeReportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZJHomeReportTableViewCell class]) forIndexPath:indexPath];
        cell.delegate = self;
        [self setupReportModelOfCell:cell atIndexPath:indexPath];
        return cell;
    }
    
    ZJHomeSportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZJHomeSportTableViewCell class]) forIndexPath:indexPath];
    [self setupSportModelOfCell:cell atIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        return [tableView fd_heightForCellWithIdentifier:NSStringFromClass([ZJHomeDataTableViewCell class]) cacheByIndexPath:indexPath configuration:^(id cell) {
            [self setupDataModelOfCell:cell atIndexPath:indexPath];
        }];
    }
    
    if (indexPath.row == 1) {
        return [tableView fd_heightForCellWithIdentifier:NSStringFromClass([ZJHomeReportTableViewCell class]) cacheByIndexPath:indexPath configuration:^(id cell) {
            [self setupReportModelOfCell:cell atIndexPath:indexPath];
        }];
    }
    
    return [tableView fd_heightForCellWithIdentifier:NSStringFromClass([ZJHomeSportTableViewCell class]) cacheByIndexPath:indexPath configuration:^(id cell) {
        [self setupSportModelOfCell:cell atIndexPath:indexPath];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        
    }
    
    if (indexPath.row == 1) {
        
    }
    
    if (indexPath.row == 2) {
        
    }
}


- (void)setupDataModelOfCell:(ZJHomeDataTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    if (!kObjectIsEmpty(_bpModelData)) {
        NSInteger sbp = [_bpModelData.sPressure integerValue];
        NSInteger dbp = [_bpModelData.dPressure integerValue];
        cell.bpValueLabel.text = [NSString stringWithFormat:@"%ld/%ld", (long)sbp, (long)dbp];
        cell.sbpValue = sbp;
        cell.dbpValue = dbp;
        cell.bpStateButton.hidden = NO;
    } else {
        cell.bpValueLabel.text = @"--";
        cell.bpStateButton.hidden = YES;
        cell.sbpValue = 0;
    }
    
    if (!kObjectIsEmpty(_bsModelData)) {
        double bs = [_bsModelData.bloodGluVal doubleValue];
        NSInteger condition = [_bsModelData.mCondition integerValue];
        cell.bsValueLabel.text = [NSString stringWithFormat:@"%.1f", bs];
        cell.bsValue = bs;
        cell.bsCondition = condition;
        cell.bsStateButton.hidden = NO;
    } else {
        cell.bsValueLabel.text = @"--";
        cell.bsStateButton.hidden = YES;
        cell.bsValue = 0;
    }
    
}

- (void)setupReportModelOfCell:(ZJHomeReportTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)setupSportModelOfCell:(ZJHomeSportTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
   
    if (!kObjectIsEmpty(_stepNumberModelData)) {
        cell.timeLable.text = [NSString stringWithFormat:@"%@", _stepNumberModelData.upsavetime];
        cell.stepValueLable.text = _stepNumberModelData.steps;
        NSInteger calorie = [_stepNumberModelData.steps integerValue]*_height*_weight*1.036/300000;    // 计算卡路里 步数*身高/300000*体重*1.036 身高默认170 体重默认0
        cell.kcalValueLable.text = [NSString stringWithFormat:@"%ld", calorie];
    } else {
        cell.stepValueLable.text = @"--";
        cell.kcalValueLable.text = @"--";
    }
    
    
}

#pragma mark ZJHomeReportTableViewCellDelegate
- (void)homeReportDataTableViewCellDetailButtonAction:(UIButton *)button
{
    // 周报告
    if (button.tag == 100) {
        //        DBXReportDetailViewController *reportDetailVC = [DBXReportDetailViewController new];
        //        reportDetailVC.flag = 1;
        //        [self.navigationController pushViewController:reportDetailVC animated:YES];
        ZJReportDetailViewController *reportDetailVC = [ZJReportDetailViewController new];
        reportDetailVC.title = @"周报告";
        reportDetailVC.reportStyle = WeekReport;
        [self.navigationController pushViewController:reportDetailVC animated:YES];
    }
    
    // 月报告
    if (button.tag == 101) {
        ZJReportDetailViewController *reportDetailVC = [ZJReportDetailViewController new];
        reportDetailVC.title = @"月报告";
        reportDetailVC.reportStyle = MonthReport;
        [self.navigationController pushViewController:reportDetailVC animated:YES];
    }
    
    // 年报告
    if (button.tag == 102) {
        ZJReportDetailViewController *reportDetailVC = [ZJReportDetailViewController new];
        reportDetailVC.title = @"年报告";
        reportDetailVC.reportStyle = YearReport;
        [self.navigationController pushViewController:reportDetailVC animated:YES];
    }
}

#pragma mark - homeDataTableViewCellDetailButtonAction
- (void)homeDataTableViewCellDetailButtonAction
{
    // 条形图展示用户生理指标
    ZJDataBarChatController *dataBarChatVC = [ZJDataBarChatController new];
    [self.navigationController pushViewController:dataBarChatVC animated:YES];
}

@end
