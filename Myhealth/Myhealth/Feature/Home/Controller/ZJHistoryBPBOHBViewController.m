
//  Created by zhoujian on 2018/8/31.
//  Copyright © 2018年 mc. All rights reserved.
//

#import "ZJHistoryBPBOHBViewController.h"
#import "ZJHistoryBPBOHBTableViewCell.h"
#import "SMKJSelectDateView.h"
#import "ZJHistoryBPBSHBModel.h"
#import "ZJHistoryBPBOHBApi.h"
#import "ZJHistoryDeleteBPBOHBApi.h"

static NSString * const kBpBsHbTableViewCellIdentifier = @"DBXBpBsHbTableViewCell.h";

@interface ZJHistoryBPBOHBViewController ()<UITableViewDelegate, UITableViewDataSource, SMKJSelectDateViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) SMKJSelectDateView *selectDateView;
@property (nonatomic, strong) NSDate *chooseDate;
@property (nonatomic, copy) NSString *selectDateStr;
@property(nonatomic, assign) NSInteger currentPage;
@property (nonatomic, strong) NSMutableArray*dataSourceArray;

@end

@implementation ZJHistoryBPBOHBViewController

#pragma mark - 懒加载
- (NSMutableArray *)dataSourceArray {
    if (!_dataSourceArray) {
        _dataSourceArray = [NSMutableArray array];
    }
    return _dataSourceArray;
}

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 50, kScreenWidth, kScreenHeight-kSafeAreaToHeight-50) style:UITableViewStylePlain];
        [_tableView registerClass:[ZJHistoryBPBOHBTableViewCell class] forCellReuseIdentifier:kBpBsHbTableViewCellIdentifier];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.96 alpha:1.00];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(handleHeaderEvent)];
        _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(handleFooterEvent)];
        _tableView.tableFooterView = [UIView new];
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.emptyDataSetSource = self;
        _tableView.emptyDataSetDelegate = self;
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"查询历史";
    
    _chooseDate = [NSDate date];
    _selectDateView = [[SMKJSelectDateView alloc] initWithDate:_chooseDate];
    _selectDateView.delegate = self;
    _selectDateView.frame = CGRectMake(0, 0, kScreenWidth, 50);
    _selectDateView.backgroundColor = COLOR_PICTONBLUE;
    [self.view addSubview: _selectDateView];
    
    [self.view addSubview:self.tableView];
    
    [self handleHeaderEvent];
}

/**
 下拉刷新
 */
- (void)handleHeaderEvent {
    _currentPage = 0;
    [self requestListDataWithDate:[self dateFormatterWithDate:_chooseDate]];
}

/**
 上拉刷新
 */
- (void)handleFooterEvent {
    [self requestListDataWithDate:[self dateFormatterWithDate:_chooseDate]];
}

/**
 将日期格式化指定样式的字符串
 NO:YYYY-MM
 
 @param date 需要转成字符串的日期
 @return 指定样式字符串
 */
- (NSString *)dateFormatterWithDate:(NSDate *)date {
    
    
    NSDateFormatter *formatterYearAndMonth = [[NSDateFormatter alloc] init];
    
    formatterYearAndMonth.dateFormat = @"YYYY-MM";
    
    NSString *dateStr = [formatterYearAndMonth stringFromDate:date];
    return dateStr;
}

- (void)requestListDataWithDate:(NSString *)dateStr {
    
    ZJHistoryBPBOHBApi *api = [[ZJHistoryBPBOHBApi alloc] initWithSelectTime:dateStr currentPage:_currentPage];
    __weak __typeof(self)weakSelf = self;
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
                if (request.responseJSONObject) {
        
                    NSLog(@"%@", request.responseJSONObject);
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
                        ZJHistoryBPBSHBModel *model = [ZJHistoryBPBSHBModel yy_modelWithJSON:request.responseJSONObject];
        
                        //  请求成功
                        if ([model.state integerValue] == 0) {
        
                            // currentPage为0
                            if (weakSelf.currentPage == 0) {
                                [weakSelf.dataSourceArray removeAllObjects];
                            }
                            [weakSelf.dataSourceArray addObjectsFromArray:[NSArray yy_modelArrayWithClass:[ZJHistoryBPBSHBModelData class] json:model.data]];
        
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (weakSelf.currentPage != 0) {
        
                                    [weakSelf.tableView reloadData];
                                    [weakSelf.tableView.mj_footer endRefreshing];
        
                                } else {
        
                                    [weakSelf.tableView reloadData];
                                    [weakSelf.tableView.mj_header endRefreshing];
        
                                }
        
                                if (model.data.count != 0) {
                                    weakSelf.currentPage += 1;
                                }
        
                                // TODO: 根据数据源控制上拉刷新显隐
                                if (weakSelf.dataSourceArray.count < 10) {
                                    weakSelf.tableView.mj_footer.hidden = YES;
                                } else {
                                    weakSelf.tableView.mj_footer.hidden = NO;
                                }
        
        
                            });
        
                        }
                    });
                }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
    
}

# pragma mark - selectDateView Delegate
- (void)selectDateViewClick:(SMKJSelectDateBtnType)type
{
    
    [self.tableView  scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    
    if (type == MonthReduce) {
        
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        dateComponents.month = -1;
        NSDate *fromMonth = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:_chooseDate options:0];
        _chooseDate = fromMonth;
        NSDateFormatter *formatterYearAndMonth = [[NSDateFormatter alloc] init];
        formatterYearAndMonth.dateFormat = @"YYYY年MM月";
        NSString *chooseDate = [formatterYearAndMonth stringFromDate:_chooseDate];
        _selectDateView.selectDateString = chooseDate;
        
        NSDateFormatter *formatterYearAndMonth1 = [[NSDateFormatter alloc] init];
        formatterYearAndMonth1.dateFormat = @"YYYY-MM";
        _selectDateStr = [formatterYearAndMonth1 stringFromDate:_chooseDate];
        [self handleHeaderEvent];
        
    }
    
    if (type == MonthAdd) {
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        dateComponents.month = +1;
        NSDate *fromMonth = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:_chooseDate options:0];
        _chooseDate = fromMonth;
        NSDateFormatter *formatterYearAndMonth = [[NSDateFormatter alloc] init];
        formatterYearAndMonth.dateFormat = @"YYYY年MM月";
        NSString *chooseDate = [formatterYearAndMonth stringFromDate:_chooseDate];
        _selectDateView.selectDateString = chooseDate;
        
        NSDateFormatter *formatterYearAndMonth1 = [[NSDateFormatter alloc] init];
        formatterYearAndMonth1.dateFormat = @"YYYY-MM";
        _selectDateStr = [formatterYearAndMonth1 stringFromDate:_chooseDate];
        [self handleHeaderEvent];
    }
    
    if (type == CalendarShow) {
        //        SMKJCalendarView *calendarView = [[SMKJCalendarView alloc] initWithCurrentSelectDate:_chooseDate];
        //        calendarView.delegate = self;
        //        [calendarView show];
    }
}

#pragma mark - UITableView代理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZJHistoryBPBOHBTableViewCell *bpBsHbTableViewCell = [tableView dequeueReusableCellWithIdentifier:kBpBsHbTableViewCellIdentifier forIndexPath:indexPath];
    
    [self setupModelOfCell:bpBsHbTableViewCell cellForRowAtIndexPath:indexPath];
    
    return bpBsHbTableViewCell;
    
}

- (void)setupModelOfCell:(ZJHistoryBPBOHBTableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZJHistoryBPBSHBModelData *model = self.dataSourceArray[indexPath.row];
    
    NSString *time = model.upsaveTime;
    NSString *sbp = kStringIsEmpty(model.sPressure)?@"--":model.sPressure;
    NSString *dbp = kStringIsEmpty(model.dPressure)?@"--":model.dPressure;
    NSString *hb = kStringIsEmpty(model.pulse)?@"--":model.pulse;
    NSString *bo = kStringIsEmpty(model.oxygen)?@"--":model.oxygen;
    
    cell.dateLbl.text = time;
    cell.sbpLabel.text = [NSString stringWithFormat:@"收缩压：%@ mmHg", sbp];
    cell.dbpLabel.text = [NSString stringWithFormat:@"舒张压：%@ mmHg", dbp];
    cell.heartBeatLbl.text = [NSString stringWithFormat:@"心率：%@ bpm", hb];
    cell.oxyLbl.text = [NSString stringWithFormat:@"血氧：%@ %%", bo];
    
    cell.sbpValue = [sbp integerValue];
    cell.dbpValue = [dbp integerValue];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView fd_heightForCellWithIdentifier:kBpBsHbTableViewCellIdentifier configuration:^(id cell) {
        [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    }];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath*)indexPath
{
    // 执行删除操作
    ZJHistoryBPBSHBModelData *model = self.dataSourceArray[indexPath.row];
    ZJHistoryDeleteBPBOHBApi *api = [[ZJHistoryDeleteBPBOHBApi alloc] initWithId:model.Id];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [self.dataSourceArray removeObjectAtIndex:indexPath.row];
        
        if (kArrayIsEmpty(_dataSourceArray)) {
            [self.tableView reloadData];
        } else {
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:indexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        [self.tableView reloadData];
        [MBProgressHUD showErrorMessage:@"请稍后再试"];
        
    }];

}


# pragma mark - emptyDataSet
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"无数据";
    UIFont *font = [UIFont systemFontOfSize:14.0];
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:text];
    [attStr addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, text.length)];
    return attStr;
}
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    UIImage *image = [UIImage imageNamed:@"ic_no_data"];
    return image;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


@end
