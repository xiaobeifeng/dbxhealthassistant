//
//  ZJDataAnalysisViewController.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/8.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZJReportDetailViewController.h"

NS_ASSUME_NONNULL_BEGIN

/**
 数据分析
 */
@interface ZJDataAnalysisViewController : UIViewController

@property (nonatomic, assign) ReportStyle reportStyle;
/** 开始时间 */
@property(nonatomic, copy) NSString *startTime;
/** 结束时间 */
@property(nonatomic, copy) NSString *endTime;
- (void)requestBeforeApi;


@end

NS_ASSUME_NONNULL_END
