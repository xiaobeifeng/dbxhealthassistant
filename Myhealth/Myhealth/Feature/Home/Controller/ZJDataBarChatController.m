
//  Created by zhoujian on 2018/11/2.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJDataBarChatController.h"
#import "ZJBarChatBPTableViewCell.h"
#import "ZJBarChatBSTableViewCell.h"
#import "ZJBloodPressModel.h"
#import "ZJBloodPressApi.h"
#import "ZJHeartBeatModel.h"
#import "ZJHeartbeatApi.h"
#import "ZJBloodOxygenModel.h"
#import "ZJBloodOxygenApi.h"
#import "ZJBloodSugarModel.h"
#import "ZJBloodSugarApi.h"
#import "ZJBarChatHBTableViewCell.h"
#import "ZJDataBarDetailViewController.h"

@interface ZJDataBarChatController ()

@property (nonatomic, assign) NSInteger newestSbpValue;     // 最新高压数据
@property (nonatomic, assign) NSInteger newestDbpValue;     // 最新低压数据
@property (nonatomic, assign) NSInteger newestHBValue;      // 最新心率数据
@property (nonatomic, assign) NSInteger newestBSValue;      // 最新血糖数据
@property (nonatomic, assign) NSInteger newestBOValue;      // 最新血氧数据

@property (nonatomic, strong) ZJBloodPressModelData *bloodPressModelData;
@property (nonatomic, strong) ZJHeartBeatModelData *heartBeatModelData;
@property (nonatomic, strong) ZJBloodSugarModelData *bloodSugarModelData;
@property (nonatomic, strong) ZJBloodOxygenModelData *bloodOxygenModelData;

@end

@implementation ZJDataBarChatController

- (instancetype)initWithStyle:(UITableViewStyle)style
{
    return [super initWithStyle:UITableViewStylePlain];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self startRequestBpApi];
    [self startRequestHbApi];
    [self startRequestBSApi];
    [self startRequestBOApi];
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.navigationItem.title = @"数据";
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = COLOR_GALLERY;
    [self.tableView registerClass:[ZJBarChatBPTableViewCell class] forCellReuseIdentifier:NSStringFromClass([ZJBarChatBPTableViewCell class])];
    [self.tableView registerClass:[ZJBarChatBSTableViewCell class] forCellReuseIdentifier:NSStringFromClass([ZJBarChatBSTableViewCell class])];
    [self.tableView registerClass:[ZJBarChatHBTableViewCell class] forCellReuseIdentifier:NSStringFromClass([ZJBarChatHBTableViewCell class])];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}


#pragma mark 请求Api
/**
 获取最新一条血压数据
 */
- (void)startRequestBpApi {
    ZJBloodPressApi *api = [[ZJBloodPressApi alloc] initWithCount:1 doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        ZJBloodPressModel *model = [ZJBloodPressModel yy_modelWithJSON:api.result];
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJBloodPressModelData class] json:model.data];
        if (!kArrayIsEmpty(array)) {
            _bloodPressModelData = array[0];
        } else {
            _bloodPressModelData = nil;
        }
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
         _bloodPressModelData = nil;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }];
}

/**
 获取最新一条心率数据
 */
- (void)startRequestHbApi {
    ZJHeartbeatApi *api = [[ZJHeartbeatApi alloc] initWithCount:1 doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        ZJHeartBeatModel *model = [ZJHeartBeatModel yy_modelWithJSON:api.result];
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJHeartBeatModelData class] json:model.data];
        if (!kArrayIsEmpty(array)) {
            _heartBeatModelData = array[0];
        } else {
            _heartBeatModelData = nil;
        }
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        _heartBeatModelData = nil;
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];

    }];
}

/**
 获取最新一条血糖数据
 */
- (void)startRequestBSApi {
    ZJBloodSugarApi *api = [[ZJBloodSugarApi alloc] initWithCount:1 doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        ZJBloodSugarModel *model = [ZJBloodSugarModel yy_modelWithJSON:api.result];
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJBloodSugarModelData class] json:model.data];
        if (!kArrayIsEmpty(array)) {
            _bloodSugarModelData = array[0];
        } else {
            _bloodSugarModelData = nil;
        }
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        _bloodSugarModelData = nil;
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }];
}

/**
 获取最新一条血氧数据
 */
- (void)startRequestBOApi {
    ZJBloodOxygenApi *api = [[ZJBloodOxygenApi alloc] initWithCount:1 doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", api.result);
        
        ZJBloodOxygenModel *model = [ZJBloodOxygenModel yy_modelWithJSON:api.result];
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJBloodOxygenModelData class] json:model.data];
        if (!kArrayIsEmpty(array)) {
            _bloodOxygenModelData = array[0];
        } else {
            _bloodOxygenModelData = nil;
        }
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:3 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        _bloodOxygenModelData = nil;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:3 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }];
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        ZJBarChatBPTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZJBarChatBPTableViewCell class]) forIndexPath:indexPath];
        [self setupBloodPreModelOfCell:cell atIndexPath:indexPath];
        
        return cell;
    }
    
    if (indexPath.row == 1) {
        ZJBarChatHBTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZJBarChatHBTableViewCell class]) forIndexPath:indexPath];
        [self setupHeartBeatModelOfCell:cell atIndexPath:indexPath];
        
        return cell;
    }
    
    ZJBarChatBSTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZJBarChatBSTableViewCell class]) forIndexPath:indexPath];
    [self setupBloodSugarModelOfCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        return [tableView fd_heightForCellWithIdentifier:NSStringFromClass([ZJBarChatBPTableViewCell class]) cacheByIndexPath:indexPath configuration:^(id cell) {
            [self setupBloodPreModelOfCell:cell atIndexPath:indexPath];
        }];
    }
    
    if (indexPath.row == 1) {
        return [tableView fd_heightForCellWithIdentifier:NSStringFromClass([ZJBarChatHBTableViewCell class]) cacheByIndexPath:indexPath configuration:^(id cell) {
            [self setupHeartBeatModelOfCell:cell atIndexPath:indexPath];
        }];
    }
    
    
    return [tableView fd_heightForCellWithIdentifier:NSStringFromClass([ZJBarChatBSTableViewCell class]) cacheByIndexPath:indexPath configuration:^(id cell) {
        [self setupBloodSugarModelOfCell:cell atIndexPath:indexPath];
    }];
    
}

- (void)setupBloodPreModelOfCell:(ZJBarChatBPTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    if (!kObjectIsEmpty(_bloodPressModelData)) {
        cell.sbpStateButton.hidden = NO;
        cell.dbpStateButton.hidden = NO;
        cell.timeLabel.hidden = NO;
        NSString *sbp = _bloodPressModelData.sPressure;
        NSString *dbp = _bloodPressModelData.dPressure;
        NSString *time = _bloodPressModelData.upsaveTime;
        cell.sbpValue = [sbp integerValue];
        cell.dbpValue = [dbp integerValue];
        cell.timeLabel.text = time;
        cell.subTitleLabel.text = [NSString stringWithFormat:@"%@/%@ mmHg", sbp, dbp];
    } else {
        cell.sbpStateButton.hidden = YES;
        cell.dbpStateButton.hidden = YES;
        cell.timeLabel.hidden = YES;
        cell.subTitleLabel.text = @"-- mmHg";
    }
    
}

- (void)setupHeartBeatModelOfCell:(ZJBarChatHBTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    if (!kObjectIsEmpty(_heartBeatModelData)) {
        NSString *hb = _heartBeatModelData.pulse;
        NSString *time = _heartBeatModelData.upsaveTime;
        cell.timeLabel.hidden = NO;
        cell.timeLabel.text = time;
        cell.subTitleLabel.text = [NSString stringWithFormat:@"%@ bpm", hb];
     
    } else {
        
        cell.timeLabel.hidden = YES;
        cell.subTitleLabel.text = @"-- bpm";
    }
    
}

- (void)setupBloodSugarModelOfCell:(ZJBarChatBSTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 2) {
        
        cell.titleLabel.text = @"血糖";
        cell.titleLabel.textColor = [UIColor colorWithRed:0.37 green:0.84 blue:0.65 alpha:1.00];
        cell.subTitleLabel.textColor = [UIColor colorWithRed:0.37 green:0.84 blue:0.65 alpha:1.00];
        cell.iconImageView.image = [UIImage imageNamed:@"health_bg_icon"];
        cell.lineImageView.image = [UIImage imageNamed:@"health_bg_line"];
        
        if (!kObjectIsEmpty(_bloodSugarModelData)) {
            NSString *bs = _bloodSugarModelData.bloodGluVal;
            NSString *time = _bloodSugarModelData.upsaveTime;
            NSString *condition = _bloodSugarModelData.mCondition;
            cell.stateView.hidden = NO;
            cell.timeLabel.hidden = NO;
            cell.subTitleLabel.text = [NSString stringWithFormat:@"%@ mmol/L", bs];
            cell.bsValue = [bs doubleValue];
            cell.bsCondition = [condition integerValue];
            cell.timeLabel.text = time;
        } else {
            cell.stateView.hidden = YES;
            cell.timeLabel.hidden = YES;
            cell.subTitleLabel.text = @"-- mmol/L";
        }
        
    }
    
    if (indexPath.row == 3) {
        
        cell.titleLabel.text = @"血氧";
        cell.titleLabel.textColor = [UIColor colorWithRed:0.94 green:0.48 blue:0.99 alpha:1.00];
        cell.subTitleLabel.textColor = [UIColor colorWithRed:0.94 green:0.48 blue:0.99 alpha:1.00];
        cell.iconImageView.image = [UIImage imageNamed:@"xieyanggai"];
        cell.lineImageView.image = [UIImage imageNamed:@"health_oxygen_line"];
        
        if (!kObjectIsEmpty(_bloodOxygenModelData)) {
            cell.stateView.hidden = NO;
            cell.timeLabel.hidden = NO;
            NSString *oxygen = _bloodOxygenModelData.oxygen;
            NSString *time = _bloodOxygenModelData.upsaveTime;
            cell.boValue = [oxygen integerValue];
            cell.timeLabel.text = time;
            cell.subTitleLabel.text = [NSString stringWithFormat:@"%@ %%", oxygen];
        } else {
            cell.stateView.hidden = YES;
            cell.timeLabel.hidden = YES;
            cell.subTitleLabel.text = @"-- %";
        }
        
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        ZJDataBarDetailViewController *detailVC = [ZJDataBarDetailViewController new];
        detailVC.tag = 100;
        detailVC.navigationItem.title = @"血压";
        [self.navigationController pushViewController:detailVC animated:YES];
    }
    if (indexPath.row == 1) {
        ZJDataBarDetailViewController *detailVC = [ZJDataBarDetailViewController new];
        detailVC.tag = 200;
        detailVC.navigationItem.title = @"心率";
        [self.navigationController pushViewController:detailVC animated:YES];
    }
    if (indexPath.row == 2) {
        ZJDataBarDetailViewController *detailVC = [ZJDataBarDetailViewController new];
        detailVC.tag = 500;
        detailVC.navigationItem.title = @"血糖";
        [self.navigationController pushViewController:detailVC animated:YES];
    }
    if (indexPath.row == 3) {
        ZJDataBarDetailViewController *detailVC = [ZJDataBarDetailViewController new];
        detailVC.tag = 300;
        detailVC.navigationItem.title = @"血氧";
        [self.navigationController pushViewController:detailVC animated:YES];
    }
}

@end
