//
//  ZJReportShowViewController.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/8.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "ZJReportShowViewController.h"
#import "ZJDataReportTableViewCell.h"
#import "ZJFirstReportApi.h"
#import "ZJReportModel.h"
#import "ZJReportByTimeApi.h"

static NSString *tableViewCellIdentifier = @"DBXDataReportTableViewCell";
static NSString *kFooterTitleInfo = @"以上评估报告和指导意见仅供参考，不作为诊断标准和治疗依据。如有相关疾病，请及时就医，谨遵医嘱！";
@interface ZJReportShowViewController ()<UITableViewDelegate, UITableViewDataSource>
@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) NSMutableArray *dataMarray;
/** 开始时间 */
@property(nonatomic, copy) NSString *startTime;
/** 结束时间 */
@property(nonatomic, copy) NSString *endTime;
@end

@implementation ZJReportShowViewController

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 40, kScreenWidth-26, kScreenHeight-kSafeAreaToHeight-13-250-40-50-20) style:UITableViewStyleGrouped];
        _tableView.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.sectionHeaderHeight = CGFLOAT_MIN;
        _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, CGFLOAT_MIN)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[ZJDataReportTableViewCell class] forCellReuseIdentifier:tableViewCellIdentifier];
        _tableView.tableFooterView = [UIView new];
        
    }
    return _tableView;
}

- (NSMutableArray *)dataMarray
{
    if (!_dataMarray) {
        _dataMarray = [NSMutableArray new];
    }
    return _dataMarray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.view.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    
    UILabel *chartTitleLbl = [UILabel new];
    chartTitleLbl.frame = CGRectMake(0, 0, kScreenWidth-26, 40);
    chartTitleLbl.text = @"数据报告";
    chartTitleLbl.font = [UIFont systemFontOfSize:14];
    chartTitleLbl.textAlignment = NSTextAlignmentCenter;
    chartTitleLbl.backgroundColor = [UIColor colorWithHexString:@"#F2F2F2"];
    [self.view addSubview:chartTitleLbl];
    
    [self.view addSubview:self.tableView];
    
    [self startRequestFirstReportApi];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadView:) name:@"RELOAD_CHILDVIEWS" object:nil];
    
}

- (void)reloadView:(NSNotification *)notification {
    
    NSLog(@"%@", notification);
    
    NSString *startTime = [notification.userInfo objectForKey:@"STARTTIME"];
    NSString *endTime = [notification.userInfo objectForKey:@"ENDTIME"];
    NSInteger order = [[notification.userInfo objectForKey:@"ORDER"] integerValue];
    if (order == 0) {
        [self requestBeforeApiWithTime:startTime];
    } else {
        [self requestNextApiWithTime:endTime];
    }
    
    
}

- (void)requestBeforeApiWithTime:(NSString *)time {
    
    ZJReportByTimeApi *api = [[ZJReportByTimeApi alloc] initWithFlag:_reportStyle order:0 time:time];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        
        if (request.responseJSONObject) {
            
            ZJReportModel *model = [ZJReportModel yy_modelWithJSON:request.responseObject];
            
            NSArray *array = [NSArray yy_modelArrayWithClass:[ZJReportModelData class] json:model.data];
            
            if (!kArrayIsEmpty(array)) {
                
                [self.dataMarray removeAllObjects];
                
                ZJReportModelData *modelData = array.firstObject;
                
                NSString *suggest = !kStringIsEmpty(modelData.strsuggest) ? modelData.strsuggest : @"暂无信息";   // 建议
                NSString *pinggu =!kStringIsEmpty(modelData.strpinggu) ? modelData.strpinggu : @"暂无信息";     // 评估
                
                [self.dataMarray addObject:suggest];
                [self.dataMarray addObject:pinggu];
                
                [self.tableView reloadData];
                
            }
        }
        
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

- (void)requestNextApiWithTime:(NSString *)time {
    
    ZJReportByTimeApi *api = [[ZJReportByTimeApi alloc] initWithFlag:_reportStyle order:1 time:time];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        
        if (request.responseJSONObject) {
            
            ZJReportModel *model = [ZJReportModel yy_modelWithJSON:request.responseObject];
            
            NSArray *array = [NSArray yy_modelArrayWithClass:[ZJReportModelData class] json:model.data];
            
            if (!kArrayIsEmpty(array)) {
                
                [self.dataMarray removeAllObjects];
                
                ZJReportModelData *modelData = array.firstObject;
                
                NSString *pinggu = !kStringIsEmpty(modelData.strsuggest) ? modelData.strpinggu : @"暂无信息";
                NSString *suggest =!kStringIsEmpty(modelData.strpinggu) ? modelData.strsuggest : @"暂无信息";
                
                [self.dataMarray addObject:pinggu];
                [self.dataMarray addObject:suggest];
                
                [self.tableView reloadData];
                
            }
        }
        
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}


#pragma mark 请求第一条报告
- (void)startRequestFirstReportApi {
    
    ZJFirstReportApi *api = [[ZJFirstReportApi alloc] initWithCount:1 flag:_reportStyle doa:0];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        ZJReportModel *model = [ZJReportModel yy_modelWithJSON:request.responseObject];
        
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJReportModelData class] json:model.data];
        
        if (!kArrayIsEmpty(array)) {
            
            ZJReportModelData *modelData = array.firstObject;
            
            NSString *pinggu = !kStringIsEmpty(modelData.strpinggu) ? modelData.strpinggu : @"暂无信息";
            NSString *suggest =!kStringIsEmpty(modelData.strsuggest) ? modelData.strsuggest : @"暂无信息";
            
            [self.dataMarray addObject:pinggu];
            [self.dataMarray addObject:suggest];
            
            [self.tableView reloadData];
            
        } else {
            
            
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}


#pragma mark - UITableViewDelegate and DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataMarray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZJDataReportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tableViewCellIdentifier forIndexPath:indexPath];
    
    [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)setupModelOfCell:(ZJDataReportTableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.content = self.dataMarray[indexPath.section];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 20)];
    
    
    NSArray *titles = @[@"综合评价", @"指导意见"];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(13, 0, 100, 20)];
    label.font = [UIFont systemFontOfSize:14];
    label.text = titles[section];
    [view addSubview:label];
    
    return view;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 20;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return [tableView fd_heightForCellWithIdentifier:tableViewCellIdentifier cacheByIndexPath:indexPath configuration:^(id cell) {
        
        [self setupModelOfCell:cell cellForRowAtIndexPath:indexPath];
    }];
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    
    if (section == 0) {
        return @"";
    }

    return @"以上评估报告和指导意见仅供参考，不作为诊断标准和治疗依据。如有相关疾病，请及时就医，谨遵医嘱！";
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    NSString *sectionTitle = [self tableView:tableView titleForFooterInSection:section];
    if (sectionTitle == nil) {
        return nil;
    }
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(13, 0, kScreenWidth-26, 50);
    label.numberOfLines = 0;
    label.text = kFooterTitleInfo;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor lightGrayColor];
    label.font = [UIFont systemFontOfSize:12];
    label.text = sectionTitle;
    UIView *view = [[UIView alloc] init];
    [view addSubview:label];
    
    return view;
}


@end
