//
//  ZJReportDetailViewController.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/7.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum _ReportStyle {
    WeekReport = 1,
    MonthReport,
    YearReport
} ReportStyle;

NS_ASSUME_NONNULL_BEGIN


@interface ZJReportDetailViewController : UIViewController


@property (nonatomic, assign) ReportStyle reportStyle;

@end

NS_ASSUME_NONNULL_END
