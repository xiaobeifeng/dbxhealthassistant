
//  Created by zhoujian on 2018/9/17.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZJReportCalendarViewControllerDelegate <NSObject>

- (void)reloadViewWithCalendarDate:(NSString *)date;

@end

@interface ZJReportCalendarViewController : UIViewController

/** 报告周期 */
@property(nonatomic, assign) NSInteger flag;

@property(nonatomic, weak) id<ZJReportCalendarViewControllerDelegate> delegate;

@end
