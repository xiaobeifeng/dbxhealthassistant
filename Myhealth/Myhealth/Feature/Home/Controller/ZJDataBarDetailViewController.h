//
//  ZJDataBarDetailViewController.h
//  Myhealth
//
//  Created by zhoujian on 2018/11/21.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


/**
 数据详情模块：展示血压，心率，血氧的详细信息
 */
@interface ZJDataBarDetailViewController : UIViewController

@property (nonatomic, assign) NSInteger tag;

@end

NS_ASSUME_NONNULL_END
