//
//  ZJIsNormalNormTool.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/26.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJIsNormalNormTool.h"

@implementation ZJIsNormalNormTool

+ (BOOL)zj_isNormalBp:(NSInteger)sbp dbp:(NSInteger)dbp {
    if (sbp < 90 || sbp > 140 || dbp < 60 || dbp > 90) {    // 高压,低压异常
        return NO;
    } else {
        return YES;
    }
}

+ (BOOL)zj_isNormalSbp:(NSInteger)sbp {
    if (sbp < 90 || sbp > 140) {    // 高压异常
        return NO;
    } else {
        return YES;
    }
}

+ (BOOL)zj_isNormalDbp:(NSInteger)dbp {
    if (dbp < 60 || dbp > 90) {    // 低压异常
        return NO;
    } else {
        return YES;
    }
}

+ (BOOL)zj_isNormalBs:(NSInteger)condition bs:(double)bs {
    if (condition == 0 ||  condition == 1 || condition == 3 || condition == 5) {     // 空腹 早餐前 午餐前 晚餐前
        if (bs >= 3.9 && bs <= 6.1) {
            return YES;
        } else {
            return NO;
        }
    }
    
    if (condition == 2 || condition == 4 || condition == 6 || condition == 7) {     // 早餐后 午餐后 晚餐后 睡前
        if (bs >= 3.9 && bs <= 7.8) {
            return YES;
        } else {
            return NO;
        }
    }
    
    return YES;
}

+ (BOOL)zj_isNormalBo:(NSInteger)bo {
    if (bo >= 90) {
        return YES;
    } else {
        return NO;
    }
}

+ (BOOL)zj_isNormalHb:(NSInteger)hb {
    if (hb >= 60) {
        return YES;
    } else {
        return NO;
    }
}

@end
