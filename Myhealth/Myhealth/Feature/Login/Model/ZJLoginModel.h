//
//  ZJLoginModel.h
//  Myhealth
//
//  Created by zhoujian on 2018/11/14.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZJLoginModelData :NSObject
@property (nonatomic , copy) NSString              * token;

@end

@interface ZJLoginModel :NSObject
@property (nonatomic , copy) NSString              * message;
@property (nonatomic , strong) ZJLoginModelData              * data;
@property (nonatomic , copy) NSString              * state;

@end


NS_ASSUME_NONNULL_END
