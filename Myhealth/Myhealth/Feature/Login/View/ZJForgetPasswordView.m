//
//  ZJForgetPasswordView.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/15.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJForgetPasswordView.h"

@implementation ZJForgetPasswordView

- (instancetype)initWithTitle:(NSString *)title placeholder:(NSString *)placeholder {
    self = [super init];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        
        UILabel *titleLabel = [UILabel new];
        titleLabel.text = title;
        titleLabel.textColor = COLOR_BLACKGRAY;
        titleLabel.font = [UIFont systemFontOfSize:16];
        CGSize sizeNew = [titleLabel.text sizeWithAttributes:@{NSFontAttributeName:titleLabel.font}];
        [self addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(20);
            make.centerY.equalTo(self.mas_centerY);
            make.width.mas_equalTo(sizeNew.width);
            make.height.mas_equalTo(sizeNew.height);
        }];
        
        _textField = [UITextField new];
        _textField.placeholder = placeholder;
        _textField.textColor = COLOR_BLACKGRAY;
        _textField.autocorrectionType = UITextAutocorrectionTypeNo;
        _textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _textField.keyboardType = UIKeyboardTypeASCIICapable;
        _textField.returnKeyType = UIReturnKeyDone;
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.font = [UIFont systemFontOfSize:16];
        [self addSubview:_textField];
        [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(titleLabel.mas_right).with.offset(13);
            make.centerY.equalTo(self.mas_centerY);
            make.right.equalTo(self).with.offset(-13);
            
        }];
    }
    return self;
}

@end
