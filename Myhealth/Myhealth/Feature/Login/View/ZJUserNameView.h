
//  Created by zhoujian on 2018/8/30.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZJUserNameView : UIView


/**
 输入框
 */
@property(nonatomic, strong) UITextField *textField;


/**
 创建用户名输入框

 @param placeholder 占位文字
 @return UITextField
 */
- (instancetype)initWithPlaceholder:(NSString *)placeholder;
@end
