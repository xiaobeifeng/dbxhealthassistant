
//  Created by zhoujian on 2018/10/19.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 登录模块密码加密输入框
 */
@interface ZJPasswordView : UIView

/**
 输入框
 */
@property(nonatomic, strong) UITextField *textField;


/**
 创建密码输入框

 @param placeholder 占位文字
 @return UITextField
 */
- (instancetype)initWithPlaceholder:(NSString *)placeholder;

@end

NS_ASSUME_NONNULL_END
