
//  Created by zhoujian on 2018/10/19.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "ZJPasswordView.h"

@interface  ZJPasswordView()

@property(nonatomic, strong) UIImageView *inputIconImageView;
@property(nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UIButton *encryptBtn;

@end

@implementation ZJPasswordView

- (instancetype)initWithPlaceholder:(NSString *)placeholder {
    self = [super init];
    if (self) {
        
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor colorWithRed:0.70 green:0.86 blue:1.00 alpha:1.00];
        kViewRadiusBorder(view, 20.0f, 1, [UIColor whiteColor]);
        [self addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.bottom.equalTo(self);
        }];
        
        _encryptBtn = [UIButton new];
        [_encryptBtn setImage:[UIImage imageNamed:@"regist_eye"] forState:UIControlStateNormal];
        [_encryptBtn setImage:[UIImage imageNamed:@"regist_eye_select"] forState:UIControlStateSelected];
        [_encryptBtn addTarget:self action:@selector(handleEncryptBtnEvent:) forControlEvents:UIControlEventTouchUpInside];
        _encryptBtn.backgroundColor = [UIColor redColor];
        _encryptBtn.backgroundColor = [UIColor clearColor];
        [self addSubview:_encryptBtn];
        [_encryptBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self).with.offset(-15);
            make.width.height.mas_equalTo(20);
            make.centerY.equalTo(self.mas_centerY);
        }];
        
        _textField = [UITextField new];
        _textField.secureTextEntry = YES;
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.backgroundColor = [UIColor clearColor];
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.font = [UIFont systemFontOfSize:14];
        _textField.placeholder = placeholder;
        _textField.autocorrectionType = UITextAutocorrectionTypeNo;
        _textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _textField.tintColor = [UIColor colorWithRed:0.40 green:0.49 blue:0.58 alpha:1.00];
        _textField.textColor = [UIColor colorWithRed:0.40 green:0.49 blue:0.58 alpha:1.00];
        [_textField setValue:[UIColor colorWithRed:0.40 green:0.49 blue:0.58 alpha:1.00] forKeyPath:@"_placeholderLabel.textColor"];
        [self addSubview:_textField];
        [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(view).with.offset(15);
            make.top.equalTo(view);
            make.bottom.equalTo(view);
            make.right.equalTo(_encryptBtn).with.offset(-25);
        }];
        
        
        
        
    }
    return self;
}

/**
 密码加密状态切换
 */
- (void)handleEncryptBtnEvent:(UIButton *)button
{
    [_textField resignFirstResponder];  // 避免切换小圆点导致光标位置错误
    
    button.selected = !button.selected;
    
    if (button.selected) { // 按下去了就是明文
        
        NSString *tempPwdStr = _textField.text;
        _textField.secureTextEntry = NO;
        _textField.text = tempPwdStr;
        
    } else { // 暗文
        
        NSString *tempPwdStr = _textField.text;
        _textField.secureTextEntry = YES;
        _textField.text = tempPwdStr;
    }
}

@end
