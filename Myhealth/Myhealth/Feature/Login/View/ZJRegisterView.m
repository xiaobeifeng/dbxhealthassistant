
//  Created by zhoujian on 2018/8/6.
//  Copyright © 2018年 mc. All rights reserved.
//

#import "ZJRegisterView.h"

@interface ZJRegisterView()

@end

@implementation ZJRegisterView

- (instancetype)initWithTitle:(NSString *)title placeholder:(NSString *)placeholder
{
    if (self == [super init]) {
        
        self.userInteractionEnabled = YES;
        
        _titleLbl = [UILabel new];
        _titleLbl.text = title;
        _titleLbl.font = [UIFont systemFontOfSize:16];
        _titleLbl.textColor = [UIColor whiteColor];
        [self addSubview:_titleLbl];
        [_titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.top.equalTo(self);
            make.height.equalTo(@20);
            make.width.equalTo(self.mas_width);
        }];
        
        _textField = [UITextField new];
        _textField.tintColor = [UIColor colorWithRed:0.18 green:0.26 blue:0.45 alpha:1.00];
        _textField.textColor = [UIColor colorWithRed:0.18 green:0.26 blue:0.45 alpha:1.00];
        _textField.placeholder = placeholder;
        _textField.autocorrectionType = UITextAutocorrectionTypeNo;
        _textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _textField.keyboardType = UIKeyboardTypeASCIICapable;
        _textField.returnKeyType = UIReturnKeyDone;
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.font = [UIFont systemFontOfSize:14];
        [_textField setValue:[UIColor colorWithRed:0.40 green:0.49 blue:0.58 alpha:1.00] forKeyPath:@"_placeholderLabel.textColor"];
        [self addSubview:_textField];
        [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_titleLbl.mas_left);
            make.width.equalTo(self.mas_width);
            make.height.equalTo(@25);
            make.bottom.equalTo(self).with.offset(-1);
        }];
        
        UIView *line = [UIView new];
        line.backgroundColor = [UIColor whiteColor];
        [self addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_textField.mas_bottom).with.offset(2);
            make.height.equalTo(@1);
            make.width.equalTo(self.mas_width);
            make.left.equalTo(_titleLbl.mas_left);
        }];
        
    }
    
    return self;
}



@end
