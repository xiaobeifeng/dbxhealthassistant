
//  Created by zhoujian on 2018/8/30.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJUserNameView.h"

@interface ZJUserNameView()

@property(nonatomic, strong) UIImageView *inputIconImageView;
@property(nonatomic, strong) UIView *lineView;

@end

@implementation ZJUserNameView

- (instancetype)initWithPlaceholder:(NSString *)placeholder {
    self = [super init];
    if (self) {
        
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor colorWithRed:0.70 green:0.86 blue:1.00 alpha:1.00];
        kViewRadiusBorder(view, 20.0f, 1, [UIColor whiteColor]);
        [self addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.bottom.equalTo(self);
        }];
        
        _textField = [UITextField new];
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.backgroundColor = [UIColor clearColor];
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _textField.font = [UIFont systemFontOfSize:14];
        _textField.placeholder = placeholder;
        _textField.autocorrectionType = UITextAutocorrectionTypeNo;
        _textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _textField.tintColor = [UIColor colorWithRed:0.40 green:0.49 blue:0.58 alpha:1.00];
        _textField.textColor = [UIColor colorWithRed:0.40 green:0.49 blue:0.58 alpha:1.00];
        [_textField setValue:[UIColor colorWithRed:0.40 green:0.49 blue:0.58 alpha:1.00] forKeyPath:@"_placeholderLabel.textColor"];
        [self addSubview:_textField];
        [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(view).with.offset(15);
            make.top.equalTo(view);
            make.bottom.equalTo(view);
            make.right.equalTo(view).with.offset(-15);
        }];
        
        
    }
    return self;
}


@end
