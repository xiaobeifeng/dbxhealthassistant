//
//  ZJForgetPasswordView.h
//  Myhealth
//
//  Created by zhoujian on 2018/11/15.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZJForgetPasswordView : UIView

@property (nonatomic, strong) UITextField *textField;

- (instancetype)initWithTitle:(NSString *)title placeholder:(NSString *)placeholder;

@end

NS_ASSUME_NONNULL_END
