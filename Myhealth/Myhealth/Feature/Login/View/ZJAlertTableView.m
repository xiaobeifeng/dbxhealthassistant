//
//  ZJAlertTableView.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/27.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJAlertTableView.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import <BabyBluetooth.h>
#import "ZJTitleTableViewCell.h"

@interface ZJAlertTableView()<UITableViewDelegate, UITableViewDataSource, CBCentralManagerDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic,copy)   NSString *title;
@property (nonatomic,copy)   NSString *leftBtnTitle;
@property (nonatomic,copy)   NSString *rightBtnTitle;
@property (nonatomic,strong) UIView   *contentView;
@property (nonatomic,strong) UILabel  *titleLbl;
@property (nonatomic,strong) UILabel  *messageLbl;
@property (nonatomic,strong) UIButton  *leftBtn;
@property (nonatomic,strong) UIButton  *rightBtn;
@property (nonatomic, strong) BabyBluetooth *baby;
@property (nonatomic, strong) CBPeripheral *currPeripheral;
@property (nonatomic, strong) NSMutableArray *peripheralMutableArray;
@property (nonatomic, strong) CBCentralManager *centerManager;

@end

@implementation ZJAlertTableView

- (instancetype)initWithTitle:(NSString *)title leftBtnTitle:(NSString *)leftBtnTitle rightBtnTitle:(NSString *)rightBtnTitle delegate:(id)delegate
{
    if (self = [super init]) {
        self.title = title;
        self.leftBtnTitle = leftBtnTitle;
        self.rightBtnTitle = rightBtnTitle;
        self.delegate = delegate;
        [self setupUI];
        
        _peripheralMutableArray = [NSMutableArray new];
        _centerManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:nil];
        
    }
    return self;
}

- (void)setupUI
{
    self.frame = [UIScreen mainScreen].bounds;
    self.backgroundColor = [UIColor colorWithRGB:0x000000 alpha:0.5];
    [UIView animateWithDuration:0.1 animations:^{
        self.alpha = 1;
    }];
    
    self.contentView = [[UIView alloc]init];
    [self addSubview:self.contentView];
    self.contentView.backgroundColor = [UIColor whiteColor];
    kViewRadius(self, 5.0f);
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.centerX.equalTo(self.mas_centerX);
        make.width.mas_equalTo(300);
    }];
    
    self.titleLbl = [UILabel new];
    self.titleLbl.font = [UIFont systemFontOfSize:16];
    self.titleLbl.textColor = COLOR_BLACKGRAY;
    self.titleLbl.text = self.title;
    [self.titleLbl sizeToFit];
    [self.contentView addSubview:self.titleLbl];
    [self.titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(10);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[ZJTitleTableViewCell class] forCellReuseIdentifier:NSStringFromClass([ZJTitleTableViewCell class])];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    [self addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLbl.mas_bottom).with.offset(13);
        make.height.mas_equalTo(150);
        make.width.mas_equalTo(self.contentView.mas_width);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
    self.leftBtn = [UIButton new];
    self.leftBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    self.leftBtn.backgroundColor = COLOR_PICTONBLUE;
    kViewRadius(self.leftBtn, 5.0f);
    [self.leftBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.leftBtn.adjustsImageWhenHighlighted = NO;
    [self.leftBtn setTitle:self.leftBtnTitle forState:UIControlStateNormal];
    [self.leftBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.leftBtn addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.leftBtn];
    [self.leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tableView.mas_bottom).with.offset(25);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(40);
        make.centerX.equalTo(self.tableView.mas_centerX);
    }];

    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.leftBtn.mas_bottom).with.offset(20);
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _peripheralMutableArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZJTitleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZJTitleTableViewCell class]) forIndexPath:indexPath];
    CBPeripheral *peripheral = _peripheralMutableArray[indexPath.row];
    cell.titleLabel.text = peripheral.name;
    cell.detailImageView.hidden = YES;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CBPeripheral *peripheral = _peripheralMutableArray[indexPath.row];
    [self.delegate zj_alertView:self selectDeviceName:peripheral.name];
}

/**
 弹出此弹窗
 */
- (void)show {
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    [keyWindow addSubview:self];
    
    [_centerManager scanForPeripheralsWithServices:nil options:nil];
    
}

/**
 移除弹窗
 */
- (void)dismiss
{
    
    [_centerManager stopScan];
    
    [self removeFromSuperview];
}


- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    // 第一次打开或者每次蓝牙状态改变都会调用这个函数
    if(central.state==CBCentralManagerStatePoweredOn) {
        
        [_centerManager scanForPeripheralsWithServices:nil options:nil];
       
    } else {
        
        [self removeFromSuperview];
        
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"请检查你蓝牙使用权限或检查设备问题" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
        [alertVC addAction:okAction];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertVC animated:YES completion:nil];
     
    }
}

// 扫描到设备会进入方法
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI{
    
    if (peripheral.name.length == 17 && [peripheral.name containsString:@":"]) {
        
        if (![self.peripheralMutableArray containsObject:peripheral]) {
            [self.peripheralMutableArray addObject:peripheral];
            [self.tableView reloadData];
        }
        
    }
    
}

# pragma mark - emptyDataSet
/**
 *  返回标题文字
 */
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"暂无可用设备";
    UIFont *font = [UIFont systemFontOfSize:16.0];
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:text];
    [attStr addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, text.length)];
    return attStr;
}
/**
 *  处理空白区域的点击事件
 */
- (void)emptyDataSet:(UIScrollView *)scrollView didTapView:(UIView *)view {
    NSLog(@"%s",__FUNCTION__);
}


@end
