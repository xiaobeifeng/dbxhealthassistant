//
//  ZJAlertTableView.h
//  Myhealth
//
//  Created by zhoujian on 2018/11/27.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, ZJAlertViewBtn) {
    AlertBtnLeft = 0,
    AlertBtnRight
};

@class ZJAlertTableView;
@protocol ZJAlertTableViewDelegate <NSObject>

- (void)zj_alertView:(ZJAlertTableView *)alertView selectDeviceName:(NSString *)deviceName;

@end

@interface ZJAlertTableView : UIView

@property (nonatomic,weak) id<ZJAlertTableViewDelegate> delegate;
- (instancetype)initWithTitle:(NSString *)title leftBtnTitle:(NSString *)leftBtnTitle rightBtnTitle:(NSString *)rightBtnTitle delegate:(id)delegate;
- (void)show;
- (void)dismiss;
@end

NS_ASSUME_NONNULL_END
