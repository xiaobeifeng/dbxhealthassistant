//
//  ZJForgetPasswordViewController.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/14.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJForgetPasswordViewController.h"
#import "ZJForgetPasswordView.h"
#import "ZJForgetPasswordApi.h"

@interface ZJForgetPasswordViewController ()<UITextFieldDelegate>
{
    ZJForgetPasswordView *_userNameView;
    ZJForgetPasswordView *_deviceNameView;
    ZJForgetPasswordView *_passwordView;
    ZJForgetPasswordView *_checkPasswordView;
}

@end

@implementation ZJForgetPasswordViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:COLOR_GALLERY]];
 
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"忘记密码";
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem titleItem:@"提交" target:self action:@selector(rightBarButtonItemAction)];
    self.view.backgroundColor = COLOR_GALLERY;
    
    [self setupUI];

}

- (void)setupUI {
    
    _userNameView = [[ZJForgetPasswordView alloc] initWithTitle:@"用户名" placeholder:@"请输入用户名"];
    _userNameView.backgroundColor = [UIColor whiteColor];
    _userNameView.textField.delegate = self;
    _userNameView.textField.tag = 10;
    [self.view addSubview:_userNameView];
    [_userNameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(44);
        make.top.equalTo(self.view).with.offset(13);
    }];
    
    _deviceNameView = [[ZJForgetPasswordView alloc] initWithTitle:@"设备号" placeholder:@"请输入设备号"];
    _deviceNameView.backgroundColor = [UIColor whiteColor];
    _deviceNameView.textField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;    // 自动大写
    _deviceNameView.textField.delegate = self;
    _deviceNameView.textField.tag = 20;
    [self.view addSubview:_deviceNameView];
    [_deviceNameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(44);
        make.top.equalTo(_userNameView.mas_bottom).with.offset(1);
    }];
    
    _passwordView = [[ZJForgetPasswordView alloc] initWithTitle:@"新密码" placeholder:@"请输入新密码"];
    _passwordView.backgroundColor = [UIColor whiteColor];
    _passwordView.textField.delegate = self;
    _passwordView.textField.secureTextEntry = YES;
    _passwordView.textField.tag = 30;
    [self.view addSubview:_passwordView];
    [_passwordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(44);
        make.top.equalTo(_deviceNameView.mas_bottom).with.offset(1);
    }];
    
    _checkPasswordView = [[ZJForgetPasswordView alloc] initWithTitle:@"再次输入密码" placeholder:@"请再次输入密码"];
    _checkPasswordView.backgroundColor = [UIColor whiteColor];
    _checkPasswordView.textField.secureTextEntry = YES;
    _checkPasswordView.textField.delegate = self;
    _checkPasswordView.textField.tag = 40;
    [self.view addSubview:_checkPasswordView];
    [_checkPasswordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(44);
        make.top.equalTo(_passwordView.mas_bottom).with.offset(1);
    }];
}

#pragma mark - textField Delegate
/**
 点击Done 收起键盘
 */
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

/**
 用户输入校验和限制输入长度
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    // 用户名
    if (textField.tag == 10) {
        
        NSString *allow = @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        
        if (textField.text.length + string.length - range.length > 20) {
            return NO;
        } else {
            NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:allow] invertedSet];
            NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            return [string isEqualToString:filtered];
        }
    }
    
    // 设备号
    if (textField.tag == 20) {
        NSString *allow = @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:allow] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    
    return YES;
    
}


/**
 右侧导航栏提交按钮点击事件
 */
- (void)rightBarButtonItemAction {
    
    NSString *userName = _userNameView.textField.text;
    if (kStringIsEmpty(userName)) {
        [MBProgressHUD showWarnMessage:@"请输入用户名"]; return;
    }
    
    NSString *deviceName = _deviceNameView.textField.text;
    if (kStringIsEmpty(deviceName)) {
        [MBProgressHUD showWarnMessage:@"请输入设备号"]; return;
    }
    
    NSString *password = _passwordView.textField.text;
    if (kStringIsEmpty(password)) {
        [MBProgressHUD showWarnMessage:@"请输入密码"]; return;
    }
    
    NSString *checkPassword = _passwordView.textField.text;
    if (kStringIsEmpty(checkPassword)) {
        [MBProgressHUD showWarnMessage:@"请再次输入密码"]; return;
    }
    
    if (![password isEqualToString:checkPassword]) {
        [MBProgressHUD showWarnMessage:@"两次输入密码不一致，请检查后再试"]; return;
    }
    
    ZJForgetPasswordApi *api = [[ZJForgetPasswordApi alloc] initWithUsername:userName deviceName:deviceName password:password];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSLog(@"%@", api.result);
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
    
}


@end
