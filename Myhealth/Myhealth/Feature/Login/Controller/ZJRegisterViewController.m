//
//  ZJRegisterViewController.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/14.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJRegisterViewController.h"
#import "ZJRegisterView.h"
#import "ZJRegisterApi.h"
#import "ZJDeviceManageViewController.h"
#import "ZJLoginApi.h"
#import "ZJLoginModel.h"
#import "ZJAlertTableView.h"

@interface ZJRegisterViewController ()<UITextFieldDelegate, ZJAlertTableViewDelegate>
{
    ZJRegisterView *_userNameView;              // 用户名
    ZJRegisterView *_passwordView;              // 密码
    ZJRegisterView *_checkPasswordView;         // 再次输入密码
    ZJRegisterView *_deviceNameView;            // 设备名
    ZJRegisterView *_areaCodeView;              // 区域编码
}

@end

@implementation ZJRegisterViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor]] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:[UIColor clearColor]]];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)loadView {
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.view = scrollView;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:0.57 green:0.79 blue:1.00 alpha:1.00];
    
    [self setupNav];
    [self setupUI];

}

/**
 导航栏自定义
 */
- (void)setupNav {
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithimage:[UIImage imageNamed:@"nav_pop_white"] selImage:[UIImage imageNamed:@"nav_pop_white"] target:self action:@selector(leftBarButtonItemAction)];
}

/**
 界面自定义
 */
- (void)setupUI {
    UIView *container = [UIView new];
    [self.view addSubview:container];
    [container mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
        make.width.equalTo(self.view);
    }];
    
    UILabel *titleLbl = [UILabel new];
    titleLbl.text = @"注册一个新账号";
    titleLbl.textColor = [UIColor whiteColor];
    titleLbl.font = [UIFont boldSystemFontOfSize:25];
    [container addSubview:titleLbl];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(0);
        make.centerX.equalTo(self.view.mas_centerX);
    }];
    
    _userNameView = [[ZJRegisterView alloc] initWithTitle:@"用户名" placeholder:@"请输入您的用户名（必须为英文或数字）"];
    _userNameView.textField.delegate = self;
    _userNameView.textField.tag = 10;
    [container addSubview:_userNameView];
    [_userNameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLbl.mas_bottom).with.offset(50);
        make.centerX.equalTo(container.mas_centerX);
        make.left.equalTo(container).with.offset(30);
        make.right.equalTo(container).with.offset(-30);
        make.height.equalTo(@50);
    }];
    
    _passwordView = [[ZJRegisterView alloc] initWithTitle:@"设置密码" placeholder:@"请输入您的密码"];
    _passwordView.textField.delegate = self;
    _passwordView.textField.secureTextEntry = YES;
    _passwordView.textField.tag = 20;
    [container addSubview:_passwordView];
    [_passwordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_userNameView.mas_bottom).with.offset(30);
        make.centerX.equalTo(container.mas_centerX);
        make.left.equalTo(container).with.offset(30);
        make.right.equalTo(container).with.offset(-30);
        make.height.equalTo(@50);
    }];
    
    _checkPasswordView = [[ZJRegisterView alloc] initWithTitle:@"再次输入密码" placeholder:@"请再次输入您的密码"];
    _checkPasswordView.textField.delegate = self;
    _checkPasswordView.textField.secureTextEntry = YES;
    _checkPasswordView.textField.tag = 30;
    [container addSubview:_checkPasswordView];
    [_checkPasswordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_passwordView.mas_bottom).with.offset(30);
        make.centerX.equalTo(container.mas_centerX);
        make.left.equalTo(container).with.offset(30);
        make.right.equalTo(container).with.offset(-30);
        make.height.equalTo(@50);
    }];
    
    _deviceNameView = [[ZJRegisterView alloc] initWithTitle:@"设备号" placeholder:@"单击此处选择您的设备号"];
    _deviceNameView.textField.delegate = self;
    _deviceNameView.textField.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;    // 自动大写
    _deviceNameView.textField.tag = 40;
    [container addSubview:_deviceNameView];
    [_deviceNameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_checkPasswordView.mas_bottom).with.offset(30);
        make.centerX.equalTo(container.mas_centerX);
        make.left.equalTo(container).with.offset(30);
        make.right.equalTo(container).with.offset(-30);
        make.height.equalTo(@50);
    }];
    UIButton *chooseDeviceButton = [UIButton new];
    chooseDeviceButton.backgroundColor = [UIColor clearColor];
    [chooseDeviceButton addTarget:self action:@selector(chooseDeviceButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [_deviceNameView.textField addSubview:chooseDeviceButton];
    [chooseDeviceButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_deviceNameView.textField.mas_centerX);
        make.centerY.equalTo(_deviceNameView.textField.mas_centerY);
        make.width.equalTo(_deviceNameView.textField.mas_width);
        make.height.equalTo(_deviceNameView.textField.mas_height);
    }];
    
    _areaCodeView = [[ZJRegisterView alloc] initWithTitle:@"区域编码" placeholder:@"请输入您的区域编码"];
    _areaCodeView.textField.delegate = self;
    _areaCodeView.textField.tag = 50;
    [container addSubview:_areaCodeView];
    [_areaCodeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_deviceNameView.mas_bottom).with.offset(30);
        make.centerX.equalTo(container.mas_centerX);
        make.left.equalTo(container).with.offset(30);
        make.right.equalTo(container).with.offset(-30);
        make.height.equalTo(@50);
    }];
    
    UIButton *regiserButton = [UIButton new];
    [regiserButton setTitle:@"注 册" forState:UIControlStateNormal];
    regiserButton.titleLabel.font = [UIFont systemFontOfSize:17];
    regiserButton.backgroundColor = [UIColor colorWithRed:1.00 green:0.74 blue:0.35 alpha:1.00];
    kViewRadius(regiserButton, 23.0f);
    [regiserButton addTarget:self action:@selector(regiserButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [regiserButton sizeToFit];
    [container addSubview:regiserButton];
    [regiserButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_areaCodeView.mas_bottom).with.offset(30);
        make.centerX.equalTo(container.mas_centerX);
        make.left.equalTo(container).with.offset(70);
        make.right.equalTo(container).with.offset(-70);
        make.height.equalTo(@44);
    }];
    
    [container mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(regiserButton.mas_bottom).with.offset(20);
    }];
    
}


/**
 左侧导航栏按钮返回
 */
- (void)leftBarButtonItemAction {
    [self.navigationController popViewControllerAnimated:YES];
}


/**
 注册按钮点击事件
 */
- (void)regiserButtonAction:(UIButton *)button {
    
    // 防止按钮重复点击
    button.enabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        button.enabled = YES;
    });

    
    [self checkRegisterInfo];

}


/**
 检查注册信息
 */
- (void)checkRegisterInfo {
    
    NSString *userName = _userNameView.textField.text;
    if (kStringIsEmpty(userName)) { // 用户名校验
        [MBProgressHUD showWarnMessage:@"请设置您的用户名"]; return;
    }
    
    NSString *password = _passwordView.textField.text;
    if (kStringIsEmpty(password)) {
        [MBProgressHUD showWarnMessage:@"请设置您的密码"]; return;
    }
    
    NSString *checkPassword = _checkPasswordView.textField.text;
    if (kStringIsEmpty(checkPassword)) {
        [MBProgressHUD showWarnMessage:@"请再次输入您的密码"]; return;
    }
    
    if (![password isEqualToString:checkPassword]) {
        [MBProgressHUD showWarnMessage:@"两次输入的密码不一致，请检查后再试"]; return;
    }
    
    NSString *deviceName = _deviceNameView.textField.text;
    if (kStringIsEmpty(deviceName)) {
        [MBProgressHUD showWarnMessage:@"请选择您的设备号"]; return;
    }
    
    NSString *areaCode = _areaCodeView.textField.text;
    if (kStringIsEmpty(areaCode)) {
        [MBProgressHUD showWarnMessage:@"请输入您的区域编码"]; return;
    }
    
    
//    NSString *userName = @"sky4";
//    NSString *password = @"1";
//    NSString *deviceName = @"94E36D9A8ECB";
//    NSString *areaCode =  @"AJJ0000FF";
    
    [MBProgressHUD showActivityMessageInView:@""];
    // 请求注册接口
    ZJRegisterApi *api  = [[ZJRegisterApi alloc] initWithUsername:userName password:password deviceName:deviceName areaCode:areaCode];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
      
        if (api.code == 0) { // 注册成功
            // !!!: 接口返回信息为绑定成功，不符合当前逻辑，故改为现实注册成功
            [MBProgressHUD showSuccessMessage:@"注册成功"];
            
            [[NSUserDefaults standardUserDefaults] zj_setObject:userName key:UD_KEY_USERNAME];
            // 根据是否记住密码来决定
            if (_isRemberPassword) {
                // 本地保存密码
                [[NSUserDefaults standardUserDefaults] zj_setObject:password key:UD_KEY_PASSWORD];
            }
            
            // 调取登录接口
            [self startRequestLoginApi:userName password:password];
            
            
        } else {    // 注册信息错误
            [MBProgressHUD showErrorMessage:api.message];
        }

    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
 
    }];
    
}

- (void)startRequestLoginApi:(NSString *)userName password:(NSString *)password {
    ZJLoginApi *api = [[ZJLoginApi alloc] initWithUsername:userName password:password];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSLog(@"%@", request.requestUrl);
        NSLog(@"result - %@", api.result);
        NSLog(@"result - %@", api.message);
        NSLog(@"result - %ld", (long)api.code);
        ZJLoginModel *model = [ZJLoginModel yy_modelWithJSON:api.result];
        
        if (api.code == 0) {
            [[NSUserDefaults standardUserDefaults] zj_setObject:model.data.token key:UD_KEY_TOKEN];
            
            // 进入绑定设备界面
            ZJDeviceManageViewController *deviceMangeVC = [ZJDeviceManageViewController new];
            ZJNavigationController *navC = [[ZJNavigationController alloc] initWithRootViewController:deviceMangeVC];
            deviceMangeVC.isShowChangeUser = YES;
            [UIApplication sharedApplication].keyWindow.rootViewController = navC;
            
        } else {
            [MBProgressHUD showErrorMessage:api.message];
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

#pragma mark - textField Delegate
/**
 点击Done 收起键盘
 */
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

/**
 用户输入校验和限制输入长度
 */
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

    // 用户名
    if (textField.tag == 10) {

        NSString *allow = @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        
        if (textField.text.length + string.length - range.length > 20) {
            return NO;
        } else {
            NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:allow] invertedSet];
            NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            return [string isEqualToString:filtered];
        }
    }
 
    // 设备号
    if (textField.tag == 40) {
        NSString *allow = @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:allow] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    
    // 区域编码，不允许输入空格
    if (textField.tag == 50) {

        NSString *tem = [[string componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]componentsJoinedByString:@""];
        if (![string isEqualToString:tem]) {
            return NO;
        }

    }
    
    return YES;
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.tag == 40) {
        return NO;
    } else {
        return YES;
    }
}


/**
 选择设备按钮点击事件
 */
- (void)chooseDeviceButtonAction {
    
    ZJAlertTableView *alertView = [[ZJAlertTableView alloc] initWithTitle:@"设备列表" leftBtnTitle:@"取消" rightBtnTitle:@"确定" delegate:self];
    
    [alertView show];
}

- (void)zj_alertView:(ZJAlertTableView *)alertView selectDeviceName:(NSString *)deviceName {
    [alertView dismiss];

    _deviceNameView.textField.text = deviceName;
    
}

@end
