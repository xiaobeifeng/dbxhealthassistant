//
//  ZJLoginViewController.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/13.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJLoginViewController.h"
#import "ZJUserNameView.h"
#import "ZJPasswordView.h"
#import "ZJLoginApi.h"
#import "ZJLoginModel.h"
#import "ZJRegisterViewController.h"
#import "ZJForgetPasswordViewController.h"
#import "ZJLinkyorntwoApi.h"
#import "ZJDeviceManageViewController.h"

@interface ZJLoginViewController ()<UITextFieldDelegate>

@property (nonatomic, strong) ZJUserNameView *userNameView ;     // 用户名输入
@property (nonatomic, strong) ZJPasswordView *passwordView;      // 密码输入
@property(nonatomic, strong) UIButton *switchButton;               // 记住密码开关

@end

@implementation ZJLoginViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];   // 隐藏导航栏
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES; // 点击背景收起键盘
    
    self.view.backgroundColor = [UIColor colorWithRed:0.57 green:0.79 blue:1.00 alpha:1.00];
    
    [self setupUI];
    
    NSLog(@"userName - %@", [[NSUserDefaults standardUserDefaults] zj_objcetForKey:UD_KEY_USERNAME]);
    NSLog(@"password - %@", [[NSUserDefaults standardUserDefaults] zj_objcetForKey:UD_KEY_PASSWORD]);
    
    if (!_switchButton.selected) {
        NSString *userName = [[NSUserDefaults standardUserDefaults] zj_objcetForKey:UD_KEY_USERNAME];
        NSString *password = [[NSUserDefaults standardUserDefaults] zj_objcetForKey:UD_KEY_PASSWORD];
        _userNameView.textField.text = userName;
        _passwordView.textField.text = password;
    }
    
}


/**
 搭建界面
 */
- (void)setupUI {
    
    // logo
    UIImageView *logoImageView = [UIImageView new];
    logoImageView.image = [UIImage imageNamed:@"logo_launch"];
    logoImageView.userInteractionEnabled = YES;
    [self.view addSubview:logoImageView];
    [logoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).with.offset(80);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(80);
        make.centerX.mas_equalTo(self.view.mas_centerX);
    }];
    
    // 用户名
    _userNameView = [[ZJUserNameView alloc] initWithPlaceholder:@"请输入您的用户名"];
    _userNameView.textField.delegate = self;
    _userNameView.textField.tag = 10;
    [self.view addSubview:_userNameView];
    [_userNameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(logoImageView.mas_bottom).with.offset(50);
        make.left.mas_equalTo(self.view).with.offset(40);
        make.right.mas_equalTo(self.view).with.offset(-40);
        make.height.mas_equalTo(40);
    }];

    // 密码
    _passwordView = [[ZJPasswordView alloc] initWithPlaceholder:@"请输入您的密码"];
    _passwordView.textField.delegate = self;
    _passwordView.textField.tag = 20;
    _passwordView.textField.keyboardType = UIKeyboardTypeASCIICapable;
    [self.view addSubview:_passwordView];
    [_passwordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_userNameView.mas_bottom).with.offset(20);
        make.width.mas_equalTo(_userNameView.mas_width);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.height.mas_equalTo(40);
    }];
    
    // 记住密码开关(默认开启)
    _switchButton = [[UIButton alloc] init];
    [_switchButton setBackgroundImage:[UIImage imageNamed:@"button_forgotPassword_normal"] forState:UIControlStateNormal];
    [_switchButton setBackgroundImage:[UIImage imageNamed:@"button_forgotPassword_select"] forState:UIControlStateSelected];
    _switchButton.adjustsImageWhenHighlighted = NO;
    [_switchButton addTarget:self action:@selector(switchButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    BOOL isOn = [[NSUserDefaults standardUserDefaults] zj_boolForKey:UD_KEY_ISON];
    if (isOn) {
        _switchButton.selected = YES;
    }
    [self.view addSubview:_switchButton];
    [_switchButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_passwordView.mas_right).with.offset(-13);
        make.top.mas_equalTo(_passwordView.mas_bottom).with.offset(10);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(50);
    }];
   
    // 记住密码
    UILabel *switchLabel = [[UILabel alloc] init];
    switchLabel.text = @"记住密码";
    switchLabel.textColor = [UIColor whiteColor];
    switchLabel.font = [UIFont systemFontOfSize:14];
    [switchLabel sizeToFit];
    [self.view addSubview:switchLabel];
    [switchLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_switchButton.mas_left).with.offset(-13);
        make.centerY.mas_equalTo(_switchButton.mas_centerY);
    }];
    
    // 登录按钮
    UIButton *loginBtn = [UIButton new];
    [loginBtn setTitle:@"登 录" forState:UIControlStateNormal];
    [loginBtn addTarget:self action:@selector(loginBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    loginBtn.backgroundColor = [UIColor colorWithRed:1.00 green:0.74 blue:0.35 alpha:1.00];
    [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    kViewRadius(loginBtn, 20.0f);
    [self.view addSubview:loginBtn];
    [loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_switchButton.mas_bottom).with.offset(30);
        make.left.mas_equalTo(self.view).with.offset(40);
        make.right.mas_equalTo(self.view).with.offset(-40);
        make.height.mas_equalTo(44);
    }];
    
    // 注册按钮
    UIButton *registBtn = [UIButton new];
    [registBtn setTitle:@"注 册" forState:UIControlStateNormal];
    [registBtn addTarget:self action:@selector(registBtnAction) forControlEvents:UIControlEventTouchUpInside];
    registBtn.backgroundColor = [UIColor colorWithRed:0.28 green:0.65 blue:1.00 alpha:1.00];
    [registBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    kViewRadius(registBtn, 20.0f);
    [self.view addSubview:registBtn];
    [registBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(loginBtn.mas_bottom).with.offset(20);
        make.left.mas_equalTo(self.view).with.offset(40);
        make.right.mas_equalTo(self.view).with.offset(-40);
        make.height.mas_equalTo(44);
    }];
    
    // 忘记密码
    UIButton *forgetPasswordBtn = [UIButton new];
    [forgetPasswordBtn setTitle:@"忘记密码？" forState:UIControlStateNormal];
    forgetPasswordBtn.backgroundColor = [UIColor clearColor];
    forgetPasswordBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    forgetPasswordBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [forgetPasswordBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [forgetPasswordBtn addTarget:self action:@selector(forgetPasswordBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:forgetPasswordBtn];
    [forgetPasswordBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(registBtn.mas_bottom).with.offset(20);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.width.mas_equalTo(@100);
    }];

}

// 记住密码开关
- (void)switchButtonClick:(UIButton *)button {
    
    if (!button.selected) {
        button.selected = YES;
        [[NSUserDefaults standardUserDefaults] zj_removeObjectForKey:UD_KEY_PASSWORD];
        [[NSUserDefaults standardUserDefaults] zj_setBool:YES key:UD_KEY_ISON];
       
    } else {
        button.selected = NO;
        NSString *userName = _userNameView.textField.text;
        if (!kStringIsEmpty(userName)) {
            [[NSUserDefaults standardUserDefaults] zj_setObject:userName key:UD_KEY_USERNAME];
        }
        
        NSString *password = _passwordView.textField.text;
        if (!kStringIsEmpty(password)) {
            [[NSUserDefaults standardUserDefaults] zj_setObject:password key:UD_KEY_PASSWORD];
        }
        
        [[NSUserDefaults standardUserDefaults] zj_setBool:NO key:UD_KEY_ISON];
    }
}

/**
 登陆按钮点击事件
 */
- (void)loginBtnAction:(UIButton *)button {
    
    
    // 防止按钮重复点击
    button.enabled = NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        button.enabled = YES;
    });
    
    NSString *userName = _userNameView.textField.text;
    NSString *password = _passwordView.textField.text;
    
    
    if (kStringIsEmpty(userName)) {
        [MBProgressHUD showWarnMessage:@"请输入您的用户名"]; return;
    }
    if (kStringIsEmpty(password)) {
        [MBProgressHUD showWarnMessage:@"请输入您的密码"]; return;
    }
    
    [MBProgressHUD showActivityMessageInView:@""];
    
    ZJLoginApi *api = [[ZJLoginApi alloc] initWithUsername:userName password:password];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        NSLog(@"%@", request.requestUrl);
        NSLog(@"result - %@", api.result);
        NSLog(@"result - %@", api.message);
        NSLog(@"result - %ld", (long)api.code);
        ZJLoginModel *model = [ZJLoginModel yy_modelWithJSON:api.result];
       
        if (api.code == 0) {    
            [[NSUserDefaults standardUserDefaults] zj_setObject:model.data.token key:UD_KEY_TOKEN];
            
            [self checkBindingDevice];
            
        } else {
            [MBProgressHUD showErrorMessage:api.message];
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {

    }];
    
    
}

- (void)checkBindingDevice {
    ZJLinkyorntwoApi *api = [[ZJLinkyorntwoApi alloc] init];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        if (api.code == 1043 || api.code == 1041) {
            ZJDeviceManageViewController *deviceMangeVC = [ZJDeviceManageViewController new];
            ZJNavigationController *navC = [[ZJNavigationController alloc] initWithRootViewController:deviceMangeVC];
            deviceMangeVC.isShowChangeUser = YES;
            [UIApplication sharedApplication].keyWindow.rootViewController = navC;
        }
        
        // 当前账户有绑定设备可以正常进入主界面
        if (api.code == 1042) {
            ZJTabBarController *tabBarC = [ZJTabBarController new];
            [UIApplication sharedApplication].keyWindow.rootViewController = tabBarC;
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}


/**
 注册按钮点击事件
 */
- (void)registBtnAction {
    ZJRegisterViewController *registerVC = [ZJRegisterViewController new];
    registerVC.isRemberPassword = !_switchButton.selected;
    [self.navigationController pushViewController:registerVC animated:YES];
}

/**
 忘记密码
 */
- (void)forgetPasswordBtnAction {
    ZJForgetPasswordViewController *forgetPasswordVC = [ZJForgetPasswordViewController new];
    [self.navigationController pushViewController:forgetPasswordVC animated:YES];
}

- (void)switchChange:(UISwitch *)swithButton {
    
    if (swithButton.isOn) {
        NSLog(@"开启");
        
        NSString *userName = _userNameView.textField.text;
        if (!kStringIsEmpty(userName)) {
            [[NSUserDefaults standardUserDefaults] zj_setObject:userName key:UD_KEY_USERNAME];
            [[NSUserDefaults standardUserDefaults] zj_setBool:YES key:UD_KEY_ISON];
        }
        
        NSString *password = _passwordView.textField.text;
        if (!kStringIsEmpty(password)) {
             [[NSUserDefaults standardUserDefaults] zj_setObject:password key:UD_KEY_PASSWORD];
        }
        
    } else {
        NSLog(@"关闭");
//        [[NSUserDefaults standardUserDefaults] zj_removeObjectForKey:UD_KEY_USERNAME];
        [[NSUserDefaults standardUserDefaults] zj_removeObjectForKey:UD_KEY_PASSWORD];
        [[NSUserDefaults standardUserDefaults] zj_setBool:NO key:UD_KEY_ISON];
    }
    
}

#pragma mark UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    // 忘记密码按钮选中时，不保存用户名和密码
    if (_switchButton.selected) {
        return;
    }
    
    if (textField.tag == 10) {
        NSLog(@"userName - %@", textField.text);
        // 保存用户名
        NSString *userName = textField.text;
        [[NSUserDefaults standardUserDefaults] zj_setObject:userName key:UD_KEY_USERNAME];
    }
    
    if (textField.tag == 20) {
        NSLog(@"20 - %@", textField.text);
        // 保存密码
        NSString *password = textField.text;
        [[NSUserDefaults standardUserDefaults] zj_setObject:password key:UD_KEY_PASSWORD];
        
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField.tag == 10) {
    
        NSString *allow = @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        
        if (textField.text.length + string.length - range.length > 20) {
            return NO;
        } else {
            NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:allow] invertedSet];
            NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
            return [string isEqualToString:filtered];
        }
    }
    
    return YES;
}

- (void)dealloc {
    
}

@end
