//
//  ZJLoginApi.h
//  Myhealth
//
//  Created by zhoujian on 2018/11/14.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZJLoginApi : ZJBaseRequest

- (id)initWithUsername:(NSString *)username password:(NSString *)password;;

@end

NS_ASSUME_NONNULL_END
