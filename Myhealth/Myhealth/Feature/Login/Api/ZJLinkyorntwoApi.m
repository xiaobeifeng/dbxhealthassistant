
//  Created by zhoujian on 2018/9/18.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJLinkyorntwoApi.h"

@implementation ZJLinkyorntwoApi

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    return @"linkyorntwo";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
