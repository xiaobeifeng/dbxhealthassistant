
//  Created by zhoujian on 2018/9/18.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZJRegisterApi : ZJBaseRequest

- (id)initWithUsername:(NSString *)username
              password:(NSString *)password
            deviceName:(NSString *)deviceName
              areaCode:(NSString *)areaCode;

@end

