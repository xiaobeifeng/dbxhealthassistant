//
//  ZJLoginApi.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/14.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJLoginApi.h"


@implementation ZJLoginApi {
    NSString *_userName;
    NSString *_password;
}

- (id)initWithUsername:(NSString *)username password:(NSString *)password; {
    
    self = [super init];
    if (self) {
        _userName = username;
        _password = password;
    }
    return self;
}

- (NSString *)requestUrl {
    return URL_LOGIN;
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (id)requestArgument {
    return @{
             @"username": _userName,
             @"password": _password
             };
}

@end
