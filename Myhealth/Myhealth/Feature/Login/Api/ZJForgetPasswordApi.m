//
//  ZJForgetPasswordApi.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/16.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJForgetPasswordApi.h"

@implementation ZJForgetPasswordApi {
    NSString *_userName;
    NSString *_deviceName;
    NSString *_password;
}

- (id)initWithUsername:(NSString *)username deviceName:(NSString *)deviceName password:(NSString *)password {
    self = [super init];
    if (self) {
        _userName = username;
        _deviceName = deviceName;
        _password = password;
    }
    return self;
}

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return @"auto/user/password/reset/dept";
}

// 请求方法
- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

// 请求体
- (id)requestArgument {
    return @{
             @"username": _userName,
             @"device_code": _deviceName,
             @"newPassword": _password
             };
}


@end
