
//  Created by zhoujian on 2018/9/18.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJRegisterApi.h"


@implementation ZJRegisterApi {
    NSString *_userName;
    NSString *_password;
    NSString *_deviceName;
    NSString *_areaCode;
}
- (id)initWithUsername:(NSString *)username
              password:(NSString *)password
       deviceName:(NSString *)deviceName
            areaCode:(NSString *)areaCode {
    
    self = [super init];
    if (self) {
        _userName = username;
        _password = password;
        _deviceName = deviceName;
        _areaCode = areaCode;
    }
    return self;
}

- (NSString *)requestUrl {
    
    return @"auto/register/state/role/dept";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (id)requestArgument {
    return @{
             @"username": _userName,
             @"password": _password,
             @"device_code":_deviceName,
             @"shop_name":_areaCode
             };
}
@end
