//
//  ZJForgetPasswordApi.h
//  Myhealth
//
//  Created by zhoujian on 2018/11/16.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZJForgetPasswordApi : ZJBaseRequest

- (id)initWithUsername:(NSString *)username deviceName:(NSString *)deviceName password:(NSString *)password;

@end

NS_ASSUME_NONNULL_END
