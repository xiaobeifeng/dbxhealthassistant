//
//  ZJTabBarController.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/13.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJTabBarController.h"
#import "ZJNavigationController.h"
#import "ZJHomeViewController.h"
#import "ZJNewsViewController.h"
#import "ZJDeviceViewController.h"
#import "ZJMeViewController.h"

@interface ZJTabBarController ()

@end

@implementation ZJTabBarController

+ (void)load {
    
    UITabBarItem *item = [UITabBarItem appearanceWhenContainedInInstancesOfClasses:@[self]];
    
    // 普通状态下颜色
    NSMutableDictionary *attrsNor = [NSMutableDictionary dictionary];
    attrsNor[NSFontAttributeName] = [UIFont systemFontOfSize:10];
    attrsNor[NSForegroundColorAttributeName] = [UIColor colorWithHexString:@"#537BB7"];
    [item setTitleTextAttributes:attrsNor forState:UIControlStateNormal];
    
    // 选中颜色
    NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
    attrs[NSForegroundColorAttributeName] = [UIColor whiteColor];
    attrs[NSFontAttributeName] = [UIFont systemFontOfSize:10];
    [item setTitleTextAttributes:attrs forState:UIControlStateSelected];
    
    UITabBar *tabBar = [UITabBar appearance];
    tabBar.translucent = NO;
    [tabBar setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"#74A9FD"]]];
    tabBar.shadowImage = [UIImage new];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    ZJHomeViewController *homeVC = [ZJHomeViewController new];
    [self setupChildViewController:homeVC
                          navTitle:@"健康生活管家"
                   tabBarItemTitle:@"首页"
               tabBarItemImageName:@"tab_home"
         tabBarItemSelectImageName:@"tab_home_pre"];
    
    ZJNewsViewController *newsVC = [ZJNewsViewController new];
    [self setupChildViewController:newsVC
                          navTitle:@"资讯"
                   tabBarItemTitle:@"资讯"
               tabBarItemImageName:@"tab_info"
         tabBarItemSelectImageName:@"tab_info_pre"];
    
    ZJDeviceViewController *deviceVC = [ZJDeviceViewController new];
    [self setupChildViewController:deviceVC
                          navTitle:@"我的设备"
                   tabBarItemTitle:@"设备"
               tabBarItemImageName:@"tab_device"
         tabBarItemSelectImageName:@"tab_device_pre"];
    
    
    ZJMeViewController *meVC = [ZJMeViewController new];
    [self setupChildViewController:meVC
                          navTitle:@"我的"
                   tabBarItemTitle:@"我的"
               tabBarItemImageName:@"tab_me"
         tabBarItemSelectImageName:@"tab_me_pre"];
    
}

/**
 向TabBarControllerr添加子控制器
 
 @param viewController 子控制器
 @param navTitle 子控制器导航栏标题
 @param tabBarItemTitle 子控制器TabBarItem标题
 @param tabBarItemImageName 子控制器TabBarItem默认图片
 @param tabBarItemSelectImageName 子控制器TabBarItem选中图片
 */
- (void)setupChildViewController:(UIViewController *)viewController
                        navTitle:(NSString *)navTitle
                 tabBarItemTitle:(NSString *)tabBarItemTitle
             tabBarItemImageName:(NSString *)tabBarItemImageName
       tabBarItemSelectImageName:(NSString *)tabBarItemSelectImageName {
    
    viewController.navigationItem.title = navTitle;
    viewController.tabBarItem.title = tabBarItemTitle;
    viewController.tabBarItem.image = [UIImage imageNamed:tabBarItemImageName];
    viewController.tabBarItem.selectedImage = [UIImage imageNamed:tabBarItemSelectImageName];
    ZJNavigationController *navC= [[ZJNavigationController alloc] initWithRootViewController:viewController];
    [self addChildViewController:navC];
}


@end
