//
//  ZJNewsTableViewCell.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/20.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJNewsTableViewCell.h"

@implementation ZJNewsTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _showImageView = [UIImageView new];
        _showImageView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_showImageView];
        [_showImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contentView).with.offset(13);
            make.bottom.mas_equalTo(self.contentView).with.offset(-13);
            make.right.equalTo(self.contentView).with.offset(-13);
            make.height.mas_equalTo(80);
            make.width.mas_equalTo(120);
        }];
        
        _titleLabel = [UILabel new];
        _titleLabel.font = [UIFont boldSystemFontOfSize:16];
        _titleLabel.numberOfLines = 1;
        [_titleLabel sizeToFit];
        [self.contentView addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_showImageView.mas_top);
            make.left.mas_equalTo(self.contentView).with.offset(13);
            make.right.mas_equalTo(_showImageView.mas_left).with.offset(-13);
        }];
        
        _timeLabel = [UILabel new];
        _timeLabel.font = [UIFont systemFontOfSize:12];
        _timeLabel.textColor = [UIColor lightGrayColor];
        [_timeLabel sizeToFit];
        [self.contentView addSubview:_timeLabel];
        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_titleLabel.mas_bottom).with.offset(5);
            make.left.mas_equalTo(self.contentView).with.offset(13);
            make.right.mas_equalTo(_showImageView.mas_left).with.offset(-13);
        }];
        
        _contentLabel = [UILabel new];
        _contentLabel.font = [UIFont systemFontOfSize:13];
        _contentLabel.numberOfLines = 2;
        [_contentLabel sizeToFit];
        [self.contentView addSubview:_contentLabel];
        [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_timeLabel.mas_bottom).with.offset(5);
            make.left.mas_equalTo(self.contentView).with.offset(13);
            make.right.mas_equalTo(_showImageView.mas_left).with.offset(-13);
            make.bottom.mas_equalTo(_showImageView.mas_bottom);
        }];
        
        UIView *lineView = [UIView new];
        lineView.backgroundColor = COLOR_GALLERY;
        [self.contentView addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.contentView);
            make.height.mas_equalTo(1);
            make.bottom.equalTo(self.contentView);
        }];
        
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
