//
//  WebviewCell.m
//  LoadingHtml
//
//  Created by chuanglong02 on 16/10/25.
//  Copyright © 2016年 漫漫. All rights reserved.
//

#import "WebviewCell.h"
@interface WebviewCell()<UIWebViewDelegate>
@property(nonatomic,strong)UIWebView *webview;
@end
static CGFloat staticheight = 0;
@implementation WebviewCell
+(CGFloat)cellHeight
{
    return staticheight;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle: style reuseIdentifier:reuseIdentifier]) {
        
        self.webview.scrollView.backgroundColor =[UIColor orangeColor];
        [self.contentView addSubview:self.webview];
    }
    return self;
    
}
-(void)setHtmlString:(NSString *)htmlString
{
    _htmlString = htmlString;
    
    self.webview.delegate = self;
    [self.webview loadHTMLString:htmlString baseURL:nil];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    // 拦截网页图片  并修改图片大小
//    [webView stringByEvaluatingJavaScriptFromString:
//    [NSString stringWithFormat:@"var script = document.createElement('script');"
//      "script.type = 'text/javascript';"
//      "script.text = \"function ResizeImages() { "
//      "var myimg,oldwidth,oldheight;"
//      "var maxwidth= %f;" //缩放系数
//      "for(i=0;i <document.images.length;i++){"
//      "myimg = document.images[i];"
//      "if(myimg.width > maxwidth){"
//      "myimg.removeAttribute('width');"
//      "myimg.removeAttribute('height');"
//      "oldwidth = myimg.width;"
//      "oldheight = myimg.height;"
//      "myimg.width = maxwidth;"
//      "myimg.height = maxwidth * (oldheight/oldwidth);"
//      "}"
//      "}"
//      "}\";"
//      "document.getElementsByTagName('head')[0].appendChild(script);", SCREEN_WIDTH-15]];
//    [webView stringByEvaluatingJavaScriptFromString:@"ResizeImages();"];
    
     CGFloat height = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight"] floatValue]+20 ;
    self.webview.frame = CGRectMake(0, 0, kScreenWidth, height);
     self.webview.hidden = NO;
    if (staticheight != height+1) {
        
        staticheight = height+1;
        
        if (staticheight > 0) {
            
            
           
            if (_reloadBlock) {
                _reloadBlock();
            }
        }
    }
}
-(UIWebView *)webview
{
    if (!_webview) {
        _webview =[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, 1)];
        _webview.userInteractionEnabled = NO;
        _webview.hidden = YES;
    }
    return _webview;
}
@end
