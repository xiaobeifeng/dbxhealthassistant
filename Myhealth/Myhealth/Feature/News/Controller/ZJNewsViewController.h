//
//  ZJNewsViewController.h
//  Myhealth
//
//  Created by zhoujian on 2018/11/13.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 资讯
 */
@interface ZJNewsViewController : UITableViewController

@end

NS_ASSUME_NONNULL_END
