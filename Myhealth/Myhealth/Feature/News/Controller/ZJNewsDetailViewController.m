//
//  ZJNewsDetailViewController.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/20.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJNewsDetailViewController.h"
#import "WebviewCell.h"
#import "ZJNewsDetailApi.h"
#import "ZJNewsModel.h"

@interface ZJNewsDetailViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic, strong) UITableView *tableview;
@property(nonatomic, copy) NSString *html;

@end

@implementation ZJNewsDetailViewController

- (UITableView *)tableview {
    if (!_tableview) {
        _tableview =[[UITableView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStylePlain];
        _tableview.delegate = self;
        _tableview.dataSource = self;
        _tableview.backgroundColor = [UIColor whiteColor];
        _tableview.separatorStyle = UITableViewCellSelectionStyleNone;
        _tableview.showsHorizontalScrollIndicator = NO;
        _tableview.showsVerticalScrollIndicator = NO;
        [_tableview registerClass:[WebviewCell class] forCellReuseIdentifier:NSStringFromClass([WebviewCell class])];
        _tableview.tableFooterView = [UIView new];
    }
    return _tableview;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"详情";
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableview];
    
    [MBProgressHUD showActivityMessageInView:@""];
    ZJNewsDetailApi *api = [[ZJNewsDetailApi alloc] initWithNewsID:_ID];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        ZJNewsModel *model = [ZJNewsModel yy_modelWithJSON:api.result];
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJNewsModelData class] json:model.data];
        if (!kArrayIsEmpty(array)) {
            ZJNewsModelData *modelData = array[0];
            NSString *title = modelData.title;
            NSString *time = modelData.addtime;
            NSString *imageUrl = [NSString stringWithFormat:@"src=\"%@", SERVER_ADDRESS];
            NSString *content = [modelData.content stringByReplacingOccurrencesOfString:@"src=\"" withString:imageUrl];
            
            _html = [NSString stringWithFormat:@"<head><style>img{width:90%% !important;height:auto}</style></head><p style=\"text-align: left;font-size:20px;\"> %@ </p><p style=\"text-align: left;font-size:12px;\">%@</p>%@", title, time, content];
            
            [self.tableview reloadData];
            
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [WebviewCell cellHeight] + 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    WebviewCell *cell =[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([WebviewCell class])];
    
    if (!cell) {
        cell = [[WebviewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:NSStringFromClass([WebviewCell class])];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.htmlString = _html;
    __weak ZJNewsDetailViewController *weakSelf = self;
    
    cell.reloadBlock =^()
    {
        [weakSelf.tableview reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    };
    return cell;
    
}



@end
