//
//  ZJNewsViewController.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/13.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJNewsViewController.h"
#import "ZJNewsApi.h"
#import "ZJNewsTableViewCell.h"
#import "ZJNewsModel.h"
#import "ZJNewsDetailViewController.h"

@interface ZJNewsViewController ()

@property (nonatomic, strong) NSMutableArray *dataMarray;
@property (nonatomic, assign) NSInteger currentPage;

@end

@implementation ZJNewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _dataMarray = [NSMutableArray new];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[ZJNewsTableViewCell class] forCellReuseIdentifier:NSStringFromClass([ZJNewsTableViewCell class])];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRefresh)];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRefresh)];
    
    [self headerRefresh];
    
}

- (void)headerRefresh {
    
    _currentPage = 0;
    [self startRequestNewsApi];
}

- (void)footerRefresh {
    
    [self startRequestNewsApi];
}

- (void)startRequestNewsApi {
    
    ZJNewsApi *api = [[ZJNewsApi alloc] initWithPageSize:10 pageNow:_currentPage];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        if (_currentPage == 0) {
            _dataMarray = [NSMutableArray new];
        }
        
        ZJNewsModel *model = [ZJNewsModel yy_modelWithJSON:api.result];
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJNewsModelData class] json:model.data];
        if (!kArrayIsEmpty(array)) {
            [_dataMarray addObjectsFromArray:array];
            
            if (_currentPage != 0) {
                [self.tableView reloadData];
                [self.tableView.mj_footer endRefreshing];
                
            } else {
                [self.tableView reloadData];
                [self.tableView.mj_header endRefreshing];
            }
            
            _currentPage += 1;
            
        } else {
            
            if (_currentPage != 0) {
                [self.tableView reloadData];
                [self.tableView.mj_footer endRefreshing];
                
            } else {
                [self.tableView reloadData];
                [self.tableView.mj_header endRefreshing];
            }
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        if (_currentPage != 0) {
            [self.tableView reloadData];
            [self.tableView.mj_footer endRefreshing];
            
        } else {
            [self.tableView reloadData];
            [self.tableView.mj_header endRefreshing];
        }
        
    }];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataMarray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZJNewsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZJNewsTableViewCell class]) forIndexPath:indexPath];
    [self setupModelOfCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)setupModelOfCell:(ZJNewsTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    ZJNewsModelData *modelData = _dataMarray[indexPath.row];
    cell.titleLabel.text = modelData.title;
    cell.timeLabel.text = modelData.addtime;
    cell.contentLabel.text = [self removeSpaceAndNewline:modelData.summary];
    [cell.showImageView sd_setImageWithURL:[NSURL URLWithString:URL_NEWS(modelData.path)] placeholderImage:[UIImage imageNamed:@"view_load_error"]];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [tableView fd_heightForCellWithIdentifier:NSStringFromClass([ZJNewsTableViewCell class]) cacheByIndexPath:indexPath configuration:^(id cell) {
        [self setupModelOfCell:cell atIndexPath:indexPath];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ZJNewsModelData *modelData = _dataMarray[indexPath.row];
    ZJNewsDetailViewController *newsDetailVC = [ZJNewsDetailViewController new];
    newsDetailVC.ID = [NSString stringWithFormat:@"%ld", (long)modelData.ID];
    [self.navigationController pushViewController:newsDetailVC animated:YES];

}


- (NSString *)removeSpaceAndNewline:(NSString *)str {
    NSString *temp = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    temp = [temp stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return temp;
}


@end
