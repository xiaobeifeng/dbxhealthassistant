//
//  ZJNewsDetailViewController.h
//  Myhealth
//
//  Created by zhoujian on 2018/11/20.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZJNewsDetailViewController : UIViewController

@property (nonatomic, copy) NSString *ID;

@end

NS_ASSUME_NONNULL_END
