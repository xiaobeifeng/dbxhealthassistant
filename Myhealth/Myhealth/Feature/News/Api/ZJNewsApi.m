//
//  ZJNewsApi.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/8.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "ZJNewsApi.h"

@interface ZJNewsApi()

@property (nonatomic, assign) NSInteger pageSize;
@property (nonatomic, assign) NSInteger pageNow;

@end
@implementation ZJNewsApi

- (id)initWithPageSize:(NSInteger)pageSize pageNow:(NSInteger)pageNow {
    self = [super init];
    if (self) {
        _pageSize = pageSize;
        _pageNow = pageNow;
    }
    return self;
}

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return @"newsdatabypaging";
}

// 请求方法，某人是GET
- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

// 请求体
- (id)requestArgument {
    return @{
             @"pagesize": @(_pageSize),
             @"pagenow":@(_pageNow),
             @"doa":@0
             };
}

@end
