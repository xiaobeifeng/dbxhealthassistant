//
//  ZJNewsDetailApi.h
//  Myhealth
//
//  Created by zhoujian on 2018/11/20.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZJNewsDetailApi : ZJBaseRequest

- (id)initWithNewsID:(NSString *)ID;

@end

NS_ASSUME_NONNULL_END
