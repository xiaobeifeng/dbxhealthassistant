
//  Created by zhoujian on 2018/11/8.
//  Copyright © 2018 曙马科技. All rights reserved.
//



NS_ASSUME_NONNULL_BEGIN

@interface ZJNewsApi : ZJBaseRequest

- (id)initWithPageSize:(NSInteger)pageSize pageNow:(NSInteger)pageNow;

@end

NS_ASSUME_NONNULL_END
