//
//  ZJNewsDetailApi.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/20.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJNewsDetailApi.h"

@implementation ZJNewsDetailApi {
    NSString *_ID;
}

- (id)initWithNewsID:(NSString *)ID {
    self = [super init];
    if (self) {
        _ID = ID;
    }
    return self;
}

- (NSString *)requestUrl {
    return @"getpicturenews";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (id)requestArgument {
    return @{
             @"newsid": _ID
             };
}



@end
