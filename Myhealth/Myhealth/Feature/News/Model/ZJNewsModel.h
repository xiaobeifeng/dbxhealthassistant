//
//  ZJNewsModel.h
//  Myhealth
//
//  Created by zhoujian on 2018/11/20.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@interface ZJNewsModelData :NSObject
@property (nonatomic , copy) NSString              * summary;
@property (nonatomic , copy) NSString              * addtime;
@property (nonatomic , copy) NSString              * content;
@property (nonatomic , assign) NSInteger              ID;
@property (nonatomic , copy) NSString              * title;
@property (nonatomic , copy) NSString              * publisher;
@property (nonatomic , copy) NSString              * path;

@end

@interface ZJNewsModel :NSObject
@property (nonatomic , copy) NSString              * message;
@property (nonatomic , assign) NSInteger              state;
@property (nonatomic , copy) NSArray<ZJNewsModelData *>              * data;

@end


NS_ASSUME_NONNULL_END
