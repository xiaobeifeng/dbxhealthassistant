//
//  ZJBaseRequest.h
//  Myhealth
//
//  Created by zhoujian on 2018/11/14.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "YTKBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZJBaseRequest : YTKBaseRequest

@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSDictionary *result;
@property (nonatomic, assign) NSInteger code;
@end

NS_ASSUME_NONNULL_END
