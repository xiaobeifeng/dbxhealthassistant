
//  Created by zhoujian on 2018/11/1.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZJTitleTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *detailImageView;
@end

NS_ASSUME_NONNULL_END
