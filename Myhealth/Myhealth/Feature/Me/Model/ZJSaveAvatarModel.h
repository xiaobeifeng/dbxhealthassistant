
//  Created by zhoujian on 2018/10/18.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZJSaveAvatarModelData :NSObject
@property (nonatomic , copy) NSString              * path;
@property (nonatomic , copy) NSString              * USERNAME;

@end

@interface ZJSaveAvatarModel :NSObject
@property (nonatomic , copy) NSString              * message;
@property (nonatomic , copy) NSArray<ZJSaveAvatarModelData *>              * data;
@property (nonatomic , copy) NSString              * state;

@end

NS_ASSUME_NONNULL_END
