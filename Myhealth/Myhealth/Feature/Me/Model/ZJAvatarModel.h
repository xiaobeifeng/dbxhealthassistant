//
//  ZJAvatarModel.h
//  Myhealth
//
//  Created by zhoujian on 2018/11/19.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZJAvatarModelData :NSObject
@property (nonatomic , copy) NSString              * path;
@property (nonatomic , copy) NSString              * USERNAME;

@end

@interface ZJAvatarModel :NSObject
@property (nonatomic , copy) NSString              * message;
@property (nonatomic , assign) NSInteger              state;
@property (nonatomic , copy) NSArray<ZJAvatarModelData *>              * data;

@end

NS_ASSUME_NONNULL_END
