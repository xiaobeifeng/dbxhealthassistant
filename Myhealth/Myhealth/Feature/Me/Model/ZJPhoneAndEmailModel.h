//
//  ZJPhoneAndEmailModel.h
//  Myhealth
//
//  Created by zhoujian on 2018/11/19.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZJPhoneAndEmailModelData :NSObject
@property (nonatomic , copy) NSString              * PHONE;
@property (nonatomic , copy) NSString              * EMAIL;

@end

@interface ZJPhoneAndEmailModel : NSObject
@property (nonatomic , copy) NSString              * message;
@property (nonatomic , assign) NSInteger              state;
@property (nonatomic , copy) NSArray<ZJPhoneAndEmailModelData *>              * data;
@end

NS_ASSUME_NONNULL_END
