//
//  ZJFeedBackViewController.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/19.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJFeedBackViewController.h"
#import "ZJForgetPasswordView.h"
#import "ZJFeedBackApi.h"

@interface ZJFeedBackViewController ()
@property (nonatomic, strong) UITextView *feedBackView;
@property (nonatomic, strong) ZJForgetPasswordView *contactPhoneView;   // 联系电话
@end

@implementation ZJFeedBackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"意见反馈";
    self.view.backgroundColor = COLOR_GALLERY;
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem titleItem:@"提交" target:self action:@selector(rightBarButtonItemAction)];
    
    _feedBackView = [[UITextView alloc] init];
    _feedBackView.backgroundColor = [UIColor whiteColor];
    _feedBackView.textColor = COLOR_BLACKGRAY;
    _feedBackView.font = [UIFont systemFontOfSize:16];
    _feedBackView.textContainerInset = UIEdgeInsetsMake(5.0f, 20.0f, 5.0f, 20.0f);
    [self.view addSubview:_feedBackView];
    [_feedBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_offset(kScreenWidth);
        make.height.mas_equalTo(150);
        make.top.mas_equalTo(10);
    }];
    
    // UITextViews占位文字
    UILabel *placeHolderLabel = [[UILabel alloc] init];
    placeHolderLabel.text = @"请输入遇到的问题或建议";
    placeHolderLabel.numberOfLines = 0;
    placeHolderLabel.textColor = [UIColor lightGrayColor];
    [placeHolderLabel sizeToFit];
    [_feedBackView addSubview:placeHolderLabel];
    placeHolderLabel.font = [UIFont systemFontOfSize:16.f];
    [_feedBackView setValue:placeHolderLabel forKey:@"_placeholderLabel"];
    
    _contactPhoneView = [[ZJForgetPasswordView alloc] initWithTitle:@"联系电话" placeholder:@"选填，便于我们联系你"];
    [self.view addSubview:_contactPhoneView];
    [_contactPhoneView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_offset(kScreenWidth);
        make.height.mas_equalTo(44);
        make.top.equalTo(_feedBackView.mas_bottom).with.offset(10);
    }];
    
}

- (void)rightBarButtonItemAction {
    NSString *feedBack = _feedBackView.text;
    if (kStringIsEmpty(feedBack)) {
        [MBProgressHUD showWarnMessage:@"请输入遇到的问题或建议"]; return;
    }
    
    [MBProgressHUD showActivityMessageInView:@""];
    ZJFeedBackApi *api = [[ZJFeedBackApi alloc] initWithBackmessage:feedBack];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        if (api.code == 0) {
            [MBProgressHUD showInfoMessage:@"提交成功"];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
    
}

@end
