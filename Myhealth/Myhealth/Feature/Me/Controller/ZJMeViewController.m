//
//  ZJMeViewController.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/13.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJMeViewController.h"
#import "ZJMeHeaderView.h"
#import "ZJTitleTableViewCell.h"
#import "ZJProfileInfoManageViewController.h"
#import "ZJUserAccountManageViewController.h"
#import "ZJAboutViewController.h"
#import "ZJFeedBackViewController.h"
#import "ZJExitApi.h"
#import "ZJShowAvatarApi.h"
#import "ZJUploadAvatarApi.h"
#import "ZJSaveAvatarModel.h"
#import "ZJAvatarModel.h"

@interface ZJMeViewController ()<UITableViewDataSource, UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) ZJMeHeaderView *meHeaderView;
@property (nonatomic, copy) NSArray *titlesArray;
@end

@implementation ZJMeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"我的";

    _titlesArray = @[@"个人信息", @"账号管理", @"关于应用", @"意见反馈"];

    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStylePlain];
    [self.tableView registerClass:[ZJTitleTableViewCell class] forCellReuseIdentifier:NSStringFromClass([ZJTitleTableViewCell class])];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = COLOR_GALLERY;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    
    NSString *userName = [[NSUserDefaults standardUserDefaults] zj_objcetForKey:UD_KEY_USERNAME];
    _meHeaderView = [[ZJMeHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 200)];
    _meHeaderView.avatarImageView.image = [UIImage imageNamed:@"me_avatar_placeholder"];
    _meHeaderView.avatarImageView.userInteractionEnabled = YES;
    _meHeaderView.accountLabel.text = [NSString stringWithFormat:@"账号：%@", userName];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(avatarAction)];
    [_meHeaderView.avatarImageView addGestureRecognizer:tap];
    self.tableView.tableHeaderView = _meHeaderView;
    
    UIView *footerView = [UIView new];
    footerView.frame = CGRectMake(0, 0, kScreenWidth, 80);
    footerView.backgroundColor = COLOR_GALLERY;
    
    UIButton *exitButton = [UIButton new];
    exitButton.backgroundColor = [UIColor colorWithHexString:@"#FF6051"];
    [exitButton setTitle:@"注销账号并退出" forState:UIControlStateNormal];
    [exitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [exitButton addTarget:self action:@selector(exitButtonAction) forControlEvents:UIControlEventTouchUpInside];
    exitButton.adjustsImageWhenHighlighted = NO;
    kViewRadius(exitButton, 15.0f);
    [footerView addSubview:exitButton];
    [exitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(footerView);
        make.left.equalTo(footerView).with.offset(40);
        make.right.equalTo(footerView).with.offset(-40);
        make.height.mas_equalTo(40);
    }];
    
    self.tableView.tableFooterView = footerView;
    
    [self startRequestAvatarApi];
}

- (void)startRequestAvatarApi {
    ZJShowAvatarApi *api = [[ZJShowAvatarApi alloc] init];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        ZJAvatarModel *model = [ZJAvatarModel yy_modelWithJSON:api.responseJSONObject];
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJAvatarModelData class] json:model.data];
        if (!kArrayIsEmpty(array)) {
            ZJAvatarModelData *modelData = array[0];
            [_meHeaderView.avatarImageView sd_setImageWithURL:[NSURL URLWithString:URL_AVATAR(modelData.path)] placeholderImage:[UIImage imageNamed:@"me_avatar_placeholder"]];
            
            NSLog(@"URL_AVATAR - %@", URL_AVATAR(modelData.path));
            NSLog(@"token - %@", [[NSUserDefaults standardUserDefaults] zj_objcetForKey:UD_KEY_TOKEN]);
        }
        
        [[NSUserDefaults standardUserDefaults] zj_objcetForKey:UD_KEY_USERNAME];
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
       
    }];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.titlesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ZJTitleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZJTitleTableViewCell class]) forIndexPath:indexPath];
    [self setupModelOfCell:cell atIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [tableView fd_heightForCellWithIdentifier:NSStringFromClass([ZJTitleTableViewCell class]) cacheByIndexPath:indexPath configuration:^(id cell) {
        [self setupModelOfCell:cell atIndexPath:indexPath];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) {
        ZJProfileInfoManageViewController *profileInfoManageVC = [ZJProfileInfoManageViewController new];
        [self.navigationController pushViewController:profileInfoManageVC animated:YES];
    }
    
    if (indexPath.row == 1) {
        ZJUserAccountManageViewController *userAccountMangeVC = [ZJUserAccountManageViewController new];
        [self.navigationController pushViewController:userAccountMangeVC animated:YES];
    }
    
    if (indexPath.row == 2) {
        ZJAboutViewController *aboutVC = [ZJAboutViewController new];
        [self.navigationController pushViewController:aboutVC animated:YES];
    }
    
    if (indexPath.row == 3) {
        ZJFeedBackViewController *feedBackVC = [ZJFeedBackViewController new];
        [self.navigationController pushViewController:feedBackVC animated:YES];
    }
}

- (void)setupModelOfCell:(ZJTitleTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.titleLabel.text = self.titlesArray[indexPath.row];
}


/**
 退出按钮点击事件
 */
- (void)exitButtonAction {
    
    // 调用移除token接口（并没有返回值去判断）
    ZJExitApi *api = [ZJExitApi new];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", request.responseJSONObject);
        
        // 移除本地存储的token
        [[NSUserDefaults standardUserDefaults] zj_removeObjectForKey:UD_KEY_TOKEN];
        
        ZJNavigationController *navC = [[ZJNavigationController alloc] initWithRootViewController:[ZJLoginViewController new]];
        [UIApplication sharedApplication].keyWindow.rootViewController = navC;

    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
      
}

/**
 更换头像
 */
- (void)avatarAction {
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"" message:@"设置头像" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *takePhotoAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate = self;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePickerController.allowsEditing = YES;
        [self presentViewController:imagePickerController
                           animated:YES
                         completion:NULL];
        
    }];
    
    UIAlertAction *choosePhotoAction = [UIAlertAction actionWithTitle:@"从手机相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate = self;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.allowsEditing = YES;
        [self presentViewController:imagePickerController
                           animated:YES
                         completion:NULL];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alertVC addAction:takePhotoAction];
    [alertVC addAction:choosePhotoAction];
    [alertVC addAction:cancelAction];
    
    [self presentViewController:alertVC animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate Methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    [MBProgressHUD showActivityMessageInView:@""];
    if (info[UIImagePickerControllerEditedImage] == nil) {
        [MBProgressHUD hideHUD];
        [MBProgressHUD showErrorMessage:@"请重试"];
        return;
    }
    
    NSData *data = UIImageJPEGRepresentation(info[UIImagePickerControllerEditedImage], 0.5);
    NSInteger dataLength = [data length];
    NSString *sizeName = [NSString stringWithFormat:@"%ld.png", (long)dataLength];
    NSString *userName = [[NSUserDefaults standardUserDefaults] zj_objcetForKey:UD_KEY_USERNAME];
    // 上传用户头像
    ZJUploadAvatarApi *api = [[ZJUploadAvatarApi alloc] initWithImage:info[UIImagePickerControllerEditedImage] userName:userName sizeName:sizeName];
    
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        // 上传成功
        if (api.code == 0) {
            // 展示上传成功的图片
            _meHeaderView.avatarImageView.image = info[UIImagePickerControllerEditedImage];
            
        } else {
            // 对接口返回的错误信息进行展示，不更新头像
            [MBProgressHUD showErrorMessage:api.message];
        }
        
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
  
    }];
    
    // 处理完毕   回到个人信息页面
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES
                               completion:NULL];
}


@end
