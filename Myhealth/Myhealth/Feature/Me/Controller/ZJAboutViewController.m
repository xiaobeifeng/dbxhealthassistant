//
//  ZJAboutViewController.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/19.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJAboutViewController.h"
#import "ZJAboutTableViewCell.h"

@interface ZJAboutViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, copy) NSArray *titleArray;
@end

@implementation ZJAboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [XDUpdateManager CheckVersionUpadateWithForce:NO];
    
    _titleArray = @[@"当前版本"];
    
    self.view.backgroundColor = COLOR_GALLERY;
    self.navigationItem.title = @"关于应用";
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStylePlain];
    _tableView.backgroundColor = COLOR_GALLERY;
    [_tableView registerClass:[ZJAboutTableViewCell class] forCellReuseIdentifier:NSStringFromClass([ZJAboutTableViewCell class])];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    UIView *headerView = [UIView new];
    headerView.frame = CGRectMake(0, 0, kScreenWidth, 150);
    headerView.backgroundColor = COLOR_GALLERY;
    _tableView.tableHeaderView = headerView;
    
    UIImageView *logoImageView = [UIImageView new];
    logoImageView.image = [UIImage imageNamed:@"logo_launch"];
    [headerView addSubview:logoImageView];
    [logoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(headerView.mas_centerY);
        make.centerX.equalTo(headerView.mas_centerX);
        make.width.height.mas_equalTo(80);
    }];
    
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _titleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZJAboutTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZJAboutTableViewCell class]) forIndexPath:indexPath];
    cell.titleLabel.text = _titleArray[indexPath.row];
    
    if (indexPath.row == 0) {
        // 获取App的版本号
        NSDictionary *infoDic = [[NSBundle mainBundle] infoDictionary];
        NSString *appVersion = [infoDic objectForKey:@"CFBundleShortVersionString"];
        cell.subTitleLabel.text = appVersion;
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
