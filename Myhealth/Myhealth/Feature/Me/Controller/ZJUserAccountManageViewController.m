
//  Created by zhoujian on 2018/9/10.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJUserAccountManageViewController.h"
#import "ZJSavePhoneAndEmailApi.h"
#import "ZJPhoneAndEmailApi.h"
#import "ZJForgetPasswordView.h"
#import "ZJPhoneAndEmailModel.h"
#import "ZJChangePasswordViewController.h"

#define kTextColor [UIColor colorWithRed:0.62 green:0.63 blue:0.64 alpha:1.00]

@interface ZJUserAccountManageViewController ()<UITextFieldDelegate>
@property(nonatomic, copy) NSString *currentPhone;
@property(nonatomic, copy) NSString *currentMail;
@property (nonatomic, strong) ZJForgetPasswordView *phoneView;
@property (nonatomic, strong) ZJForgetPasswordView *mailView;
@end

@implementation ZJUserAccountManageViewController

//- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self){
//        [self initializeForm];
//    }
//    return self;
//}
//
//- (id)initWithCoder:(NSCoder *)aDecoder {
//    self = [super initWithCoder:aDecoder];
//    if (self){
//        [self initializeForm];
//    }
//    return self;
//}
//
//- (void)initializeForm
//{
//
//    XLFormDescriptor * form;
//    XLFormSectionDescriptor * section;
//    XLFormRowDescriptor * row;
//
//    form = [XLFormDescriptor formDescriptorWithTitle:@"个人资料"];
//    section = [XLFormSectionDescriptor formSection];
//
//    [form addFormSection:section];
//
//    // 手机号码
//    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"phone" rowType:XLFormRowDescriptorTypePhone title:@"手机号码"];
//    [row.cellConfig setObject:[[NSAttributedString alloc] initWithString:@"暂未填写"
//                                                              attributes:@{
//                                                                           NSForegroundColorAttributeName:kTextColor
////                                                                           NSFontAttributeName:kFont(14)
//                                                                           }]
//                       forKey:@"textField.attributedPlaceholder"];
//    [row.cellConfig setObject:kTextColor forKey:@"textField.textColor"];
////    [row.cellConfig setObject:[UIFont fontWithName:@"Helvetica" size:14] forKey:@"textLabel.font"];
//    [row.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
//    [section addFormRow:row];
//
//    // 邮件地址
//    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"mail" rowType:XLFormRowDescriptorTypeEmail title:@"邮件地址"];
//    [row.cellConfig setObject:[[NSAttributedString alloc] initWithString:@"暂未填写"
//                                                              attributes:@{
//                                                                           NSForegroundColorAttributeName:kTextColor
////                                                                           NSFontAttributeName:kFont(14)
//                                                                           }]
//                       forKey:@"textField.attributedPlaceholder"];
//    [row.cellConfig setObject:kTextColor forKey:@"textField.textColor"];
//    [row.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
////    [row.cellConfig setObject:[UIFont fontWithName:@"Helvetica" size:14] forKey:@"textLabel.font"];
//    [section addFormRow:row];
//
//    XLFormSectionDescriptor *section2 = [XLFormSectionDescriptor formSection];
//    [form addFormSection:section2];
//
//    // 修改密码
//    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"changePassword" rowType:XLFormRowDescriptorTypeButton title:@"修改密码"];
//    [row.cellConfig setObject:[UIColor blackColor] forKey:@"textLabel.color"];
//    [row.cellConfigAtConfigure setObject:[UIColor whiteColor] forKey:@"backgroundColor"];
//    [row.cellConfig setObject:@(NSTextAlignmentNatural) forKey:@"textLabel.textAlignment"];
//    [row.cellConfig setObject:@(UITableViewCellAccessoryDisclosureIndicator) forKey:@"accessoryType"];
////    [row.cellConfig setObject:[UIFont fontWithName:@"Helvetica" size:14] forKey:@"textLabel.font"];
//    [section2 addFormRow:row];
//
//    self.form = form;
//
//}
//
//- (void)endEditing:(XLFormRowDescriptor *)rowDescriptor
//{
//    NSLog(@"%@, %@", rowDescriptor.tag, rowDescriptor.value);
//    if ([rowDescriptor.tag isEqualToString:@"phone"]) {
//
//        if (ISEMPTY_OBJECT(rowDescriptor.value)) {
//            self.currentPhone = @"";
//        } else {
//            self.currentPhone = [NSString stringWithFormat:@"%@", rowDescriptor.value];
//        }
//
//        DBXUpdatePhoneAndEmailApi *api = [[DBXUpdatePhoneAndEmailApi alloc] initWithPhone:self.currentPhone email:self.currentMail];
//        [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
//
//            NSLog(@"%@", request.responseJSONObject);
//
//        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
//
//        }];
//    }
//
//    if ([rowDescriptor.tag isEqualToString:@"mail"]) {
//
//        if (ISEMPTY_OBJECT(rowDescriptor.value)) {
//            self.currentMail = @"";
//        } else {
//            self.currentMail = [NSString stringWithFormat:@"%@", rowDescriptor.value];
//        }
//        DBXUpdatePhoneAndEmailApi *api = [[DBXUpdatePhoneAndEmailApi alloc] initWithPhone:self.currentPhone email:self.currentMail];
//        [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
//
//            NSLog(@"%@", request.responseJSONObject);
//
//        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
//
//        }];
//    }
//
//}
//
//- (void)didSelectFormRow:(XLFormRowDescriptor *)formRow
//{
//    // 点击修改密码栏
//    if([formRow.tag isEqualToString:@"changePassword"] && formRow.rowType == XLFormRowDescriptorTypeButton) {
//        NSLog(@"点击修改密码栏");
//        [self deselectFormRow:formRow];
//        DBXChangePasswordViewController *changePasswordVC = [DBXChangePasswordViewController new];
//        [self.navigationController pushViewController:changePasswordVC animated:YES];
//    }
//
//}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self startRequestPhoneAndEmailApi];
    self.view.backgroundColor = COLOR_GALLERY;
    self.navigationItem.title = @"账号管理";
    
    _phoneView = [[ZJForgetPasswordView alloc] initWithTitle:@"手机号码" placeholder:@"请输入手机号码"];
    _phoneView.textField.delegate = self;
    _phoneView.textField.keyboardType = UIKeyboardTypePhonePad;
    _phoneView.textField.tag = 10;
    [self.view addSubview:_phoneView];
    [_phoneView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(44);
    }];
    
    _mailView = [[ZJForgetPasswordView alloc] initWithTitle:@"邮件地址" placeholder:@"请输入邮件地址"];
    _mailView.textField.delegate = self;
    _mailView.textField.tag = 20;
    [self.view addSubview:_mailView];
    [_mailView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_phoneView.mas_bottom).with.offset(1);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(44);
    }];
    
    UIView *changePasswordView = [UIView new];
    changePasswordView.userInteractionEnabled = YES;
    changePasswordView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:changePasswordView];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changePasswordViewAction)];
    [changePasswordView addGestureRecognizer:tap];
    [changePasswordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_mailView.mas_bottom).with.offset(10);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(44);
    }];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.font = [UIFont systemFontOfSize:16];
    titleLabel.text = @"修改密码";
    titleLabel.textColor = COLOR_BLACKGRAY;
    [titleLabel sizeToFit];
    [changePasswordView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(changePasswordView).with.offset(13);
        make.left.equalTo(changePasswordView).with.offset(20);
    }];
    
    UIImageView *detailImageView = [UIImageView new];
    detailImageView.image = [UIImage imageNamed:@"detail_arrow"];
    [changePasswordView addSubview:detailImageView];
    [detailImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(changePasswordView);
        make.right.equalTo(changePasswordView).with.offset(-13);
        make.width.height.mas_equalTo(20);
    }];
    
}

- (void)changePasswordViewAction {
    ZJChangePasswordViewController *changePasswordVC = [ZJChangePasswordViewController new];
    [self.navigationController pushViewController:changePasswordVC animated:YES];
}

- (void)startRequestPhoneAndEmailApi {
    
    ZJPhoneAndEmailApi *api = [[ZJPhoneAndEmailApi alloc] init];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        ZJPhoneAndEmailModel *model = [ZJPhoneAndEmailModel yy_modelWithJSON:api.result];
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJPhoneAndEmailModelData class] json:model.data];
        if (!kArrayIsEmpty(array)) {
            ZJPhoneAndEmailModelData *modelData = array[0];
            _phoneView.textField.text = modelData.PHONE;
            _mailView.textField.text = modelData.EMAIL;
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

#pragma mark UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    // 手机号码输入框完成编辑时，上传输入的信息
    if (textField.tag == 10) {
        NSString *phone = textField.text;
        [self startRequestSavePhoneApi:phone];
    }
    
    // 邮箱输入框完成编辑时，上传输入的信息
    if (textField.tag == 20) {
        NSString *email = textField.text;
        [self startRequestSaveEmailApi:email];
    }
}

- (void)startRequestSavePhoneApi:(NSString *)phone {
    
    // 获取当前邮箱输入框的文本信息，进行上传
    NSString *mail = _mailView.textField.text;
    
    ZJSavePhoneAndEmailApi *api = [[ZJSavePhoneAndEmailApi alloc] initWithPhone:phone email:mail];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

- (void)startRequestSaveEmailApi:(NSString *)email {
    
    // 获取当前电话输入框的文本信息，进行上传
    NSString *phone = _phoneView.textField.text;
    
    ZJSavePhoneAndEmailApi *api = [[ZJSavePhoneAndEmailApi alloc] initWithPhone:phone email:email];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
