
//  Created by zhoujian on 2018/9/3.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJProfileInfoManageViewController.h"
#import "ZJProfileInfoApi.h"
#import "ZJSaveProfileInfoApi.h"
#import "ZJProfileInfoModel.h"
#import "ZJSOSContactApi.h"
#import "ZJSOSContactModel.h"

#define kTextColor [UIColor colorWithRed:0.62 green:0.63 blue:0.64 alpha:1.00]

@interface ZJProfileInfoManageViewController ()

@property (nonatomic, strong) NSMutableArray *datasMarray;

@end

@implementation ZJProfileInfoManageViewController {
    NSString *_height;                  // 身高
    NSString *_weight;                  // 体重
    NSString *_birth;                   // 出生日期
    NSString *_sex;                     // 性别
    NSString *_bloodType;               // 血型
    NSString *_nation;                  // 民族
    NSString *_idCard;                  // 身份证
    NSString *_position;                // 职位
    NSString *_companyAddress;          // 公司地址
    NSString *_homeAddress;             // 家庭地址
    NSString *_isMarry;                 // 婚否
    NSString *_SOSContact1;             // 紧急联系人1
    NSString *_SOSContact2;             // 紧急联系人2
    NSString *_SOSContact3;             // 紧急联系人3
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self){
        [self initializeForm];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self){
        [self initializeForm];
    }
    return self;
}

- (void)initializeForm {
    
    XLFormDescriptor * form;
    XLFormSectionDescriptor * section;
    XLFormRowDescriptor * row;
    
    form = [XLFormDescriptor formDescriptorWithTitle:@"个人资料"];
    section = [XLFormSectionDescriptor formSection];
    section.title = @"基本信息";
    [form addFormSection:section];
    
    // 身高
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"height" rowType:XLFormRowDescriptorTypeNumber];
    [row.cellConfig setObject:[[NSAttributedString alloc] initWithString:@"暂未填写(cm)" attributes:@{NSForegroundColorAttributeName:kTextColor}] forKey:@"textField.attributedPlaceholder"];
    [row.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    NSMutableAttributedString *heightTitleAttString = [[NSMutableAttributedString alloc] initWithString:@"* 身高"];
    [heightTitleAttString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, 1)];
    [heightTitleAttString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(2, 2)];
    [row.cellConfig setObject:heightTitleAttString forKey:@"textLabel.attributedText"];
    [row.cellConfig setObject:kTextColor forKey:@"textField.textColor"];
    [section addFormRow:row];
    
    // 体重
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"weight" rowType:XLFormRowDescriptorTypeNumber title:@"体重"];
    [row.cellConfig setObject:[[NSAttributedString alloc] initWithString:@"暂未填写(kg)" attributes:@{NSForegroundColorAttributeName:kTextColor}] forKey:@"textField.attributedPlaceholder"];
    [row.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    NSMutableAttributedString *weightTitleAttString = [[NSMutableAttributedString alloc] initWithString:@"* 体重"];
    [weightTitleAttString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, 1)];
    [row.cellConfig setObject:kTextColor forKey:@"textField.textColor"];
    [weightTitleAttString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(2, 2)];
    [row.cellConfig setObject:weightTitleAttString forKey:@"textLabel.attributedText"];
    [section addFormRow:row];
    
    
    // 出生日期
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"birth" rowType:XLFormRowDescriptorTypeDate title:@"出生日期"];
    row.noValueDisplayText = @"暂未填写";
    NSMutableAttributedString *birthTitleAttString = [[NSMutableAttributedString alloc] initWithString:@"* 出生日期"];
    [birthTitleAttString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, 1)];
    
    [birthTitleAttString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(2, 4)];
    [row.cellConfig setObject:birthTitleAttString forKey:@"textLabel.attributedText"];
    [section addFormRow:row];
    
    // 性别
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"sex" rowType:XLFormRowDescriptorTypeSelectorPickerView title:@"性别"];
    row.noValueDisplayText = @"暂未填写";
    row.selectorOptions = @[@"男", @"女"];
    NSMutableAttributedString *sexTitleAttString = [[NSMutableAttributedString alloc] initWithString:@"* 性别"];
    [sexTitleAttString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, 1)];
    [sexTitleAttString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(2, 2)];
    [row.cellConfig setObject:sexTitleAttString forKey:@"textLabel.attributedText"];
    [section addFormRow:row];
    
    // 血型
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"blood" rowType:XLFormRowDescriptorTypeSelectorPickerView title:@"血型"];
    row.noValueDisplayText = @"暂未填写";
    row.selectorOptions = @[@"A型", @"B型", @"AB型", @"O型", @"其他型"];
    [section addFormRow:row];
    
    // 民族
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"nation" rowType:XLFormRowDescriptorTypeSelectorPickerView title:@"民族"];
    row.noValueDisplayText = @"暂未填写";
    row.selectorOptions = @[@"汉族",@"壮族",@"回族",@"满族",@"维吾尔族",@"苗族",@"彝族",@"土家族",@"藏族",@"蒙古族",@"侗族",@"布依族",@"瑶族",@"白族",@"朝鲜族",@"哈尼族",@"黎族",@"哈萨克族",@"傣族",@"畲族",@"傈僳族",@"东乡族",@"仡佬族",@"拉祜族",@"佤族",@"水族",@"纳西族",@"羌族",@"土族",@"仫佬族",@"锡伯族",@"柯尔克孜族",@"景颇族",@"达斡尔族",@"撒拉族",@"布朗族",@"毛南族",@"塔吉克族",@"普米族",@"阿昌族",@"怒族",@"鄂温克族",@"京族",@"基诺族",@"德昂族",@"保安族",@"俄罗斯族",@"裕固族",@"乌孜别克族",@"门巴族",@"鄂伦春族",@"独龙族",@"赫哲族",@"高山族",@"珞巴族",@"塔塔尔族"];
    [section addFormRow:row];
    
    // 身份证号
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"cardId" rowType:XLFormRowDescriptorTypeNumber title:@"身份证号"];
    [row.cellConfig setObject:[[NSAttributedString alloc] initWithString:@"暂未填写" attributes:@{NSForegroundColorAttributeName:kTextColor}] forKey:@"textField.attributedPlaceholder"];
    [row.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [row.cellConfig setObject:kTextColor forKey:@"textField.textColor"];
    [section addFormRow:row];
    
    // 职业
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"vocation" rowType:XLFormRowDescriptorTypeSelectorPickerView title:@"职业"];
    row.noValueDisplayText = @"暂未填写";
    row.selectorOptions = @[@"国家公务员",@"专业技术人员",@"职员",@"企业管理人员",@"工人",@"农民",@"学生",@"现役军人",@"自由职业者",@"个体经营者",@"无业人员",@"离（退）休人员",@"其他"];
    [section addFormRow:row];
    
    // 单位地址
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"workAddress" rowType:XLFormRowDescriptorTypeText title:@"单位地址"];
    [row.cellConfig setObject:[[NSAttributedString alloc] initWithString:@"暂未填写" attributes:@{NSForegroundColorAttributeName:kTextColor}] forKey:@"textField.attributedPlaceholder"];
    [row.cellConfig setObject:kTextColor forKey:@"textField.textColor"];
    [row.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [section addFormRow:row];
    
    // 家庭地址
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"familyAddress" rowType:XLFormRowDescriptorTypeText title:@"家庭地址"];
    [row.cellConfig setObject:[[NSAttributedString alloc] initWithString:@"暂未填写" attributes:@{NSForegroundColorAttributeName:kTextColor}] forKey:@"textField.attributedPlaceholder"];
    [row.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [row.cellConfig setObject:kTextColor forKey:@"textField.textColor"];
    [section addFormRow:row];
    
    // 婚否
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"married" rowType:XLFormRowDescriptorTypeSelectorPickerView title:@"婚否"];
    row.selectorOptions = @[@"已婚", @"未婚"];
    row.noValueDisplayText = @"暂未填写";
    [section addFormRow:row];
    
    XLFormSectionDescriptor *section1 = [XLFormSectionDescriptor formSection];
    section1.title = @"紧急联系人";
    [form addFormSection:section1];
    // 联系电话1
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"tel1" rowType:XLFormRowDescriptorTypeNumber title:@"联系电话1"];
    [row.cellConfig setObject:@"暂未填写" forKey:@"textField.placeholder"];
    [row.cellConfig setObject:[[NSAttributedString alloc] initWithString:@"暂未填写" attributes:@{NSForegroundColorAttributeName:kTextColor}] forKey:@"textField.attributedPlaceholder"];
    [row.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [row.cellConfig setObject:kTextColor forKey:@"textField.textColor"];
    [row.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [section1 addFormRow:row];
    // 联系电话2
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"tel2" rowType:XLFormRowDescriptorTypeNumber title:@"联系电话2"];
    [row.cellConfig setObject:@"暂未填写" forKey:@"textField.placeholder"];
    [row.cellConfig setObject:[[NSAttributedString alloc] initWithString:@"暂未填写" attributes:@{NSForegroundColorAttributeName:kTextColor}] forKey:@"textField.attributedPlaceholder"];
    [row.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [row.cellConfig setObject:kTextColor forKey:@"textField.textColor"];
    [row.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [section1 addFormRow:row];
    // 联系电话3
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"tel3" rowType:XLFormRowDescriptorTypeNumber title:@"联系电话3"];
    [row.cellConfig setObject:@"暂未填写" forKey:@"textField.placeholder"];
    [row.cellConfig setObject:[[NSAttributedString alloc] initWithString:@"暂未填写"attributes:@{NSForegroundColorAttributeName:kTextColor}] forKey:@"textField.attributedPlaceholder"];
    [row.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [row.cellConfig setObject:kTextColor forKey:@"textField.textColor"];
    [row.cellConfigAtConfigure setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [section1 addFormRow:row];
    
    XLFormSectionDescriptor *section2 = [XLFormSectionDescriptor formSection];
    [form addFormSection:section2];
    
    self.form = form;
}

- (void)saveAction {
    
    // 获取表单所有到的值
    NSDictionary *values =  [self formValues];
    
    NSString *height = [NSString stringWithFormat:@"%@", values[@"height"]];
    NSString *weight = [NSString stringWithFormat:@"%@", values[@"weight"]];
    
    NSDate *date = values[@"birth"];
    //用于格式化NSDate对象
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设置格式：zzz表示时区
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    //NSDate转NSString
    NSString *birth = [dateFormatter stringFromDate:date];
    
    NSString *sex = [NSString stringWithFormat:@"%@", values[@"sex"]];;
    
    NSString *blood = values[@"blood"];
    NSString *nation = values[@"nation"];
    NSString *cardId = values[@"cardId"];
    NSString *vocation = values[@"vocation"];
    NSString *workAddress = values[@"workAddress"];
    NSString *familyAddress = values[@"familyAddress"];
    NSString *married = values[@"married"];
    NSString *tel1 = values[@"tel1"];
    NSString *tel2 = values[@"tel2"];
    NSString *tel3 = values[@"tel3"];
    
    if (kStringIsEmpty(height) || kStringIsEmpty(weight)|| kStringIsEmpty(birth) || kStringIsEmpty(sex)) {
        [MBProgressHUD showWarnMessage:@"带*为必填项，请完善后再试"];
        return;
    }
    
    // 开始上传更改的个人信息到服务器
    [MBProgressHUD showActivityMessageInView:@"保存中"];
    ZJSaveProfileInfoApi *api = [[ZJSaveProfileInfoApi alloc] initWithHeight:height
                                                                      weight:weight
                                                                       birth:birth
                                                                         sex:sex
                                                                       blood:blood
                                                                      nation:nation
                                                                      cardId:cardId
                                                                    vocation:vocation
                                                                 workAddress:workAddress
                                                               familyAddress:familyAddress
                                                                     married:married
                                                                        tel1:tel1
                                                                        tel2:tel2
                                                                        tel3:tel3];
    
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        if (api.code == 0) {
            [MBProgressHUD showSuccessMessage:@"保存成功"];
            
            // 返回上一级
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        } else {
            [MBProgressHUD showErrorMessage:TRAY_AGAIN_LATER];
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem titleItem:@"保存" target:self action:@selector(saveAction)];
    
    _datasMarray = [NSMutableArray new];
    
    [self startRequestProfileInfoApi];
    [self startRequestSOSContactApi];
}

- (void)startRequestProfileInfoApi {
    
    ZJProfileInfoApi *api = [ZJProfileInfoApi new];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", api.result);
        
        ZJProfileInfoModel *model = [ZJProfileInfoModel yy_modelWithJSON:api.result];
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJProfileInfoModelData class] json:model.data];
        if (!kArrayIsEmpty(array)) {
            ZJProfileInfoModelData *modelData = array[0];
            _height = modelData.hight;
            _weight = modelData.weight;
            _birth = modelData.birth;
            _sex = modelData.sex;
            _bloodType = modelData.blood;
            _nation = modelData.nation;
            _idCard = modelData.cardId;
            _position = modelData.vocation;
            _companyAddress = modelData.wadd;
            _homeAddress = modelData.addr;
            _isMarry = modelData.married;
            
            [self reloadFormWithHeight:_height
                                weight:_weight
                                 birth:_birth
                                   sex:_sex
                             bloodType:_bloodType
                                nation:_nation
                                cardId:_idCard
                              vocation:_position
                           workAddress:_companyAddress
                         familyAddress:_homeAddress
                               married:_isMarry];
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
    
    
}

- (void)reloadFormWithHeight:(NSString *)height
                      weight:(NSString *)weight
                       birth:(NSString *)birth
                         sex:(NSString *)sex
                   bloodType:(NSString *)bloodType
                      nation:(NSString *)nation
                      cardId:(NSString *)cardId
                    vocation:(NSString *)vocation
                 workAddress:(NSString *)workAddress
               familyAddress:(NSString *)familyAddress
                     married:(NSString *)married {
    
    // 得到的数值不为nil的时候进行展示
    if (!kStringIsEmpty(_height)) {
        XLFormRowDescriptor *heightRow = [self.form formRowWithTag:@"height"];
        heightRow.value = _height;
        [self reloadFormRow:heightRow];
    }
    
    
    if (!kStringIsEmpty(_weight)) {
        XLFormRowDescriptor *weightRow = [self.form formRowWithTag:@"weight"];
        weightRow.value = _weight;
        [self reloadFormRow:weightRow];
    }
    
    if (!kStringIsEmpty(_birth)) {
        
        NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSDate *date =[dateFormat dateFromString:_birth];
        
        XLFormRowDescriptor *birthRow = [self.form formRowWithTag:@"birth"];
        birthRow.value = date;
        [self reloadFormRow:birthRow];
        
    }
    
    if (!kStringIsEmpty(_sex)) {
        XLFormRowDescriptor *sexRow = [self.form formRowWithTag:@"sex"];
        sexRow.value = _sex;
        [self reloadFormRow:sexRow];
    }
    
    if (!kStringIsEmpty(_bloodType)) {
        XLFormRowDescriptor *bloodRow = [self.form formRowWithTag:@"blood"];
        bloodRow.value = _bloodType;
        [self reloadFormRow:bloodRow];
    }
    
    if (!kStringIsEmpty(_nation)) {
        XLFormRowDescriptor *nationRow = [self.form formRowWithTag:@"nation"];
        nationRow.value = _nation;
        [self reloadFormRow:nationRow];
    }
    
    if (!kStringIsEmpty(_idCard)) {
        XLFormRowDescriptor *cardIdRow = [self.form formRowWithTag:@"cardId"];
        cardIdRow.value = _idCard;
        [self reloadFormRow:cardIdRow];
    }
    
    if (!kStringIsEmpty(_position)) {
        XLFormRowDescriptor *vocationRow = [self.form formRowWithTag:@"vocation"];
        vocationRow.value = _position;
        [self reloadFormRow:vocationRow];
    }
    
    if (!kStringIsEmpty(_companyAddress)) {
        XLFormRowDescriptor *workAddressRow = [self.form formRowWithTag:@"workAddress"];
        workAddressRow.value = _companyAddress;
        [self reloadFormRow:workAddressRow];
    }
    
    if (!kStringIsEmpty(_homeAddress)) {
        XLFormRowDescriptor *familyAddressRow = [self.form formRowWithTag:@"familyAddress"];
        familyAddressRow.value = _homeAddress;
        [self reloadFormRow:familyAddressRow];
    }
    
    if (!kStringIsEmpty(_isMarry)) {
        XLFormRowDescriptor *marriedRow = [self.form formRowWithTag:@"married"];
        marriedRow.value = _isMarry;
        [self reloadFormRow:marriedRow];
    }
    
}

- (void)startRequestSOSContactApi {
    ZJSOSContactApi *api = [ZJSOSContactApi new];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        NSLog(@"%@", api.result);
        ZJSOSContactModel *model = [ZJSOSContactModel yy_modelWithJSON:api.result];
        NSArray *array = [NSArray yy_modelArrayWithClass:[ZJSOSContactModelData class] json:model.data];
        if (!kArrayIsEmpty(array)) {
            ZJSOSContactModelData *modelData = array[0];
            _SOSContact1 = modelData.teone;
            _SOSContact2 = modelData.tetwo;
            _SOSContact3 = modelData.tethree;
            
            [self reloadFormWithSOSContact1:_SOSContact1 SOSContact2:_SOSContact2 SOSContact3:_SOSContact3];
        }
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}

- (void)reloadFormWithSOSContact1:(NSString *)contace1
                      SOSContact2:(NSString *)contace2
                      SOSContact3:(NSString *)contace3 {
    
    XLFormRowDescriptor *tel1Row = [self.form formRowWithTag:@"tel1"];
    tel1Row.value = contace1;
    [self reloadFormRow:tel1Row];
    
    XLFormRowDescriptor *tel2Row = [self.form formRowWithTag:@"tel2"];
    tel2Row.value = contace2;
    [self reloadFormRow:tel2Row];
    
    XLFormRowDescriptor *tel3Row = [self.form formRowWithTag:@"tel3"];
    tel3Row.value = contace3;
    [self reloadFormRow:tel3Row];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
