//
//  ZJChangePasswordViewController.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/19.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJChangePasswordViewController.h"
#import "ZJForgetPasswordView.h"
#import "ZJChangePasswordApi.h"

@interface ZJChangePasswordViewController ()
@property (nonatomic, strong) ZJForgetPasswordView *oldPasswordView;
@property (nonatomic, strong) ZJForgetPasswordView *passwordView;
@property (nonatomic, strong) ZJForgetPasswordView *checkPasswordView;
@end

@implementation ZJChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = COLOR_GALLERY;
    self.navigationItem.title = @"修改密码";
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem titleItem:@"保存" target:self action:@selector(rightBarButtonItemAction)];
    
    _oldPasswordView = [[ZJForgetPasswordView alloc] initWithTitle:@"旧密码" placeholder:@"请输入旧密码"];
    [self.view addSubview:_oldPasswordView];
    [_oldPasswordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(44);
    }];
    
    _passwordView = [[ZJForgetPasswordView alloc] initWithTitle:@"新密码" placeholder:@"请输入新密码"];
    [self.view addSubview:_passwordView];
    [_passwordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_oldPasswordView.mas_bottom).with.offset(1);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(44);
    }];
    
    _checkPasswordView = [[ZJForgetPasswordView alloc] initWithTitle:@"确认密码" placeholder:@"请输入确认密码"];
    [self.view addSubview:_checkPasswordView];
    [_checkPasswordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_passwordView.mas_bottom).with.offset(1);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(44);
    }];
}

- (void)rightBarButtonItemAction {
    
    NSString *oldPassword = _oldPasswordView.textField.text;
    if (kStringIsEmpty(oldPassword)) {
        [MBProgressHUD showWarnMessage:@"请输入旧密码"]; return;
    }
    
    NSString *password = _passwordView.textField.text;
    if (kStringIsEmpty(password)) {
        [MBProgressHUD showWarnMessage:@"请输入新密码"]; return;
    }
    
    NSString *checkPassword = _checkPasswordView.textField.text;
    if (kStringIsEmpty(checkPassword)) {
        [MBProgressHUD showWarnMessage:@"请输入确认密码"]; return;
    }
    
    if (![password isEqualToString:checkPassword]) {
        [MBProgressHUD showWarnMessage:@"两次输入的密码不一致，请检查后再试"]; return;
    }
    
    [MBProgressHUD showActivityMessageInView:@""];
    
    ZJChangePasswordApi *api = [[ZJChangePasswordApi alloc] initWithOldPassword:oldPassword newsPassword:password];
    [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
        
        if (api.code == 0) {
            
            // 设置登录界面为根控制器
            ZJNavigationController *nav = [[ZJNavigationController alloc] initWithRootViewController:[ZJLoginViewController new]];
            [UIApplication sharedApplication].keyWindow.rootViewController = nav;
            
            [MBProgressHUD showInfoMessage:@"密码修改成功，请重新登录"];
            
            // 移除本地存储的token
            [[NSUserDefaults standardUserDefaults] zj_removeObjectForKey:UD_KEY_TOKEN];
            
            
        } else {
            [MBProgressHUD showWarnMessage:api.message];
        }
        
        
    } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
        
    }];
}



@end
