
//  Created by zhoujian on 2018/11/2.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZJMeHeaderView : UIView

@property (nonatomic, strong) UIImageView *avatarImageView;
@property (nonatomic, strong) UILabel *accountLabel;

@end

NS_ASSUME_NONNULL_END
