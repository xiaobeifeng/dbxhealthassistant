//
//  ZJAboutTableViewCell.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/19.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJAboutTableViewCell.h"

@implementation ZJAboutTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.titleLabel = [UILabel new];
        self.titleLabel.font = [UIFont systemFontOfSize:16];
        self.titleLabel.textColor = COLOR_BLACKGRAY;
        [self.titleLabel sizeToFit];
        [self.contentView addSubview:self.titleLabel];
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).with.offset(13);
            make.left.equalTo(self.contentView).with.offset(20);
        }];
        
        self.subTitleLabel = [UILabel new];
        self.subTitleLabel.font = [UIFont systemFontOfSize:16];
        self.subTitleLabel.textColor = [UIColor lightGrayColor];
        [self.subTitleLabel sizeToFit];
        [self.contentView addSubview:self.subTitleLabel];
        [self.subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.titleLabel.mas_centerY);
            make.right.equalTo(self.contentView).with.offset(-13);
        }];
        
        self.lineView = [UIView new];
        self.lineView.backgroundColor = COLOR_GALLERY;
        [self.contentView addSubview:self.lineView];
        [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titleLabel.mas_bottom).with.offset(13);
            make.left.right.equalTo(self.contentView);
            make.height.mas_equalTo(1);
            make.bottom.equalTo(self.contentView);
        }];
        
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    self.lineView.backgroundColor = COLOR_GALLERY;
}

@end
