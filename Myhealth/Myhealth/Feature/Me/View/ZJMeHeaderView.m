
//  Created by zhoujian on 2018/11/2.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJMeHeaderView.h"

@interface ZJMeHeaderView()

@property (nonatomic, strong) UIView *view;

@end

@implementation ZJMeHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = COLOR_GALLERY;
        
        self.view = [UIView new];
        self.view.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.view];
        
        self.avatarImageView = [UIImageView new];
        kViewRadiusBorder(self.avatarImageView, 50, 1, COLOR_GALLERY);
        [self addSubview:self.avatarImageView];
        
        self.accountLabel = [UILabel new];
        self.accountLabel.font = [UIFont systemFontOfSize:16];
        self.accountLabel.textColor = COLOR_BLACKGRAY;
        [self.accountLabel sizeToFit];
        [self addSubview:self.accountLabel];
        
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).with.offset(13);
        make.bottom.equalTo(self).with.offset(-30);
        make.width.mas_equalTo(kScreenWidth);
    }];
    
    [self.avatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.width.height.mas_equalTo(100);
        make.centerY.equalTo(self.view.mas_centerY).with.offset(-20);
    }];
    
    [self.accountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.top.equalTo(self.avatarImageView.mas_bottom).with.offset(13);
        make.width.mas_lessThanOrEqualTo(kScreenWidth-100);
    }];
}

@end
