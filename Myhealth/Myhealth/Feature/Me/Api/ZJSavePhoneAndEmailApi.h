
//  Created by zhoujian on 2018/9/14.
//  Copyright © 2018年 曙马科技. All rights reserved.
//



/**
 更新电话和邮箱
 */
@interface ZJSavePhoneAndEmailApi : ZJBaseRequest

- (id)initWithPhone:(NSString *)phone
              email:(NSString *)email;

@end
