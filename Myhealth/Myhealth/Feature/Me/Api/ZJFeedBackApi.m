
//  Created by zhoujian on 2018/9/14.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJFeedBackApi.h"

@interface ZJFeedBackApi()

@property(nonatomic, copy) NSString *backmessage;

@end

@implementation ZJFeedBackApi
- (id)initWithBackmessage:(NSString *)backmessage
{
    
    self = [super init];
    if (self) {
        _backmessage = backmessage;
    }
    return self;
}

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return @"msgsave";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

// 请求体
- (id)requestArgument {
    return @{
             @"backmessage": _backmessage
             };
}
@end
