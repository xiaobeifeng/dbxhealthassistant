
//  Created by zhoujian on 2018/9/14.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJChangePasswordApi.h"

@interface ZJChangePasswordApi()

@property(nonatomic, copy) NSString *oldPassword;
@property(nonatomic, copy) NSString *newsPassword;

@end

@implementation ZJChangePasswordApi
- (id)initWithOldPassword:(NSString *)oldPassword
              newsPassword:(NSString *)newsPassword
{
    
    self = [super init];
    if (self) {
        _oldPassword = oldPassword;
        _newsPassword = newsPassword;
    }
    return self;
}

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return @"userpwquery";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

// 请求体
- (id)requestArgument {
    return @{
             @"oldPassword": _oldPassword,
             @"newPassword": _newsPassword
             };
}
@end
