
//  Created by zhoujian on 2018/10/15.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "ZJShowAvatarApi.h"

@implementation ZJShowAvatarApi

- (NSString *)requestUrl {
    
    return @"avatar";
}


- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
