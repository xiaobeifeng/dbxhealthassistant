
//  Created by zhoujian on 2018/10/15.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "ZJUploadAvatarApi.h"

@interface ZJUploadAvatarApi()

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *sizeName;
@end

@implementation ZJUploadAvatarApi

- (id)initWithImage:(UIImage *)image userName:(NSString *)userName sizeName:(NSString *)sizeName
{
    self = [super init];
    if (self) {
        _image = image;
        _userName = userName;
        _sizeName = sizeName;
    }
    return self;
}

- (YTKRequestMethod)requestMethod
{
    return YTKRequestMethodPOST;
}

- (NSString *)requestUrl {
    
    return [NSString stringWithFormat:@"auto/avatar/save/%@/%@", _sizeName,_userName];
}

- (AFConstructingBlock)constructingBodyBlock
{
    return ^(id<AFMultipartFormData> formData) {
        NSData *data = UIImageJPEGRepresentation(self.image, 0.5);
        NSString *name = @"file";
        NSString *formKey = @"file";
        NSString *type = @"multipart/form-data";
        [formData appendPartWithFileData:data name:formKey fileName:name mimeType:type];
    };
}


@end
