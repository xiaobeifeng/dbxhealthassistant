
//  Created by zhoujian on 2018/9/3.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJSOSContactApi.h"

@implementation ZJSOSContactApi

- (NSString *)requestUrl {
    
    return @"person";
}


- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
