
//  Created by zhoujian on 2018/9/14.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJPhoneAndEmailApi.h"

@implementation ZJPhoneAndEmailApi

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    return @"phoneandemail";
}

// 请求方法，某人是GET
- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
