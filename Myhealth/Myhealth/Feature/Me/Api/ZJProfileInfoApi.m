
//  Created by zhoujian on 2018/9/3.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJProfileInfoApi.h"

@implementation ZJProfileInfoApi

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return @"archives";
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodGET;
}

@end
