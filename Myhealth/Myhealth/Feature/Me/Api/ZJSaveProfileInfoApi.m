
//  Created by zhoujian on 2018/9/3.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJSaveProfileInfoApi.h"

@interface ZJSaveProfileInfoApi()

@property(nonatomic, copy) NSString *height;
@property(nonatomic, copy) NSString *weight;
@property(nonatomic, copy) NSString *birth;
@property(nonatomic, copy) NSString *sex;
@property(nonatomic, copy) NSString *blood;
@property(nonatomic, copy) NSString *nation;
@property(nonatomic, copy) NSString *cardId;
@property(nonatomic, copy) NSString *vocation;
@property(nonatomic, copy) NSString *workAddress;
@property(nonatomic, copy) NSString *familyAddress;
@property(nonatomic, copy) NSString *married;
@property(nonatomic, copy) NSString *tel1;
@property(nonatomic, copy) NSString *tel2;
@property(nonatomic, copy) NSString *tel3;

@end

@implementation ZJSaveProfileInfoApi

- (id)initWithHeight :(NSString *)height
               weight:(NSString *)weight
                birth:(NSString *)birth
                  sex:(NSString *)sex
                blood:(NSString *)blood
               nation:(NSString *)nation
               cardId:(NSString *)cardId
             vocation:(NSString *)vocation
          workAddress:(NSString *)workAddress
        familyAddress:(NSString *)familyAddress
              married:(NSString *)married
                 tel1:(NSString *)tel1
                 tel2:(NSString *)tel2
                 tel3:(NSString *)tel3
{
    self = [super init];
    if (self) {
        
        _height = height;
        _weight = weight;
        _birth = birth;
        _sex = sex;
        _blood = blood;
        _nation = nation;
        _cardId = cardId;
        _vocation = vocation;
        _workAddress = workAddress;
        _familyAddress = familyAddress;
        _married = married;
        _tel1 = tel1;
        _tel2 = tel2;
        _tel3 = tel3;
        
    }
    return self;
}

- (NSString *)requestUrl {
    
    return @"arvquery";
}


- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

// 请求体
- (id)requestArgument {
    return @{
             @"hight": _height,
             @"weight": _weight,
             @"birth": _birth,
             @"sex": _sex,
             @"blood": _blood,
             @"nation": _nation,
             @"cardId": _cardId,
             @"vocation": _vocation,
             @"wadd": _workAddress,
             @"addr": _familyAddress,
             @"married": _married,
             @"teone": _tel1,
             @"tetwo": _tel2,
             @"tethree": _tel3
             };
}


@end
