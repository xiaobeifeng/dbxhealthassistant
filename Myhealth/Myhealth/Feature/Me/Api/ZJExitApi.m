
//  Created by zhoujian on 2018/9/14.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJExitApi.h"

@implementation ZJExitApi

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return URL_LOGIN;
}

- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodDELETE;
}

@end
