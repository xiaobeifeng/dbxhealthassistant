
//  Created by zhoujian on 2018/9/3.
//  Copyright © 2018年 曙马科技. All rights reserved.
//



@interface ZJSaveProfileInfoApi : ZJBaseRequest

- (id)initWithHeight :(NSString *)height
               weight:(NSString *)weight
                birth:(NSString *)birth
                  sex:(NSString *)sex
                blood:(NSString *)blood
               nation:(NSString *)nation
               cardId:(NSString *)cardId
             vocation:(NSString *)vocation
          workAddress:(NSString *)workAddress
        familyAddress:(NSString *)familyAddress
              married:(NSString *)married
                 tel1:(NSString *)tel1
                 tel2:(NSString *)tel2
                 tel3:(NSString *)tel3;

@end
