
//  Created by zhoujian on 2018/11/1.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJTitleTableViewCell.h"

@interface ZJTitleTableViewCell()


@property (nonatomic, strong) UIView *lineView;

@end

@implementation ZJTitleTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.titleLabel = [UILabel new];
        self.titleLabel.font = [UIFont systemFontOfSize:16];
        self.titleLabel.textColor = COLOR_BLACKGRAY;
        [self.titleLabel sizeToFit];
        [self.contentView addSubview:self.titleLabel];
        
        self.detailImageView = [UIImageView new];
        self.detailImageView.image = [UIImage imageNamed:@"detail_arrow"];
        [self.contentView addSubview:self.detailImageView];
        
        self.lineView = [UIView new];
        self.lineView.backgroundColor = COLOR_GALLERY;
        [self.contentView addSubview:self.lineView];
        
    }
    return self;
}

+ (BOOL)requiresConstraintBasedLayout
{
    return YES;
}

- (void)updateConstraints
{
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(13);
        make.left.equalTo(self.contentView).with.offset(20);
    }];
    
    [self.detailImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(self.contentView).with.offset(-13);
        make.width.height.mas_equalTo(20);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).with.offset(13);
        make.left.right.equalTo(self.contentView);
        make.height.mas_equalTo(1);
        make.bottom.equalTo(self.contentView);
    }];
    
    [super updateConstraints];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    self.detailImageView.backgroundColor = [UIColor clearColor];
    self.lineView.backgroundColor = COLOR_GALLERY;
    
}

@end
