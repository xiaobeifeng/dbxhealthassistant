//
//  ZJSyncHPHOHBTableViewCell.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/6.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "ZJSyncHPHOHBTableViewCell.h"


@interface ZJSyncHPHOHBTableViewCell()


@end

@implementation ZJSyncHPHOHBTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.selectButton = [UIButton new];
        [self.selectButton setImage:[UIImage imageNamed:@"button_cell"] forState:UIControlStateNormal];
        [self.selectButton setImage:[UIImage imageNamed:@"button_cell_select"] forState:UIControlStateSelected];
        [self.selectButton addTarget:self action:@selector(selectButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.selectButton];
        [self.selectButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(25);
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.left.equalTo(self.contentView).with.offset(20);
        }];
        
        self.bloodPreLabel = [UILabel new];
        self.bloodPreLabel.font = [UIFont systemFontOfSize:14];
        self.bloodPreLabel.textAlignment = NSTextAlignmentCenter;
        self.bloodPreLabel.text = @"-";
        [self.contentView addSubview:self.bloodPreLabel];
        
        self.bloodOxygenLabel = [UILabel new];
        self.bloodOxygenLabel.font = [UIFont systemFontOfSize:12];
        self.bloodOxygenLabel.textAlignment = NSTextAlignmentCenter;
        self.bloodOxygenLabel.text = @"-";
        [self.contentView addSubview:self.bloodOxygenLabel];
        
        self.heartBeatLabel = [UILabel new];
        self.heartBeatLabel.font = [UIFont systemFontOfSize:12];
        self.heartBeatLabel.textAlignment = NSTextAlignmentCenter;
        self.heartBeatLabel.text = @"-";
        [self.contentView addSubview:self.heartBeatLabel];
        
        self.timeLabel = [UILabel new];
        self.timeLabel.font = [UIFont systemFontOfSize:12];
        self.timeLabel.numberOfLines = 0;
        self.timeLabel.textAlignment = NSTextAlignmentCenter;
        self.timeLabel.text = @"-";
        [self.contentView addSubview:self.timeLabel];
        
        NSArray *buttons = @[self.bloodPreLabel, self.bloodOxygenLabel, self.heartBeatLabel, self.timeLabel];
        // 水平方向控件间隔固定等间隔
        [buttons mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:2 leadSpacing:65 tailSpacing:20];
        [buttons mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.height.mas_equalTo(40);
        }];
        
        UIView *line = [UIView new];
        line.backgroundColor = COLOR_GALLERY;
        [self.contentView addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.contentView.mas_bottom);
            make.width.mas_equalTo(kScreenWidth);
            make.height.mas_equalTo(0.5);
        }];
        
        
        
    }
    return self;
}

- (void)setModel:(ZJSyncHPHOHBModel *)model {
    
    self.selectButton.selected = model.isSelect;
    
}

- (void)selectButtonAction:(UIButton *)button {
    
    [self.delegate cellButtonAcion:button];
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
