//
//  ZJSyncBSTableViewCell.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/6.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZJSyncBSModel.h"
NS_ASSUME_NONNULL_BEGIN

@protocol ZJSyncBSTableViewCellDelegate <NSObject>

- (void)cellButtonAcion:(UIButton *)button;
- (void)typeButtonAcion:(UIButton *)button;

@end

@interface ZJSyncBSTableViewCell : UITableViewCell

@property (nonatomic, strong) UIButton *selectButton;
@property (nonatomic, strong) UILabel *bloodSugarLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, assign) NSInteger type;   //  测量类型
@property (nonatomic, strong) UIButton *typeButton;   //  测量类型选择按钮
@property (nonatomic, strong) ZJSyncBSModel *model;
@property (nonatomic, assign) id<ZJSyncBSTableViewCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
