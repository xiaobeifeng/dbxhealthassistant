//
//  ZJSyncBSTableViewCell.m
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/6.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "ZJSyncBSTableViewCell.h"

@interface ZJSyncBSTableViewCell()

@property (nonatomic, strong)  UIView *buttonLine;

@end

@implementation ZJSyncBSTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.selectButton = [UIButton new];
        [self.selectButton setImage:[UIImage imageNamed:@"button_cell"] forState:UIControlStateNormal];
        [self.selectButton setImage:[UIImage imageNamed:@"button_cell_select"] forState:UIControlStateSelected];
        [self.selectButton addTarget:self action:@selector(selectButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.selectButton];
        [self.selectButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.mas_equalTo(25);
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.left.equalTo(self.contentView).with.offset(20);
        }];
        
        self.bloodSugarLabel = [UILabel new];
        self.bloodSugarLabel.font = [UIFont systemFontOfSize:12];
        self.bloodSugarLabel.textAlignment = NSTextAlignmentCenter;
        self.bloodSugarLabel.text = @"--";
        [self.contentView addSubview:self.bloodSugarLabel];
        
        self.typeButton = [UIButton new];
        self.typeButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [self.typeButton setTitle:@"空腹" forState:UIControlStateNormal];
        [self.typeButton setTitleColor:COLOR_PICTONBLUE forState:UIControlStateNormal];
        [self.typeButton addTarget:self action:@selector(typeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
//        self.typeButton.backgroundColor = kTableSeparatorLineColor;
//        kViewRadius(self.typeButton, 5.0f);
        [self.contentView addSubview:self.typeButton];
        
        self.timeLabel = [UILabel new];
        self.timeLabel.font = [UIFont systemFontOfSize:12];
        self.timeLabel.numberOfLines = 0;
        self.timeLabel.textAlignment = NSTextAlignmentCenter;
        self.timeLabel.text = @"--";
        [self.contentView addSubview:self.timeLabel];
        
        NSArray *buttons = @[self.bloodSugarLabel, self.typeButton, self.timeLabel];
        // 水平方向控件间隔固定等间隔
        [buttons mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:2 leadSpacing:65 tailSpacing:20];
        [buttons mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.height.mas_equalTo(40);
        }];
        
        UIView *line = [UIView new];
        line.backgroundColor = COLOR_GALLERY;
        [self.contentView addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.contentView.mas_bottom);
            make.width.mas_equalTo(kScreenWidth);
            make.height.mas_equalTo(0.5);
        }];
        
        _buttonLine = [UIView new];
        _buttonLine.backgroundColor = COLOR_PICTONBLUE;
        [self.contentView addSubview:_buttonLine];
        [_buttonLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.typeButton.mas_bottom);
            make.width.mas_equalTo(20);
            make.height.mas_equalTo(1);
            make.centerX.equalTo(_typeButton.mas_centerX);
        }];
        
    }
    return self;
}

- (void)selectButtonAction:(UIButton *)button {
    [self.delegate cellButtonAcion:button];
}

- (void)setModel:(ZJSyncBSModel *)model {
    self.selectButton.selected = model.isSelect;
}

- (void)typeButtonAction:(UIButton *)button {
    [self.delegate typeButtonAcion:button];
}

- (void)setType:(NSInteger)type {
    
    switch (type) {
        case 0:
            [_typeButton setTitle:@"空腹" forState:UIControlStateNormal];
            break;
        case 1:
            [_typeButton setTitle:@"早餐前" forState:UIControlStateNormal];
            break;
        case 2:
            [_typeButton setTitle:@"早餐后" forState:UIControlStateNormal];
            break;
        case 3:
            [_typeButton setTitle:@"午餐前" forState:UIControlStateNormal];
            break;
        case 4:
            [_typeButton setTitle:@"午餐后" forState:UIControlStateNormal];
            break;
        case 5:
            [_typeButton setTitle:@"晚餐前" forState:UIControlStateNormal];
            break;
        case 6:
            [_typeButton setTitle:@"晚餐后" forState:UIControlStateNormal];
            break;
        case 7:
            [_typeButton setTitle:@"睡前" forState:UIControlStateNormal];
            break;
        default:
            break;
    }

}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
