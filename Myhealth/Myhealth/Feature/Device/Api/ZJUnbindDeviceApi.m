
//  Created by zhoujian on 2018/9/26.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import "ZJUnbindDeviceApi.h"

@interface ZJUnbindDeviceApi()
@property(nonatomic, copy) NSString *deviceCode;
@end

@implementation ZJUnbindDeviceApi
- (id)initWithDeviceCode:(NSString *)deviceCode
{
    
    self = [super init];
    if (self) {
        _deviceCode = deviceCode;
    }
    return self;
}

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return @"removebind";
}

// 请求方法，某人是GET
- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

// 请求体
- (id)requestArgument {
    return @{
             @"device_code": _deviceCode,
             };
}

@end
