
//  Created by zhoujian on 2018/9/19.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJLinkyornoneApi.h"

@interface ZJLinkyornoneApi()


@end

@implementation ZJLinkyornoneApi {
    NSString *_deviceCode;
}
- (id)initWithDeviceCode:(NSString *)deviceCode
{
    
    self = [super init];
    if (self) {
        _deviceCode = deviceCode;
    }
    return self;
}

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    
    return @"linkyornone";
}

// 请求方法，某人是GET
- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

// 请求体
- (id)requestArgument {
    return @{
             @"device_code": _deviceCode,
             };
}
@end
