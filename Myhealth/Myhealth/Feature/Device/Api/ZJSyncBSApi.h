
//  Created by zhoujian on 2018/8/29.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZJSyncBSApi : YTKBaseRequest

- (id)initWithJsonData:(NSData *)jsonData;

@end
