
//  Created by zhoujian on 2018/8/29.
//  Copyright © 2018年 曙马科技. All rights reserved.
//

#import "ZJSyncBSApi.h"

@interface ZJSyncBSApi()
{
    NSData *_bodyData;
}

@end

@implementation ZJSyncBSApi

- (id)initWithJsonData:(NSData *)jsonData
{
    self = [super init];
    
    if (self) {
        _bodyData = jsonData;
    }
    return self;
}

// 需要和baseUrl拼接的地址
- (NSString *)requestUrl {
    return @"dbxbgsave";
}

// 请求方法
- (YTKRequestMethod)requestMethod {
    return YTKRequestMethodPOST;
}

- (YTKRequestSerializerType)requestSerializerType
{
    return YTKRequestSerializerTypeJSON;
}

- (NSURLRequest *)buildCustomUrlRequest
{
    YTKNetworkConfig *config = [YTKNetworkConfig sharedConfig];
    NSString *url = [NSString stringWithFormat:@"%@%@", config.baseUrl, self.requestUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[[NSUserDefaults standardUserDefaults] zj_objcetForKey:UD_KEY_TOKEN] forHTTPHeaderField:@"token"];
    [request setHTTPBody:_bodyData];
    return request;
}


@end
