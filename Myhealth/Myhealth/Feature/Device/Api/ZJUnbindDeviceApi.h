
//  Created by zhoujian on 2018/9/26.
//  Copyright © 2018 曙马科技. All rights reserved.
//



NS_ASSUME_NONNULL_BEGIN

@interface ZJUnbindDeviceApi : ZJBaseRequest

- (id)initWithDeviceCode:(NSString *)deviceCode;

@end

NS_ASSUME_NONNULL_END
