//
//  ZJDeviceViewController.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/13.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "ZJDeviceViewController.h"
#import "ZJTitleTableViewCell.h"
#import "ZJDeviceManageViewController.h"
#import "ZJDeviceTimeViewController.h"
#import "ZJDeviceSyncViewController.h"

@interface ZJDeviceViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, copy) NSArray *titlesArray;
@property (nonatomic, strong) UILabel *deviceNameLabel;
@end

@implementation ZJDeviceViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSString *deviceName = [[NSUserDefaults standardUserDefaults] zj_objcetForKey:UD_KEY_DEVICE_NAME];  // 获得设备名称
    if (kStringIsEmpty(deviceName)) {
        _deviceNameLabel.text = @"暂无绑定设备";
    } else {
        _deviceNameLabel.text = [NSString stringWithFormat:@"当前绑定设备：%@", deviceName];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _titlesArray = @[@"设备管理", @"时间校准", @"数据同步"];
    
    [self setupUI];
   
}

- (void)setupUI {
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStylePlain];
    [self.tableView registerClass:[ZJTitleTableViewCell class] forCellReuseIdentifier:NSStringFromClass([ZJTitleTableViewCell class])];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = COLOR_GALLERY;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    
    UIView *headerView = [UIView new];
    headerView.backgroundColor = COLOR_PICTONBLUE;
    headerView.frame = CGRectMake(0, 0, kScreenWidth, 60);
    self.tableView.tableHeaderView = headerView;
    
    _deviceNameLabel = [UILabel new];
    _deviceNameLabel.text = @"暂无绑定设备";
    _deviceNameLabel.textColor = [UIColor whiteColor];
    _deviceNameLabel.font = [UIFont systemFontOfSize:14];
    [_deviceNameLabel sizeToFit];
    [headerView addSubview:_deviceNameLabel];
    [_deviceNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.centerY.equalTo(headerView.mas_centerY);
    }];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.titlesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ZJTitleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ZJTitleTableViewCell class]) forIndexPath:indexPath];
    [self setupModelOfCell:cell atIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [tableView fd_heightForCellWithIdentifier:NSStringFromClass([ZJTitleTableViewCell class]) cacheByIndexPath:indexPath configuration:^(id cell) {
        [self setupModelOfCell:cell atIndexPath:indexPath];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *deviceName = [[NSUserDefaults standardUserDefaults] zj_objcetForKey:UD_KEY_DEVICE_NAME];

    if (indexPath.row == 0) {
        ZJDeviceManageViewController *deviceManageVC = [ZJDeviceManageViewController new];
        deviceManageVC.navigationItem.title = @"设备管理";
        [self.navigationController pushViewController:deviceManageVC animated:YES];
    }

    if (indexPath.row == 1) {

        if (!kStringIsEmpty(deviceName)) {
            ZJDeviceTimeViewController *deviceTimeVC = [ZJDeviceTimeViewController new];
            deviceTimeVC.navigationItem.title = @"时间同步";
            [self.navigationController pushViewController:deviceTimeVC animated:YES];
        } else {
            [MBProgressHUD showWarnMessage:@"请绑定设备后再试"];
        }
    }

    if (indexPath.row == 2) {

        if (!kStringIsEmpty(deviceName)) {
            ZJDeviceSyncViewController *deviceTimeVC = [ZJDeviceSyncViewController new];
            deviceTimeVC.navigationItem.title = @"数据同步";
            [self.navigationController pushViewController:deviceTimeVC animated:YES];
        } else {
            [MBProgressHUD showWarnMessage:@"请绑定设备后再试"];
        }

    }
}

- (void)setupModelOfCell:(ZJTitleTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.titleLabel.text = self.titlesArray[indexPath.row];
}


@end
