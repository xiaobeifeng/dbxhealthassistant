
//  Created by zhoujian on 2018/11/9.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 设备管理
 */
@interface ZJDeviceManageViewController : UIViewController

@property (nonatomic, assign) BOOL isShowChangeUser;    // 是否展示切换用户按钮

@end

NS_ASSUME_NONNULL_END
