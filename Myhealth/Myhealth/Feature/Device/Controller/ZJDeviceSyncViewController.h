//
//  ZJDeviceSyncViewController.h
//  DBXMyhealth
//
//  Created by zhoujian on 2018/11/6.
//  Copyright © 2018 曙马科技. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 设备同步
 */
@interface ZJDeviceSyncViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
