//
//  Macro.h
//  Myhealth
//
//  Created by zhoujian on 2018/11/13.
//  Copyright © 2018 zhoujian. All rights reserved.
//


#ifndef Macro_h
#define Macro_h

/******************************视图圆角，边框******************************/
// 圆角带边框
#define kViewRadiusBorder(View, Radius, Width, Color)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES];\
[View.layer setBorderWidth:(Width)];\
[View.layer setBorderColor:[Color CGColor]]
// 圆角
#define kViewRadius(View, Radius)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES]

/******************************Screen Property******************************/
#pragma mark - 屏幕尺寸相关
#define kCurrentModeSize       [[UIScreen mainScreen] currentMode].size
#define kScreenScale           [UIScreen mainScreen].scale
#define kNativeScale           [[UIScreen mainScreen] nativeScale]
#define kScreenBounds          [UIScreen mainScreen].bounds
#define kScreenSize            kScreenBounds.size
#define kScreenWidth           kScreenSize.width
#define kScreenHeight          kScreenSize.height
#define kSafeAreaToHeight (kScreenHeight == 812.0 ? 88 : 64)
/*******************************自定义的 NSLog******************************/
#pragma mark - 自定义的 NSLog
#ifdef DEBUG
#define kNSLog(fmt, ...) NSLog((@"%s " fmt), __PRETTY_FUNCTION__, ##__VA_ARGS__);
#else
#define kNSLog(...)
#endif

/*********************不需要打印时间戳等信息，使用如下宏定义***********************/
#ifdef DEBUG
#define kCNSLog(FORMAT, ...) fprintf(stderr,"%s\n",[[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
#else
#define kCNSLog(...)
#endif

/**********************打印日志,当前行 并弹出一个警告**************************/
#ifdef DEBUG
#   define kALERTLog(fmt, ...)  { UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%s\n [Line %d] ", __PRETTY_FUNCTION__, __LINE__] message:[NSString stringWithFormat:fmt, ##__VA_ARGS__]  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]; [alert show]; }
#else
#   define kALERTLog(...)
#endif

/*********************************************空值判断************************************************/
/** 字符串是否为空*/
#define kStringIsEmpty(str) ([str isKindOfClass:[NSNull class]] || str == nil || [str length] < 1 ? YES : NO )
/** 数组是否为空*/
#define kArrayIsEmpty(array) (array == nil || [array isKindOfClass:[NSNull class]] || array.count == 0)
/** 字典是否为空*/
#define kDictIsEmpty(dic) (dic == nil || [dic isKindOfClass:[NSNull class]] || dic.allKeys == 0)
/** 是否是空对象*/
#define kObjectIsEmpty(_object) (_object == nil \
|| [_object isKindOfClass:[NSNull class]] \
|| ([_object respondsToSelector:@selector(length)] && [(NSData *)_object length] == 0) \
|| ([_object respondsToSelector:@selector(count)] && [(NSArray *)_object count] == 0))

/*********************************************沙盒路径:temp,Document,Cache*******************************/
#define kPathTemp NSTemporaryDirectory()
#define kPathDocument [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]
#define kPathCache [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject]

#endif /* Macro_h */
