//
//  NSUserDefaults+ZJUserDefaults.h
//  Myhealth
//
//  Created by zhoujian on 2018/11/14.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSUserDefaults (ZJUserDefaults)

- (void)zj_setObject:(id)object key:(NSString *)key;
- (id)zj_objcetForKey:(NSString *)key;
- (void)zj_removeObjectForKey:(NSString *)key;
- (void)zj_setBool:(BOOL)isBool key:(NSString *)key;
- (BOOL)zj_boolForKey:(NSString *)key;

@end

NS_ASSUME_NONNULL_END
