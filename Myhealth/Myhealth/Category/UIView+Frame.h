

#import <UIKit/UIKit.h>
/*
 
    写分类:避免跟其他开发者产生冲突,加前缀
 
 */
@interface UIView (Frame)

@property CGFloat zj_width;
@property CGFloat zj_height;
@property CGFloat zj_x;
@property CGFloat zj_y;
@property CGFloat zj_centerX;
@property CGFloat zj_centerY;

@end
