//
//  NSUserDefaults+ZJUserDefaults.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/14.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "NSUserDefaults+ZJUserDefaults.h"

@implementation NSUserDefaults (ZJUserDefaults)

- (void)zj_setObject:(id)object key:(NSString *)key {
    
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (id)zj_objcetForKey:(NSString *)key {
    id object = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    return object;
}

- (void)zj_removeObjectForKey:(NSString *)key {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
}

- (void)zj_setBool:(BOOL)isBool key:(NSString *)key {
    [[NSUserDefaults standardUserDefaults] setBool:isBool forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)zj_boolForKey:(NSString *)key {
    BOOL isBool =  [[NSUserDefaults standardUserDefaults] boolForKey:key];
    return isBool;
}

@end
