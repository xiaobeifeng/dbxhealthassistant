

#import "UIView+Frame.h"

@implementation UIView (Frame)

- (void)setZj_height:(CGFloat)zj_height
{
    CGRect rect = self.frame;
    rect.size.height = zj_height;
    self.frame = rect;
}

- (CGFloat)zj_height
{
    return self.frame.size.height;
}

- (CGFloat)zj_width
{
    return self.frame.size.width;
}
- (void)setZj_width:(CGFloat)zj_width
{
    CGRect rect = self.frame;
    rect.size.width = zj_width;
    self.frame = rect;
}

- (CGFloat)zj_x
{
    return self.frame.origin.x;
    
}

- (void)setZj_x:(CGFloat)zj_x
{
    CGRect rect = self.frame;
    rect.origin.x = zj_x;
    self.frame = rect;
}

- (void)setZj_y:(CGFloat)zj_y
{
    CGRect rect = self.frame;
    rect.origin.y = zj_y;
    self.frame = rect;
}

- (CGFloat)zj_y
{

    return self.frame.origin.y;
}

- (void)setZj_centerX:(CGFloat)zj_centerX
{
    CGPoint center = self.center;
    center.x = zj_centerX;
    self.center = center;
}

- (CGFloat)zj_centerX
{
    return self.center.x;
}

- (void)setZj_centerY:(CGFloat)zj_centerY
{
    CGPoint center = self.center;
    center.y = zj_centerY;
    self.center = center;
}

- (CGFloat)zj_centerY
{
    return self.center.y;
}


@end
