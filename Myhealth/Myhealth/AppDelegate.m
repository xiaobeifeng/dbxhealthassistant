//
//  AppDelegate.m
//  Myhealth
//
//  Created by zhoujian on 2018/11/13.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#import "AppDelegate.h"
#import "ZJLoginViewController.h"
#import "ZJDeviceManageViewController.h"
#import "ZJLinkyorntwoApi.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    YTKNetworkConfig *config = [YTKNetworkConfig sharedConfig];
    config.baseUrl = URL_BASE;
    config.cdnUrl = @"http://fen.bi";
    
    NSLog(@"%@", URL_BASE);
    NSLog(@"UD_KEY_DEVICE_NAME - %@", UD_KEY_DEVICE_NAME);
    NSLog(@"deviceName - %@", [[NSUserDefaults standardUserDefaults] zj_objcetForKey:UD_KEY_DEVICE_NAME]);
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] zj_objcetForKey:UD_KEY_TOKEN]; // 获取token
    
    if (kStringIsEmpty(token)) {
        ZJNavigationController *navC = [[ZJNavigationController alloc] initWithRootViewController:[ZJLoginViewController new]];
        self.window.rootViewController = navC;
    } else {
        
        ZJTabBarController *tabBarC = [[ZJTabBarController alloc] init];
        self.window.rootViewController = tabBarC;
        
        ZJLinkyorntwoApi *api = [[ZJLinkyorntwoApi alloc] init];
        [api startWithCompletionBlockWithSuccess:^(__kindof YTKBaseRequest * _Nonnull request) {
            
            if (api.code == 1043 || api.code == 1041) {
                ZJDeviceManageViewController *deviceMangeVC = [ZJDeviceManageViewController new];
                ZJNavigationController *navC = [[ZJNavigationController alloc] initWithRootViewController:deviceMangeVC];
                deviceMangeVC.isShowChangeUser = YES;
                self.window.rootViewController = navC;
            }
            
            // 用户未登录
            if (api.code == 2) {
                ZJNavigationController *navC = [[ZJNavigationController alloc] initWithRootViewController:[ZJLoginViewController new]];
                self.window.rootViewController = navC;
            }
            
        } failure:^(__kindof YTKBaseRequest * _Nonnull request) {
            
            // 接口请求失败
            ZJTabBarController *tabBarC = [[ZJTabBarController alloc] init];
            self.window.rootViewController = tabBarC;
        }];
    }
    
    [self.window makeKeyAndVisible];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
