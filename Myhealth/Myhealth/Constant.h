//
//  Constant.h
//  Myhealth
//
//  Created by zhoujian on 2018/11/14.
//  Copyright © 2018 zhoujian. All rights reserved.
//

#ifndef Constant_h
#define Constant_h


#define SERVER_ADDRESS @"http://192.168.10.80:8080"

//#define SERVER_ADDRESS @"http://zglohas.com"


#define URL_BASE @""SERVER_ADDRESS"/MQService/sm/mq/"            // 正常请求数据的接口
#define URL_LOGIN @""SERVER_ADDRESS"/MQService/sm/user/token"   // 登陆
#define URL_AVATAR(path)  [NSString stringWithFormat:@""SERVER_ADDRESS"/mp/uploadFiles/touxiang/%@", path] // 头像路径
#define URL_NEWS(path)  [NSString stringWithFormat:@""SERVER_ADDRESS"/mp/uploadFiles/news/%@", path] // 新闻图片路径
#define URL_BANNER(path)  [NSString stringWithFormat:@""SERVER_ADDRESS"/mqfiles/carousel//%@", path] // 首页banner图片路径

#define UD_KEY_TOKEN @"key_token"    // 本地存储token的key
#define UD_KEY_USERNAME @"key_userName"    // 本地存储userName的key
#define UD_KEY_PASSWORD @"key_password"    // 本地存储password的key
#define UD_KEY_ISON @"key_isOn"    // 本地存储记住密码isOn的key
#define UD_KEY_DEVICE_NAME [NSString stringWithFormat:@"%@-deviceName", [[NSUserDefaults standardUserDefaults] objectForKey:UD_KEY_USERNAME]]


#define COLOR_GALLERY [UIColor colorWithRed:0.94 green:0.93 blue:0.94 alpha:1.00]   // 背景色的灰
#define COLOR_BLACKGRAY [UIColor colorWithHexString:@"#434343"]                 // 字体颜色：黑灰
#define COLOR_PICTONBLUE [UIColor colorWithRed:0.28 green:0.65 blue:1.00 alpha:1.00]   // 背景色的灰

#define TRAY_AGAIN_LATER @"请稍后再试"                 // 请稍后再试
#endif /* Constant_h */
